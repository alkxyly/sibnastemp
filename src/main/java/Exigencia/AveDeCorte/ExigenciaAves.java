package Exigencia.AveDeCorte;

import java.io.Serializable;
import java.util.Arrays;

public class ExigenciaAves implements Serializable{

	private static final long serialVersionUID = 7174567983984169768L;
	
	private int[][] fasesPadrao = {{1,7},{8,21},{22,33},{34,42},{43,56}};
	private int[][] fasesDinamico;
	
	//Consumo de ra��o
	private Double[] consumoRacaoMachoMedio = {16.1, 18.7, 21.6, 24.7, 28.1, 31.8, 35.8, 39.1, 43.6, 48.4, 
			                                   53.4, 58.8, 64.4, 70.3, 76.4, 82.8, 89.3, 96.1, 103.0, 110.0, 
			                                   117.1, 120.3, 127.3, 134.2, 141.1, 147.9, 154.6, 161.1, 167.5, 
			                                   173.6, 179.5, 185.1, 190.4, 192.4, 197.0, 201.3, 205.3, 208.9, 
			                                   212.1, 215.1, 217.6, 219.9, 218.4, 220.0, 221.2, 222.2, 222.9,
			                                   223.4, 223.6, 223.6, 223.4, 222.9, 222.3, 221.6, 220.7, 219.8};
	
	private Double[] consumoRacaoFemeaMedio = {17.6, 20.2, 23.0, 26.2, 29.5, 33.1, 37.0, 40.1, 44.3, 48.8, 
			                                   53.5, 58.5, 63.6, 68.9, 74.4, 80.0, 85.8, 91.7, 97.8, 103.8, 
			                                   110.0, 112.4, 118.4, 124.3, 130.2, 136.0, 141.7, 147.3, 152.7, 
			                                   157.9, 163.0, 167.8, 172.5, 174.1, 178.2, 182.0, 185.6, 188.9, 
			                                   191.9, 194.6, 197.1, 199.3, 198.1, 199.8, 201.2, 202.3, 203.2, 
			                                   203.9, 204.3, 204.6, 204.6, 204.5, 204.2, 203.7, 203.1, 202.3 };
	
	private Double[] consumoRacaoMachoSuperior = {16.9, 19.6, 22.5, 25.7, 29.2, 33.0, 37.1, 40.1, 44.7, 49.6, 
			                                      54.7,	60.1, 65.9, 71.9, 78.1, 84.6, 91.2, 98.1, 105.1, 112.3, 
			                                      119.5, 122.9, 130.0, 137.0, 144.1, 151.0, 157.8,164.4, 170.9, 
			                                      177.1, 183.0, 188.7, 194.1, 196.1, 200.7, 205.0, 209.0, 212.6, 
			                                      215.8, 218.7, 221.2, 223.3, 221.8, 223.2, 224.4, 225.3, 225.9,
			                                      226.2, 226.3, 226.1, 225.7, 225.2, 224.4, 223.5, 222.5, 221.3};
	
	private Double[] consumoRacaoFemeaSuperior = {17.4, 19.9, 22.7, 25.7, 29.0, 32.5, 36.2, 38.9, 43.0, 47.4, 
			                                      51.9,	56.6, 61.6, 66.7, 72.1, 77.5, 83.1, 88.9, 94.7, 100.6, 
			                                      106.6, 109.1, 114.9, 120.7, 126.4, 132.1, 137.7, 143.1, 148.4,
                                                  153.6, 158.6, 163.3, 167.9, 169.6, 173.6, 177.4, 180.9, 184.1, 
                                                  187.1, 189.8, 192.3, 194.5, 193.4, 195.0, 196.4, 197.5, 198.4, 
                                                  199.1, 199.5, 199.8, 199.8, 199.7, 199.4, 198.9, 198.3, 182.7};
	
	private Double[] consumoRacaoMistoMedio = {16.85, 19.45, 22.3, 25.45, 28.8, 32.45, 36.4, 39.6, 43.95, 48.60, 
			                                  53.45, 58.65, 64.0, 69.6, 75.4, 81.4, 87.55, 93.9, 100.4, 106.9,
			                                  113.55, 116.35, 122.85, 129.25, 135.65, 141.95, 148.15, 154.2, 160.1, 
			                                  165.75, 171.25, 176.45, 181.45, 183.25, 187.6, 191.65, 195.45, 198.9,
			                                  202.00, 204.85, 207.35, 209.60, 208.25, 209.9, 211.2, 212.25, 213.05, 
			                                  213.65, 213.95, 214.1, 214.0, 213.7, 213.25, 212.65, 211.90, 211.05 };
	
	private Double[] consumoRacaoMistoSuperior = {17.15, 19.75, 22.6, 25.7, 29.1, 32.75, 36.65, 39.5, 43.85, 48.5, 
			                                      53.30, 58.35, 63.75, 69.30, 75.10, 81.05, 87.15, 93.5, 99.90, 106.45,
			                                     113.05, 116.0, 122.45, 128.85, 135.25, 141.55, 147.75, 153.75, 159.65, 
			                                     165.35, 170.8, 176.00, 181.00, 182.85,	187.15, 191.20, 194.95, 198.35, 
			                                     201.45, 204.25, 206.75, 208.9, 207.60, 209.1, 210.4, 211.4, 212.15, 
			                                     212.65, 212.90, 212.95, 212.75, 212.45, 211.90, 211.2, 210.40, 202.00};

	//Ganho de peso

	private Double[] ganhoPesoMachoMedio = {14.50, 16.80, 19.20, 21.80, 24.60, 27.60, 30.80, 34.00, 37.40, 
			                                40.90, 44.50, 48.20, 51.90, 55.60, 59.30, 62.90, 66.50, 70.00,
			                                73.40, 76.70, 79.80, 82.80, 85.60, 88.20, 90.60, 92.80, 94.80,
			                                96.60, 98.20, 99.50, 100.60, 101.60, 102.30, 102.70, 103.00, 103.10, 
			                                103.00, 102.80, 102.30,	101.70, 101.00, 100.10, 99.00, 97.90, 96.60, 
			                                95.20, 93.80, 92.20, 90.60, 88.90, 87.20, 85.40, 83.60, 81.70, 79.90, 78.10};
	
	private Double[] ganhoPesoFemeaMedio = { 13.48, 15.47, 17.61, 19.90, 22.34, 24.90, 27.57, 30.34, 33.19, 
			                                 36.10,	39.05, 42.02, 44.99, 47.94, 50.85, 53.70, 56.47, 59.15, 
			                                 61.71, 64.15, 66.45, 68.60, 70.59, 72.41, 74.06, 75.54, 76.83, 
			                                 77.94, 78.88, 79.63, 80.21, 80.61, 80.85, 80.93, 80.85, 80.62, 
			                                 80.26, 79.76, 79.14, 78.40, 77.55, 76.61, 75.57, 74.46, 73.26, 
			                                 72.01, 70.69, 69.32, 67.91, 66.46, 64.98, 63.48, 61.96, 60.42, 
			                                 58.87,	57.32 };
	
	private Double[] ganhoPesoMachoSuperior = {15.00, 17.33, 19.87, 22.61, 25.55, 28.67, 31.96, 35.41, 39.00, 
			                                   42.71, 46.51, 50.38, 54.30, 58.24, 62.18, 66.08, 69.94, 73.71, 
			                                   77.38, 80.93, 84.34, 87.58, 90.66, 93.54, 96.21,	98.68, 100.92, 
			                                   102.94, 104.73, 106.29, 107.62, 108.72, 109.59, 110.23, 110.66, 
			                                   110.88, 110.89, 110.71, 110.35, 109.81, 109.10, 108.23, 107.22, 
			                                   106.07, 104.80, 103.42, 101.93, 100.34, 98.67, 96.93, 95.12, 93.25,
			                                   91.34, 89.38, 87.39, 85.38};
	
	private Double[] ganhoPesoFemeaSuperior = {14.96, 17.12, 19.44, 21.92, 24.55, 27.30, 30.18, 33.15, 36.20, 
			                                   39.32, 42.47, 45.64, 48.81, 51.96, 55.05, 58.09, 61.03, 63.88, 
			                                   66.60, 69.19, 71.63, 73.91, 76.02, 77.95, 79.70,	81.26, 82.63, 
			                                   83.81, 84.80, 85.60, 86.21, 86.64, 86.89, 86.97, 86.89, 86.65, 
			                                   86.27, 85.74, 85.08, 84.30, 83.40, 82.40, 81.30, 80.11, 78.85, 
			                                   77.51, 76.11, 74.66, 73.16, 71.62, 70.04, 68.44, 66.82, 65.18, 
			                                   63.53, 61.87};
	
	private Double[] ganhoPesoMistoMedio = {13.99, 16.13, 18.41, 20.85, 23.47, 26.25, 29.18, 32.17, 35.30, 38.50, 
			                                41.77, 45.11, 48.45, 51.77, 55.08, 58.30, 61.49, 64.57, 67.56, 70.43, 
			                                73.13, 75.70, 78.10, 80.30, 82.33, 84.17, 85.82, 87.27,	88.52, 89.56, 
			                                90.40, 91.11, 91.57, 91.81, 91.92, 91.86, 91.63, 91.28, 90.72, 90.05, 
			                                89.28, 88.35, 87.29, 86.18, 84.93, 83.60, 82.25, 80.76, 79.26, 77.68, 
			                                76.09, 74.44, 72.78, 71.06, 69.39, 67.71 };
	
	private Double[] ganhoPesoMistoSuperior = {14.98, 17.22, 19.66, 22.27, 25.05, 27.99, 31.07, 34.28, 37.6, 41.01, 
			                                   44.49, 48.01, 51.56, 55.10, 58.62, 62.08, 65.48, 68.79, 71.99, 75.06, 
			                                   77.98, 80.74, 83.34, 85.74, 87.96, 89.97, 91.78, 93.38, 94.77, 95.94,
			                                   96.91, 97.68, 98.24, 98.60, 98.78, 98.77, 98.58, 98.23, 97.71, 97.05, 
			                                   96.25, 95.32, 94.26, 93.09, 91.83, 90.46, 89.02, 87.50, 85.92, 84.27, 
			                                   82.58, 80.85, 79.08, 77.28, 75.46, 73.63 };
	
	
	//Dados fornecidos pelo usuario
	private Double preInicial;
	private Double inicial;
	private Double crescimento;
	private Double engorda;
	private Double abate;
	
	//Dados para simula��o de exigencia
	private Double pesoDia1;
	private int idadeAbate; //Em dias
	private Double ganhoPesoDiario; //Em Quilograma
	private Double consumoAcumulado; //O grama
	private Double pesoAbate; //Em quilograma
	private Double consumoRacao; //Em quilograma
	private Double ganhoAbate; //Em quilograma
		
	//Exig�ncia - g/dia
	private Double fosforoDisponivel;
	private Double fosforoDigestivel;
	private Double lisinaDigestivel;
	
	//Exig�ncia - %
	private Double proteina;
	private Double calcio;
	private Double fosforoDisponivelEP;
	private Double fosforoDigestivelEP;
	private Double potassio;
	private Double sodio;
	private Double cloro;
	private Double lisinaDigestivelEP;
	private Double acidoLinoleico;
	
	//AA Digest�vel%
	private Double lisinaAAD;
	private Double metioninaAAD;
	private Double metioninaCistinaAAD;
	private Double treoninaAAD;
	private Double triptofanoAAD;
	private Double argininaAAD;
	private Double glicinaSerinaAAD;
	private Double valinaAAD;
	private Double isoleucinaAAD;
	private Double leucinaAAD;
	private Double histidinaAAD;
	private Double fenilalaninaAAD;
	private Double fenil_TirAAD;
	
	//AA Total%
	private Double lisinaAAT;
	private Double metioninaAAT;
	private Double metioninaCistinaAAT;
	private Double treoninaAAT;
	private Double triptofanoAAT;
	private Double argininaAAT;
	private Double glicinaSerinaAAT;
	private Double valinaAAT;
	private Double isoleucinaAAT;
	private Double leucinaAAT;
	private Double histidinaAAT;
	private Double fenilalaninaAAT;
	private Double fenil_TirAAT;
	public int[][] getFasesPadrao() {
		return fasesPadrao;
	}
	public void setFasesPadrao(int[][] fasesPadrao) {
		this.fasesPadrao = fasesPadrao;
	}
	public int[][] getFasesDinamico() {
		return fasesDinamico;
	}
	public void setFasesDinamico(int[][] fasesDinamico) {
		this.fasesDinamico = fasesDinamico;
	}
	public Double[] getConsumoRacaoMachoMedio() {
		return consumoRacaoMachoMedio;
	}
	public void setConsumoRacaoMachoMedio(Double[] consumoRacaoMachoMedio) {
		this.consumoRacaoMachoMedio = consumoRacaoMachoMedio;
	}
	public Double[] getConsumoRacaoFemeaMedio() {
		return consumoRacaoFemeaMedio;
	}
	public void setConsumoRacaoFemeaMedio(Double[] consumoRacaoFemeaMedio) {
		this.consumoRacaoFemeaMedio = consumoRacaoFemeaMedio;
	}
	public Double[] getConsumoRacaoMachoSuperior() {
		return consumoRacaoMachoSuperior;
	}
	public void setConsumoRacaoMachoSuperior(Double[] consumoRacaoMachoSuperior) {
		this.consumoRacaoMachoSuperior = consumoRacaoMachoSuperior;
	}
	public Double[] getConsumoRacaoFemeaSuperior() {
		return consumoRacaoFemeaSuperior;
	}
	public void setConsumoRacaoFemeaSuperior(Double[] consumoRacaoFemeaSuperior) {
		this.consumoRacaoFemeaSuperior = consumoRacaoFemeaSuperior;
	}
	public Double[] getConsumoRacaoMistoMedio() {
		return consumoRacaoMistoMedio;
	}
	public void setConsumoRacaoMistoMedio(Double[] consumoRacaoMistoMedio) {
		this.consumoRacaoMistoMedio = consumoRacaoMistoMedio;
	}
	public Double[] getConsumoRacaoMistoSuperior() {
		return consumoRacaoMistoSuperior;
	}
	public void setConsumoRacaoMistoSuperior(Double[] consumoRacaoMistoSuperior) {
		this.consumoRacaoMistoSuperior = consumoRacaoMistoSuperior;
	}
	public Double[] getGanhoPesoMachoMedio() {
		return ganhoPesoMachoMedio;
	}
	public void setGanhoPesoMachoMedio(Double[] ganhoPesoMachoMedio) {
		this.ganhoPesoMachoMedio = ganhoPesoMachoMedio;
	}
	public Double[] getGanhoPesoFemeaMedio() {
		return ganhoPesoFemeaMedio;
	}
	public void setGanhoPesoFemeaMedio(Double[] ganhoPesoFemeaMedio) {
		this.ganhoPesoFemeaMedio = ganhoPesoFemeaMedio;
	}
	public Double[] getGanhoPesoMachoSuperior() {
		return ganhoPesoMachoSuperior;
	}
	public void setGanhoPesoMachoSuperior(Double[] ganhoPesoMachoSuperior) {
		this.ganhoPesoMachoSuperior = ganhoPesoMachoSuperior;
	}
	public Double[] getGanhoPesoFemeaSuperior() {
		return ganhoPesoFemeaSuperior;
	}
	public void setGanhoPesoFemeaSuperior(Double[] ganhoPesoFemeaSuperior) {
		this.ganhoPesoFemeaSuperior = ganhoPesoFemeaSuperior;
	}
	public Double[] getGanhoPesoMistoMedio() {
		return ganhoPesoMistoMedio;
	}
	public void setGanhoPesoMistoMedio(Double[] ganhoPesoMistoMedio) {
		this.ganhoPesoMistoMedio = ganhoPesoMistoMedio;
	}
	public Double[] getGanhoPesoMistoSuperior() {
		return ganhoPesoMistoSuperior;
	}
	public void setGanhoPesoMistoSuperior(Double[] ganhoPesoMistoSuperior) {
		this.ganhoPesoMistoSuperior = ganhoPesoMistoSuperior;
	}
	public Double getPreInicial() {
		return preInicial;
	}
	public void setPreInicial(Double preInicial) {
		this.preInicial = preInicial;
	}
	public Double getInicial() {
		return inicial;
	}
	public void setInicial(Double inicial) {
		this.inicial = inicial;
	}
	public Double getCrescimento() {
		return crescimento;
	}
	public void setCrescimento(Double crescimento) {
		this.crescimento = crescimento;
	}
	public Double getEngorda() {
		return engorda;
	}
	public void setEngorda(Double engorda) {
		this.engorda = engorda;
	}
	public Double getAbate() {
		return abate;
	}
	public void setAbate(Double abate) {
		this.abate = abate;
	}
	public Double getPesoDia1() {
		return pesoDia1;
	}
	public void setPesoDia1(Double pesoDia1) {
		this.pesoDia1 = pesoDia1;
	}
	public int getIdadeAbate() {
		return idadeAbate;
	}
	public void setIdadeAbate(int idadeAbate) {
		this.idadeAbate = idadeAbate;
	}
	public Double getGanhoPesoDiario() {
		return ganhoPesoDiario;
	}
	public void setGanhoPesoDiario(Double ganhoPesoDiario) {
		this.ganhoPesoDiario = ganhoPesoDiario;
	}
	public Double getConsumoAcumulado() {
		return consumoAcumulado;
	}
	public void setConsumoAcumulado(Double consumoAcumulado) {
		this.consumoAcumulado = consumoAcumulado;
	}
	public Double getPesoAbate() {
		return pesoAbate;
	}
	public void setPesoAbate(Double pesoAbate) {
		this.pesoAbate = pesoAbate;
	}
	public Double getConsumoRacao() {
		return consumoRacao;
	}
	public void setConsumoRacao(Double consumoRacao) {
		this.consumoRacao = consumoRacao;
	}
	public Double getGanhoAbate() {
		return ganhoAbate;
	}
	public void setGanhoAbate(Double ganhoAbate) {
		this.ganhoAbate = ganhoAbate;
	}
	public Double getFosforoDisponivel() {
		return fosforoDisponivel;
	}
	public void setFosforoDisponivel(Double fosforoDisponivel) {
		this.fosforoDisponivel = fosforoDisponivel;
	}
	public Double getFosforoDigestivel() {
		return fosforoDigestivel;
	}
	public void setFosforoDigestivel(Double fosforoDigestivel) {
		this.fosforoDigestivel = fosforoDigestivel;
	}
	public Double getLisinaDigestivel() {
		return lisinaDigestivel;
	}
	public void setLisinaDigestivel(Double lisinaDigestivel) {
		this.lisinaDigestivel = lisinaDigestivel;
	}
	public Double getProteina() {
		return proteina;
	}
	public void setProteina(Double proteina) {
		this.proteina = proteina;
	}
	public Double getCalcio() {
		return calcio;
	}
	public void setCalcio(Double calcio) {
		this.calcio = calcio;
	}
	public Double getFosforoDisponivelEP() {
		return fosforoDisponivelEP;
	}
	public void setFosforoDisponivelEP(Double fosforoDisponivelEP) {
		this.fosforoDisponivelEP = fosforoDisponivelEP;
	}
	public Double getFosforoDigestivelEP() {
		return fosforoDigestivelEP;
	}
	public void setFosforoDigestivelEP(Double fosforoDigestivelEP) {
		this.fosforoDigestivelEP = fosforoDigestivelEP;
	}
	public Double getPotassio() {
		return potassio;
	}
	public void setPotassio(Double potassio) {
		this.potassio = potassio;
	}
	public Double getSodio() {
		return sodio;
	}
	public void setSodio(Double sodio) {
		this.sodio = sodio;
	}
	public Double getCloro() {
		return cloro;
	}
	public void setCloro(Double cloro) {
		this.cloro = cloro;
	}
	public Double getLisinaDigestivelEP() {
		return lisinaDigestivelEP;
	}
	public void setLisinaDigestivelEP(Double lisinaDigestivelEP) {
		this.lisinaDigestivelEP = lisinaDigestivelEP;
	}
	public Double getAcidoLinoleico() {
		return acidoLinoleico;
	}
	public void setAcidoLinoleico(Double acidoLinoleico) {
		this.acidoLinoleico = acidoLinoleico;
	}
	public Double getLisinaAAD() {
		return lisinaAAD;
	}
	public void setLisinaAAD(Double lisinaAAD) {
		this.lisinaAAD = lisinaAAD;
	}
	public Double getMetioninaAAD() {
		return metioninaAAD;
	}
	public void setMetioninaAAD(Double metioninaAAD) {
		this.metioninaAAD = metioninaAAD;
	}
	public Double getMetioninaCistinaAAD() {
		return metioninaCistinaAAD;
	}
	public void setMetioninaCistinaAAD(Double metioninaCistinaAAD) {
		this.metioninaCistinaAAD = metioninaCistinaAAD;
	}
	public Double getTreoninaAAD() {
		return treoninaAAD;
	}
	public void setTreoninaAAD(Double treoninaAAD) {
		this.treoninaAAD = treoninaAAD;
	}
	public Double getTriptofanoAAD() {
		return triptofanoAAD;
	}
	public void setTriptofanoAAD(Double triptofanoAAD) {
		this.triptofanoAAD = triptofanoAAD;
	}
	public Double getArgininaAAD() {
		return argininaAAD;
	}
	public void setArgininaAAD(Double argininaAAD) {
		this.argininaAAD = argininaAAD;
	}
	public Double getGlicinaSerinaAAD() {
		return glicinaSerinaAAD;
	}
	public void setGlicinaSerinaAAD(Double glicinaSerinaAAD) {
		this.glicinaSerinaAAD = glicinaSerinaAAD;
	}
	public Double getValinaAAD() {
		return valinaAAD;
	}
	public void setValinaAAD(Double valinaAAD) {
		this.valinaAAD = valinaAAD;
	}
	public Double getIsoleucinaAAD() {
		return isoleucinaAAD;
	}
	public void setIsoleucinaAAD(Double isoleucinaAAD) {
		this.isoleucinaAAD = isoleucinaAAD;
	}
	public Double getLeucinaAAD() {
		return leucinaAAD;
	}
	public void setLeucinaAAD(Double leucinaAAD) {
		this.leucinaAAD = leucinaAAD;
	}
	public Double getHistidinaAAD() {
		return histidinaAAD;
	}
	public void setHistidinaAAD(Double histidinaAAD) {
		this.histidinaAAD = histidinaAAD;
	}
	public Double getFenilalaninaAAD() {
		return fenilalaninaAAD;
	}
	public void setFenilalaninaAAD(Double fenilalaninaAAD) {
		this.fenilalaninaAAD = fenilalaninaAAD;
	}
	public Double getFenil_TirAAD() {
		return fenil_TirAAD;
	}
	public void setFenil_TirAAD(Double fenil_TirAAD) {
		this.fenil_TirAAD = fenil_TirAAD;
	}
	public Double getLisinaAAT() {
		return lisinaAAT;
	}
	public void setLisinaAAT(Double lisinaAAT) {
		this.lisinaAAT = lisinaAAT;
	}
	public Double getMetioninaAAT() {
		return metioninaAAT;
	}
	public void setMetioninaAAT(Double metioninaAAT) {
		this.metioninaAAT = metioninaAAT;
	}
	public Double getMetioninaCistinaAAT() {
		return metioninaCistinaAAT;
	}
	public void setMetioninaCistinaAAT(Double metioninaCistinaAAT) {
		this.metioninaCistinaAAT = metioninaCistinaAAT;
	}
	public Double getTreoninaAAT() {
		return treoninaAAT;
	}
	public void setTreoninaAAT(Double treoninaAAT) {
		this.treoninaAAT = treoninaAAT;
	}
	public Double getTriptofanoAAT() {
		return triptofanoAAT;
	}
	public void setTriptofanoAAT(Double triptofanoAAT) {
		this.triptofanoAAT = triptofanoAAT;
	}
	public Double getArgininaAAT() {
		return argininaAAT;
	}
	public void setArgininaAAT(Double argininaAAT) {
		this.argininaAAT = argininaAAT;
	}
	public Double getGlicinaSerinaAAT() {
		return glicinaSerinaAAT;
	}
	public void setGlicinaSerinaAAT(Double glicinaSerinaAAT) {
		this.glicinaSerinaAAT = glicinaSerinaAAT;
	}
	public Double getValinaAAT() {
		return valinaAAT;
	}
	public void setValinaAAT(Double valinaAAT) {
		this.valinaAAT = valinaAAT;
	}
	public Double getIsoleucinaAAT() {
		return isoleucinaAAT;
	}
	public void setIsoleucinaAAT(Double isoleucinaAAT) {
		this.isoleucinaAAT = isoleucinaAAT;
	}
	public Double getLeucinaAAT() {
		return leucinaAAT;
	}
	public void setLeucinaAAT(Double leucinaAAT) {
		this.leucinaAAT = leucinaAAT;
	}
	public Double getHistidinaAAT() {
		return histidinaAAT;
	}
	public void setHistidinaAAT(Double histidinaAAT) {
		this.histidinaAAT = histidinaAAT;
	}
	public Double getFenilalaninaAAT() {
		return fenilalaninaAAT;
	}
	public void setFenilalaninaAAT(Double fenilalaninaAAT) {
		this.fenilalaninaAAT = fenilalaninaAAT;
	}
	public Double getFenil_TirAAT() {
		return fenil_TirAAT;
	}
	public void setFenil_TirAAT(Double fenil_TirAAT) {
		this.fenil_TirAAT = fenil_TirAAT;
	}
	
												
}
  