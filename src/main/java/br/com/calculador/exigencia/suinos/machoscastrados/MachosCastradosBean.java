package br.com.calculador.exigencia.suinos.machoscastrados;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;

import Util.Mensagens;
import br.com.calulador.exigencia.Exigencia;

@ManagedBean(name = "machosCastradosBean")
@ViewScoped
public class MachosCastradosBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public double numero;
	private Suino suinoInicial1 =  new Suino();
	private Exigencia exigencia = new Exigencia();
	private Exigencia exigencia2 = new Exigencia();
	private Suino suinoInicial2 = new Suino();

	private Suino suinoCrescimento1 =  new Suino();
	private Suino suinoCrescimento2 = new Suino();
	private Exigencia exigenciaCrescimento = new Exigencia();
	private Exigencia exigenciaCrescimento2 = new Exigencia();
	
	
	private int sexo;
	private int idadeInicial;
	private int idadeAbate;
	private double pesoAlojamento;
	private double ganhoPeso;
	private double conversaoAlimentar;
	
	private double pesoAbate = 0.0;
	private double consumoRacao = 0.0;
	private double ganhoAbate = 0.0;

	
	private int fase1 = 42;
	private int fase2a = 43;
	private int fase2 = 70;
	private int fase3a = 71;
	private int fase3 = 91;
	private int fase4a = 92;
	private int fase4 = 112;
	private int fase5a = 113;
	private int fase5 = 140;
	private int fase6a = 141;
	
	private double pesoMedio1 = 0.0;
	private double pesoMedio2 = 0.0;
	private double pesoMedio3 = 0.0;
	private double pesoMedio4 = 0.0;
	private double pesoMedio5 = 0.0;
	private double pesoMedio6 = 0.0;
	
	private double pesoMedio1Simulado = 0.0;
	private double pesoMedio2Simulado = 0.0;
	private double pesoMedio3Simulado = 0.0;
	private double pesoMedio4Simulado = 0.0;
	private double pesoMedio5Simulado = 0.0;
	private double pesoMedio6Simulado = 0.0;
	
	private double ganhoPeso1 = 0.0;
	private double ganhoPeso2 = 0.0;
	private double ganhoPeso3 = 0.0;
	private double ganhoPeso4 = 0.0;
	private double ganhoPeso5 = 0.0;
	private double ganhoPeso6 = 0.0;
	
	private double ganhoPeso1Simulado = 0.0;
	private double ganhoPeso2Simulado = 0.0;
	private double ganhoPeso3Simulado = 0.0;
	private double ganhoPeso4Simulado = 0.0;
	private double ganhoPeso5Simulado = 0.0;
	private double ganhoPeso6Simulado = 0.0;
	
	
	private double consumo1 = 0.0;
	private double consumo2 = 0.0;
	private double consumo3 = 0.0;
	private double consumo4 = 0.0;
	private double consumo5 = 0.0;
	private double consumo6 = 0.0;
	
	private double consumo1Simulado = 0.0;
	private double consumo2Simulado = 0.0;
	private double consumo3Simulado = 0.0;
	private double consumo4Simulado = 0.0;
	private double consumo5Simulado = 0.0;
	private double consumo6Simulado = 0.0;
	
	private double conversaoAlimentar1 = 0.0;
	private double conversaoAlimentar2 = 0.0;
	private double conversaoAlimentar3 = 0.0;
	private double conversaoAlimentar4 = 0.0;
	private double conversaoAlimentar5 = 0.0;
	private double conversaoAlimentar6 = 0.0;
	
	private double conversaoAlimentar1Simulado = 0.0;
	private double conversaoAlimentar2Simulado = 0.0;
	private double conversaoAlimentar3Simulado = 0.0;
	private double conversaoAlimentar4Simulado = 0.0;
	private double conversaoAlimentar5Simulado = 0.0;
	private double conversaoAlimentar6Simulado = 0.0;
	
	private double cr1 = 0.0;
	private double cr2 = 0.0;
	private double cr3 = 0.0;
	private double cr4 = 0.0;
	private double cr5 = 0.0;
	private double cr6 = 0.0;
	
	private double cr1Simulado = 0.0;
	private double cr2Simulado = 0.0;
	private double cr3Simulado = 0.0;
	private double cr4Simulado = 0.0;
	private double cr5Simulado = 0.0;
	private double cr6Simulado = 0.0;
	
	private double gp1 = 0.0;
	private double gp2 = 0.0;
	private double gp3 = 0.0;
	private double gp4 = 0.0;
	private double gp5 = 0.0;
	private double gp6 = 0.0;
	
	private double gp1Simulado = 0.0;
	private double gp2Simulado = 0.0;
	private double gp3Simulado = 0.0;
	private double gp4Simulado = 0.0;
	private double gp5Simulado = 0.0;
	private double gp6Simulado = 0.0;
	
	private Exigencia exigenciaFase1 = new Exigencia();
	private Exigencia exigenciaFase2 = new Exigencia();
	private Exigencia exigenciaFase3 = new Exigencia();
	private Exigencia exigenciaFase4 = new Exigencia();
	private Exigencia exigenciaFase5 = new Exigencia();
	private Exigencia exigenciaFase6 = new Exigencia();
	
	private Exigencia exigenciaFase1Simulada = new Exigencia();
	private Exigencia exigenciaFase2Simulada = new Exigencia();
	private Exigencia exigenciaFase3Simulada = new Exigencia();
	private Exigencia exigenciaFase4Simulada = new Exigencia();
	private Exigencia exigenciaFase5Simulada = new Exigencia();
	private Exigencia exigenciaFase6Simulada = new Exigencia();
	
	//Amostral
	private double pesoMedioAmostral = 0.0;
	private double ganhoPesoAmostral = 0.0;
	private double conversaoAlimentarAmostral = 0.0;
	private double consumoAmostral;
	private double pesoMedioAmostralSimulado = 0.0;
	private double ganhoPesoAmostralSimulado = 0.0;
	private double conversaoAlimentarAmostralSimulado = 0.0;
	private double consumoAmostralSimulado;
	
	private double temperatura;
	private boolean ativaTemperatura;
	
	private String dias = "0";
	private String ppm = "0";
	
	private LineChartModel lineModel1 = new LineChartModel();
    private LineChartModel lineModel2  =  new LineChartModel();
    private String[] variantes;     
    
    private double[][] matriz;
    @PostConstruct
    public void init() {
       
    }
	public void simular(){
//		 exigenciaFase1 = new Exigencia();
//		 exigenciaFase2 = new Exigencia();
//		 exigenciaFase3 = new Exigencia();
//		 exigenciaFase4 = new Exigencia();
//		 exigenciaFase5 = new Exigencia();
//		 exigenciaFase6 = new Exigencia();
//		 exigenciaFase1Simulada = new Exigencia();
//		 exigenciaFase2Simulada = new Exigencia();
//		 exigenciaFase3Simulada = new Exigencia();
//		 exigenciaFase4Simulada = new Exigencia();
//		 exigenciaFase5Simulada = new Exigencia();
//		 exigenciaFase6Simulada = new Exigencia();
		 
		SuinosRN suinosRN = new SuinosRN();
		
		 fase1 = this.idadeInicial + 21;
		 fase2a = this.fase1 + 1;
//		 fase2 = this.fase2a + 21;
//		 fase3a = this.fase2+ 1;
//		 fase3 = this.fase3a + 21;
//		  fase4a = fase3 + 1;
//		  fase4 = this.fase4a + 21;
//		  fase5a = this.fase4 + 1;
//		  fase5 = this.fase5a + 21;
//		  fase6a = this.fase5 + 1;
		
		
		this.calcularFases();
		this.calcularExigencia();
		this.calcularExigenciaSimulada();
		this.calcularAmostral();
		this.calcularAmostralSimulado();
		this.calcularRactomapina();
		 createLineModels();
		
	}
	/**
	 * Realiza os calculos que serão para o usuário validar sua simulação
	 */
	public void calcularAmostral(){
		this.ganhoPesoAmostral = ganhoPeso1 + ganhoPeso2 + ganhoPeso3 + ganhoPeso4 + ganhoPeso5 + ganhoPeso6 ;
		this.pesoMedioAmostral = ganhoPesoAmostral+ Tabelas.peso_medio[this.idadeInicial - 21][this.sexo];
		this.consumoAmostral = consumo1 + consumo2 +consumo3 +  consumo4 +  consumo5 +  consumo6;
		this.conversaoAlimentarAmostral = consumoAmostral/ganhoPesoAmostral;
		this.testar();
	}
	public void calcularAmostralSimulado(){	
		
		this.ganhoPesoAmostralSimulado = (ganhoPeso1Simulado + ganhoPeso2Simulado + ganhoPeso3Simulado + ganhoPeso4Simulado + ganhoPeso5Simulado + ganhoPeso6Simulado) - (this.pesoAlojamento - matriz[idadeInicial - 21][0]) ;
		this.pesoMedioAmostralSimulado = ganhoPesoAmostralSimulado+ this.pesoAlojamento;
		this.consumoAmostralSimulado = consumo1Simulado + consumo2Simulado +consumo3Simulado +  consumo4Simulado +  consumo5Simulado +  consumo6Simulado;
		this.conversaoAlimentarAmostralSimulado = consumoAmostralSimulado/ganhoPesoAmostralSimulado;
	}

	public void alterarfase1(){
		this.fase2a = this.fase1 + 1;
		this.calcularFases();
		this.calcularExigencia();
		this.calcularAmostral();

	}
	public void alterarfase2(){
		this.fase3a = this.fase2 + 1;
		this.calcularFases();
		this.calcularExigencia();
		this.calcularAmostral();
	}
	public void alterarfase3(){
		this.fase4a = this.fase3 + 1;
		this.calcularFases();
		this.calcularExigencia();
		this.calcularAmostral();
	}
	public void alterarfase4(){
		this.fase5a = this.fase4 + 1;
		this.calcularFases();
		this.calcularExigencia();
		this.calcularAmostral();
	}
	public void alterarfase5(){
		this.fase6a = this.fase5 + 1;
		this.calcularFases();
		this.calcularExigencia();
		this.calcularAmostral();
	}
	/**
	 * Metodo chamado para calcular a ractopamina
	 */
	public void calcularRactomapina(){
		if(dias != "0" && ppm != "0"){
			int dia = Integer.valueOf(dias);
			int ppm = Integer.valueOf(this.ppm);
			SuinosRN suinosRN = new  SuinosRN();
			
			double ganho = suinosRN.ractopaminaGanho(dia, ppm)/1000;
			System.out.println("Ganho "+ganho);
			double consumo = suinosRN.ractopaminaConsumo(dia, ppm)/1000;
			double lisinaDia = suinosRN.ractopaminaLisinaDigDia(dia, ppm);
			double lisinaKg = suinosRN.ractopaminaLisinaDigKg(dia, ppm);
			this.gp6 += ganho;
			this.cr6 += consumo;
			this.exigenciaFase6.setLisinaDigestivelExigencia(	this.exigenciaFase6.getLisinaDigestivelExigencia() + lisinaDia);
			
			this.exigenciaFase6.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase6.getLisinaDigestivelExigencia(), cr6));
			this.dependeLisina();
			
		}
	}
	public void dependeLisina(){
		SuinosRN suinosRN= new SuinosRN();
		this.exigenciaFase6.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase6.getLisinaDigestivelExigencia(), cr6));
		this.exigenciaFase6.setMetioninaDigestivel(suinosRN.metioninaDigestivelNovo(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setMetioninaCistina(suinosRN.metioninaCistinaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setTreoninaDigestivel(suinosRN.treoninaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setTriptofano(suinosRN.triptofanoDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setArginina(suinosRN.argininaDigestiva(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setValina(suinosRN.valinaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setIsoleucina(suinosRN.isoleucinaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setLeucina(suinosRN.leucinaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setHistidina(suinosRN.histidinaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setFenilalanina(suinosRN.fenilalaninaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setFenilalaninaTirosina(suinosRN.fenilalaninaTirosinaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setNitrogenioEssencial(suinosRN.nitrogenioEssencial(exigenciaFase6,this.pesoMedio6));
		this.exigenciaFase6.setProteinaBrutaDisgestivel(suinosRN.proteinaBrutaDigestivel(exigenciaFase6.getNitrogenioEssencial()));
		this.exigenciaFase6.setLisinaTotal(suinosRN.lisinaProteinaIdealTotal(exigenciaFase6.getLisinaProteinaIdeal()));
		this.exigenciaFase6.setMetioninaTotal(suinosRN.metioninaTotalNovo(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setMetioninaCistinaTotal(suinosRN.metioninaCistinaTotal(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setTreoninaTotal(suinosRN.treoninaTotal(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setTriptofanoTotal(suinosRN.triptofanoDigestivel(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setArgininaTotal(suinosRN.argininaTotal(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setValinaTotal(suinosRN.valinaTotal(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setIsoleucinaTotal(suinosRN.isoleucinaDigestivel(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setLeucinaTotal(suinosRN.leucinaTotal(this.exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setHistidinaTotal(suinosRN.histidinaTotal(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setFenilalaninaTotal(suinosRN.fenilalaninaTotal(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setFenilalaninaTirosinaTotal(suinosRN.fenilalaninaTirosinaTotal(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setNitrogenioEssencialTotal(suinosRN.nitrogenioEssencialTotal(this.exigenciaFase6,pesoMedio6));
		this.exigenciaFase6.setProteinaBrutaTotal(suinosRN.proteinaBrutaTotal(exigenciaFase6.getNitrogenioEssencialTotal()));
		
	}
	
	public void calcularExigenciaSimulada(){
		SuinosRN suinosRN =  new SuinosRN();
//		double matriz[][] = suinosRN.curvaSimulada(idadeInicial - 21,idadeAbate-21,this.sexo,pesoAbate,pesoAlojamento,consumoRacao);
		Suino s = new Suino();
		s.setPesoMedio(this.pesoMedio1Simulado);
		s.setGanhoPeso(this.gp1Simulado);
		double lisinaDigestivelExigencia = suinosRN.lisinaExigenciaCategoria(this.sexo, s);
		double fosforoDisponivelExigencia = suinosRN.fosforoDisponivelCategoria(this.sexo, s);
		double fosforoDigestExigencia = suinosRN.fosforoDigestivelCategoria(this.sexo, s);
		
		this.exigenciaFase1Simulada.setLisinaDigestivelExigencia(lisinaDigestivelExigencia);
		this.exigenciaFase1Simulada.setFosforoDisponivelExigencia(fosforoDisponivelExigencia);
		this.exigenciaFase1Simulada.setFosforoDigestivelExigencia(fosforoDigestExigencia);
		s.setConsumoRacao(cr1Simulado);
		this.exigenciaFase1Simulada.setFosforoDisponivel(suinosRN.fosforoDisponivelExigencia(exigenciaFase1Simulada, s));
		this.exigenciaFase1Simulada.setFosforoDigestivel(suinosRN.fosforoDigestivelExigencia(exigenciaFase1Simulada, s));
		double calcio = suinosRN.calcio(exigenciaFase1Simulada, s);
		this.exigenciaFase1Simulada.setCalcio(calcio);
		double em = suinosRN.energiaMetabolizavelMediaSimulada(matriz,this.idadeInicial - 21,this.fase1 - 21, this.sexo);
		double consumomedio = suinosRN.consumoMedio(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
		double emDia = suinosRN.energiaMetabolizavelDiaMediaSimulada(this.matriz,this.idadeInicial - 21,this.fase1 - 21, this.sexo);
		this.exigenciaFase1Simulada.setEnergiaMetabolizavelKg((int)(this.exigenciaFase1.getEnergiaMetabolizavelKg()));
		this.exigenciaFase1Simulada.setEnergiaMetabolizavelDia(exigenciaFase1Simulada.getEnergiaMetabolizavelKg() * cr1Simulado);
		double pot = suinosRN.postassioNovo(pesoMedio1Simulado,exigenciaFase1Simulada.getEnergiaMetabolizavelKg());
		this.exigenciaFase1Simulada.setPotassio(pot);
		this.exigenciaFase1Simulada.setSodio(suinosRN.sodioNovo(pesoMedio1Simulado, exigenciaFase1Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase1Simulada.setCloro(suinosRN.cloroNovo(pesoMedio1Simulado, exigenciaFase1Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase1Simulada.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase1Simulada.getLisinaDigestivelExigencia(), cr1Simulado));
		this.exigenciaFase1Simulada.setMetioninaDigestivel(suinosRN.metioninaDigestivelNovo(exigenciaFase1Simulada.getLisinaProteinaIdeal(),pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setMetioninaCistina(suinosRN.metioninaCistinaDigestivel(exigenciaFase1Simulada.getLisinaProteinaIdeal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setTreoninaDigestivel(suinosRN.treoninaDigestivel(exigenciaFase1Simulada.getLisinaProteinaIdeal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setTriptofano(suinosRN.triptofanoDigestivel(exigenciaFase1Simulada.getLisinaProteinaIdeal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setArginina(suinosRN.argininaDigestiva(exigenciaFase1Simulada.getLisinaProteinaIdeal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setValina(suinosRN.valinaDigestivel(exigenciaFase1Simulada.getLisinaProteinaIdeal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setIsoleucina(suinosRN.isoleucinaDigestivel(exigenciaFase1Simulada.getLisinaProteinaIdeal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setLeucina(suinosRN.leucinaDigestivel(exigenciaFase1Simulada.getLisinaProteinaIdeal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setHistidina(suinosRN.histidinaDigestivel(exigenciaFase1Simulada.getLisinaProteinaIdeal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setFenilalanina(suinosRN.fenilalaninaDigestivel(exigenciaFase1Simulada.getLisinaProteinaIdeal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setFenilalaninaTirosina(suinosRN.fenilalaninaTirosinaDigestivel(exigenciaFase1Simulada.getLisinaProteinaIdeal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setNitrogenioEssencial(suinosRN.nitrogenioEssencial(exigenciaFase1Simulada,this.pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setProteinaBrutaDisgestivel(suinosRN.proteinaBrutaDigestivel(exigenciaFase1Simulada.getNitrogenioEssencial()));
		this.exigenciaFase1Simulada.setLisinaTotal(suinosRN.lisinaProteinaIdealTotal(exigenciaFase1Simulada.getLisinaProteinaIdeal()));
		this.exigenciaFase1Simulada.setMetioninaTotal(suinosRN.metioninaTotalNovo(exigenciaFase1Simulada.getLisinaTotal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setMetioninaCistinaTotal(suinosRN.metioninaCistinaTotal(exigenciaFase1Simulada.getLisinaTotal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setTreoninaTotal(suinosRN.treoninaTotal(exigenciaFase1Simulada.getLisinaTotal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setTriptofanoTotal(suinosRN.triptofanoDigestivel(exigenciaFase1Simulada.getLisinaTotal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setArgininaTotal(suinosRN.argininaTotal(exigenciaFase1Simulada.getLisinaTotal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setValinaTotal(suinosRN.valinaTotal(exigenciaFase1Simulada.getLisinaTotal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setIsoleucinaTotal(suinosRN.isoleucinaDigestivel(exigenciaFase1Simulada.getLisinaTotal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setLeucinaTotal(suinosRN.leucinaTotal(this.exigenciaFase1Simulada.getLisinaTotal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setHistidinaTotal(suinosRN.histidinaTotal(exigenciaFase1Simulada.getLisinaTotal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setFenilalaninaTotal(suinosRN.fenilalaninaTotal(exigenciaFase1Simulada.getLisinaTotal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setFenilalaninaTirosinaTotal(suinosRN.fenilalaninaTirosinaTotal(exigenciaFase1Simulada.getLisinaTotal(), pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setNitrogenioEssencialTotal(suinosRN.nitrogenioEssencialTotal(this.exigenciaFase1Simulada,pesoMedio1Simulado));
		this.exigenciaFase1Simulada.setProteinaBrutaTotal(suinosRN.proteinaBrutaTotal(exigenciaFase1Simulada.getNitrogenioEssencialTotal()));
		
		s.setPesoMedio(this.pesoMedio2Simulado);
		s.setGanhoPeso(this.gp2Simulado);
		lisinaDigestivelExigencia = suinosRN.lisinaExigenciaCategoria(this.sexo, s);
		fosforoDisponivelExigencia = suinosRN.fosforoDisponivelCategoria(this.sexo, s);
		fosforoDigestExigencia = suinosRN.fosforoDigestivelCategoria(this.sexo, s);
		this.exigenciaFase2Simulada.setLisinaDigestivelExigencia(lisinaDigestivelExigencia);
		this.exigenciaFase2Simulada.setFosforoDisponivelExigencia(fosforoDisponivelExigencia);
		this.exigenciaFase2Simulada.setFosforoDigestivelExigencia(fosforoDigestExigencia);
		s.setConsumoRacao(cr2Simulado);
		this.exigenciaFase2Simulada.setFosforoDisponivel(suinosRN.fosforoDisponivelExigencia(exigenciaFase2Simulada, s));
		this.exigenciaFase2Simulada.setFosforoDigestivel(suinosRN.fosforoDigestivelExigencia(exigenciaFase2Simulada, s));
		calcio = suinosRN.calcio(exigenciaFase2Simulada, s);
		this.exigenciaFase2Simulada.setCalcio(calcio);
		em = suinosRN.energiaMetabolizavelMediaSimulada(matriz,this.fase2a - 21,this.fase2 - 21, this.sexo);
		consumomedio = suinosRN.consumoMedio(this.fase2a - 21,this.fase2 - 21, this.sexo);
		 emDia = suinosRN.energiaMetabolizavelDiaMediaSimulada(this.matriz,this.fase2a - 21,this.fase2 - 21, this.sexo);
		this.exigenciaFase2Simulada.setEnergiaMetabolizavelDia(emDia);
		this.exigenciaFase2Simulada.setEnergiaMetabolizavelKg((int)Math.ceil(this.exigenciaFase2.getEnergiaMetabolizavelKg()));
		this.exigenciaFase2Simulada.setPotassio(suinosRN.postassioNovo(pesoMedio2Simulado,exigenciaFase2Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase2Simulada.setSodio(suinosRN.sodioNovo(pesoMedio2Simulado, exigenciaFase2Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase2Simulada.setCloro(suinosRN.cloroNovo(pesoMedio2Simulado, exigenciaFase2Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase2Simulada.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase2Simulada.getLisinaDigestivelExigencia(), cr2Simulado));
		this.exigenciaFase2Simulada.setMetioninaDigestivel(suinosRN.metioninaDigestivelNovo(exigenciaFase2Simulada.getLisinaProteinaIdeal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setMetioninaCistina(suinosRN.metioninaCistinaDigestivel(exigenciaFase2Simulada.getLisinaProteinaIdeal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setTreoninaDigestivel(suinosRN.treoninaDigestivel(exigenciaFase2Simulada.getLisinaProteinaIdeal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setTriptofano(suinosRN.triptofanoDigestivel(exigenciaFase2Simulada.getLisinaProteinaIdeal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setArginina(suinosRN.argininaDigestiva(exigenciaFase2Simulada.getLisinaProteinaIdeal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setValina(suinosRN.valinaDigestivel(exigenciaFase2Simulada.getLisinaProteinaIdeal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setIsoleucina(suinosRN.isoleucinaDigestivel(exigenciaFase2Simulada.getLisinaProteinaIdeal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setLeucina(suinosRN.leucinaDigestivel(exigenciaFase2Simulada.getLisinaProteinaIdeal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setHistidina(suinosRN.histidinaDigestivel(exigenciaFase2Simulada.getLisinaProteinaIdeal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setFenilalanina(suinosRN.fenilalaninaDigestivel(exigenciaFase2Simulada.getLisinaProteinaIdeal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setFenilalaninaTirosina(suinosRN.fenilalaninaTirosinaDigestivel(exigenciaFase2Simulada.getLisinaProteinaIdeal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setNitrogenioEssencial(suinosRN.nitrogenioEssencial(exigenciaFase2Simulada,this.pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setProteinaBrutaDisgestivel(suinosRN.proteinaBrutaDigestivel(exigenciaFase2Simulada.getNitrogenioEssencial()));
		this.exigenciaFase2Simulada.setLisinaTotal(suinosRN.lisinaProteinaIdealTotal(exigenciaFase2Simulada.getLisinaProteinaIdeal()));
		this.exigenciaFase2Simulada.setMetioninaTotal(suinosRN.metioninaTotalNovo(exigenciaFase2Simulada.getLisinaTotal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setMetioninaCistinaTotal(suinosRN.metioninaCistinaTotal(exigenciaFase2Simulada.getLisinaTotal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setTreoninaTotal(suinosRN.treoninaTotal(exigenciaFase2Simulada.getLisinaTotal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setTriptofanoTotal(suinosRN.triptofanoDigestivel(exigenciaFase2Simulada.getLisinaTotal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setArgininaTotal(suinosRN.argininaTotal(exigenciaFase2Simulada.getLisinaTotal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setValinaTotal(suinosRN.valinaTotal(exigenciaFase2Simulada.getLisinaTotal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setIsoleucinaTotal(suinosRN.isoleucinaDigestivel(exigenciaFase2Simulada.getLisinaTotal(), pesoMedio2Simulado));		
		this.exigenciaFase2Simulada.setLeucinaTotal(suinosRN.leucinaTotal(this.exigenciaFase2Simulada.getLisinaTotal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setHistidinaTotal(suinosRN.histidinaTotal(exigenciaFase2Simulada.getLisinaTotal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setFenilalaninaTotal(suinosRN.fenilalaninaTotal(exigenciaFase2Simulada.getLisinaTotal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setFenilalaninaTirosinaTotal(suinosRN.fenilalaninaTirosinaTotal(exigenciaFase2Simulada.getLisinaTotal(), pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setNitrogenioEssencialTotal(suinosRN.nitrogenioEssencialTotal(this.exigenciaFase2Simulada,pesoMedio2Simulado));
		this.exigenciaFase2Simulada.setProteinaBrutaTotal(suinosRN.proteinaBrutaTotal(exigenciaFase2Simulada.getNitrogenioEssencialTotal()));
		
		s.setPesoMedio(this.pesoMedio3Simulado);
		s.setGanhoPeso(this.gp3Simulado);
		lisinaDigestivelExigencia = suinosRN.lisinaExigenciaCategoria(this.sexo, s);
		fosforoDisponivelExigencia = suinosRN.fosforoDisponivelCategoria(this.sexo, s);
		fosforoDigestExigencia = suinosRN.fosforoDigestivelCategoria(this.sexo, s);
		this.exigenciaFase3Simulada.setLisinaDigestivelExigencia(lisinaDigestivelExigencia);
		this.exigenciaFase3Simulada.setFosforoDisponivelExigencia(fosforoDisponivelExigencia);
		this.exigenciaFase3Simulada.setFosforoDigestivelExigencia(fosforoDigestExigencia);
		s.setConsumoRacao(cr3Simulado);
		this.exigenciaFase3Simulada.setFosforoDisponivel(suinosRN.fosforoDisponivelExigencia(exigenciaFase3Simulada, s));
		this.exigenciaFase3Simulada.setFosforoDigestivel(suinosRN.fosforoDigestivelExigencia(exigenciaFase3Simulada, s));
		calcio = suinosRN.calcio(exigenciaFase3Simulada, s);
		this.exigenciaFase3Simulada.setCalcio(calcio);
		em = suinosRN.energiaMetabolizavelMediaSimulada(matriz,this.fase3a - 21,this.fase3 - 21,this.sexo);
		consumomedio = suinosRN.consumoMedio(this.fase3a - 21,this.fase3 - 21, this.sexo);
		 emDia = suinosRN.energiaMetabolizavelDiaMediaSimulada(this.matriz,this.fase3a - 21,this.fase3 - 21, this.sexo);
		this.exigenciaFase3Simulada.setEnergiaMetabolizavelDia(emDia);
		this.exigenciaFase3Simulada.setEnergiaMetabolizavelKg((int)Math.ceil(this.exigenciaFase3.getEnergiaMetabolizavelKg()));
		this.exigenciaFase3Simulada.setPotassio(suinosRN.postassioNovo(pesoMedio2Simulado,exigenciaFase3Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase3Simulada.setSodio(suinosRN.sodioNovo(pesoMedio3Simulado, exigenciaFase3Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase3Simulada.setCloro(suinosRN.cloroNovo(pesoMedio3Simulado, exigenciaFase3Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase3Simulada.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase3Simulada.getLisinaDigestivelExigencia(), cr3Simulado));
		this.exigenciaFase3Simulada.setMetioninaDigestivel(suinosRN.metioninaDigestivelNovo(exigenciaFase3Simulada.getLisinaProteinaIdeal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setMetioninaCistina(suinosRN.metioninaCistinaDigestivel(exigenciaFase3Simulada.getLisinaProteinaIdeal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setTreoninaDigestivel(suinosRN.treoninaDigestivel(exigenciaFase3Simulada.getLisinaProteinaIdeal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setTriptofano(suinosRN.triptofanoDigestivel(exigenciaFase3Simulada.getLisinaProteinaIdeal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setArginina(suinosRN.argininaDigestiva(exigenciaFase3Simulada.getLisinaProteinaIdeal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setValina(suinosRN.valinaDigestivel(exigenciaFase3Simulada.getLisinaProteinaIdeal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setIsoleucina(suinosRN.isoleucinaDigestivel(exigenciaFase3Simulada.getLisinaProteinaIdeal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setLeucina(suinosRN.leucinaDigestivel(exigenciaFase3Simulada.getLisinaProteinaIdeal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setHistidina(suinosRN.histidinaDigestivel(exigenciaFase3Simulada.getLisinaProteinaIdeal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setFenilalanina(suinosRN.fenilalaninaDigestivel(exigenciaFase3Simulada.getLisinaProteinaIdeal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setFenilalaninaTirosina(suinosRN.fenilalaninaTirosinaDigestivel(exigenciaFase3Simulada.getLisinaProteinaIdeal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setNitrogenioEssencial(suinosRN.nitrogenioEssencial(exigenciaFase3Simulada,this.pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setProteinaBrutaDisgestivel(suinosRN.proteinaBrutaDigestivel(exigenciaFase3Simulada.getNitrogenioEssencial()));
		this.exigenciaFase3Simulada.setLisinaTotal(suinosRN.lisinaProteinaIdealTotal(exigenciaFase3Simulada.getLisinaProteinaIdeal()));
		this.exigenciaFase3Simulada.setMetioninaTotal(suinosRN.metioninaTotalNovo(exigenciaFase3Simulada.getLisinaTotal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setMetioninaCistinaTotal(suinosRN.metioninaCistinaTotal(exigenciaFase3Simulada.getLisinaTotal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setTreoninaTotal(suinosRN.treoninaTotal(exigenciaFase3Simulada.getLisinaTotal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setTriptofanoTotal(suinosRN.triptofanoDigestivel(exigenciaFase3Simulada.getLisinaTotal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setArgininaTotal(suinosRN.argininaTotal(exigenciaFase3Simulada.getLisinaTotal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setValinaTotal(suinosRN.valinaTotal(exigenciaFase3Simulada.getLisinaTotal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setIsoleucinaTotal(suinosRN.isoleucinaDigestivel(exigenciaFase3Simulada.getLisinaTotal(), pesoMedio3Simulado));		
		this.exigenciaFase3Simulada.setLeucinaTotal(suinosRN.leucinaTotal(this.exigenciaFase3Simulada.getLisinaTotal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setHistidinaTotal(suinosRN.histidinaTotal(exigenciaFase3Simulada.getLisinaTotal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setFenilalaninaTotal(suinosRN.fenilalaninaTotal(exigenciaFase3Simulada.getLisinaTotal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setFenilalaninaTirosinaTotal(suinosRN.fenilalaninaTirosinaTotal(exigenciaFase3Simulada.getLisinaTotal(), pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setNitrogenioEssencialTotal(suinosRN.nitrogenioEssencialTotal(this.exigenciaFase3Simulada,pesoMedio3Simulado));
		this.exigenciaFase3Simulada.setProteinaBrutaTotal(suinosRN.proteinaBrutaTotal(exigenciaFase3Simulada.getNitrogenioEssencialTotal()));
		 
		
		s.setPesoMedio(this.pesoMedio4Simulado);
		s.setGanhoPeso(this.gp4Simulado);
		lisinaDigestivelExigencia = suinosRN.lisinaExigenciaCategoria(this.sexo, s);
		fosforoDisponivelExigencia = suinosRN.fosforoDisponivelCategoria(this.sexo, s);
		fosforoDigestExigencia = suinosRN.fosforoDigestivelCategoria(this.sexo, s);
		this.exigenciaFase4Simulada.setLisinaDigestivelExigencia(lisinaDigestivelExigencia);
		this.exigenciaFase4Simulada.setFosforoDisponivelExigencia(fosforoDisponivelExigencia);
		this.exigenciaFase4Simulada.setFosforoDigestivelExigencia(fosforoDigestExigencia);
		s.setConsumoRacao(cr4Simulado);
		this.exigenciaFase4Simulada.setFosforoDisponivel(suinosRN.fosforoDisponivelExigencia(exigenciaFase4Simulada, s));
		this.exigenciaFase4Simulada.setFosforoDigestivel(suinosRN.fosforoDigestivelExigencia(exigenciaFase4Simulada, s));
		calcio = suinosRN.calcio(exigenciaFase4Simulada, s);
		this.exigenciaFase4Simulada.setCalcio(calcio);
		em = suinosRN.energiaMetabolizavelMediaSimulada(matriz,this.fase4a - 21,this.fase4 - 21, this.sexo);
		consumomedio = suinosRN.consumoMedio(this.fase4a - 21,this.fase4 - 21, this.sexo);
		 emDia = suinosRN.energiaMetabolizavelDiaMediaSimulada(this.matriz,this.fase4a - 21,this.fase4 - 21, this.sexo);
		this.exigenciaFase4Simulada.setEnergiaMetabolizavelDia(emDia);
		this.exigenciaFase4Simulada.setEnergiaMetabolizavelKg((int)Math.ceil(this.exigenciaFase4.getEnergiaMetabolizavelKg()));
		this.exigenciaFase4Simulada.setPotassio(suinosRN.postassioNovo(pesoMedio2Simulado,exigenciaFase4Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase4Simulada.setSodio(suinosRN.sodioNovo(pesoMedio4Simulado, exigenciaFase4Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase4Simulada.setCloro(suinosRN.cloroNovo(pesoMedio4Simulado, exigenciaFase4Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase4Simulada.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase4Simulada.getLisinaDigestivelExigencia(), cr4Simulado));
		this.exigenciaFase4Simulada.setMetioninaDigestivel(suinosRN.metioninaDigestivelNovo(exigenciaFase4Simulada.getLisinaProteinaIdeal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setMetioninaCistina(suinosRN.metioninaCistinaDigestivel(exigenciaFase4Simulada.getLisinaProteinaIdeal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setTreoninaDigestivel(suinosRN.treoninaDigestivel(exigenciaFase4Simulada.getLisinaProteinaIdeal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setTriptofano(suinosRN.triptofanoDigestivel(exigenciaFase4Simulada.getLisinaProteinaIdeal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setArginina(suinosRN.argininaDigestiva(exigenciaFase4Simulada.getLisinaProteinaIdeal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setValina(suinosRN.valinaDigestivel(exigenciaFase4Simulada.getLisinaProteinaIdeal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setIsoleucina(suinosRN.isoleucinaDigestivel(exigenciaFase4Simulada.getLisinaProteinaIdeal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setLeucina(suinosRN.leucinaDigestivel(exigenciaFase4Simulada.getLisinaProteinaIdeal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setHistidina(suinosRN.histidinaDigestivel(exigenciaFase4Simulada.getLisinaProteinaIdeal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setFenilalanina(suinosRN.fenilalaninaDigestivel(exigenciaFase4Simulada.getLisinaProteinaIdeal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setFenilalaninaTirosina(suinosRN.fenilalaninaTirosinaDigestivel(exigenciaFase4Simulada.getLisinaProteinaIdeal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setNitrogenioEssencial(suinosRN.nitrogenioEssencial(exigenciaFase4Simulada,this.pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setProteinaBrutaDisgestivel(suinosRN.proteinaBrutaDigestivel(exigenciaFase4Simulada.getNitrogenioEssencial()));
		this.exigenciaFase4Simulada.setLisinaTotal(suinosRN.lisinaProteinaIdealTotal(exigenciaFase4Simulada.getLisinaProteinaIdeal()));
		this.exigenciaFase4Simulada.setMetioninaTotal(suinosRN.metioninaTotalNovo(exigenciaFase4Simulada.getLisinaTotal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setMetioninaCistinaTotal(suinosRN.metioninaCistinaTotal(exigenciaFase4Simulada.getLisinaTotal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setTreoninaTotal(suinosRN.treoninaTotal(exigenciaFase4Simulada.getLisinaTotal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setTriptofanoTotal(suinosRN.triptofanoDigestivel(exigenciaFase4Simulada.getLisinaTotal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setArgininaTotal(suinosRN.argininaTotal(exigenciaFase4Simulada.getLisinaTotal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setValinaTotal(suinosRN.valinaTotal(exigenciaFase4Simulada.getLisinaTotal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setIsoleucinaTotal(suinosRN.isoleucinaDigestivel(exigenciaFase4Simulada.getLisinaTotal(), pesoMedio4Simulado));		
		this.exigenciaFase4Simulada.setLeucinaTotal(suinosRN.leucinaTotal(this.exigenciaFase4Simulada.getLisinaTotal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setHistidinaTotal(suinosRN.histidinaTotal(exigenciaFase4Simulada.getLisinaTotal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setFenilalaninaTotal(suinosRN.fenilalaninaTotal(exigenciaFase4Simulada.getLisinaTotal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setFenilalaninaTirosinaTotal(suinosRN.fenilalaninaTirosinaTotal(exigenciaFase4Simulada.getLisinaTotal(), pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setNitrogenioEssencialTotal(suinosRN.nitrogenioEssencialTotal(this.exigenciaFase4Simulada,pesoMedio4Simulado));
		this.exigenciaFase4Simulada.setProteinaBrutaTotal(suinosRN.proteinaBrutaTotal(exigenciaFase4Simulada.getNitrogenioEssencialTotal()));
		 
		 
		
		s.setPesoMedio(this.pesoMedio5Simulado);
		s.setGanhoPeso(this.gp5Simulado);
		lisinaDigestivelExigencia = suinosRN.lisinaExigenciaCategoria(this.sexo, s);
		fosforoDisponivelExigencia = suinosRN.fosforoDisponivelCategoria(this.sexo, s);
		fosforoDigestExigencia = suinosRN.fosforoDigestivelCategoria(this.sexo, s);
		this.exigenciaFase5Simulada.setLisinaDigestivelExigencia(lisinaDigestivelExigencia);
		this.exigenciaFase5Simulada.setFosforoDisponivelExigencia(fosforoDisponivelExigencia);
		this.exigenciaFase5Simulada.setFosforoDigestivelExigencia(fosforoDigestExigencia);
		s.setConsumoRacao(cr5Simulado);
		this.exigenciaFase5Simulada.setFosforoDisponivel(suinosRN.fosforoDisponivelExigencia(exigenciaFase5Simulada, s));
		this.exigenciaFase5Simulada.setFosforoDigestivel(suinosRN.fosforoDigestivelExigencia(exigenciaFase5Simulada, s));
		calcio = suinosRN.calcio(exigenciaFase5Simulada, s);
		this.exigenciaFase5Simulada.setCalcio(calcio);
		em = suinosRN.energiaMetabolizavelMediaSimulada(matriz,this.fase5a - 21,this.fase5 - 21, this.sexo);
		consumomedio = suinosRN.consumoMedio(this.fase5a - 21,this.fase5 - 21, this.sexo);
		 emDia = suinosRN.energiaMetabolizavelDiaMediaSimulada(this.matriz,this.fase5a - 21,this.fase5 - 21, this.sexo);
		this.exigenciaFase5Simulada.setEnergiaMetabolizavelDia(emDia);
		this.exigenciaFase5Simulada.setEnergiaMetabolizavelKg((int)Math.ceil(this.exigenciaFase5.getEnergiaMetabolizavelKg()));
		this.exigenciaFase5Simulada.setPotassio(suinosRN.postassioNovo(pesoMedio5Simulado,exigenciaFase5Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase5Simulada.setSodio(suinosRN.sodioNovo(pesoMedio5Simulado, exigenciaFase5Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase5Simulada.setCloro(suinosRN.cloroNovo(pesoMedio5Simulado, exigenciaFase5Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase5Simulada.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase5Simulada.getLisinaDigestivelExigencia(), cr5Simulado));
		this.exigenciaFase5Simulada.setMetioninaDigestivel(suinosRN.metioninaDigestivelNovo(exigenciaFase5Simulada.getLisinaProteinaIdeal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setMetioninaCistina(suinosRN.metioninaCistinaDigestivel(exigenciaFase5Simulada.getLisinaProteinaIdeal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setTreoninaDigestivel(suinosRN.treoninaDigestivel(exigenciaFase5Simulada.getLisinaProteinaIdeal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setTriptofano(suinosRN.triptofanoDigestivel(exigenciaFase5Simulada.getLisinaProteinaIdeal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setArginina(suinosRN.argininaDigestiva(exigenciaFase5Simulada.getLisinaProteinaIdeal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setValina(suinosRN.valinaDigestivel(exigenciaFase5Simulada.getLisinaProteinaIdeal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setIsoleucina(suinosRN.isoleucinaDigestivel(exigenciaFase5Simulada.getLisinaProteinaIdeal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setLeucina(suinosRN.leucinaDigestivel(exigenciaFase5Simulada.getLisinaProteinaIdeal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setHistidina(suinosRN.histidinaDigestivel(exigenciaFase5Simulada.getLisinaProteinaIdeal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setFenilalanina(suinosRN.fenilalaninaDigestivel(exigenciaFase5Simulada.getLisinaProteinaIdeal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setFenilalaninaTirosina(suinosRN.fenilalaninaTirosinaDigestivel(exigenciaFase5Simulada.getLisinaProteinaIdeal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setNitrogenioEssencial(suinosRN.nitrogenioEssencial(exigenciaFase5Simulada,this.pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setProteinaBrutaDisgestivel(suinosRN.proteinaBrutaDigestivel(exigenciaFase5Simulada.getNitrogenioEssencial()));
		this.exigenciaFase5Simulada.setLisinaTotal(suinosRN.lisinaProteinaIdealTotal(exigenciaFase5Simulada.getLisinaProteinaIdeal()));
		this.exigenciaFase5Simulada.setMetioninaTotal(suinosRN.metioninaTotalNovo(exigenciaFase5Simulada.getLisinaTotal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setMetioninaCistinaTotal(suinosRN.metioninaCistinaTotal(exigenciaFase5Simulada.getLisinaTotal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setTreoninaTotal(suinosRN.treoninaTotal(exigenciaFase5Simulada.getLisinaTotal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setTriptofanoTotal(suinosRN.triptofanoDigestivel(exigenciaFase5Simulada.getLisinaTotal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setArgininaTotal(suinosRN.argininaTotal(exigenciaFase5Simulada.getLisinaTotal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setValinaTotal(suinosRN.valinaTotal(exigenciaFase5Simulada.getLisinaTotal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setIsoleucinaTotal(suinosRN.isoleucinaDigestivel(exigenciaFase5Simulada.getLisinaTotal(), pesoMedio5Simulado));		
		this.exigenciaFase5Simulada.setLeucinaTotal(suinosRN.leucinaTotal(this.exigenciaFase5Simulada.getLisinaTotal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setHistidinaTotal(suinosRN.histidinaTotal(exigenciaFase5Simulada.getLisinaTotal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setFenilalaninaTotal(suinosRN.fenilalaninaTotal(exigenciaFase5Simulada.getLisinaTotal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setFenilalaninaTirosinaTotal(suinosRN.fenilalaninaTirosinaTotal(exigenciaFase5Simulada.getLisinaTotal(), pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setNitrogenioEssencialTotal(suinosRN.nitrogenioEssencialTotal(this.exigenciaFase5Simulada,pesoMedio5Simulado));
		this.exigenciaFase5Simulada.setProteinaBrutaTotal(suinosRN.proteinaBrutaTotal(exigenciaFase5Simulada.getNitrogenioEssencialTotal()));
		 
		s.setPesoMedio(this.pesoMedio6Simulado);
		s.setGanhoPeso(this.gp6Simulado);
		lisinaDigestivelExigencia = suinosRN.lisinaExigenciaCategoria(this.sexo, s);
		fosforoDisponivelExigencia = suinosRN.fosforoDisponivelCategoria(this.sexo, s);
		fosforoDigestExigencia = suinosRN.fosforoDigestivelCategoria(this.sexo, s);
		this.exigenciaFase6Simulada.setLisinaDigestivelExigencia(lisinaDigestivelExigencia);
		this.exigenciaFase6Simulada.setFosforoDisponivelExigencia(fosforoDisponivelExigencia);
		this.exigenciaFase6Simulada.setFosforoDigestivelExigencia(fosforoDigestExigencia);
		s.setConsumoRacao(cr6Simulado);
		this.exigenciaFase6Simulada.setFosforoDisponivel(suinosRN.fosforoDisponivelExigencia(exigenciaFase6Simulada, s));
		this.exigenciaFase6Simulada.setFosforoDigestivel(suinosRN.fosforoDigestivelExigencia(exigenciaFase6Simulada, s));
		calcio = suinosRN.calcio(exigenciaFase6Simulada, s);
		this.exigenciaFase6Simulada.setCalcio(calcio);
		em = suinosRN.energiaMetabolizavelMediaSimulada(matriz,this.fase6a - 21,this.idadeAbate - 21, this.sexo);
		consumomedio = suinosRN.consumoMedio(this.fase6a - 21,this.idadeAbate - 21, this.sexo);
		 emDia = suinosRN.energiaMetabolizavelDiaMediaSimulada(this.matriz,this.fase6a - 21,this.idadeAbate - 21, this.sexo);
		this.exigenciaFase6Simulada.setEnergiaMetabolizavelDia(emDia);
		this.exigenciaFase6Simulada.setEnergiaMetabolizavelKg((int)Math.ceil(this.exigenciaFase6.getEnergiaMetabolizavelKg()));
		this.exigenciaFase6Simulada.setPotassio(suinosRN.postassioNovo(pesoMedio6Simulado,exigenciaFase6Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase6Simulada.setSodio(suinosRN.sodioNovo(pesoMedio6Simulado, exigenciaFase6Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase6Simulada.setCloro(suinosRN.cloroNovo(pesoMedio6Simulado, exigenciaFase6Simulada.getEnergiaMetabolizavelKg()));
		this.exigenciaFase6Simulada.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase6Simulada.getLisinaDigestivelExigencia(), cr6Simulado));
		this.exigenciaFase6Simulada.setMetioninaDigestivel(suinosRN.metioninaDigestivelNovo(exigenciaFase6Simulada.getLisinaProteinaIdeal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setMetioninaCistina(suinosRN.metioninaCistinaDigestivel(exigenciaFase6Simulada.getLisinaProteinaIdeal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setTreoninaDigestivel(suinosRN.treoninaDigestivel(exigenciaFase6Simulada.getLisinaProteinaIdeal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setTriptofano(suinosRN.triptofanoDigestivel(exigenciaFase6Simulada.getLisinaProteinaIdeal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setArginina(suinosRN.argininaDigestiva(exigenciaFase6Simulada.getLisinaProteinaIdeal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setValina(suinosRN.valinaDigestivel(exigenciaFase6Simulada.getLisinaProteinaIdeal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setIsoleucina(suinosRN.isoleucinaDigestivel(exigenciaFase6Simulada.getLisinaProteinaIdeal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setLeucina(suinosRN.leucinaDigestivel(exigenciaFase6Simulada.getLisinaProteinaIdeal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setHistidina(suinosRN.histidinaDigestivel(exigenciaFase6Simulada.getLisinaProteinaIdeal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setFenilalanina(suinosRN.fenilalaninaDigestivel(exigenciaFase6Simulada.getLisinaProteinaIdeal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setFenilalaninaTirosina(suinosRN.fenilalaninaTirosinaDigestivel(exigenciaFase6Simulada.getLisinaProteinaIdeal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setNitrogenioEssencial(suinosRN.nitrogenioEssencial(exigenciaFase6Simulada,this.pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setProteinaBrutaDisgestivel(suinosRN.proteinaBrutaDigestivel(exigenciaFase6Simulada.getNitrogenioEssencial()));
		this.exigenciaFase6Simulada.setLisinaTotal(suinosRN.lisinaProteinaIdealTotal(exigenciaFase6Simulada.getLisinaProteinaIdeal()));
		this.exigenciaFase6Simulada.setMetioninaTotal(suinosRN.metioninaTotalNovo(exigenciaFase6Simulada.getLisinaTotal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setMetioninaCistinaTotal(suinosRN.metioninaCistinaTotal(exigenciaFase6Simulada.getLisinaTotal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setTreoninaTotal(suinosRN.treoninaTotal(exigenciaFase6Simulada.getLisinaTotal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setTriptofanoTotal(suinosRN.triptofanoDigestivel(exigenciaFase6Simulada.getLisinaTotal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setArgininaTotal(suinosRN.argininaTotal(exigenciaFase6Simulada.getLisinaTotal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setValinaTotal(suinosRN.valinaTotal(exigenciaFase6Simulada.getLisinaTotal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setIsoleucinaTotal(suinosRN.isoleucinaDigestivel(exigenciaFase6Simulada.getLisinaTotal(), pesoMedio6Simulado));		
		this.exigenciaFase6Simulada.setLeucinaTotal(suinosRN.leucinaTotal(this.exigenciaFase6Simulada.getLisinaTotal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setHistidinaTotal(suinosRN.histidinaTotal(exigenciaFase6Simulada.getLisinaTotal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setFenilalaninaTotal(suinosRN.fenilalaninaTotal(exigenciaFase6Simulada.getLisinaTotal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setFenilalaninaTirosinaTotal(suinosRN.fenilalaninaTirosinaTotal(exigenciaFase6Simulada.getLisinaTotal(), pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setNitrogenioEssencialTotal(suinosRN.nitrogenioEssencialTotal(this.exigenciaFase6Simulada,pesoMedio6Simulado));
		this.exigenciaFase6Simulada.setProteinaBrutaTotal(suinosRN.proteinaBrutaTotal(exigenciaFase6Simulada.getNitrogenioEssencialTotal()));
		
		if(ativaTemperatura){
			
			s.setTemperatura(this.temperatura);
			s.setPesoMedio(pesoMedio2Simulado);
			double correcao = suinosRN.correcaoTemperatura(s, suinosRN.temperaturaMedia(pesoMedio2Simulado));
			System.out.println("Correcao "+correcao);
			System.out.println("VALOR "+(exigenciaFase2Simulada.getEnergiaMetabolizavelDia() + correcao));
			this.exigenciaFase2Simulada.setEnergiaMetabolizavelDia(( exigenciaFase2Simulada.getEnergiaMetabolizavelDia() + correcao ) );	
			if(exigenciaFase2Simulada.getEnergiaMetDet() != 0){
				this.cr2Simulado = exigenciaFase2Simulada.getEnergiaMetabolizavelDia()/exigenciaFase2Simulada.getEnergiaMetDet();
			}else this.cr2Simulado = exigenciaFase2Simulada.getEnergiaMetabolizavelDia()/exigenciaFase2Simulada.getEnergiaMetabolizavelKg();
			this.dependeTemperatura(this.exigenciaFase2Simulada,pesoMedio2Simulado, this.cr2Simulado);
			
			s.setTemperatura(this.temperatura);
			s.setPesoMedio(pesoMedio3Simulado);
			correcao = suinosRN.correcaoTemperatura(s, suinosRN.temperaturaMedia(pesoMedio3Simulado));
			System.out.println("Correcao "+correcao);
			this.exigenciaFase3Simulada.setEnergiaMetabolizavelDia( (exigenciaFase3Simulada.getEnergiaMetabolizavelDia() + correcao ) );
			this.cr3Simulado = exigenciaFase3Simulada.getEnergiaMetabolizavelDia()/exigenciaFase3Simulada.getEnergiaMetabolizavelKg();
			this.dependeTemperatura(this.exigenciaFase3Simulada,pesoMedio3Simulado, this.cr3Simulado);
			
			s.setTemperatura(this.temperatura);
			s.setPesoMedio(pesoMedio4Simulado);
			 correcao = suinosRN.correcaoTemperatura(s, suinosRN.temperaturaMedia(pesoMedio4Simulado));
			System.out.println("Correcao "+correcao);
			this.exigenciaFase4Simulada.setEnergiaMetabolizavelDia( exigenciaFase4Simulada.getEnergiaMetabolizavelDia() + correcao  );
			this.cr4Simulado = exigenciaFase4Simulada.getEnergiaMetabolizavelDia()/exigenciaFase4Simulada.getEnergiaMetabolizavelKg();
			this.dependeTemperatura(this.exigenciaFase4Simulada,pesoMedio4Simulado, this.cr4Simulado);
			
			s.setTemperatura(this.temperatura);
			s.setPesoMedio(pesoMedio5Simulado);
			 correcao = suinosRN.correcaoTemperatura(s, suinosRN.temperaturaMedia(pesoMedio5Simulado));
			System.out.println("Correcao "+correcao);
			this.exigenciaFase5Simulada.setEnergiaMetabolizavelDia( exigenciaFase5Simulada.getEnergiaMetabolizavelDia() + correcao  );
			this.cr5Simulado = exigenciaFase5Simulada.getEnergiaMetabolizavelDia()/exigenciaFase5Simulada.getEnergiaMetabolizavelKg();
			this.dependeTemperatura(this.exigenciaFase5Simulada,pesoMedio5Simulado, this.cr5Simulado);
			
			s.setTemperatura(this.temperatura);
			s.setPesoMedio(pesoMedio6Simulado);
			 correcao = suinosRN.correcaoTemperatura(s, suinosRN.temperaturaMedia(pesoMedio6Simulado));
			System.out.println("Correcao "+correcao);
			this.exigenciaFase6Simulada.setEnergiaMetabolizavelDia( exigenciaFase6Simulada.getEnergiaMetabolizavelDia() + correcao  );
			this.cr6Simulado = exigenciaFase6Simulada.getEnergiaMetabolizavelDia()/exigenciaFase6Simulada.getEnergiaMetabolizavelKg();
			this.dependeTemperatura(this.exigenciaFase6Simulada,pesoMedio6Simulado, this.cr6Simulado);
			
		
		}
	}

	public void calcularExigencia(){
		
		
		SuinosRN suinosRN = new SuinosRN();
		Suino s = new Suino();
		s.setPesoMedio(this.pesoMedio1);
		s.setGanhoPeso(this.gp1);
		double lisinaDigestivelExigencia = suinosRN.lisinaExigenciaCategoria(this.sexo, s);

		double fosforoDisponivelExigencia = suinosRN.fosforoDisponivelCategoria(this.sexo, s);
		double fosforoDigestExigencia = suinosRN.fosforoDigestivelCategoria(this.sexo, s);
		
		this.exigenciaFase1.setLisinaDigestivelExigencia(lisinaDigestivelExigencia);
		this.exigenciaFase1.setFosforoDisponivelExigencia(fosforoDisponivelExigencia);
		this.exigenciaFase1.setFosforoDigestivelExigencia(fosforoDigestExigencia);
		s.setConsumoRacao(cr1);
		this.exigenciaFase1.setFosforoDisponivel(suinosRN.fosforoDisponivelExigencia(exigenciaFase1, s));
		this.exigenciaFase1.setFosforoDigestivel(suinosRN.fosforoDigestivelExigencia(exigenciaFase1, s));
		double calcio = suinosRN.calcio(exigenciaFase1, s);
		this.exigenciaFase1.setCalcio(calcio);
		double energiaMetFase1 = suinosRN.energiaMetabolizavelKgFaixa(this.idadeInicial ,this.fase1);
		
		double em = suinosRN.energiaMetabolizavelMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
		double consumomedio = suinosRN.consumoMedio(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
		double emDia = suinosRN.energiaMetabolizavelDiaMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
		if(energiaMetFase1 != -1){
			this.exigenciaFase1.setEnergiaMetabolizavelKg((int)(energiaMetFase1));
		}else 	this.exigenciaFase1.setEnergiaMetabolizavelKg((int)(em));
		this.exigenciaFase1.setEnergiaMetabolizavelDia(exigenciaFase1.getEnergiaMetabolizavelKg() * cr1);
		double pot = suinosRN.postassioNovo(pesoMedio1,exigenciaFase1.getEnergiaMetabolizavelKg());
		this.exigenciaFase1.setPotassio(pot);
		this.exigenciaFase1.setSodio(suinosRN.sodioNovo(pesoMedio1, exigenciaFase1.getEnergiaMetabolizavelKg()));
		this.exigenciaFase1.setCloro(suinosRN.cloroNovo(pesoMedio1, exigenciaFase1.getEnergiaMetabolizavelKg()));
		this.exigenciaFase1.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase1.getLisinaDigestivelExigencia(), cr1));
		this.exigenciaFase1.setMetioninaDigestivel(suinosRN.metioninaDigestivelNovo(exigenciaFase1.getLisinaProteinaIdeal(),pesoMedio1));
		this.exigenciaFase1.setMetioninaCistina(suinosRN.metioninaCistinaDigestivel(exigenciaFase1.getLisinaProteinaIdeal(), pesoMedio1));
		this.exigenciaFase1.setTreoninaDigestivel(suinosRN.treoninaDigestivel(exigenciaFase1.getLisinaProteinaIdeal(), pesoMedio1));
		this.exigenciaFase1.setTriptofano(suinosRN.triptofanoDigestivel(exigenciaFase1.getLisinaProteinaIdeal(), pesoMedio1));
		this.exigenciaFase1.setArginina(suinosRN.argininaDigestiva(exigenciaFase1.getLisinaProteinaIdeal(), pesoMedio1));
		this.exigenciaFase1.setValina(suinosRN.valinaDigestivel(exigenciaFase1.getLisinaProteinaIdeal(), pesoMedio1));
		this.exigenciaFase1.setIsoleucina(suinosRN.isoleucinaDigestivel(exigenciaFase1.getLisinaProteinaIdeal(), pesoMedio1));
		this.exigenciaFase1.setLeucina(suinosRN.leucinaDigestivel(exigenciaFase1.getLisinaProteinaIdeal(), pesoMedio1));
		this.exigenciaFase1.setHistidina(suinosRN.histidinaDigestivel(exigenciaFase1.getLisinaProteinaIdeal(), pesoMedio1));
		this.exigenciaFase1.setFenilalanina(suinosRN.fenilalaninaDigestivel(exigenciaFase1.getLisinaProteinaIdeal(), pesoMedio1));
		this.exigenciaFase1.setFenilalaninaTirosina(suinosRN.fenilalaninaTirosinaDigestivel(exigenciaFase1.getLisinaProteinaIdeal(), pesoMedio1));
		this.exigenciaFase1.setNitrogenioEssencial(suinosRN.nitrogenioEssencial(exigenciaFase1,this.pesoMedio1));
		this.exigenciaFase1.setProteinaBrutaDisgestivel(suinosRN.proteinaBrutaDigestivel(exigenciaFase1.getNitrogenioEssencial()));
		this.exigenciaFase1.setLisinaTotal(suinosRN.lisinaProteinaIdealTotal(exigenciaFase1.getLisinaProteinaIdeal()));
		this.exigenciaFase1.setMetioninaTotal(suinosRN.metioninaTotalNovo(exigenciaFase1.getLisinaTotal(), pesoMedio1));
		this.exigenciaFase1.setMetioninaCistinaTotal(suinosRN.metioninaCistinaTotal(exigenciaFase1.getLisinaTotal(), pesoMedio1));
		this.exigenciaFase1.setTreoninaTotal(suinosRN.treoninaTotal(exigenciaFase1.getLisinaTotal(), pesoMedio1));
		this.exigenciaFase1.setTriptofanoTotal(suinosRN.triptofanoDigestivel(exigenciaFase1.getLisinaTotal(), pesoMedio1));
		this.exigenciaFase1.setArgininaTotal(suinosRN.argininaTotal(exigenciaFase1.getLisinaTotal(), pesoMedio1));
		this.exigenciaFase1.setValinaTotal(suinosRN.valinaTotal(exigenciaFase1.getLisinaTotal(), pesoMedio1));
		this.exigenciaFase1.setIsoleucinaTotal(suinosRN.isoleucinaDigestivel(exigenciaFase1.getLisinaTotal(), pesoMedio1));
		this.exigenciaFase1.setLeucinaTotal(suinosRN.leucinaTotal(this.exigenciaFase1.getLisinaTotal(), pesoMedio1));
		this.exigenciaFase1.setHistidinaTotal(suinosRN.histidinaTotal(exigenciaFase1.getLisinaTotal(), pesoMedio1));
		this.exigenciaFase1.setFenilalaninaTotal(suinosRN.fenilalaninaTotal(exigenciaFase1.getLisinaTotal(), pesoMedio1));
		this.exigenciaFase1.setFenilalaninaTirosinaTotal(suinosRN.fenilalaninaTirosinaTotal(exigenciaFase1.getLisinaTotal(), pesoMedio1));
		this.exigenciaFase1.setNitrogenioEssencialTotal(suinosRN.nitrogenioEssencialTotal(this.exigenciaFase1,pesoMedio1));
		this.exigenciaFase1.setProteinaBrutaTotal(suinosRN.proteinaBrutaTotal(exigenciaFase1.getNitrogenioEssencialTotal()));
		
		

		s.setPesoMedio(this.pesoMedio2);
		s.setGanhoPeso(this.gp2);
		lisinaDigestivelExigencia = suinosRN.lisinaExigenciaCategoria(this.sexo, s);
		fosforoDisponivelExigencia = suinosRN.fosforoDisponivelCategoria(this.sexo, s);
		fosforoDigestExigencia = suinosRN.fosforoDigestivelCategoria(this.sexo, s);
		this.exigenciaFase2.setLisinaDigestivelExigencia(lisinaDigestivelExigencia);
		this.exigenciaFase2.setFosforoDisponivelExigencia(fosforoDisponivelExigencia);
		this.exigenciaFase2.setFosforoDigestivelExigencia(fosforoDigestExigencia);
		s.setConsumoRacao(cr2);
		this.exigenciaFase2.setFosforoDisponivel(suinosRN.fosforoDisponivelExigencia(exigenciaFase2, s));
		this.exigenciaFase2.setFosforoDigestivel(suinosRN.fosforoDigestivelExigencia(exigenciaFase2, s));
		calcio = suinosRN.calcio(exigenciaFase2, s);
		this.exigenciaFase2.setCalcio(calcio);
		em = suinosRN.energiaMetabolizavelMedia(this.fase2a - 21,this.fase2 - 21, this.sexo);
		consumomedio = suinosRN.consumoMedio(this.fase2a - 21,this.fase2 - 21, this.sexo);
		 emDia = suinosRN.energiaMetabolizavelDiaMedia(this.fase2a - 21,this.fase2 - 21, this.sexo);
		this.exigenciaFase2.setEnergiaMetabolizavelDia(emDia);
		this.exigenciaFase2.setEnergiaMetabolizavelKg((int)Math.ceil(em));
		this.exigenciaFase2.setPotassio(suinosRN.postassioNovo(pesoMedio2,exigenciaFase2.getEnergiaMetabolizavelKg()));
		this.exigenciaFase2.setSodio(suinosRN.sodioNovo(pesoMedio2, exigenciaFase2.getEnergiaMetabolizavelKg()));
		this.exigenciaFase2.setCloro(suinosRN.cloroNovo(pesoMedio2, exigenciaFase2.getEnergiaMetabolizavelKg()));
		this.exigenciaFase2.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase2.getLisinaDigestivelExigencia(), cr2));
		this.exigenciaFase2.setMetioninaDigestivel(suinosRN.metioninaDigestivelNovo(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		this.exigenciaFase2.setMetioninaCistina(suinosRN.metioninaCistinaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		this.exigenciaFase2.setTreoninaDigestivel(suinosRN.treoninaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		this.exigenciaFase2.setTriptofano(suinosRN.triptofanoDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		this.exigenciaFase2.setArginina(suinosRN.argininaDigestiva(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		this.exigenciaFase2.setValina(suinosRN.valinaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		this.exigenciaFase2.setIsoleucina(suinosRN.isoleucinaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		this.exigenciaFase2.setLeucina(suinosRN.leucinaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		this.exigenciaFase2.setHistidina(suinosRN.histidinaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		this.exigenciaFase2.setFenilalanina(suinosRN.fenilalaninaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		this.exigenciaFase2.setFenilalaninaTirosina(suinosRN.fenilalaninaTirosinaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		this.exigenciaFase2.setNitrogenioEssencial(suinosRN.nitrogenioEssencial(exigenciaFase2,this.pesoMedio2));
		this.exigenciaFase2.setProteinaBrutaDisgestivel(suinosRN.proteinaBrutaDigestivel(exigenciaFase2.getNitrogenioEssencial()));
		this.exigenciaFase2.setLisinaTotal(suinosRN.lisinaProteinaIdealTotal(exigenciaFase2.getLisinaProteinaIdeal()));
		this.exigenciaFase2.setMetioninaTotal(suinosRN.metioninaTotalNovo(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		this.exigenciaFase2.setMetioninaCistinaTotal(suinosRN.metioninaCistinaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		this.exigenciaFase2.setTreoninaTotal(suinosRN.treoninaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		this.exigenciaFase2.setTriptofanoTotal(suinosRN.triptofanoDigestivel(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		this.exigenciaFase2.setArgininaTotal(suinosRN.argininaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		this.exigenciaFase2.setValinaTotal(suinosRN.valinaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		this.exigenciaFase2.setIsoleucinaTotal(suinosRN.isoleucinaDigestivel(exigenciaFase2.getLisinaTotal(), pesoMedio2));		
		this.exigenciaFase2.setLeucinaTotal(suinosRN.leucinaTotal(this.exigenciaFase2.getLisinaTotal(), pesoMedio2));
		this.exigenciaFase2.setHistidinaTotal(suinosRN.histidinaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		this.exigenciaFase2.setFenilalaninaTotal(suinosRN.fenilalaninaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		this.exigenciaFase2.setFenilalaninaTirosinaTotal(suinosRN.fenilalaninaTirosinaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		this.exigenciaFase2.setNitrogenioEssencialTotal(suinosRN.nitrogenioEssencialTotal(this.exigenciaFase2,pesoMedio2));
		this.exigenciaFase2.setProteinaBrutaTotal(suinosRN.proteinaBrutaTotal(exigenciaFase2.getNitrogenioEssencialTotal()));
		
		s.setPesoMedio(this.pesoMedio3);
		s.setGanhoPeso(this.gp3);
		lisinaDigestivelExigencia = suinosRN.lisinaExigenciaCategoria(this.sexo, s);
		fosforoDisponivelExigencia = suinosRN.fosforoDisponivelCategoria(this.sexo, s);
		fosforoDigestExigencia = suinosRN.fosforoDigestivelCategoria(this.sexo, s);
		this.exigenciaFase3.setLisinaDigestivelExigencia(lisinaDigestivelExigencia);
		this.exigenciaFase3.setFosforoDisponivelExigencia(fosforoDisponivelExigencia);
		this.exigenciaFase3.setFosforoDigestivelExigencia(fosforoDigestExigencia);
		s.setConsumoRacao(cr3);
		this.exigenciaFase3.setFosforoDisponivel(suinosRN.fosforoDisponivelExigencia(exigenciaFase3, s));
		this.exigenciaFase3.setFosforoDigestivel(suinosRN.fosforoDigestivelExigencia(exigenciaFase3, s));
		calcio = suinosRN.calcio(exigenciaFase3, s);
		this.exigenciaFase3.setCalcio(calcio);
		em = suinosRN.energiaMetabolizavelMedia(this.fase3a - 21,this.fase3 - 21, this.sexo);
		
		consumomedio = suinosRN.consumoMedio(this.fase3a - 21,this.fase3 - 21, this.sexo);
		 emDia = suinosRN.energiaMetabolizavelDiaMedia(this.fase3a - 21,this.fase3 - 21, this.sexo);
		this.exigenciaFase3.setEnergiaMetabolizavelDia(emDia);
		this.exigenciaFase3.setEnergiaMetabolizavelKg((int)(Math.ceil(em)));
		this.exigenciaFase3.setPotassio(suinosRN.postassioNovo(pesoMedio3,exigenciaFase3.getEnergiaMetabolizavelKg()));
		this.exigenciaFase3.setSodio(suinosRN.sodioNovo(pesoMedio3, exigenciaFase3.getEnergiaMetabolizavelKg()));
		this.exigenciaFase3.setCloro(suinosRN.cloroNovo(pesoMedio3, exigenciaFase3.getEnergiaMetabolizavelKg()));
		this.exigenciaFase3.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase3.getLisinaDigestivelExigencia(), cr3));
		this.exigenciaFase3.setMetioninaDigestivel(suinosRN.metioninaDigestivelNovo(exigenciaFase3.getLisinaProteinaIdeal(), pesoMedio3));
		this.exigenciaFase3.setMetioninaCistina(suinosRN.metioninaCistinaDigestivel(exigenciaFase3.getLisinaProteinaIdeal(), pesoMedio3));
		this.exigenciaFase3.setTreoninaDigestivel(suinosRN.treoninaDigestivel(exigenciaFase3.getLisinaProteinaIdeal(), pesoMedio3));
		this.exigenciaFase3.setTriptofano(suinosRN.triptofanoDigestivel(exigenciaFase3.getLisinaProteinaIdeal(), pesoMedio3));
		this.exigenciaFase3.setArginina(suinosRN.argininaDigestiva(exigenciaFase3.getLisinaProteinaIdeal(), pesoMedio3));
		this.exigenciaFase3.setValina(suinosRN.valinaDigestivel(exigenciaFase3.getLisinaProteinaIdeal(), pesoMedio3));
		this.exigenciaFase3.setIsoleucina(suinosRN.isoleucinaDigestivel(exigenciaFase3.getLisinaProteinaIdeal(), pesoMedio3));
		this.exigenciaFase3.setLeucina(suinosRN.leucinaDigestivel(exigenciaFase3.getLisinaProteinaIdeal(), pesoMedio3));
		this.exigenciaFase3.setHistidina(suinosRN.histidinaDigestivel(exigenciaFase3.getLisinaProteinaIdeal(), pesoMedio3));
		this.exigenciaFase3.setFenilalanina(suinosRN.fenilalaninaDigestivel(exigenciaFase3.getLisinaProteinaIdeal(), pesoMedio3));
		this.exigenciaFase3.setFenilalaninaTirosina(suinosRN.fenilalaninaTirosinaDigestivel(exigenciaFase3.getLisinaProteinaIdeal(), pesoMedio3));
		this.exigenciaFase3.setNitrogenioEssencial(suinosRN.nitrogenioEssencial(exigenciaFase3,this.pesoMedio3));
		this.exigenciaFase3.setProteinaBrutaDisgestivel(suinosRN.proteinaBrutaDigestivel(exigenciaFase3.getNitrogenioEssencial()));
		this.exigenciaFase3.setLisinaTotal(suinosRN.lisinaProteinaIdealTotal(exigenciaFase3.getLisinaProteinaIdeal()));
		this.exigenciaFase3.setMetioninaTotal(suinosRN.metioninaTotalNovo(exigenciaFase3.getLisinaTotal(), pesoMedio3));
		this.exigenciaFase3.setMetioninaCistinaTotal(suinosRN.metioninaCistinaTotal(exigenciaFase3.getLisinaTotal(), pesoMedio3));
		this.exigenciaFase3.setTreoninaTotal(suinosRN.treoninaTotal(exigenciaFase3.getLisinaTotal(), pesoMedio3));
		this.exigenciaFase3.setTriptofanoTotal(suinosRN.triptofanoDigestivel(exigenciaFase3.getLisinaTotal(), pesoMedio3));
		this.exigenciaFase3.setArgininaTotal(suinosRN.argininaTotal(exigenciaFase3.getLisinaTotal(), pesoMedio3));
		this.exigenciaFase3.setValinaTotal(suinosRN.valinaTotal(exigenciaFase3.getLisinaTotal(), pesoMedio3));
		this.exigenciaFase3.setIsoleucinaTotal(suinosRN.isoleucinaDigestivel(exigenciaFase3.getLisinaTotal(), pesoMedio3));
		this.exigenciaFase3.setLeucinaTotal(suinosRN.leucinaTotal(this.exigenciaFase3.getLisinaTotal(), pesoMedio3));
		this.exigenciaFase3.setHistidinaTotal(suinosRN.histidinaTotal(exigenciaFase3.getLisinaTotal(), pesoMedio3));
		this.exigenciaFase3.setFenilalaninaTotal(suinosRN.fenilalaninaTotal(exigenciaFase3.getLisinaTotal(), pesoMedio3));
		this.exigenciaFase3.setFenilalaninaTirosinaTotal(suinosRN.fenilalaninaTirosinaTotal(exigenciaFase3.getLisinaTotal(), pesoMedio3));
		this.exigenciaFase3.setNitrogenioEssencialTotal(suinosRN.nitrogenioEssencialTotal(this.exigenciaFase3,pesoMedio3));
		this.exigenciaFase3.setProteinaBrutaTotal(suinosRN.proteinaBrutaTotal(exigenciaFase3.getNitrogenioEssencialTotal()));
		
		s.setPesoMedio(this.pesoMedio4);
		s.setGanhoPeso(this.gp4);
		lisinaDigestivelExigencia = suinosRN.lisinaExigenciaCategoria(this.sexo, s);
		fosforoDisponivelExigencia = suinosRN.fosforoDisponivelCategoria(this.sexo, s);
		fosforoDigestExigencia = suinosRN.fosforoDigestivelCategoria(this.sexo, s);
		this.exigenciaFase4.setLisinaDigestivelExigencia(lisinaDigestivelExigencia);
		this.exigenciaFase4.setFosforoDisponivelExigencia(fosforoDisponivelExigencia);
		this.exigenciaFase4.setFosforoDigestivelExigencia(fosforoDigestExigencia);
		s.setConsumoRacao(cr4);
		this.exigenciaFase4.setFosforoDisponivel(suinosRN.fosforoDisponivelExigencia(exigenciaFase4, s));
		this.exigenciaFase4.setFosforoDigestivel(suinosRN.fosforoDigestivelExigencia(exigenciaFase4, s));
		calcio = suinosRN.calcio(exigenciaFase4, s);
		this.exigenciaFase4.setCalcio(calcio);
		em = suinosRN.energiaMetabolizavelMedia(this.fase4a - 21,this.fase4 - 21, this.sexo);
		consumomedio = suinosRN.consumoMedio(this.fase4a - 21,this.fase4 - 21, this.sexo);
		 emDia = suinosRN.energiaMetabolizavelDiaMedia(this.fase4a - 21,this.fase4 - 21, this.sexo);
		this.exigenciaFase4.setEnergiaMetabolizavelDia(emDia);
		this.exigenciaFase4.setEnergiaMetabolizavelKg((int)(Math.ceil(em)));
		this.exigenciaFase4.setPotassio(suinosRN.postassioNovo(pesoMedio4,exigenciaFase4.getEnergiaMetabolizavelKg()));
		this.exigenciaFase4.setSodio(suinosRN.sodioNovo(pesoMedio4, exigenciaFase4.getEnergiaMetabolizavelKg()));
		this.exigenciaFase4.setCloro(suinosRN.cloroNovo(pesoMedio4, exigenciaFase4.getEnergiaMetabolizavelKg()));
		this.exigenciaFase4.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase4.getLisinaDigestivelExigencia(), cr4));
		this.exigenciaFase4.setMetioninaDigestivel(suinosRN.metioninaDigestivelNovo(exigenciaFase4.getLisinaProteinaIdeal(), pesoMedio4));
		this.exigenciaFase4.setMetioninaCistina(suinosRN.metioninaCistinaDigestivel(exigenciaFase4.getLisinaProteinaIdeal(), pesoMedio4));
		this.exigenciaFase4.setTreoninaDigestivel(suinosRN.treoninaDigestivel(exigenciaFase4.getLisinaProteinaIdeal(), pesoMedio4));
		this.exigenciaFase4.setTriptofano(suinosRN.triptofanoDigestivel(exigenciaFase4.getLisinaProteinaIdeal(), pesoMedio4));
		this.exigenciaFase4.setArginina(suinosRN.argininaDigestiva(exigenciaFase4.getLisinaProteinaIdeal(), pesoMedio4));
		this.exigenciaFase4.setValina(suinosRN.valinaDigestivel(exigenciaFase4.getLisinaProteinaIdeal(), pesoMedio4));
		this.exigenciaFase4.setIsoleucina(suinosRN.isoleucinaDigestivel(exigenciaFase4.getLisinaProteinaIdeal(), pesoMedio4));
		this.exigenciaFase4.setLeucina(suinosRN.leucinaDigestivel(exigenciaFase4.getLisinaProteinaIdeal(), pesoMedio4));
		this.exigenciaFase4.setHistidina(suinosRN.histidinaDigestivel(exigenciaFase4.getLisinaProteinaIdeal(), pesoMedio4));
		this.exigenciaFase4.setFenilalanina(suinosRN.fenilalaninaDigestivel(exigenciaFase4.getLisinaProteinaIdeal(), pesoMedio4));
		this.exigenciaFase4.setFenilalaninaTirosina(suinosRN.fenilalaninaTirosinaDigestivel(exigenciaFase4.getLisinaProteinaIdeal(), pesoMedio4));
		this.exigenciaFase4.setNitrogenioEssencial(suinosRN.nitrogenioEssencial(exigenciaFase4,this.pesoMedio4));
		this.exigenciaFase4.setProteinaBrutaDisgestivel(suinosRN.proteinaBrutaDigestivel(exigenciaFase4.getNitrogenioEssencial()));
		this.exigenciaFase4.setLisinaTotal(suinosRN.lisinaProteinaIdealTotal(exigenciaFase4.getLisinaProteinaIdeal()));
		this.exigenciaFase4.setMetioninaTotal(suinosRN.metioninaTotalNovo(exigenciaFase4.getLisinaTotal(), pesoMedio4));
		this.exigenciaFase4.setMetioninaCistinaTotal(suinosRN.metioninaCistinaTotal(exigenciaFase4.getLisinaTotal(), pesoMedio4));
		this.exigenciaFase4.setTreoninaTotal(suinosRN.treoninaTotal(exigenciaFase4.getLisinaTotal(), pesoMedio4));
		this.exigenciaFase4.setTriptofanoTotal(suinosRN.triptofanoDigestivel(exigenciaFase4.getLisinaTotal(), pesoMedio4));
		this.exigenciaFase4.setArgininaTotal(suinosRN.argininaTotal(exigenciaFase4.getLisinaTotal(), pesoMedio4));
		this.exigenciaFase4.setValinaTotal(suinosRN.valinaTotal(exigenciaFase4.getLisinaTotal(), pesoMedio4));
		this.exigenciaFase4.setIsoleucinaTotal(suinosRN.isoleucinaDigestivel(exigenciaFase4.getLisinaTotal(), pesoMedio4));
		this.exigenciaFase4.setLeucinaTotal(suinosRN.leucinaTotal(this.exigenciaFase4.getLisinaTotal(), pesoMedio4));
		this.exigenciaFase4.setHistidinaTotal(suinosRN.histidinaTotal(exigenciaFase4.getLisinaTotal(), pesoMedio4));
		this.exigenciaFase4.setFenilalaninaTotal(suinosRN.fenilalaninaTotal(exigenciaFase4.getLisinaTotal(), pesoMedio4));
		this.exigenciaFase4.setFenilalaninaTirosinaTotal(suinosRN.fenilalaninaTirosinaTotal(exigenciaFase4.getLisinaTotal(), pesoMedio4));
		this.exigenciaFase4.setNitrogenioEssencialTotal(suinosRN.nitrogenioEssencialTotal(this.exigenciaFase4,pesoMedio4));
		this.exigenciaFase4.setProteinaBrutaTotal(suinosRN.proteinaBrutaTotal(exigenciaFase4.getNitrogenioEssencialTotal()));
		
		s.setPesoMedio(this.pesoMedio5);
		s.setGanhoPeso(this.gp5);
		lisinaDigestivelExigencia = suinosRN.lisinaExigenciaCategoria(this.sexo, s);	
		fosforoDisponivelExigencia = suinosRN.fosforoDisponivelCategoria(this.sexo, s);
		fosforoDigestExigencia = suinosRN.fosforoDigestivelCategoria(this.sexo, s);
		this.exigenciaFase5.setLisinaDigestivelExigencia(lisinaDigestivelExigencia);
		this.exigenciaFase5.setFosforoDisponivelExigencia(fosforoDisponivelExigencia);
		this.exigenciaFase5.setFosforoDigestivelExigencia(fosforoDigestExigencia);
		s.setConsumoRacao(cr5);
		this.exigenciaFase5.setFosforoDisponivel(suinosRN.fosforoDisponivelExigencia(exigenciaFase5, s));
		this.exigenciaFase5.setFosforoDigestivel(suinosRN.fosforoDigestivelExigencia(exigenciaFase5, s));
		calcio = suinosRN.calcio(exigenciaFase5, s);
		this.exigenciaFase5.setCalcio(calcio);
		em = suinosRN.energiaMetabolizavelMedia(this.fase5a - 21,this.fase5 - 21, this.sexo);
		consumomedio = suinosRN.consumoMedio(this.fase5a - 21,this.fase5 - 21, this.sexo);
		 emDia = suinosRN.energiaMetabolizavelDiaMedia(this.fase5a - 21,this.fase5 - 21, this.sexo);
		this.exigenciaFase5.setEnergiaMetabolizavelDia(emDia);
		this.exigenciaFase5.setEnergiaMetabolizavelKg((int)(Math.ceil(em)));
		this.exigenciaFase5.setPotassio(suinosRN.postassioNovo(pesoMedio5,exigenciaFase5.getEnergiaMetabolizavelKg()));
		this.exigenciaFase5.setSodio(suinosRN.sodioNovo(pesoMedio5, exigenciaFase5.getEnergiaMetabolizavelKg()));
		this.exigenciaFase5.setCloro(suinosRN.cloroNovo(pesoMedio5, exigenciaFase5.getEnergiaMetabolizavelKg()));
		this.exigenciaFase5.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase5.getLisinaDigestivelExigencia(), cr5));
		this.exigenciaFase5.setMetioninaDigestivel(suinosRN.metioninaDigestivelNovo(exigenciaFase5.getLisinaProteinaIdeal(), pesoMedio5));
		this.exigenciaFase5.setMetioninaCistina(suinosRN.metioninaCistinaDigestivel(exigenciaFase5.getLisinaProteinaIdeal(), pesoMedio5));
		this.exigenciaFase5.setTreoninaDigestivel(suinosRN.treoninaDigestivel(exigenciaFase5.getLisinaProteinaIdeal(), pesoMedio5));
		this.exigenciaFase5.setTriptofano(suinosRN.triptofanoDigestivel(exigenciaFase5.getLisinaProteinaIdeal(), pesoMedio5));
		this.exigenciaFase5.setArginina(suinosRN.argininaDigestiva(exigenciaFase5.getLisinaProteinaIdeal(), pesoMedio5));
		this.exigenciaFase5.setValina(suinosRN.valinaDigestivel(exigenciaFase5.getLisinaProteinaIdeal(), pesoMedio5));
		this.exigenciaFase5.setIsoleucina(suinosRN.isoleucinaDigestivel(exigenciaFase5.getLisinaProteinaIdeal(), pesoMedio5));
		this.exigenciaFase5.setLeucina(suinosRN.leucinaDigestivel(exigenciaFase5.getLisinaProteinaIdeal(), pesoMedio5));
		this.exigenciaFase5.setHistidina(suinosRN.histidinaDigestivel(exigenciaFase5.getLisinaProteinaIdeal(), pesoMedio5));
		this.exigenciaFase5.setFenilalanina(suinosRN.fenilalaninaDigestivel(exigenciaFase5.getLisinaProteinaIdeal(), pesoMedio5));
		this.exigenciaFase5.setFenilalaninaTirosina(suinosRN.fenilalaninaTirosinaDigestivel(exigenciaFase5.getLisinaProteinaIdeal(), pesoMedio5));
		this.exigenciaFase5.setNitrogenioEssencial(suinosRN.nitrogenioEssencial(exigenciaFase5,this.pesoMedio5));
		this.exigenciaFase5.setProteinaBrutaDisgestivel(suinosRN.proteinaBrutaDigestivel(exigenciaFase5.getNitrogenioEssencial()));
		this.exigenciaFase5.setLisinaTotal(suinosRN.lisinaProteinaIdealTotal(exigenciaFase5.getLisinaProteinaIdeal()));
		this.exigenciaFase5.setMetioninaTotal(suinosRN.metioninaTotalNovo(exigenciaFase5.getLisinaTotal(), pesoMedio5));
		this.exigenciaFase5.setMetioninaCistinaTotal(suinosRN.metioninaCistinaTotal(exigenciaFase5.getLisinaTotal(), pesoMedio5));
		this.exigenciaFase5.setTreoninaTotal(suinosRN.treoninaTotal(exigenciaFase5.getLisinaTotal(), pesoMedio5));
		this.exigenciaFase5.setTriptofanoTotal(suinosRN.triptofanoDigestivel(exigenciaFase5.getLisinaTotal(), pesoMedio5));
		this.exigenciaFase5.setArgininaTotal(suinosRN.argininaTotal(exigenciaFase5.getLisinaTotal(), pesoMedio5));
		this.exigenciaFase5.setValinaTotal(suinosRN.valinaTotal(exigenciaFase5.getLisinaTotal(), pesoMedio5));
		this.exigenciaFase5.setIsoleucinaTotal(suinosRN.isoleucinaDigestivel(exigenciaFase5.getLisinaTotal(), pesoMedio5));
		this.exigenciaFase5.setLeucinaTotal(suinosRN.leucinaTotal(this.exigenciaFase5.getLisinaTotal(), pesoMedio5));
		this.exigenciaFase5.setHistidinaTotal(suinosRN.histidinaTotal(exigenciaFase5.getLisinaTotal(), pesoMedio5));
		this.exigenciaFase5.setFenilalaninaTotal(suinosRN.fenilalaninaTotal(exigenciaFase5.getLisinaTotal(), pesoMedio5));
		this.exigenciaFase5.setFenilalaninaTirosinaTotal(suinosRN.fenilalaninaTirosinaTotal(exigenciaFase5.getLisinaTotal(), pesoMedio5));
		this.exigenciaFase5.setNitrogenioEssencialTotal(suinosRN.nitrogenioEssencialTotal(this.exigenciaFase5,pesoMedio5));
		this.exigenciaFase5.setProteinaBrutaTotal(suinosRN.proteinaBrutaTotal(exigenciaFase5.getNitrogenioEssencialTotal()));
		
		s.setPesoMedio(this.pesoMedio6);
		s.setGanhoPeso(this.gp6);
		lisinaDigestivelExigencia = suinosRN.lisinaExigenciaCategoria(this.sexo, s);
		fosforoDisponivelExigencia = suinosRN.fosforoDisponivelCategoria(this.sexo, s);
		fosforoDigestExigencia = suinosRN.fosforoDigestivelCategoria(this.sexo, s);
		this.exigenciaFase6.setLisinaDigestivelExigencia(lisinaDigestivelExigencia);
		this.exigenciaFase6.setFosforoDisponivelExigencia(fosforoDisponivelExigencia);
		this.exigenciaFase6.setFosforoDigestivelExigencia(fosforoDigestExigencia);
		s.setConsumoRacao(cr6);
		this.exigenciaFase6.setFosforoDisponivel(suinosRN.fosforoDisponivelExigencia(exigenciaFase6, s));
		this.exigenciaFase6.setFosforoDigestivel(suinosRN.fosforoDigestivelExigencia(exigenciaFase6, s));
		 calcio = suinosRN.calcio(exigenciaFase6, s);
		this.exigenciaFase6.setCalcio(calcio);
		em = suinosRN.energiaMetabolizavelMedia(this.fase6a - 21,this.idadeAbate - 21, this.sexo);
		consumomedio = suinosRN.consumoMedio(this.fase6a - 21,this.idadeAbate - 21, this.sexo);
		 emDia = suinosRN.energiaMetabolizavelDiaMedia(this.fase6a - 21,this.idadeAbate - 21, this.sexo);
		this.exigenciaFase6.setEnergiaMetabolizavelDia(emDia);
		this.exigenciaFase6.setEnergiaMetabolizavelKg((int)(Math.ceil(em)));
		this.exigenciaFase6.setPotassio(suinosRN.postassioNovo(pesoMedio6,exigenciaFase6.getEnergiaMetabolizavelKg()));
		this.exigenciaFase6.setSodio(suinosRN.sodioNovo(pesoMedio6, exigenciaFase6.getEnergiaMetabolizavelKg()));
		this.exigenciaFase6.setCloro(suinosRN.cloroNovo(pesoMedio6, exigenciaFase6.getEnergiaMetabolizavelKg()));
		this.exigenciaFase6.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase6.getLisinaDigestivelExigencia(), cr6));
		this.exigenciaFase6.setMetioninaDigestivel(suinosRN.metioninaDigestivelNovo(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setMetioninaCistina(suinosRN.metioninaCistinaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setTreoninaDigestivel(suinosRN.treoninaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setTriptofano(suinosRN.triptofanoDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setArginina(suinosRN.argininaDigestiva(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setValina(suinosRN.valinaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setIsoleucina(suinosRN.isoleucinaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setLeucina(suinosRN.leucinaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setHistidina(suinosRN.histidinaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setFenilalanina(suinosRN.fenilalaninaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setFenilalaninaTirosina(suinosRN.fenilalaninaTirosinaDigestivel(exigenciaFase6.getLisinaProteinaIdeal(), pesoMedio6));
		this.exigenciaFase6.setNitrogenioEssencial(suinosRN.nitrogenioEssencial(exigenciaFase6,this.pesoMedio6));
		this.exigenciaFase6.setProteinaBrutaDisgestivel(suinosRN.proteinaBrutaDigestivel(exigenciaFase6.getNitrogenioEssencial()));
		this.exigenciaFase6.setLisinaTotal(suinosRN.lisinaProteinaIdealTotal(exigenciaFase6.getLisinaProteinaIdeal()));
		this.exigenciaFase6.setMetioninaTotal(suinosRN.metioninaTotalNovo(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setMetioninaCistinaTotal(suinosRN.metioninaCistinaTotal(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setTreoninaTotal(suinosRN.treoninaTotal(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setTriptofanoTotal(suinosRN.triptofanoDigestivel(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setArgininaTotal(suinosRN.argininaTotal(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setValinaTotal(suinosRN.valinaTotal(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setIsoleucinaTotal(suinosRN.isoleucinaDigestivel(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setLeucinaTotal(suinosRN.leucinaTotal(this.exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setHistidinaTotal(suinosRN.histidinaTotal(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setFenilalaninaTotal(suinosRN.fenilalaninaTotal(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setFenilalaninaTirosinaTotal(suinosRN.fenilalaninaTirosinaTotal(exigenciaFase6.getLisinaTotal(), pesoMedio6));
		this.exigenciaFase6.setNitrogenioEssencialTotal(suinosRN.nitrogenioEssencialTotal(this.exigenciaFase6,pesoMedio6));
		this.exigenciaFase6.setProteinaBrutaTotal(suinosRN.proteinaBrutaTotal(exigenciaFase6.getNitrogenioEssencialTotal()));
		
		if(ativaTemperatura){
			
			s.setTemperatura(this.temperatura);
			s.setPesoMedio(pesoMedio2);
			double correcao = suinosRN.correcaoTemperatura(s, suinosRN.temperaturaMedia(pesoMedio2));
			System.out.println("Correcao "+correcao);
			this.exigenciaFase2.setEnergiaMetabolizavelDia( exigenciaFase2.getEnergiaMetabolizavelDia() + correcao  );
			if(exigenciaFase2.getEnergiaMetDet() != 0){
				this.cr2 = exigenciaFase2.getEnergiaMetabolizavelDia()/exigenciaFase2.getEnergiaMetDet();
			}else this.cr2 = exigenciaFase2.getEnergiaMetabolizavelDia()/exigenciaFase2.getEnergiaMetabolizavelKg();
			this.dependeTemperatura(this.exigenciaFase2,pesoMedio2, this.cr2);
			
			s.setTemperatura(this.temperatura);
			s.setPesoMedio(pesoMedio3);
			correcao = suinosRN.correcaoTemperatura(s, suinosRN.temperaturaMedia(pesoMedio3));
			System.out.println("Correcao "+correcao);
			this.exigenciaFase3.setEnergiaMetabolizavelDia( exigenciaFase3.getEnergiaMetabolizavelDia() + correcao  );
			this.cr3 = exigenciaFase3.getEnergiaMetabolizavelDia()/exigenciaFase3.getEnergiaMetabolizavelKg();
			this.dependeTemperatura(this.exigenciaFase3,pesoMedio3, this.cr3);
			
			s.setTemperatura(this.temperatura);
			s.setPesoMedio(pesoMedio4);
			 correcao = suinosRN.correcaoTemperatura(s, suinosRN.temperaturaMedia(pesoMedio4));
			System.out.println("Correcao "+correcao);
			this.exigenciaFase4.setEnergiaMetabolizavelDia( exigenciaFase4.getEnergiaMetabolizavelDia() + correcao  );
			this.cr4 = exigenciaFase4.getEnergiaMetabolizavelDia()/exigenciaFase4.getEnergiaMetabolizavelKg();
			this.dependeTemperatura(this.exigenciaFase4,pesoMedio4, this.cr4);
			
			s.setTemperatura(this.temperatura);
			s.setPesoMedio(pesoMedio5);
			 correcao = suinosRN.correcaoTemperatura(s, suinosRN.temperaturaMedia(pesoMedio5));
			System.out.println("Correcao "+correcao);
			this.exigenciaFase5.setEnergiaMetabolizavelDia( exigenciaFase5.getEnergiaMetabolizavelDia() + correcao  );
			this.cr5 = exigenciaFase5.getEnergiaMetabolizavelDia()/exigenciaFase5.getEnergiaMetabolizavelKg();
			this.dependeTemperatura(this.exigenciaFase5,pesoMedio5, this.cr5);
			
			s.setTemperatura(this.temperatura);
			s.setPesoMedio(pesoMedio6);
			 correcao = suinosRN.correcaoTemperatura(s, suinosRN.temperaturaMedia(pesoMedio6));
			System.out.println("Correcao "+correcao);
			this.exigenciaFase6.setEnergiaMetabolizavelDia( exigenciaFase6.getEnergiaMetabolizavelDia() + correcao  );
			this.cr6 = exigenciaFase6.getEnergiaMetabolizavelDia()/exigenciaFase6.getEnergiaMetabolizavelKg();
			this.dependeTemperatura(this.exigenciaFase6,pesoMedio6, this.cr6);
			
		
		}
	}
	
	public void dependeTemperatura(Exigencia exigenciaFase2 , double pesoMedio2 , double consumo){
		SuinosRN suinosRN = new SuinosRN();
		exigenciaFase2.setLisinaProteinaIdeal(suinosRN.lisinaDigestivelExigencia(exigenciaFase2.getLisinaDigestivelExigencia(), consumo));
		exigenciaFase2.setMetioninaDigestivel(suinosRN.metioninaDigestivelNovo(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		exigenciaFase2.setMetioninaCistina(suinosRN.metioninaCistinaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		exigenciaFase2.setTreoninaDigestivel(suinosRN.treoninaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		exigenciaFase2.setTriptofano(suinosRN.triptofanoDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		exigenciaFase2.setArginina(suinosRN.argininaDigestiva(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		exigenciaFase2.setValina(suinosRN.valinaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		exigenciaFase2.setIsoleucina(suinosRN.isoleucinaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		exigenciaFase2.setLeucina(suinosRN.leucinaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		exigenciaFase2.setHistidina(suinosRN.histidinaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		exigenciaFase2.setFenilalanina(suinosRN.fenilalaninaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		exigenciaFase2.setFenilalaninaTirosina(suinosRN.fenilalaninaTirosinaDigestivel(exigenciaFase2.getLisinaProteinaIdeal(), pesoMedio2));
		exigenciaFase2.setNitrogenioEssencial(suinosRN.nitrogenioEssencial(exigenciaFase2,pesoMedio2));
		exigenciaFase2.setProteinaBrutaDisgestivel(suinosRN.proteinaBrutaDigestivel(exigenciaFase2.getNitrogenioEssencial()));
		exigenciaFase2.setLisinaTotal(suinosRN.lisinaProteinaIdealTotal(exigenciaFase2.getLisinaProteinaIdeal()));
		exigenciaFase2.setMetioninaTotal(suinosRN.metioninaTotalNovo(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		exigenciaFase2.setMetioninaCistinaTotal(suinosRN.metioninaCistinaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		exigenciaFase2.setTreoninaTotal(suinosRN.treoninaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		exigenciaFase2.setTriptofanoTotal(suinosRN.triptofanoDigestivel(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		exigenciaFase2.setArgininaTotal(suinosRN.argininaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		exigenciaFase2.setValinaTotal(suinosRN.valinaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		exigenciaFase2.setIsoleucinaTotal(suinosRN.isoleucinaDigestivel(exigenciaFase2.getLisinaTotal(), pesoMedio2));		
		exigenciaFase2.setLeucinaTotal(suinosRN.leucinaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		exigenciaFase2.setHistidinaTotal(suinosRN.histidinaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		exigenciaFase2.setFenilalaninaTotal(suinosRN.fenilalaninaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		exigenciaFase2.setFenilalaninaTirosinaTotal(suinosRN.fenilalaninaTirosinaTotal(exigenciaFase2.getLisinaTotal(), pesoMedio2));
		exigenciaFase2.setNitrogenioEssencialTotal(suinosRN.nitrogenioEssencialTotal(exigenciaFase2,pesoMedio2));
		exigenciaFase2.setProteinaBrutaTotal(suinosRN.proteinaBrutaTotal(exigenciaFase2.getNitrogenioEssencialTotal()));
		
		
	}
	
	public void calcularFasesSimulado(){
		SuinosRN suinosRN = new SuinosRN();
//		double matriz[][] = suinosRN.curvaSimulada(idadeInicial - 21,idadeAbate-21,this.sexo,pesoAbate,pesoAlojamento,consumoRacao);
		
		this.pesoMedio1Simulado = suinosRN.pesoMediaSimulado(matriz,this.sexo, this.idadeInicial - 21, fase1 - 21);
		this.pesoMedio2Simulado = suinosRN.pesoMediaSimulado(matriz,this.sexo, this.fase2a - 21, fase2 - 21);
		this.pesoMedio3Simulado = suinosRN.pesoMediaSimulado(matriz,this.sexo, this.fase3a - 21, fase3 - 21);
		this.pesoMedio4Simulado = suinosRN.pesoMediaSimulado(matriz,this.sexo,this.fase4a - 21, fase4 - 21);
		this.pesoMedio5Simulado = suinosRN.pesoMediaSimulado(matriz,this.sexo,fase5a - 21, fase5 - 21);
		this.pesoMedio6Simulado = suinosRN.pesoMediaSimulado(matriz,this.sexo, this.fase6a - 21, this.idadeAbate - 21);
		
		this.ganhoPeso1Simulado = suinosRN.ganhoDePesoSimulado(matriz,this.sexo, this.idadeInicial - 21, fase1 - 21);
		this.ganhoPeso2Simulado = suinosRN.ganhoDePesoFasesSimulado(matriz,this.sexo,  this.fase2a - 21, fase2 - 21);
		this.ganhoPeso3Simulado = suinosRN.ganhoDePesoFasesSimulado(matriz,this.sexo, this.fase3a - 21, fase3 - 21);
		this.ganhoPeso4Simulado = suinosRN.ganhoDePesoFasesSimulado(matriz,this.sexo, this.fase4a - 21, fase4 - 21);
		this.ganhoPeso5Simulado = suinosRN.ganhoDePesoFasesSimulado(matriz,this.sexo,  this.fase5a - 21, fase5 - 21);
		this.ganhoPeso6Simulado = suinosRN.ganhoDePesoFasesSimulado(matriz,this.sexo, this.fase6a - 21, this.idadeAbate - 21);
		
		this.consumo1Simulado = suinosRN.consumoMediaSimulado(matriz,this.sexo, this.idadeInicial - 21, this.fase1 - 21);
		this.consumo2Simulado = suinosRN.consumoMediaFasesSimulado(matriz,this.sexo, this.fase2a - 21, fase2 - 21);
		this.consumo3Simulado = suinosRN.consumoMediaFasesSimulado(matriz,this.sexo, this.fase3a - 21, fase3 - 21);
		this.consumo4Simulado= suinosRN.consumoMediaFasesSimulado(matriz,this.sexo, this.fase4a - 21, fase4 - 21);
		this.consumo5Simulado = suinosRN.consumoMediaFasesSimulado(matriz,this.sexo, this.fase5a - 21, fase5 - 21);
		this.consumo6Simulado = suinosRN.consumoMediaFasesSimulado(matriz,this.sexo, this.fase6a - 21, this.idadeAbate - 21);
		

		this.conversaoAlimentar1Simulado = this.consumo1Simulado/this.ganhoPeso1Simulado;
		this.conversaoAlimentar2Simulado = this.consumo2Simulado/this.ganhoPeso2Simulado;
		this.conversaoAlimentar3Simulado = this.consumo3Simulado/this.ganhoPeso3Simulado;
		this.conversaoAlimentar4Simulado = this.consumo4Simulado/this.ganhoPeso4Simulado;
		this.conversaoAlimentar5Simulado = this.consumo5Simulado/this.ganhoPeso5Simulado;
		this.conversaoAlimentar6Simulado = this.consumo6Simulado/this.ganhoPeso6Simulado;
	
		this.cr1Simulado = this.consumo1Simulado/( this.fase1 - this.idadeInicial + 1);
		this.cr2Simulado = this.consumo2Simulado/( this.fase2 - this.fase2a + 1);
		this.cr3Simulado = this.consumo3Simulado/( this.fase3 - this.fase3a + 1);
		this.cr4Simulado = this.consumo4Simulado/( this.fase4 - this.fase4a + 1);
		this.cr5Simulado = this.consumo5Simulado/( this.fase5 - this.fase5a + 1);
		this.cr6Simulado = this.consumo6Simulado/( this.idadeAbate - this.fase6a + 1);
		
		this.gp1Simulado = this.ganhoPeso1Simulado/( this.fase1 - this.idadeInicial + 1);
		this.gp2Simulado = this.ganhoPeso2Simulado/( this.fase2 - this.fase2a + 1);
		this.gp3Simulado = this.ganhoPeso3Simulado/( this.fase3 - this.fase3a + 1);
		this.gp4Simulado = this.ganhoPeso4Simulado/( this.fase4 - this.fase4a + 1);
		this.gp5Simulado = this.ganhoPeso5Simulado/( this.fase5 - this.fase5a + 1);
		this.gp6Simulado = this.ganhoPeso6Simulado/( this.idadeAbate - this.fase6a + 1);
		
		
	}
	/**
	 * Realiza o calculo baseado nas fases
	 */
	public void calcularFases(){
		SuinosRN suinosRN = new SuinosRN();
		this.pesoAbate = suinosRN.pesoAbate(this.idadeAbate, this.idadeInicial, this.ganhoPeso, this.pesoAlojamento);
		this.ganhoAbate =suinosRN.ganhoAoAbate(this.pesoAbate, this.pesoAlojamento);
		this.consumoRacao = suinosRN.consumoRacao(this.conversaoAlimentar, this.ganhoAbate);
		
		this.pesoMedio1 = suinosRN.pesoMediaTabelado(this.sexo, this.idadeInicial - 21, fase1 - 21);
		this.pesoMedio2 = suinosRN.pesoMediaTabelado(this.sexo, this.fase2a - 21, fase2 - 21);
		this.pesoMedio3 = suinosRN.pesoMediaTabelado(this.sexo, this.fase3a - 21, fase3 - 21);
		this.pesoMedio4 = suinosRN.pesoMediaTabelado(this.sexo, this.fase4a - 21, fase4 - 21);
		this.pesoMedio5 = suinosRN.pesoMediaTabelado(this.sexo, this.fase5a - 21, fase5 - 21);
		this.pesoMedio6 = suinosRN.pesoMediaTabelado(this.sexo, this.fase6a - 21, this.idadeAbate - 21);
		
		this.ganhoPeso1 = suinosRN.ganhoDePeso(this.sexo, this.idadeInicial - 21, fase1 - 21);
		this.ganhoPeso2 = suinosRN.ganhoDePesoFases(this.sexo, this.fase2a - 21, fase2 - 21);
		this.ganhoPeso3 = suinosRN.ganhoDePesoFases(this.sexo, this.fase3a - 21, fase3 - 21);
		this.ganhoPeso4= suinosRN.ganhoDePesoFases(this.sexo, this.fase4a - 21, fase4 - 21);
		this.ganhoPeso5 = suinosRN.ganhoDePesoFases(this.sexo, this.fase5a - 21, fase5 - 21);
		this.ganhoPeso6 = suinosRN.ganhoDePesoFases(this.sexo, this.fase6a - 21, this.idadeAbate - 21);
		
		
		this.consumo1 = suinosRN.consumoMedia(this.sexo, this.idadeInicial - 21, this.fase1 - 21);
		this.consumo2 = suinosRN.consumoMediaFases(this.sexo, this.fase2a - 21, fase2 - 21);
		this.consumo3 = suinosRN.consumoMediaFases(this.sexo, this.fase3a - 21, fase3 - 21);
		this.consumo4= suinosRN.consumoMediaFases(this.sexo, this.fase4a - 21, fase4 - 21);
		this.consumo5 = suinosRN.consumoMediaFases(this.sexo, this.fase5a - 21, fase5 - 21);
		this.consumo6 = suinosRN.consumoMediaFases(this.sexo, this.fase6a - 21, this.idadeAbate - 21);
		
		this.conversaoAlimentar1 = this.consumo1/this.ganhoPeso1;
		this.conversaoAlimentar2 = this.consumo2/this.ganhoPeso2;
		this.conversaoAlimentar3 = this.consumo3/this.ganhoPeso3;
		this.conversaoAlimentar4 = this.consumo4/this.ganhoPeso4;
		this.conversaoAlimentar5 = this.consumo5/this.ganhoPeso5;
		this.conversaoAlimentar6 = this.consumo6/this.ganhoPeso6;
	
		this.cr1 = this.consumo1/( this.fase1 - this.idadeInicial + 1);
		if(exigenciaFase2.getEnergiaMetDet() != 0)
			this.cr2 = exigenciaFase2.getEnergiaMetabolizavelDia()/exigenciaFase2.getEnergiaMetDet();
		else this.cr2 = this.consumo2/( this.fase2 - this.fase2a + 1);
		this.cr3 = this.consumo3/( this.fase3 - this.fase3a + 1);
		this.cr4 = this.consumo4/( this.fase4 - this.fase4a + 1);
		this.cr5 = this.consumo5/( this.fase5 - this.fase5a + 1);
		this.cr6 = this.consumo6/( this.idadeAbate - this.fase6a + 1);
		
		this.gp1 = this.ganhoPeso1/( this.fase1 - this.idadeInicial + 1);
		this.gp2 = this.ganhoPeso2/( this.fase2 - this.fase2a + 1);
		this.gp3 = this.ganhoPeso3/( this.fase3 - this.fase3a + 1);
		this.gp4 = this.ganhoPeso4/( this.fase4 - this.fase4a + 1);
		this.gp5 = this.ganhoPeso5/( this.fase5 - this.fase5a + 1);
		this.gp6 = this.ganhoPeso6/( this.idadeAbate - this.fase6a + 1);
		this.matriz= suinosRN.curvaSimulada(idadeInicial - 21,idadeAbate-21,this.sexo,pesoAbate,pesoAlojamento,consumoRacao);
		this.calcularFasesSimulado();
	}
	/**
	 * Calcula a nova exigência a partir da energia digitada
	 */
	public void energiaDeterminada(){
		SuinosRN suinosRN = new SuinosRN();
		this.exigenciaFase1Simulada.setEnergiaMetDet(exigenciaFase1.getEnergiaMetDet());
		if(this.exigenciaFase1.getEnergiaMetDet() != 0){
//			System.out.println("kcal/dia "+exigenciaFase1.getEnergiaMetabolizavelDia());
//			System.out.println("nova energia "+exigenciaFase1.getEnergiaMetDet());
			this.cr1 = exigenciaFase1.getEnergiaMetabolizavelDia() / exigenciaFase1.getEnergiaMetDet();
			this.consumo1 = this.cr1 * (this.fase1 - this.idadeInicial +1);
			this.conversaoAlimentar1 = consumo1/ganhoPeso1;
			this.cr1Simulado = exigenciaFase1Simulada.getEnergiaMetabolizavelDia() / exigenciaFase1Simulada.getEnergiaMetDet();
			this.consumo1Simulado = this.cr1Simulado * (this.fase1 - this.idadeInicial +1);
			this.conversaoAlimentar1Simulado = consumo1Simulado/ganhoPeso1Simulado;
			this.dependeTemperatura(exigenciaFase1, pesoMedio1, cr1);
//			this.calcularExigencia();
		}else{
//			this.consumo1 = suinosRN.consumoMedia(this.sexo, this.idadeInicial - 21, this.fase1 - 21);
//			this.cr1 = this.consumo1/( this.fase1 - this.idadeInicial + 1);
//			this.conversaoAlimentar1 = this.consumo1/this.ganhoPeso1;
//			double em = suinosRN.energiaMetabolizavelMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			double consumomedio = suinosRN.consumoMedio(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			double emDia = suinosRN.energiaMetabolizavelDiaMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			this.exigenciaFase1.setEnergiaMetabolizavelDia(emDia);
//			this.exigenciaFase1.setEnergiaMetabolizavelKg((int)(em));
		}
		this.calcularAmostral();
		//		System.out.println("Consumo estimado "+suinosRN.consumoPeriodoCalc(21-21, 42-21, 2,3400)/22);
		}
	
	public void energiaDeterminadaSimulada(){
		SuinosRN suinosRN = new SuinosRN();
		this.exigenciaFase1.setEnergiaMetDet(exigenciaFase1Simulada.getEnergiaMetDet());
		if(this.exigenciaFase1Simulada.getEnergiaMetDet() != 0){
//			System.out.println("kcal/dia "+exigenciaFase1.getEnergiaMetabolizavelDia());
//			System.out.println("nova energia "+exigenciaFase1.getEnergiaMetDet());
			this.cr1 = exigenciaFase1.getEnergiaMetabolizavelDia() / exigenciaFase1.getEnergiaMetDet();
			this.consumo1 = this.cr1 * (this.fase1 - this.idadeInicial +1);
			this.conversaoAlimentar1 = consumo1/ganhoPeso1;			
			this.cr1Simulado = exigenciaFase1Simulada.getEnergiaMetabolizavelDia() / exigenciaFase1Simulada.getEnergiaMetDet();
			this.consumo1Simulado = this.cr1Simulado * (this.fase1 - this.idadeInicial +1);
			this.conversaoAlimentar1Simulado = consumo1Simulado/ganhoPeso1Simulado;
		}else{
//			this.consumo1 = suinosRN.consumoMedia(this.sexo, this.idadeInicial - 21, this.fase1 - 21);
//			this.cr1 = this.consumo1/( this.fase1 - this.idadeInicial + 1);
//			this.conversaoAlimentar1 = this.consumo1/this.ganhoPeso1;
//			double em = suinosRN.energiaMetabolizavelMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			double consumomedio = suinosRN.consumoMedio(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			double emDia = suinosRN.energiaMetabolizavelDiaMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			this.exigenciaFase1.setEnergiaMetabolizavelDia(emDia);
//			this.exigenciaFase1.setEnergiaMetabolizavelKg((int)(em));
		}
		this.calcularAmostral();
		//		System.out.println("Consumo estimado "+suinosRN.consumoPeriodoCalc(21-21, 42-21, 2,3400)/22);
		}
	/**
	 * Calcula a nova exigência a partir da energia digitada
	 */
	public void energiaDeterminada2(){
		SuinosRN suinosRN = new SuinosRN();
		this.exigenciaFase2Simulada.setEnergiaMetDet(exigenciaFase2.getEnergiaMetDet());
		if(this.exigenciaFase2.getEnergiaMetDet() != 0){
			System.out.println("kcal/dia "+exigenciaFase2.getEnergiaMetabolizavelDia());
			System.out.println("nova energia "+exigenciaFase2.getEnergiaMetDet());
			this.cr2 = exigenciaFase2.getEnergiaMetabolizavelDia() / exigenciaFase2.getEnergiaMetDet();
			this.consumo2 = this.cr2 * (this.fase2 - this.fase2a +1);
			this.conversaoAlimentar2 = consumo2/ganhoPeso2;
			this.cr2Simulado = exigenciaFase2Simulada.getEnergiaMetabolizavelDia() / exigenciaFase2Simulada.getEnergiaMetDet();
			this.consumo2Simulado = this.cr2Simulado * (this.fase2 - this.fase2a +1);
			this.conversaoAlimentar2Simulado = consumo2Simulado/ganhoPeso2Simulado;
			this.dependeTemperatura(exigenciaFase2, pesoMedio2, cr2);
//			this.calcularExigencia();
		}else{
//			this.consumo2 = suinosRN.consumoMedia(this.sexo, this.idadeInicial - 21, this.fase1 - 21);
//			this.cr2 = this.consumo2/( this.fase1 - this.idadeInicial + 1);
//			this.conversaoAlimentar2 = this.consumo2/this.ganhoPeso2;
//			double em = suinosRN.energiaMetabolizavelMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			double consumomedio = suinosRN.consumoMedio(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			this.exigenciaFase2.setEnergiaMetabolizavelDia(em);
//			this.exigenciaFase2.setEnergiaMetabolizavelKg((int)(em/consumomedio));
		}
		this.calcularAmostral();
		//		System.out.println("Consumo estimado "+suinosRN.consumoPeriodoCalc(21-21, 42-21, 2,3400)/22);
	}
	
	public void energiaDeterminada2Simulada(){
		SuinosRN suinosRN = new SuinosRN();
		this.exigenciaFase2.setEnergiaMetDet(exigenciaFase2Simulada.getEnergiaMetDet());
		if(this.exigenciaFase2Simulada.getEnergiaMetDet() != 0){
			
//			System.out.println("kcal/dia "+exigenciaFase2.getEnergiaMetabolizavelDia());
//			System.out.println("nova energia "+exigenciaFase2.getEnergiaMetDet());
			this.cr2 = exigenciaFase2.getEnergiaMetabolizavelDia() / exigenciaFase2.getEnergiaMetDet();
			this.consumo2 = this.cr2 * (this.fase2 - this.fase2a +1);
			this.conversaoAlimentar2 = consumo2/ganhoPeso2;

			this.cr2Simulado = exigenciaFase2Simulada.getEnergiaMetabolizavelDia() / exigenciaFase2Simulada.getEnergiaMetDet();
			this.consumo2Simulado = this.cr2Simulado * (this.fase2 - this.fase2a +1);
			this.conversaoAlimentar2Simulado = consumo2Simulado/ganhoPeso2Simulado;
		}else{
//			this.consumo2 = suinosRN.consumoMedia(this.sexo, this.idadeInicial - 21, this.fase1 - 21);
//			this.cr2 = this.consumo2/( this.fase1 - this.idadeInicial + 1);
//			this.conversaoAlimentar2 = this.consumo2/this.ganhoPeso2;
//			double em = suinosRN.energiaMetabolizavelMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			double consumomedio = suinosRN.consumoMedio(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			this.exigenciaFase2.setEnergiaMetabolizavelDia(em);
//			this.exigenciaFase2.setEnergiaMetabolizavelKg((int)(em/consumomedio));
		}
		this.calcularAmostral();
		//		System.out.println("Consumo estimado "+suinosRN.consumoPeriodoCalc(21-21, 42-21, 2,3400)/22);
	}
	/**
	 * Calcula a nova exigência a partir da energia digitada
	 */
	public void energiaDeterminada3(){
		SuinosRN suinosRN = new SuinosRN();
		this.exigenciaFase3Simulada.setEnergiaMetDet(exigenciaFase3.getEnergiaMetDet());
		if(this.exigenciaFase3.getEnergiaMetDet() != 0){
			System.out.println("kcal/dia "+exigenciaFase3.getEnergiaMetabolizavelDia());
			System.out.println("nova energia "+exigenciaFase3.getEnergiaMetDet());
			this.cr3 = exigenciaFase3.getEnergiaMetabolizavelDia() / exigenciaFase3.getEnergiaMetDet();
			this.consumo3 = this.cr3 * (this.fase3 - this.fase3a +1);
			this.conversaoAlimentar3 = consumo3/ganhoPeso3;
			this.cr3Simulado = exigenciaFase2Simulada.getEnergiaMetabolizavelDia() / exigenciaFase3Simulada.getEnergiaMetDet();
			this.consumo3Simulado = this.cr3Simulado *(this.fase3 - this.fase3a +1);
			this.conversaoAlimentar3Simulado = consumo3Simulado/ganhoPeso3Simulado;
			this.dependeTemperatura(exigenciaFase3, pesoMedio3, cr3);
//			this.calcularExigencia();
		}else{
//			this.consumo3 = suinosRN.consumoMedia(this.sexo, this.idadeInicial - 21, this.fase1 - 21);
//			this.cr1 = this.consumo3/( this.fase1 - this.idadeInicial + 1);
//			this.conversaoAlimentar3 = this.consumo3/this.ganhoPeso3;
//			double em = suinosRN.energiaMetabolizavelMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			double consumomedio = suinosRN.consumoMedio(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			this.exigenciaFase3.setEnergiaMetabolizavelDia(em);
//			this.exigenciaFase3.setEnergiaMetabolizavelKg((int)(em/consumomedio));
		}
		this.calcularAmostral();
		//		System.out.println("Consumo estimado "+suinosRN.consumoPeriodoCalc(21-21, 42-21, 2,3400)/22);
	}
	
	public void energiaDeterminada3Simulada(){
		SuinosRN suinosRN = new SuinosRN();
		
		this.exigenciaFase3.setEnergiaMetDet(exigenciaFase3Simulada.getEnergiaMetDet());
		if(this.exigenciaFase3Simulada.getEnergiaMetDet() != 0){
			System.out.println("kcal/dia "+exigenciaFase3.getEnergiaMetabolizavelDia());
			System.out.println("nova energia "+exigenciaFase3.getEnergiaMetDet());
			this.cr3 = exigenciaFase3.getEnergiaMetabolizavelDia() / exigenciaFase3.getEnergiaMetDet();
			this.consumo3 = this.cr3 * (this.fase3 - this.fase3a +1);
			this.conversaoAlimentar3 = consumo3/ganhoPeso3;

			this.cr3Simulado = exigenciaFase2Simulada.getEnergiaMetabolizavelDia() / exigenciaFase3Simulada.getEnergiaMetDet();
			this.consumo3Simulado = this.cr3Simulado *(this.fase3 - this.fase3a +1);
			this.conversaoAlimentar3Simulado = consumo3Simulado/ganhoPeso3Simulado;
		}else{
//			this.consumo3 = suinosRN.consumoMedia(this.sexo, this.idadeInicial - 21, this.fase1 - 21);
//			this.cr1 = this.consumo3/( this.fase1 - this.idadeInicial + 1);
//			this.conversaoAlimentar3 = this.consumo3/this.ganhoPeso3;
//			double em = suinosRN.energiaMetabolizavelMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			double consumomedio = suinosRN.consumoMedio(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			this.exigenciaFase3.setEnergiaMetabolizavelDia(em);
//			this.exigenciaFase3.setEnergiaMetabolizavelKg((int)(em/consumomedio));
		}
		this.calcularAmostral();
		//		System.out.println("Consumo estimado "+suinosRN.consumoPeriodoCalc(21-21, 42-21, 2,3400)/22);
	}
	/**
	 * Calcula a nova exigência a partir da energia digitada
	 */
	public void energiaDeterminada4(){
		SuinosRN suinosRN = new SuinosRN();
		this.exigenciaFase4Simulada.setEnergiaMetDet(exigenciaFase4.getEnergiaMetDet());
		if(this.exigenciaFase4.getEnergiaMetDet() != 0){
			System.out.println("kcal/dia "+exigenciaFase4.getEnergiaMetabolizavelDia());
			System.out.println("nova energia "+exigenciaFase4.getEnergiaMetDet());
			this.cr4 = exigenciaFase4.getEnergiaMetabolizavelDia() / exigenciaFase4.getEnergiaMetDet();
			this.consumo4 = this.cr4 * (this.fase4 - this.fase4a +1);
			this.conversaoAlimentar4 = consumo4/ganhoPeso4;
			this.cr4Simulado = exigenciaFase4Simulada.getEnergiaMetabolizavelDia() / exigenciaFase4Simulada.getEnergiaMetDet();
			this.consumo4Simulado = this.cr4Simulado * (this.fase4 - this.fase4a +1);
			this.conversaoAlimentar4Simulado = consumo4Simulado/ganhoPeso4Simulado;
			this.dependeTemperatura(exigenciaFase4, pesoMedio4, cr4);
//			this.calcularExigencia();
		}else{
//			this.consumo3 = suinosRN.consumoMedia(this.sexo, this.idadeInicial - 21, this.fase1 - 21);
//			this.cr1 = this.consumo3/( this.fase1 - this.idadeInicial + 1);
//			this.conversaoAlimentar3 = this.consumo3/this.ganhoPeso3;
//			double em = suinosRN.energiaMetabolizavelMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			double consumomedio = suinosRN.consumoMedio(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			this.exigenciaFase3.setEnergiaMetabolizavelDia(em);
//			this.exigenciaFase3.setEnergiaMetabolizavelKg((int)(em/consumomedio));
		}
		this.calcularAmostral();
		//		System.out.println("Consumo estimado "+suinosRN.consumoPeriodoCalc(21-21, 42-21, 2,3400)/22);
	}
	
	public void energiaDeterminada4Simulada(){
		SuinosRN suinosRN = new SuinosRN();
		this.exigenciaFase4.setEnergiaMetDet(exigenciaFase4Simulada.getEnergiaMetDet());
		if(this.exigenciaFase4Simulada.getEnergiaMetDet() != 0){
			
//			System.out.println("kcal/dia "+exigenciaFase4.getEnergiaMetabolizavelDia());
//			System.out.println("nova energia "+exigenciaFase4.getEnergiaMetDet());
			this.cr4 = exigenciaFase4.getEnergiaMetabolizavelDia() / exigenciaFase4.getEnergiaMetDet();
			this.consumo4 = this.cr4 * (this.fase4 - this.fase4a +1);
			this.conversaoAlimentar4 = consumo4/ganhoPeso4;
			
			this.cr4Simulado = exigenciaFase4Simulada.getEnergiaMetabolizavelDia() / exigenciaFase4Simulada.getEnergiaMetDet();
			this.consumo4Simulado = this.cr4Simulado * (this.fase4 - this.fase4a +1);
			this.conversaoAlimentar4Simulado = consumo4Simulado/ganhoPeso4Simulado;
		}else{
//			this.consumo3 = suinosRN.consumoMedia(this.sexo, this.idadeInicial - 21, this.fase1 - 21);
//			this.cr1 = this.consumo3/( this.fase1 - this.idadeInicial + 1);
//			this.conversaoAlimentar3 = this.consumo3/this.ganhoPeso3;
//			double em = suinosRN.energiaMetabolizavelMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			double consumomedio = suinosRN.consumoMedio(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			this.exigenciaFase3.setEnergiaMetabolizavelDia(em);
//			this.exigenciaFase3.setEnergiaMetabolizavelKg((int)(em/consumomedio));
		}
		this.calcularAmostral();
		//		System.out.println("Consumo estimado "+suinosRN.consumoPeriodoCalc(21-21, 42-21, 2,3400)/22);
	}
	/**
	 * Calcula a nova exigência a partir da energia digitada
	 */
	public void energiaDeterminada5(){
		SuinosRN suinosRN = new SuinosRN();
		this.exigenciaFase5Simulada.setEnergiaMetDet(exigenciaFase5.getEnergiaMetDet());
		if(this.exigenciaFase5.getEnergiaMetDet() != 0){
			System.out.println("kcal/dia "+exigenciaFase5.getEnergiaMetabolizavelDia());
			System.out.println("nova energia "+exigenciaFase5.getEnergiaMetDet());
			this.cr5 = exigenciaFase5.getEnergiaMetabolizavelDia() / exigenciaFase5.getEnergiaMetDet();
			this.consumo5 = this.cr5 * (this.fase5 - this.fase5a +1);
			this.conversaoAlimentar5 = consumo5/ganhoPeso5;
			this.cr5Simulado = exigenciaFase5Simulada.getEnergiaMetabolizavelDia() / exigenciaFase5Simulada.getEnergiaMetDet();
			this.consumo5Simulado = this.cr5Simulado * (this.fase5 - this.fase5a +1);
			this.conversaoAlimentar5Simulado = consumo5Simulado/ganhoPeso5Simulado;
			this.dependeTemperatura(exigenciaFase5, pesoMedio5, cr5);
//			this.calcularExigencia();
		}else{
//			this.consumo3 = suinosRN.consumoMedia(this.sexo, this.idadeInicial - 21, this.fase1 - 21);
//			this.cr1 = this.consumo3/( this.fase1 - this.idadeInicial + 1);
//			this.conversaoAlimentar3 = this.consumo3/this.ganhoPeso3;
//			double em = suinosRN.energiaMetabolizavelMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			double consumomedio = suinosRN.consumoMedio(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			this.exigenciaFase3.setEnergiaMetabolizavelDia(em);
//			this.exigenciaFase3.setEnergiaMetabolizavelKg((int)(em/consumomedio));
		}
		this.calcularAmostral();
		//		System.out.println("Consumo estimado "+suinosRN.consumoPeriodoCalc(21-21, 42-21, 2,3400)/22);
	}
	
	public void energiaDeterminada5Simulada(){
		SuinosRN suinosRN = new SuinosRN();
		this.exigenciaFase5.setEnergiaMetDet(exigenciaFase5Simulada.getEnergiaMetDet());
		if(this.exigenciaFase5Simulada.getEnergiaMetDet() != 0){
			System.out.println("kcal/dia "+exigenciaFase5.getEnergiaMetabolizavelDia());
			System.out.println("nova energia "+exigenciaFase5.getEnergiaMetDet());
			this.cr5 = exigenciaFase5.getEnergiaMetabolizavelDia() / exigenciaFase5.getEnergiaMetDet();
			this.consumo5 = this.cr5 * (this.fase5 - this.fase5a +1);
			this.conversaoAlimentar5 = consumo5/ganhoPeso5;
			
			this.cr5Simulado = exigenciaFase5Simulada.getEnergiaMetabolizavelDia() / exigenciaFase5Simulada.getEnergiaMetDet();
			this.consumo5Simulado = this.cr5Simulado * (this.fase5 - this.fase5a +1);
			this.conversaoAlimentar5Simulado = consumo5Simulado/ganhoPeso5Simulado;
		}else{
//			this.consumo3 = suinosRN.consumoMedia(this.sexo, this.idadeInicial - 21, this.fase1 - 21);
//			this.cr1 = this.consumo3/( this.fase1 - this.idadeInicial + 1);
//			this.conversaoAlimentar3 = this.consumo3/this.ganhoPeso3;
//			double em = suinosRN.energiaMetabolizavelMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			double consumomedio = suinosRN.consumoMedio(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			this.exigenciaFase3.setEnergiaMetabolizavelDia(em);
//			this.exigenciaFase3.setEnergiaMetabolizavelKg((int)(em/consumomedio));
		}
		this.calcularAmostral();
		//		System.out.println("Consumo estimado "+suinosRN.consumoPeriodoCalc(21-21, 42-21, 2,3400)/22);
	}
	/**
	 * Calcula a nova exigência a partir da energia digitada
	 */
	public void energiaDeterminada6(){
		SuinosRN suinosRN = new SuinosRN();
		this.exigenciaFase6Simulada.setEnergiaMetDet(exigenciaFase6.getEnergiaMetDet());
		if(this.exigenciaFase6.getEnergiaMetDet() != 0){
			System.out.println("kcal/dia "+exigenciaFase6.getEnergiaMetabolizavelDia());
			System.out.println("nova energia "+exigenciaFase6.getEnergiaMetDet());
			this.cr6 = exigenciaFase6.getEnergiaMetabolizavelDia() / exigenciaFase6.getEnergiaMetDet();
			this.consumo6 = this.cr6 * (this.idadeAbate - this.fase6a +1);
			this.conversaoAlimentar6 = consumo6/ganhoPeso6;
			
			this.cr6Simulado = exigenciaFase6Simulada.getEnergiaMetabolizavelDia() / exigenciaFase6Simulada.getEnergiaMetDet();
			this.consumo6Simulado = this.cr6Simulado *  (this.idadeAbate - this.fase6a +1);
			this.conversaoAlimentar6Simulado = consumo6Simulado/ganhoPeso6Simulado;
//			this.calcularExigencia();
			this.dependeTemperatura(exigenciaFase6, pesoMedio6, cr6);
		}else{
//			this.consumo3 = suinosRN.consumoMedia(this.sexo, this.idadeInicial - 21, this.fase1 - 21);
//			this.cr1 = this.consumo3/( this.fase1 - this.idadeInicial + 1);
//			this.conversaoAlimentar3 = this.consumo3/this.ganhoPeso3;
//			double em = suinosRN.energiaMetabolizavelMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			double consumomedio = suinosRN.consumoMedio(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			this.exigenciaFase3.setEnergiaMetabolizavelDia(em);
//			this.exigenciaFase3.setEnergiaMetabolizavelKg((int)(em/consumomedio));
		}
		this.calcularAmostral();
		//		System.out.println("Consumo estimado "+suinosRN.consumoPeriodoCalc(21-21, 42-21, 2,3400)/22);
	}
	
	public void energiaDeterminada6Simulado(){
		SuinosRN suinosRN = new SuinosRN();
		this.exigenciaFase6.setEnergiaMetDet(exigenciaFase6Simulada.getEnergiaMetDet());
		if(this.exigenciaFase6Simulada.getEnergiaMetDet() != 0){
			
			System.out.println("kcal/dia "+exigenciaFase6.getEnergiaMetabolizavelDia());
			System.out.println("nova energia "+exigenciaFase6.getEnergiaMetDet());
			this.cr6 = exigenciaFase6.getEnergiaMetabolizavelDia() / exigenciaFase6.getEnergiaMetDet();
			this.consumo6 = this.cr6 * (this.idadeAbate - this.fase6a +1);
			this.conversaoAlimentar6 = consumo6/ganhoPeso6;
			
			
			this.cr6Simulado = exigenciaFase6Simulada.getEnergiaMetabolizavelDia() / exigenciaFase6Simulada.getEnergiaMetDet();
			this.consumo6Simulado = this.cr6Simulado *  (this.idadeAbate - this.fase6a +1);
			this.conversaoAlimentar6Simulado = consumo6Simulado/ganhoPeso6Simulado;
		}else{
//			this.consumo3 = suinosRN.consumoMedia(this.sexo, this.idadeInicial - 21, this.fase1 - 21);
//			this.cr1 = this.consumo3/( this.fase1 - this.idadeInicial + 1);
//			this.conversaoAlimentar3 = this.consumo3/this.ganhoPeso3;
//			double em = suinosRN.energiaMetabolizavelMedia(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			double consumomedio = suinosRN.consumoMedio(this.idadeInicial - 21,this.fase1 - 21, this.sexo);
//			this.exigenciaFase3.setEnergiaMetabolizavelDia(em);
//			this.exigenciaFase3.setEnergiaMetabolizavelKg((int)(em/consumomedio));
		}
		this.calcularAmostral();
		//		System.out.println("Consumo estimado "+suinosRN.consumoPeriodoCalc(21-21, 42-21, 2,3400)/22);
	}
	
	/**
	 * Calcula a Exigencia para Inicial I 
	 * @author alkxyly
	 */
	public void calcular(){

		SuinosRN suinoRN = new SuinosRN();
		suinoInicial1.setEnergeriaMet(suinoRN.energiaMetabolizavel(suinoInicial1));
		System.out.println(suinoInicial1.toString());
		double correcao = suinoRN.correcaoTemperatura(suinoInicial1, 23);
		System.out.println(correcao);
		System.out.println(suinoInicial1.getEnergeriaMet() - correcao);
		
		double fd = suinoRN.fosforoDisponivel(suinoInicial1);		
		exigencia.setFosforoDisponivel(fd);
		double fdE = suinoRN.fosforoDisponivelExigencia(exigencia,suinoInicial1);
		exigencia.setFosforoDisponivelExigencia(fdE);
		double fdi = suinoRN.fosforoDigestivel(suinoInicial1);
		exigencia.setFosforoDigestivel(fdi);
		exigencia.setFosforoDigestivelExigencia(suinoRN.fosforoDigestivelExigencia(exigencia,suinoInicial1));
		exigencia.setCalcio(suinoRN.calcio(exigencia, suinoInicial1));
		exigencia.setProteina(suinoRN.proteina(suinoInicial1));
		exigencia.setCloro(suinoRN.cloro(suinoInicial1));
		exigencia.setSodio(suinoRN.sodio(suinoInicial1));
		exigencia.setPotassio(suinoRN.potassio(suinoInicial1));

		double lisinaDigestivel = suinoRN.lisinaDigestivel(suinoInicial1);
		exigencia.setLisinaDigestivel(lisinaDigestivel);		
		exigencia.setLisinaDigestivelExigencia(suinoRN.lisinaDigestivelExigencia(lisinaDigestivel,
				suinoInicial1.getConsumoRacao()));
		double lisinaDigExig = exigencia.getLisinaDigestivelExigencia();

		exigencia.setMetioninaDigestivel(suinoRN.metioninaDigestivel(lisinaDigExig));

		exigencia.setMetioninaCistina(suinoRN.metioninaCistina(lisinaDigExig));
//		exigencia.setTreoninaDigestivel(suinoRN.treoninaDigestivel(lisinaDigExig));
		exigencia.setTriptofano(suinoRN.triptofano(lisinaDigExig));
		exigencia.setArginina(suinoRN.arginina(lisinaDigExig));
		exigencia.setValina(suinoRN.valina(lisinaDigExig));
		exigencia.setIsoleucina(suinoRN.isoleucina(lisinaDigExig));
		exigencia.setLeucina(suinoRN.leucina(lisinaDigExig));
		exigencia.setHistidina(suinoRN.histidina(lisinaDigExig));
		exigencia.setFenilalanina(suinoRN.fenilalanina(lisinaDigExig));
		exigencia.setFenilalaninaTirosina(suinoRN.fenilalaninaTirosina(lisinaDigExig));
		double lisinaTotal = suinoRN.lisinaTotal(lisinaDigestivel, suinoInicial1.getConsumoRacao());
		exigencia.setLisinaTotal(lisinaTotal);
		exigencia.setMetioninaTotal(suinoRN.metioninaTotal(lisinaTotal));
		exigencia.setMetioninaCistinaTotal(suinoRN.metioninaCistinaTotal(lisinaTotal));
		exigencia.setTreoninaTotal(suinoRN.treoninaTotal(lisinaTotal));
		exigencia.setTriptofanoTotal(suinoRN.triptofanoTotal(lisinaTotal));

		exigencia.setArgininaTotal(suinoRN.argininaTotal(lisinaTotal));
		exigencia.setValinaTotal(suinoRN.valinaTotal(lisinaTotal));
		exigencia.setIsoleucinaTotal(suinoRN.isoleucinaTotal(lisinaTotal));
		exigencia.setLeucinaTotal(suinoRN.leucinaTotal(lisinaTotal));
		exigencia.setHistidinaTotal(suinoRN.histidinaTotal(lisinaTotal));
		exigencia.setFenilalaninaTotal(suinoRN.fenilalaninaTotal(lisinaTotal));
		exigencia.setFenilalaninaTirosinaTotal(suinoRN.fenilalaninaTirosinaTotal(lisinaTotal));

	}
	/**
	 * Calcula exigencia inicial bloco 2
	 * @author alkxyly
	 */
	public void calcularInicial2(){
		SuinosRN suinoRN = new SuinosRN();

		double fd = suinoRN.fosforoDisponivel(suinoInicial2);		
		exigencia2.setFosforoDisponivel(fd);
		double fdE = suinoRN.fosforoDisponivelExigencia(exigencia2,suinoInicial2);
		exigencia2.setFosforoDisponivelExigencia(fdE);
		double fdi = suinoRN.fosforoDigestivel(suinoInicial2);
		exigencia2.setFosforoDigestivel(fdi);
		exigencia2.setFosforoDigestivelExigencia(suinoRN.fosforoDigestivelExigencia(exigencia2,suinoInicial2));
		exigencia2.setCalcio(suinoRN.calcio(exigencia2, suinoInicial2));
		exigencia2.setProteina(suinoRN.proteina(suinoInicial2));
		exigencia2.setCloro(suinoRN.cloro(suinoInicial2));
		exigencia2.setSodio(suinoRN.sodio(suinoInicial2));
		exigencia2.setPotassio(suinoRN.potassio(suinoInicial2));

		double lisinaDigestivel = suinoRN.lisinaDigestivel(suinoInicial2);
		exigencia2.setLisinaDigestivel(lisinaDigestivel);		
		exigencia2.setLisinaDigestivelExigencia(suinoRN.lisinaDigestivelExigencia(lisinaDigestivel,
				suinoInicial2.getConsumoRacao()));
		double lisinaDigExig = exigencia2.getLisinaDigestivelExigencia();
		exigencia2.setMetioninaDigestivel(suinoRN.metioninaDigestivel(lisinaDigExig));
		exigencia2.setMetioninaCistina(suinoRN.metioninaCistina(lisinaDigExig));
//		exigencia2.setTreoninaDigestivel(suinoRN.treoninaDigestivel(lisinaDigExig));
		exigencia2.setTriptofano(suinoRN.triptofano(lisinaDigExig));
		exigencia2.setArginina(suinoRN.arginina(lisinaDigExig));
		exigencia2.setValina(suinoRN.valina(lisinaDigExig));
		exigencia2.setIsoleucina(suinoRN.isoleucina(lisinaDigExig));
		exigencia2.setLeucina(suinoRN.leucina(lisinaDigExig));
		exigencia2.setHistidina(suinoRN.histidina(lisinaDigExig));
		exigencia2.setFenilalanina(suinoRN.fenilalanina(lisinaDigExig));
		exigencia2.setFenilalaninaTirosina(suinoRN.fenilalaninaTirosina(lisinaDigExig));
		
		double lisinaTotal = suinoRN.lisinaTotal(lisinaDigestivel, suinoInicial1.getConsumoRacao());
		exigencia2.setLisinaTotal(lisinaTotal);
		exigencia2.setMetioninaTotal(suinoRN.metioninaTotal(lisinaTotal));
		exigencia2.setMetioninaCistinaTotal(suinoRN.metioninaCistinaTotal(lisinaTotal));
		exigencia2.setTreoninaTotal(suinoRN.treoninaTotal(lisinaTotal));
		exigencia2.setTriptofanoTotal(suinoRN.triptofanoTotal(lisinaTotal));

		exigencia2.setArgininaTotal(suinoRN.argininaTotal(lisinaTotal));
		exigencia2.setValinaTotal(suinoRN.valinaTotal(lisinaTotal));
		exigencia2.setIsoleucinaTotal(suinoRN.isoleucinaTotal(lisinaTotal));
		exigencia2.setLeucinaTotal(suinoRN.leucinaTotal(lisinaTotal));
		exigencia2.setHistidinaTotal(suinoRN.histidinaTotal(lisinaTotal));
		exigencia2.setFenilalaninaTotal(suinoRN.fenilalaninaTotal(lisinaTotal));
		exigencia2.setFenilalaninaTirosinaTotal(suinoRN.fenilalaninaTirosinaTotal(lisinaTotal));
	}
	/**
	 * Executa a simulação referente ao crescimento I
	 */
	public void calculcarCrescimento1(){
		SuinosRN suinoRN = new SuinosRN();

		double fd = suinoRN.fosforoDisponivel(suinoCrescimento1);
		exigenciaCrescimento.setFosforoDisponivel(fd);
		double fdE = suinoRN.fosforoDisponivelExigencia(exigenciaCrescimento,suinoCrescimento1);
		exigenciaCrescimento.setFosforoDisponivelExigencia(fdE);
		double fdi = suinoRN.fosforoDigestivelCrescimento(suinoCrescimento1);
		exigenciaCrescimento.setFosforoDigestivel(fdi);
		exigenciaCrescimento.setFosforoDigestivelExigencia(suinoRN.fosforoDigestivelExigencia(exigenciaCrescimento,suinoCrescimento1));
		exigenciaCrescimento.setCalcio(suinoRN.calcio(exigenciaCrescimento, suinoCrescimento1));		
		exigenciaCrescimento.setProteina(suinoRN.proteinaCrescimento(suinoCrescimento1));		
		exigenciaCrescimento.setCloro(suinoRN.cloroCrescimento(suinoCrescimento1));		
		exigenciaCrescimento.setSodio(suinoRN.sodioCrescimento(suinoCrescimento1));		
		exigenciaCrescimento.setPotassio(suinoRN.potassioCrescimento(suinoCrescimento1));
		
		double lisinaDigestivel = suinoRN.lisinaDigestivel(suinoCrescimento1);
		exigenciaCrescimento.setLisinaDigestivel(lisinaDigestivel);		
		exigenciaCrescimento.setLisinaDigestivelExigencia(suinoRN.lisinaDigestivelExigencia(lisinaDigestivel,
				suinoCrescimento1.getConsumoRacao()));
		
		double lisinaDigExig = exigenciaCrescimento.getLisinaDigestivelExigencia();

		exigenciaCrescimento.setMetioninaDigestivel(suinoRN.metioninaCrescimentoDigestivel(lisinaDigExig));

		exigenciaCrescimento.setMetioninaCistina(suinoRN.metioninaCistinaCrescimento(lisinaDigExig));
		exigenciaCrescimento.setTreoninaDigestivel(suinoRN.treoninaCrescimentoDigestivel(lisinaDigExig));
		exigenciaCrescimento.setTriptofano(suinoRN.triptofano(lisinaDigExig));
		exigenciaCrescimento.setArginina(suinoRN.argininaCrescimento(lisinaDigExig));
		exigenciaCrescimento.setValina(suinoRN.valina(lisinaDigExig));
		exigenciaCrescimento.setIsoleucina(suinoRN.isoleucina(lisinaDigExig));
		exigenciaCrescimento.setLeucina(suinoRN.leucina(lisinaDigExig));
		exigenciaCrescimento.setHistidina(suinoRN.histidina(lisinaDigExig));
		exigenciaCrescimento.setFenilalanina(suinoRN.fenilalanina(lisinaDigExig));
		exigenciaCrescimento.setFenilalaninaTirosina(suinoRN.fenilalaninaTirosina(lisinaDigExig));
		
		//Aminácidos Totais
		double lisinaTotal = suinoRN.lisinaTotal(lisinaDigestivel, suinoCrescimento1.getConsumoRacao());
		exigenciaCrescimento.setLisinaTotal(lisinaTotal);
		exigenciaCrescimento.setMetioninaTotal(suinoRN.metioninaCrescimentoTotal(lisinaTotal));
		System.out.println(exigenciaCrescimento.getMetioninaTotal()+" <<");
		exigenciaCrescimento.setMetioninaCistinaTotal(suinoRN.metioninaCistinaCrescimentoTotal(lisinaTotal));
		exigenciaCrescimento.setTreoninaTotal(suinoRN.treoninaCrescimentoTotal(lisinaTotal));
		exigenciaCrescimento.setTriptofanoTotal(suinoRN.triptofanoTotal(lisinaTotal));
		exigenciaCrescimento.setArgininaTotal(suinoRN.argininaCrescimentoTotal(lisinaTotal));
		exigenciaCrescimento.setValinaTotal(suinoRN.valinaTotal(lisinaTotal));
		exigenciaCrescimento.setIsoleucinaTotal(suinoRN.isoleucinaTotal(lisinaTotal));
		exigenciaCrescimento.setLeucinaTotal(suinoRN.leucinaTotal(lisinaTotal));
		exigenciaCrescimento.setHistidinaTotal(suinoRN.histidinaTotal(lisinaTotal));
		exigenciaCrescimento.setFenilalaninaTotal(suinoRN.fenilalaninaTotal(lisinaTotal));
		exigenciaCrescimento.setFenilalaninaTirosinaTotal(suinoRN.fenilalaninaTirosinaTotal(lisinaTotal));

		System.out.println(exigenciaCrescimento.toString());

	}
	/**
	 * Executa a simulação referente ao crescimento II
	 */
	public void calculcarCrescimento2(){
		SuinosRN suinoRN = new SuinosRN();
		
		double fd = suinoRN.fosforoDisponivel(suinoCrescimento2);
		exigenciaCrescimento2.setFosforoDisponivel(fd);
		double fdE = suinoRN.fosforoDisponivelExigencia(exigenciaCrescimento,suinoCrescimento2);
		exigenciaCrescimento2.setFosforoDisponivelExigencia(fdE);
		double fdi = suinoRN.fosforoDigestivelCrescimento(suinoCrescimento2);
		exigenciaCrescimento2.setFosforoDigestivel(fdi);
		exigenciaCrescimento2.setFosforoDigestivelExigencia(suinoRN.fosforoDigestivelExigencia(exigenciaCrescimento2,suinoCrescimento2));
		exigenciaCrescimento2.setCalcio(suinoRN.calcio(exigenciaCrescimento2, suinoCrescimento2));		
		exigenciaCrescimento2.setProteina(suinoRN.proteinaCrescimento(suinoCrescimento2));		
		exigenciaCrescimento2.setCloro(suinoRN.cloroCrescimento(suinoCrescimento2));		
		exigenciaCrescimento2.setSodio(suinoRN.sodioCrescimento(suinoCrescimento2));		
		exigenciaCrescimento2.setPotassio(suinoRN.potassioCrescimento(suinoCrescimento2));
		
		double lisinaDigestivel = suinoRN.lisinaDigestivel(suinoCrescimento2);
		exigenciaCrescimento2.setLisinaDigestivel(lisinaDigestivel);		
		exigenciaCrescimento2.setLisinaDigestivelExigencia(suinoRN.lisinaDigestivelExigencia(lisinaDigestivel,
				suinoCrescimento2.getConsumoRacao()));
		
		double lisinaDigExig = exigenciaCrescimento2.getLisinaDigestivelExigencia();
		
		exigenciaCrescimento2.setMetioninaDigestivel(suinoRN.metioninaCrescimentoDigestivel(lisinaDigExig));
		
		exigenciaCrescimento2.setMetioninaCistina(suinoRN.metioninaCistinaCrescimento(lisinaDigExig));
		exigenciaCrescimento2.setTreoninaDigestivel(suinoRN.treoninaCrescimentoDigestivel(lisinaDigExig));
		exigenciaCrescimento2.setTriptofano(suinoRN.triptofano(lisinaDigExig));
		exigenciaCrescimento2.setArginina(suinoRN.argininaCrescimento(lisinaDigExig));
		exigenciaCrescimento2.setValina(suinoRN.valina(lisinaDigExig));
		exigenciaCrescimento2.setIsoleucina(suinoRN.isoleucina(lisinaDigExig));
		exigenciaCrescimento2.setLeucina(suinoRN.leucina(lisinaDigExig));
		exigenciaCrescimento2.setHistidina(suinoRN.histidina(lisinaDigExig));
		exigenciaCrescimento2.setFenilalanina(suinoRN.fenilalanina(lisinaDigExig));
		exigenciaCrescimento2.setFenilalaninaTirosina(suinoRN.fenilalaninaTirosina(lisinaDigExig));
		
		//Aminácidos Totais
		double lisinaTotal = suinoRN.lisinaTotal(lisinaDigestivel, suinoCrescimento2.getConsumoRacao());
		exigenciaCrescimento2.setLisinaTotal(lisinaTotal);
		exigenciaCrescimento2.setMetioninaTotal(suinoRN.metioninaCrescimentoTotal(lisinaTotal));
		exigenciaCrescimento2.setMetioninaCistinaTotal(suinoRN.metioninaCistinaCrescimentoTotal(lisinaTotal));
		exigenciaCrescimento2.setTreoninaTotal(suinoRN.treoninaCrescimentoTotal(lisinaTotal));
		exigenciaCrescimento2.setTriptofanoTotal(suinoRN.triptofanoTotal(lisinaTotal));
		exigenciaCrescimento2.setArgininaTotal(suinoRN.argininaCrescimentoTotal(lisinaTotal));
		exigenciaCrescimento2.setValinaTotal(suinoRN.valinaTotal(lisinaTotal));
		exigenciaCrescimento2.setIsoleucinaTotal(suinoRN.isoleucinaTotal(lisinaTotal));
		exigenciaCrescimento2.setLeucinaTotal(suinoRN.leucinaTotal(lisinaTotal));
		exigenciaCrescimento2.setHistidinaTotal(suinoRN.histidinaTotal(lisinaTotal));
		exigenciaCrescimento2.setFenilalaninaTotal(suinoRN.fenilalaninaTotal(lisinaTotal));
		exigenciaCrescimento2.setFenilalaninaTirosinaTotal(suinoRN.fenilalaninaTirosinaTotal(lisinaTotal));
		
		System.out.println(exigenciaCrescimento2.toString());
		
	}
	/**
	 * Verifica se o peso médio está na faixa aceitavel
	 */
	public void verificarPesoMedioInicial(){
		if(suinoInicial1.getPesoMedio() >= 15 && suinoInicial1.getPesoMedio() <= 30.9){			
		}else
			Mensagens.adicionarMensagemErro("Inicial I : Peso médio fora da faixar, inserir peso entre 15 e 30.9");
	}
	/**
	 * 
	 * valida inicial 2
	 */
	public void verificarPesoMedioInicial2(){
		if(suinoInicial2.getPesoMedio() >= 15 && suinoInicial2.getPesoMedio() <= 30.9){			
		}else
			Mensagens.adicionarMensagemErro("Inicial II : Peso médio fora da faixar, inserir peso entre 15 e 30.9");
	}
	/**
	 * 
	 * valida crescimento 1
	 */
	public void verificarPesoMedioCrescimento1(){
		if(suinoCrescimento1.getPesoMedio() >= 30 && suinoCrescimento1.getPesoMedio() <= 69.9){			
		}else
			Mensagens.adicionarMensagemErro("Crescimento I : Peso médio fora da faixar, inserir peso entre 30 e 69.9");
	}
	/**
	 * 
	 * valida crescimento 2
	 */
	public void verificarPesoMedioCrescimento2(){
		if(suinoCrescimento2.getPesoMedio() >= 30 && suinoCrescimento2.getPesoMedio() <= 69.9){			
		}else
			Mensagens.adicionarMensagemErro("Crescimento I : Peso médio fora da faixar, inserir peso entre 30 e 69.9");
	}

	public void copiarInicial2(){
		if(this.suinoInicial1 != null){
			suinoInicial2.setPesoMedio(suinoInicial1.getPesoMedio());
			suinoInicial2.setGanhoPeso(suinoInicial1.getGanhoPeso());
			suinoInicial2.setEnergeriaMet(suinoInicial1.getEnergeriaMet());
			suinoInicial2.setDesempenho(suinoInicial1.getDesempenho());
			suinoInicial2.setConsumoRacao(suinoInicial1.getConsumoRacao());
		}
	}
	/**
	 * Copia os valores dos campos inicial II para o crescimento I
	 */
	public void copiarCrescimento1(){
		
		if(this.suinoInicial2 != null){
		
			if(suinoInicial2.getPesoMedio() < 30 ){			
				suinoCrescimento1.setPesoMedio(suinoInicial2.getPesoMedio());
				Mensagens.adicionarMensagemErro("Crescimento I : Peso médio fora da faixar, inserir peso entre 30 e 69.9");
			}else
				suinoCrescimento1.setPesoMedio(suinoInicial2.getPesoMedio());
			
			suinoCrescimento1.setGanhoPeso(suinoInicial2.getGanhoPeso());
			suinoCrescimento1.setEnergeriaMet(suinoInicial2.getEnergeriaMet());
			suinoCrescimento1.setDesempenho(suinoInicial2.getDesempenho());
			suinoCrescimento1.setConsumoRacao(suinoInicial2.getConsumoRacao());
		}
	}
	/**
	 * Copia os valores dos campos Crescimento I para o crescimento II
	 */
	public void copiarCrescimento2(){
		
		if(this.suinoCrescimento1 != null){
			
			if(suinoCrescimento1.getPesoMedio() < 30 ){			
				suinoCrescimento2.setPesoMedio(suinoCrescimento1.getPesoMedio());
				Mensagens.adicionarMensagemErro("Crescimento II : Peso médio fora da faixar, inserir peso entre 30 e 69.9");
			}else
				suinoCrescimento2.setPesoMedio(suinoCrescimento1.getPesoMedio());
			
			suinoCrescimento2.setGanhoPeso(suinoCrescimento1.getGanhoPeso());
			suinoCrescimento2.setEnergeriaMet(suinoCrescimento1.getEnergeriaMet());
			suinoCrescimento2.setDesempenho(suinoCrescimento1.getDesempenho());
			suinoCrescimento2.setConsumoRacao(suinoCrescimento1.getConsumoRacao());
		}
	}
	
	public void testar(){
		SuinosRN suinosRN = new SuinosRN();
		
//		System.out.println("Peso Abate: "+suinosRN.pesoAbate(175, 21, 0.850, 7));
//		System.out.println("Consumo ração: "+suinosRN.consumoRacao(2.600, suinosRN.ganhoAoAbate(suinosRN.pesoAbate(175, 21, 0.850, 7), 7)));
//		System.out.println("Ganho do Abate: "+suinosRN.ganhoAoAbate(suinosRN.pesoAbate(175, 21, 0.850, 7), 7));	
//		System.out.println(Tabelas.consumoRacao_machosCastrados[0][0]);
//		
//		for (int i = 0; i < Tabelas.consumoRacao_machosCastrados.length; i++) {
//			System.out.println( (i+21)+" "+Tabelas.consumoRacao_machosCastrados[i][0]);
//		}
	
		double matriz[][] = suinosRN.curvaSimulada(idadeInicial - 21,idadeAbate-21,this.sexo,pesoAbate,pesoAlojamento,consumoRacao);
	
		double media = suinosRN.media(matriz,21-21, 48-21, 1);
		double mediaPadraoGanhoDePeso = suinosRN.media(Tabelas.ganhoPeso_machosCastrados,49-21,70-21,0);
		System.out.println("Consumo Ração "+consumoRacao);
		System.out.println("MEdia Simulada de PEso "+media);
		System.out.println("MEdia Padrap Consumo Peso  "+mediaPadraoGanhoDePeso);
//		suinosRN.percentualCorrecaoPeso(null, 63 - 21, 0, pesoAbate);
		
	}
	
	 private void createLineModels() {
	        lineModel1 = initLinearModel();
	        lineModel1.setTitle("Linear Chart");
	        lineModel1.setLegendPosition("e");
	        Axis yAxis = lineModel1.getAxis(AxisType.Y);
	        yAxis.setMin(0);
	        yAxis.setMax(10);
	         
	        lineModel2 = initCategoryModel();
	        lineModel2.setTitle("Category Chart");
	        lineModel2.setLegendPosition("e");
	        lineModel2.setShowPointLabels(true);
	        lineModel2.getAxes().put(AxisType.X, new CategoryAxis("Years"));
	        yAxis = lineModel2.getAxis(AxisType.Y);
	        yAxis.setLabel("Births");
	        yAxis.setMin(0);
	        yAxis.setMax(200);
	    }
	     
	    private LineChartModel initLinearModel() {
	        LineChartModel model = new LineChartModel();

	        ChartSeries simulado = new ChartSeries();
	        ChartSeries tabelado = new ChartSeries();
	        simulado.setLabel("Peso Médio Simulado");
	        tabelado.setLabel("Peso Médio Tabelado");
	        System.out.println("VARIANTES "+variantes.length);
	        if(variantes.length == 0){
	        for (int i = this.idadeInicial; i <= this.idadeAbate; i = i + 7) {
	        	 simulado.set(String.valueOf(i), this.matriz[i-21][0]);
	        	 tabelado.set(String.valueOf(i),Tabelas.peso_medio[i-21][2]);
			}
	        }else{
	        	for (int i = this.idadeInicial; i <= this.idadeAbate; i = i + 7) {
		        	 simulado.set(String.valueOf(i), this.matriz[i-21][1]);
		        	 tabelado.set(String.valueOf(i),Tabelas.ganho_de_peso[i-21][2]);
				}
	        }
	        model.addSeries(simulado);
	        model.addSeries(tabelado);
//	        model.setExtender("skinBarAndLine");
	        model.setTitle("Category Chart");
	        model.setLegendPosition("w");
	        model.setShowPointLabels(true);
	        model.getAxes().put(AxisType.X, new CategoryAxis("Years"));
	        model.setAnimate(true);
	        Axis yAxis = model.getAxis(AxisType.Y);
	 	   Axis xAxis = model.getAxis(AxisType.X);
	 	   xAxis.setLabel("Dias");
//	 	   xAxis.setMin(0);
	 	   xAxis.setMax(idadeAbate);
	 	   yAxis.setLabel("g/dia");
//	 	   yAxis.setMin(0);
//	 	   yAxis.setMax(200);
	 	   
	 	  model.getAxes().put(AxisType.Y2, yAxis);
	        
	         
	        return model;
	    }
	     
	    private LineChartModel initCategoryModel() {
	        LineChartModel model = new LineChartModel();
	 
	        ChartSeries boys = new ChartSeries();
	        boys.setLabel("Boys");
	        boys.set("2004", 120);
	        boys.set("2005", 100);
	        boys.set("2006", 44);
	        boys.set("2007", 150);
	        boys.set("2008", 25);
	 
	        ChartSeries girls = new ChartSeries();
	        girls.setLabel("Girls");
	        girls.set("2004", 52);
	        girls.set("2005", 60);
	        girls.set("2006", 110);
	        girls.set("2007", 90);
	        girls.set("2008", 120);
	 
	        model.addSeries(boys);
	        model.addSeries(girls);
	         
	        return model;
	    }
	    public void selecionar(){
	    	createLineModels();
	    }
	public double getNumero() {
		return numero;
	}

	public void setNumero(double numero) {
		this.numero = numero;
	}

	public Suino getSuinoInicial1() {
		return suinoInicial1;
	}

	public void setSuinoInicial1(Suino suinoInicial1) {
		this.suinoInicial1 = suinoInicial1;
	}
	public Exigencia getExigencia() {
		return exigencia;
	}
	public void setExigencia(Exigencia exigencia) {
		this.exigencia = exigencia;
	}
	public Suino getSuinoInicial2() {
		return suinoInicial2;
	}
	public void setSuinoInicial2(Suino suinoInicial2) {
		this.suinoInicial2 = suinoInicial2;
	}
	public Exigencia getExigencia2() {
		return exigencia2;
	}
	public void setExigencia2(Exigencia exigencia2) {
		this.exigencia2 = exigencia2;
	}
	public Suino getSuinoCrescimento1() {
		return suinoCrescimento1;
	}
	public void setSuinoCrescimento1(Suino suinoCrescimento1) {
		this.suinoCrescimento1 = suinoCrescimento1;
	}
	public Suino getSuinoCrescimento2() {
		return suinoCrescimento2;
	}
	public void setSuinoCrescimento2(Suino suinoCrescimento2) {
		this.suinoCrescimento2 = suinoCrescimento2;
	}
	public Exigencia getExigenciaCrescimento() {
		return exigenciaCrescimento;
	}
	public void setExigenciaCrescimento(Exigencia exigenciaCrescimento) {
		this.exigenciaCrescimento = exigenciaCrescimento;
	}
	public Exigencia getExigenciaCrescimento2() {
		return exigenciaCrescimento2;
	}
	public void setExigenciaCrescimento2(Exigencia exigenciaCrescimento2) {
		this.exigenciaCrescimento2 = exigenciaCrescimento2;
	}
	public int getSexo() {
		return sexo;
	}
	public void setSexo(int sexo) {
		this.sexo = sexo;
	}
	public int getIdadeInicial() {
		return idadeInicial;
	}
	public void setIdadeInicial(int idadeInicial) {
		this.idadeInicial = idadeInicial;
	}
	public int getIdadeAbate() {
		return idadeAbate;
	}
	public void setIdadeAbate(int idadeAbate) {
		this.idadeAbate = idadeAbate;
	}
	public double getPesoAlojamento() {
		return pesoAlojamento;
	}
	public void setPesoAlojamento(double pesoAlojamento) {
		this.pesoAlojamento = pesoAlojamento;
	}
	public double getGanhoPeso() {
		return ganhoPeso;
	}
	public void setGanhoPeso(double ganhoPeso) {
		this.ganhoPeso = ganhoPeso;
	}
	public double getConversaoAlimentar() {
		return conversaoAlimentar;
	}
	public void setConversaoAlimentar(double conversaoAlimentar) {
		this.conversaoAlimentar = conversaoAlimentar;
	}

	public double getPesoAbate() {
		return pesoAbate;
	}

	public void setPesoAbate(double pesoAbate) {
		this.pesoAbate = pesoAbate;
	}

	public double getConsumoRacao() {
		return consumoRacao;
	}

	public void setConsumoRacao(double consumoRacao) {
		this.consumoRacao = consumoRacao;
	}

	public double getGanhoAbate() {
		return ganhoAbate;
	}

	public void setGanhoAbate(double ganhoAbate) {
		this.ganhoAbate = ganhoAbate;
	}

	public int getFase1() {
		return fase1;
	}

	public void setFase1(int fase1) {
		this.fase1 = fase1;
	}
	public int getFase2a() {
		return fase2a;
	}
	public void setFase2a(int fase2a) {
		this.fase2a = fase2a;
	}
	public int getFase2() {
		return fase2;
	}
	public void setFase2(int fase2) {
		this.fase2 = fase2;
	}
	public int getFase3a() {
		return fase3a;
	}
	public void setFase3a(int fase3a) {
		this.fase3a = fase3a;
	}
	public int getFase3() {
		return fase3;
	}
	public void setFase3(int fase3) {
		this.fase3 = fase3;
	}
	public int getFase4a() {
		return fase4a;
	}
	public void setFase4a(int fase4a) {
		this.fase4a = fase4a;
	}
	public int getFase4() {
		return fase4;
	}
	public void setFase4(int fase4) {
		this.fase4 = fase4;
	}
	public int getFase5a() {
		return fase5a;
	}
	public void setFase5a(int fase5a) {
		this.fase5a = fase5a;
	}
	public int getFase5() {
		return fase5;
	}
	public void setFase5(int fase5) {
		this.fase5 = fase5;
	}
	public int getFase6a() {
		return fase6a;
	}
	public void setFase6a(int fase6a) {
		this.fase6a = fase6a;
	}
	public double getPesoMedio1() {
		return pesoMedio1;
	}
	public void setPesoMedio1(double pesoMedio1) {
		this.pesoMedio1 = pesoMedio1;
	}
	public double getGanhoPeso1() {
		return ganhoPeso1;
	}
	public void setGanhoPeso1(double ganhoPeso1) {
		this.ganhoPeso1 = ganhoPeso1;
	}
	public double getPesoMedio2() {
		return pesoMedio2;
	}
	public void setPesoMedio2(double pesoMedio2) {
		this.pesoMedio2 = pesoMedio2;
	}
	public double getPesoMedio3() {
		return pesoMedio3;
	}
	public void setPesoMedio3(double pesoMedio3) {
		this.pesoMedio3 = pesoMedio3;
	}
	public double getPesoMedio4() {
		return pesoMedio4;
	}
	public void setPesoMedio4(double pesoMedio4) {
		this.pesoMedio4 = pesoMedio4;
	}
	public double getPesoMedio5() {
		return pesoMedio5;
	}
	public void setPesoMedio5(double pesoMedio5) {
		this.pesoMedio5 = pesoMedio5;
	}
	public double getPesoMedio6() {
		return pesoMedio6;
	}
	public void setPesoMedio6(double pesoMedio6) {
		this.pesoMedio6 = pesoMedio6;
	}
	public double getGanhoPeso2() {
		return ganhoPeso2;
	}
	public void setGanhoPeso2(double ganhoPeso2) {
		this.ganhoPeso2 = ganhoPeso2;
	}
	public double getGanhoPeso3() {
		return ganhoPeso3;
	}
	public void setGanhoPeso3(double ganhoPeso3) {
		this.ganhoPeso3 = ganhoPeso3;
	}
	public double getGanhoPeso4() {
		return ganhoPeso4;
	}
	public void setGanhoPeso4(double ganhoPeso4) {
		this.ganhoPeso4 = ganhoPeso4;
	}
	public double getGanhoPeso5() {
		return ganhoPeso5;
	}
	public void setGanhoPeso5(double ganhoPeso5) {
		this.ganhoPeso5 = ganhoPeso5;
	}
	public double getGanhoPeso6() {
		return ganhoPeso6;
	}
	public void setGanhoPeso6(double ganhoPeso6) {
		this.ganhoPeso6 = ganhoPeso6;
	}
	public double getConsumo1() {
		return consumo1;
	}
	public void setConsumo1(double consumo1) {
		this.consumo1 = consumo1;
	}
	public double getConsumo2() {
		return consumo2;
	}
	public void setConsumo2(double consumo2) {
		this.consumo2 = consumo2;
	}
	public double getConsumo3() {
		return consumo3;
	}
	public void setConsumo3(double consumo3) {
		this.consumo3 = consumo3;
	}
	public double getConsumo4() {
		return consumo4;
	}
	public void setConsumo4(double consumo4) {
		this.consumo4 = consumo4;
	}
	public double getConsumo5() {
		return consumo5;
	}
	public void setConsumo5(double consumo5) {
		this.consumo5 = consumo5;
	}
	public double getConsumo6() {
		return consumo6;
	}
	public void setConsumo6(double consumo6) {
		this.consumo6 = consumo6;
	}
	public double getConversaoAlimentar1() {
		return conversaoAlimentar1;
	}
	public void setConversaoAlimentar1(double conversaoAlimentar1) {
		this.conversaoAlimentar1 = conversaoAlimentar1;
	}
	public double getConversaoAlimentar2() {
		return conversaoAlimentar2;
	}
	public void setConversaoAlimentar2(double conversaoAlimentar2) {
		this.conversaoAlimentar2 = conversaoAlimentar2;
	}
	public double getConversaoAlimentar3() {
		return conversaoAlimentar3;
	}
	public void setConversaoAlimentar3(double conversaoAlimentar3) {
		this.conversaoAlimentar3 = conversaoAlimentar3;
	}
	public double getConversaoAlimentar4() {
		return conversaoAlimentar4;
	}
	public void setConversaoAlimentar4(double conversaoAlimentar4) {
		this.conversaoAlimentar4 = conversaoAlimentar4;
	}
	public double getConversaoAlimentar5() {
		return conversaoAlimentar5;
	}
	public void setConversaoAlimentar5(double conversaoAlimentar5) {
		this.conversaoAlimentar5 = conversaoAlimentar5;
	}
	public double getConversaoAlimentar6() {
		return conversaoAlimentar6;
	}
	public void setConversaoAlimentar6(double conversaoAlimentar6) {
		this.conversaoAlimentar6 = conversaoAlimentar6;
	}
	public double getCr1() {
		return cr1;
	}
	public void setCr1(double cr1) {
		this.cr1 = cr1;
	}
	public double getCr2() {
		return cr2;
	}
	public void setCr2(double cr2) {
		this.cr2 = cr2;
	}
	public double getCr3() {
		return cr3;
	}
	public void setCr3(double cr3) {
		this.cr3 = cr3;
	}
	public double getCr4() {
		return cr4;
	}
	public void setCr4(double cr4) {
		this.cr4 = cr4;
	}
	public double getCr5() {
		return cr5;
	}
	public void setCr5(double cr5) {
		this.cr5 = cr5;
	}
	public double getCr6() {
		return cr6;
	}
	public void setCr6(double cr6) {
		this.cr6 = cr6;
	}
	public double getGp1() {
		return gp1;
	}
	public void setGp1(double gp1) {
		this.gp1 = gp1;
	}
	public double getGp2() {
		return gp2;
	}
	public void setGp2(double gp2) {
		this.gp2 = gp2;
	}
	public double getGp3() {
		return gp3;
	}
	public void setGp3(double gp3) {
		this.gp3 = gp3;
	}
	public double getGp4() {
		return gp4;
	}
	public void setGp4(double gp4) {
		this.gp4 = gp4;
	}
	public double getGp5() {
		return gp5;
	}
	public void setGp5(double gp5) {
		this.gp5 = gp5;
	}
	public double getGp6() {
		return gp6;
	}
	public void setGp6(double gp6) {
		this.gp6 = gp6;
	}
	public Exigencia getExigenciaFase1() {
		return exigenciaFase1;
	}
	public void setExigenciaFase1(Exigencia exigenciaFase1) {
		this.exigenciaFase1 = exigenciaFase1;
	}
	public Exigencia getExigenciaFase2() {
		return exigenciaFase2;
	}
	public void setExigenciaFase2(Exigencia exigenciaFase2) {
		this.exigenciaFase2 = exigenciaFase2;
	}
	public Exigencia getExigenciaFase3() {
		return exigenciaFase3;
	}
	public void setExigenciaFase3(Exigencia exigenciaFase3) {
		this.exigenciaFase3 = exigenciaFase3;
	}
	public Exigencia getExigenciaFase4() {
		return exigenciaFase4;
	}
	public void setExigenciaFase4(Exigencia exigenciaFase4) {
		this.exigenciaFase4 = exigenciaFase4;
	}
	public Exigencia getExigenciaFase5() {
		return exigenciaFase5;
	}
	public void setExigenciaFase5(Exigencia exigenciaFase5) {
		this.exigenciaFase5 = exigenciaFase5;
	}
	public Exigencia getExigenciaFase6() {
		return exigenciaFase6;
	}
	public void setExigenciaFase6(Exigencia exigenciaFase6) {
		this.exigenciaFase6 = exigenciaFase6;
	}
	public double getPesoMedioAmostral() {
		return pesoMedioAmostral;
	}
	public void setPesoMedioAmostral(double pesoMedioAmostral) {
		this.pesoMedioAmostral = pesoMedioAmostral;
	}
	public double getGanhoPesoAmostral() {
		return ganhoPesoAmostral;
	}
	public void setGanhoPesoAmostral(double ganhoPesoAmostral) {
		this.ganhoPesoAmostral = ganhoPesoAmostral;
	}
	public double getConversaoAlimentarAmostral() {
		return conversaoAlimentarAmostral;
	}
	public void setConversaoAlimentarAmostral(double conversaoAlimentarAmostral) {
		this.conversaoAlimentarAmostral = conversaoAlimentarAmostral;
	}
	public double getConsumoAmostral() {
		return consumoAmostral;
	}
	public void setConsumoAmostral(double consumoAmostral) {
		this.consumoAmostral = consumoAmostral;
	}
	public double getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(double temperatura) {
		this.temperatura = temperatura;
	}
	public boolean isAtivaTemperatura() {
		return ativaTemperatura;
	}
	public void setAtivaTemperatura(boolean ativaTemperatura) {
		this.ativaTemperatura = ativaTemperatura;
	}
	public String getDias() {
		return dias;
	}
	public void setDias(String dias) {
		this.dias = dias;
	}
	public String getPpm() {
		return ppm;
	}
	public void setPpm(String ppm) {
		this.ppm = ppm;
	}
	public double getPesoMedio1Simulado() {
		return pesoMedio1Simulado;
	}
	public void setPesoMedio1Simulado(double pesoMedio1Simulado) {
		this.pesoMedio1Simulado = pesoMedio1Simulado;
	}
	public double getPesoMedio2Simulado() {
		return pesoMedio2Simulado;
	}
	public void setPesoMedio2Simulado(double pesoMedio2Simulado) {
		this.pesoMedio2Simulado = pesoMedio2Simulado;
	}
	public double getPesoMedio3Simulado() {
		return pesoMedio3Simulado;
	}
	public void setPesoMedio3Simulado(double pesoMedio3Simulado) {
		this.pesoMedio3Simulado = pesoMedio3Simulado;
	}
	public double getPesoMedio4Simulado() {
		return pesoMedio4Simulado;
	}
	public void setPesoMedio4Simulado(double pesoMedio4Simulado) {
		this.pesoMedio4Simulado = pesoMedio4Simulado;
	}
	public double getPesoMedio5Simulado() {
		return pesoMedio5Simulado;
	}
	public void setPesoMedio5Simulado(double pesoMedio5Simulado) {
		this.pesoMedio5Simulado = pesoMedio5Simulado;
	}
	public double getPesoMedio6Simulado() {
		return pesoMedio6Simulado;
	}
	public void setPesoMedio6Simulado(double pesoMedio6Simulado) {
		this.pesoMedio6Simulado = pesoMedio6Simulado;
	}
	public double getGanhoPeso1Simulado() {
		return ganhoPeso1Simulado;
	}
	public void setGanhoPeso1Simulado(double ganhoPeso1Simulado) {
		this.ganhoPeso1Simulado = ganhoPeso1Simulado;
	}
	public double getGanhoPeso2Simulado() {
		return ganhoPeso2Simulado;
	}
	public void setGanhoPeso2Simulado(double ganhoPeso2Simulado) {
		this.ganhoPeso2Simulado = ganhoPeso2Simulado;
	}
	public double getGanhoPeso3Simulado() {
		return ganhoPeso3Simulado;
	}
	public void setGanhoPeso3Simulado(double ganhoPeso3Simulado) {
		this.ganhoPeso3Simulado = ganhoPeso3Simulado;
	}
	public double getGanhoPeso4Simulado() {
		return ganhoPeso4Simulado;
	}
	public void setGanhoPeso4Simulado(double ganhoPeso4Simulado) {
		this.ganhoPeso4Simulado = ganhoPeso4Simulado;
	}
	public double getGanhoPeso5Simulado() {
		return ganhoPeso5Simulado;
	}
	public void setGanhoPeso5Simulado(double ganhoPeso5Simulado) {
		this.ganhoPeso5Simulado = ganhoPeso5Simulado;
	}
	public double getGanhoPeso6Simulado() {
		return ganhoPeso6Simulado;
	}
	public void setGanhoPeso6Simulado(double ganhoPeso6Simulado) {
		this.ganhoPeso6Simulado = ganhoPeso6Simulado;
	}
	public double getConsumo1Simulado() {
		return consumo1Simulado;
	}
	public void setConsumo1Simulado(double consumo1Simulado) {
		this.consumo1Simulado = consumo1Simulado;
	}
	public double getConsumo2Simulado() {
		return consumo2Simulado;
	}
	public void setConsumo2Simulado(double consumo2Simulado) {
		this.consumo2Simulado = consumo2Simulado;
	}
	public double getConsumo3Simulado() {
		return consumo3Simulado;
	}
	public void setConsumo3Simulado(double consumo3Simulado) {
		this.consumo3Simulado = consumo3Simulado;
	}
	public double getConsumo4Simulado() {
		return consumo4Simulado;
	}
	public void setConsumo4Simulado(double consumo4Simulado) {
		this.consumo4Simulado = consumo4Simulado;
	}
	public double getConsumo5Simulado() {
		return consumo5Simulado;
	}
	public void setConsumo5Simulado(double consumo5Simulado) {
		this.consumo5Simulado = consumo5Simulado;
	}
	public double getConsumo6Simulado() {
		return consumo6Simulado;
	}
	public void setConsumo6Simulado(double consumo6Simulado) {
		this.consumo6Simulado = consumo6Simulado;
	}
	public double getConversaoAlimentar1Simulado() {
		return conversaoAlimentar1Simulado;
	}
	public void setConversaoAlimentar1Simulado(double conversaoAlimentar1Simulado) {
		this.conversaoAlimentar1Simulado = conversaoAlimentar1Simulado;
	}
	public double getConversaoAlimentar2Simulado() {
		return conversaoAlimentar2Simulado;
	}
	public void setConversaoAlimentar2Simulado(double conversaoAlimentar2Simulado) {
		this.conversaoAlimentar2Simulado = conversaoAlimentar2Simulado;
	}
	public double getConversaoAlimentar3Simulado() {
		return conversaoAlimentar3Simulado;
	}
	public void setConversaoAlimentar3Simulado(double conversaoAlimentar3Simulado) {
		this.conversaoAlimentar3Simulado = conversaoAlimentar3Simulado;
	}
	public double getConversaoAlimentar4Simulado() {
		return conversaoAlimentar4Simulado;
	}
	public void setConversaoAlimentar4Simulado(double conversaoAlimentar4Simulado) {
		this.conversaoAlimentar4Simulado = conversaoAlimentar4Simulado;
	}
	public double getConversaoAlimentar5Simulado() {
		return conversaoAlimentar5Simulado;
	}
	public void setConversaoAlimentar5Simulado(double conversaoAlimentar5Simulado) {
		this.conversaoAlimentar5Simulado = conversaoAlimentar5Simulado;
	}
	public double getConversaoAlimentar6Simulado() {
		return conversaoAlimentar6Simulado;
	}
	public void setConversaoAlimentar6Simulado(double conversaoAlimentar6Simulado) {
		this.conversaoAlimentar6Simulado = conversaoAlimentar6Simulado;
	}
	public double getCr1Simulado() {
		return cr1Simulado;
	}
	public void setCr1Simulado(double cr1Simulado) {
		this.cr1Simulado = cr1Simulado;
	}
	public double getCr2Simulado() {
		return cr2Simulado;
	}
	public void setCr2Simulado(double cr2Simulado) {
		this.cr2Simulado = cr2Simulado;
	}
	public double getCr3Simulado() {
		return cr3Simulado;
	}
	public void setCr3Simulado(double cr3Simulado) {
		this.cr3Simulado = cr3Simulado;
	}
	public double getCr4Simulado() {
		return cr4Simulado;
	}
	public void setCr4Simulado(double cr4Simulado) {
		this.cr4Simulado = cr4Simulado;
	}
	public double getCr5Simulado() {
		return cr5Simulado;
	}
	public void setCr5Simulado(double cr5Simulado) {
		this.cr5Simulado = cr5Simulado;
	}
	public double getCr6Simulado() {
		return cr6Simulado;
	}
	public void setCr6Simulado(double cr6Simulado) {
		this.cr6Simulado = cr6Simulado;
	}
	public double getGp1Simulado() {
		return gp1Simulado;
	}
	public void setGp1Simulado(double gp1Simulado) {
		this.gp1Simulado = gp1Simulado;
	}
	public double getGp2Simulado() {
		return gp2Simulado;
	}
	public void setGp2Simulado(double gp2Simulado) {
		this.gp2Simulado = gp2Simulado;
	}
	public double getGp3Simulado() {
		return gp3Simulado;
	}
	public void setGp3Simulado(double gp3Simulado) {
		this.gp3Simulado = gp3Simulado;
	}
	public double getGp4Simulado() {
		return gp4Simulado;
	}
	public void setGp4Simulado(double gp4Simulado) {
		this.gp4Simulado = gp4Simulado;
	}
	public double getGp5Simulado() {
		return gp5Simulado;
	}
	public void setGp5Simulado(double gp5Simulado) {
		this.gp5Simulado = gp5Simulado;
	}
	public double getGp6Simulado() {
		return gp6Simulado;
	}
	public void setGp6Simulado(double gp6Simulado) {
		this.gp6Simulado = gp6Simulado;
	}
	public double getPesoMedioAmostralSimulado() {
		return pesoMedioAmostralSimulado;
	}
	public void setPesoMedioAmostralSimulado(double pesoMedioAmostralSimulado) {
		this.pesoMedioAmostralSimulado = pesoMedioAmostralSimulado;
	}
	public double getGanhoPesoAmostralSimulado() {
		return ganhoPesoAmostralSimulado;
	}
	public void setGanhoPesoAmostralSimulado(double ganhoPesoAmostralSimulado) {
		this.ganhoPesoAmostralSimulado = ganhoPesoAmostralSimulado;
	}
	public double getConversaoAlimentarAmostralSimulado() {
		return conversaoAlimentarAmostralSimulado;
	}
	public void setConversaoAlimentarAmostralSimulado(double conversaoAlimentarAmostralSimulado) {
		this.conversaoAlimentarAmostralSimulado = conversaoAlimentarAmostralSimulado;
	}
	public double getConsumoAmostralSimulado() {
		return consumoAmostralSimulado;
	}
	public void setConsumoAmostralSimulado(double consumoAmostralSimulado) {
		this.consumoAmostralSimulado = consumoAmostralSimulado;
	}
	public Exigencia getExigenciaFase1Simulada() {
		return exigenciaFase1Simulada;
	}
	public void setExigenciaFase1Simulada(Exigencia exigenciaFase1Simulada) {
		this.exigenciaFase1Simulada = exigenciaFase1Simulada;
	}
	public Exigencia getExigenciaFase2Simulada() {
		return exigenciaFase2Simulada;
	}
	public void setExigenciaFase2Simulada(Exigencia exigenciaFase2Simulada) {
		this.exigenciaFase2Simulada = exigenciaFase2Simulada;
	}
	public Exigencia getExigenciaFase3Simulada() {
		return exigenciaFase3Simulada;
	}
	public void setExigenciaFase3Simulada(Exigencia exigenciaFase3Simulada) {
		this.exigenciaFase3Simulada = exigenciaFase3Simulada;
	}
	public Exigencia getExigenciaFase4Simulada() {
		return exigenciaFase4Simulada;
	}
	public void setExigenciaFase4Simulada(Exigencia exigenciaFase4Simulada) {
		this.exigenciaFase4Simulada = exigenciaFase4Simulada;
	}
	public Exigencia getExigenciaFase5Simulada() {
		return exigenciaFase5Simulada;
	}
	public void setExigenciaFase5Simulada(Exigencia exigenciaFase5Simulada) {
		this.exigenciaFase5Simulada = exigenciaFase5Simulada;
	}
	public Exigencia getExigenciaFase6Simulada() {
		return exigenciaFase6Simulada;
	}
	public void setExigenciaFase6Simulada(Exigencia exigenciaFase6Simulada) {
		this.exigenciaFase6Simulada = exigenciaFase6Simulada;
	}
	public LineChartModel getLineModel1() {
		return lineModel1;
	}
	public void setLineModel1(LineChartModel lineModel1) {
		this.lineModel1 = lineModel1;
	}
	public LineChartModel getLineModel2() {
		return lineModel2;
	}
	public void setLineModel2(LineChartModel lineModel2) {
		this.lineModel2 = lineModel2;
	}
	public String[] getVariantes() {
		return variantes;
	}
	public void setVariantes(String[] variantes) {
		this.variantes = variantes;
	}
	




}
