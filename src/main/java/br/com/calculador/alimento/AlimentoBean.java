package br.com.calculador.alimento;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import javax.faces.context.FacesContext;


import Util.Mensagens;
import FiltrosBusca.AlimentoFilter;


@ManagedBean(name = "alimentoBean")
@SessionScoped
public class AlimentoBean {

	private final String CADASTRAR_ALIMENTO_JSF = "CadastrarAlimentos";
	private final String ATUALIZAR_ALIMENTO_JSF = "/admin/alimentos/PesquisarAlimentos";
	private final String ASSOCIACAO_ALIMENTO_JSF = "/admin/alimentos/AssociacaoAlimentos";
	private final String SIMULACAO_ENERGIA_JSF = "/cliente/simulador/SimuladorEnergiaAves";

	private Alimento alimento;
	private List<Alimento> lista;
	private Alimento alimentoSelecionado;

	private int qtdAlimentos;

	private AlimentoFilter alimentoFilter;

	public AlimentoBean() {
		this.alimento = new Alimento();
		this.qtdAlimentos = 0;
		this.alimentoFilter =  new AlimentoFilter();

	}

	/**
	 * @author alkxly
	 * @since 12 de setembro de 2015
	 * @version 1.0
	 * 
	 * Metódo responsável por chamar a regra de negócio que irá
	 * persistir o objeto alimento na base de dados.
	 * Pode ser chamado também se ouver alteração do alimento, redirecionando 
	 * para página de pesquisa.
	 */
	public String salvar() {
		AlimentoRN alimentoRN = new AlimentoRN();
		if(alimento.getId() != null){
			alimentoRN.atualizar(alimento);
			this.alimento = new Alimento();
			return ATUALIZAR_ALIMENTO_JSF;
		}else{
			alimentoRN.salvar(alimento);
			this.alimento = new Alimento();
			Mensagens.adicionarMensagemConfirmacao("Alimento cadastrado com sucesso.");
			return CADASTRAR_ALIMENTO_JSF;
		}	

	}
	
	public boolean habilataCodigo(){
		boolean habilitaCodigo = false;
		try{
		if(!alimento.getCodigo().equals(null)){
			habilitaCodigo = true;
		}
		}catch(NullPointerException e){
			
		}
		return habilitaCodigo;
	}

	/**
	 * @author alkxly
	 * @since 15 de setembro de 2015
	 * @version 1.0
	 * 
	 * Método resonsável por chamar a regra de negócio responsável
	 * por efetuar um delete na tabela.
	 */
	public void excluir() {
		AlimentoRN alimentoRN = new AlimentoRN();
		try {
			alimentoRN.excluir(this.alimentoSelecionado);
			Mensagens.adicionarMensagemConfirmacao("Alimento excluido com sucesso");

		} catch (Exception e) {
			Mensagens.adicionarMensagemConfirmacao("Não foi possivel excluir o alimento");
		}
		this.lista = alimentoRN.listar();
	}
	/**
	 * @author rodrigo
	 * 
	 */
	public void atualizar() {
		AlimentoRN alimentoRN = new AlimentoRN();
		alimentoRN.atualizar(alimento);
		this.alimento = null;
	}
	/**
	 * @author alkxly
	 * @return p[agina de cadastro de alimentos para alteração de alimento.
	 */
	public String editar() {
		return CADASTRAR_ALIMENTO_JSF;
	}
	/**
	 * @autor alkxyly
	 * @version 1.0 
	 * @return página de associação de aliento e nutrientes.
	 */
	public String associar() {
		//passa o objetoSelecionado na tabela para a tela de associação
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("alimento", this.alimentoSelecionado);
		return ASSOCIACAO_ALIMENTO_JSF;
	}
	/**
	 * @autor Rodrigo
	 * @version 1.0 
	 * @return página de simula��o de energia
	 */
	public String simuladorEnergia() {
		//passa o objetoSelecionado na tabela para a tela de associação
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("alimento", this.alimentoSelecionado);
		return SIMULACAO_ENERGIA_JSF;
	}

	/**
	 * @author alkxly
	 * @since 30 de setembro de 2015
	 * @version 1.0
	 * @param consulta - buscar de acordo com a consulta
	 * @return lista de resultados baseado na consulta informada.
	 */
	public List<String> sugerirAlimentos(String consulta){
		System.out.println(consulta);
		List<String> alimentoSugeridos = new ArrayList<String>();

		for (Alimento alimentos : this.lista) {
			if(alimentos.getNome().toLowerCase().startsWith(consulta.toLowerCase())){
				alimentoSugeridos.add(alimentos.getNome());
			}
		}
		return null;
	}

	/**
	 * @author alkxly
	 * @since 17 de outubro de 2015
	 * Efetua a pesquisa por código
	 * Direciona para a própria página de pesquisa de alimento.
	 * @return
	 */
	public void pesquisar(){
		AlimentoRN alimentoRN = new AlimentoRN();
		this.lista = alimentoRN.pesquisar(alimentoFilter);
		
	}
	
	public Alimento getAlimento() {
		return alimento;
	}

	public void setAlimento(Alimento alimento) {
		this.alimento = alimento;
	}

	public List<Alimento> getLista() {
		AlimentoRN alimentoRN = new AlimentoRN();
		lista = alimentoRN.pesquisar(alimentoFilter);
		return lista;
	}

	public void setLista(List<Alimento> lista) {
		this.lista = lista;
	}

	public Alimento getAlimentoSelecionado() {
		return alimentoSelecionado;
	}

	public void setAlimentoSelecionado(Alimento alimentoSelecionado) {
		this.alimentoSelecionado = alimentoSelecionado;
	}

	public int getQtdAlimentos() {
		AlimentoRN alimentoRN = new AlimentoRN();
		if(this.lista != null){
			this.lista = alimentoRN.listar();
			this.qtdAlimentos = this.lista.size();
		}else 
			qtdAlimentos = 0;

		return qtdAlimentos;
	}

	public void setQtdAlimentos(int qtdAlimentos) {
		this.qtdAlimentos = qtdAlimentos;
	}

	public AlimentoFilter getAlimentoFilter() {
		return alimentoFilter;
	}

	public void setAlimentoFilter(AlimentoFilter alimentoFilter) {
		this.alimentoFilter = alimentoFilter;
	}

}
