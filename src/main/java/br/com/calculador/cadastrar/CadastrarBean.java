package br.com.calculador.cadastrar;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import Endereco.Endereco;
import Util.Mensagens;
import br.com.calculador.usuario.Usuario;
import br.com.calculador.usuario.UsuarioRN;
import net.tanesha.recaptcha.ReCaptcha;
import net.tanesha.recaptcha.ReCaptchaFactory;

@ManagedBean
@SessionScoped
public class CadastrarBean {

	private String pessoa = new String();
	private Usuario usuario =  new Usuario();
	private String confirmarSenha;
	private Endereco endereco = new Endereco();
	private String tipo = new String();
	private int passo = 0;
	private boolean painel1 = true;
	private boolean painel2 = false;
	private boolean painel3 = false;
	private boolean painel4 = false;

	/**
	 * Realiza o cadastro
	 */
	public String cadastrar(){

		if(usuario.getSenha().equals(confirmarSenha)){

			UsuarioRN usuarioRN = new UsuarioRN();
			this.usuario.getPermissao().add("ROLE_USER");
			this.usuario.setEndereco(this.endereco);
			usuarioRN.salvar(this.usuario);
			return "login";
		}else
			return  "cadastrar";
	}


	/**
	 * Gera o código HTML gerado pelo componente da Google.
	 *
	 * @return String - código HTML dinâmico
	 */
	public String getRecaptcha() {
		ReCaptcha c = ReCaptchaFactory.newReCaptcha("6LcemgwUAAAAAINLLoVUDWZJ6SPm_AogK2aj8bbR", "6LcemgwUAAAAAKYvCqJsGNs98EQS05o4lFQWy3r2", false);

		return c.createRecaptchaHtml(null, null);
	}

	public void submit(ActionEvent event)
	{
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correct", "Correct");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	/**
	 * Realiza cadastro do usuário na parte 1 do formulário
	 */
	public void cadastroPasso2(){
		this.passo = 1;
		this.painel1 = false;
		this.painel2 = true;
		System.out.println(this.tipo);
	}


	/**
	 * Realiza cadastro de atributos na parte II do formulário
	 */
	public void cadastroPasso3(){
		this.passo = 2;
		this.painel1 = false;
		this.painel2 = false;
		this.painel3 = true;
	}

	public void cadastroPasso4(){
		this.passo = 3;
		this.painel1 = false;
		this.painel2 = false;
		this.painel3 = false;
		this.painel4 = true;
	}
	public void finalizar(){
//		UsuarioRN usuarioRN = new UsuarioRN();
//		usuario.setAtivo(true);
//		usuario.getPermissao().add("ROLE_USER");
//		Date dataAtual =  new Date();
//		usuario.setData_cadastro(dataAtual);
//		usuario.setEndereco(endereco);
//		usuarioRN.salvar(usuario);
//		usuario =  new Usuario();
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("visualizar.jsf");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void cadastrarUsuario(){
		UsuarioRN usuarioRN = new UsuarioRN();
		usuario.setAtivo(true);
		usuario.getPermissao().add("ROLE_USER");
		Date dataAtual =  new Date();
		usuario.setData_cadastro(dataAtual);
		usuario.setEndereco(endereco);
		usuarioRN.salvar(usuario);
		usuario =  new Usuario();
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("login.jsf");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public String getPessoa() {
		return pessoa;
	}

	public void setPessoa(String pessoa) {
		this.pessoa = pessoa;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getConfirmarSenha() {
		return confirmarSenha;
	}

	public void setConfirmarSenha(String confirmarSenha) {
		this.confirmarSenha = confirmarSenha;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}


	public int getPasso() {
		return passo;
	}


	public void setPasso(int passo) {
		this.passo = passo;
	}


	public boolean isPainel1() {
		return painel1;
	}


	public void setPainel1(boolean painel1) {
		this.painel1 = painel1;
	}


	public boolean isPainel2() {
		return painel2;
	}


	public void setPainel2(boolean painel2) {
		this.painel2 = painel2;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public boolean isPainel3() {
		return painel3;
	}


	public void setPainel3(boolean painel3) {
		this.painel3 = painel3;
	}


	public boolean isPainel4() {
		return painel4;
	}


	public void setPainel4(boolean painel4) {
		this.painel4 = painel4;
	}



}
