package Nutriente;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import FiltrosBusca.NutrienteFilter;
import br.com.calculador.alimento.Alimento;


public class NutrienteDAOHibernate  implements NutrienteDAO{
	private Session session;
	
	
	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public void salvar(Nutriente nutriente) {
		this.session.save(nutriente);
	}

	@Override
	public void excluir(Nutriente nutriente) {
		this.session.delete(nutriente);
	}

	@Override
	public void atualizar(Nutriente nutriente) {
		this.session.update(nutriente);		
	}

	@Override
	public Nutriente carregar(Integer id) {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Nutriente> listar() {
		List<Nutriente> lista = null;
		Query querySql = this.session.createSQLQuery("select * from tb_nutrientes order by codigo").addEntity(Nutriente.class);
		lista =  querySql.list();
		return lista;
		//		return this.session.createCriteria(Nutriente.class).list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Nutriente> listarNaoAssociados(Alimento alimento){
		List<Nutriente> nutrientesNaoAssociados = null;
		Query querySql = this.session.createSQLQuery("select * from tb_nutrientes as n where n.id NOT IN(select an.nutriente_id from tb_alimento_nutriente as an where an.alimento_id = :idAlimento)").addEntity(Nutriente.class).setParameter("idAlimento",alimento.getId());
		nutrientesNaoAssociados = querySql.list();
		return nutrientesNaoAssociados;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Nutriente> listarPorCodigo(String codigo) {
		Query hql = this.session.createQuery("from Nutriente as nutriente where nutriente.codigo like :codigo");
		hql.setParameter("codigo",codigo+"%");
		List<Nutriente> lista=  hql.list();
		return lista;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Nutriente> pesquisar(NutrienteFilter nutrienteFilter) {
		Criteria criteria = this.session.createCriteria(Nutriente.class);
		
		if(StringUtils.isNotBlank(nutrienteFilter.getCodigo())){
			criteria.add(Restrictions.eq("codigo",nutrienteFilter.getCodigo()));
		}
		if(StringUtils.isNotBlank(nutrienteFilter.getNome())){
			criteria.add(Restrictions.ilike("nome",nutrienteFilter.getNome(),MatchMode.ANYWHERE));
		}
		return criteria.addOrder(Order.asc("codigo")).list();
	}

	

}
