package br.com.calculador.usuario;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import br.com.calculador.alimento.Alimento;

public class UsuarioDAOHibernate implements UsuarioDAO {
	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}
	@Override
	public void salvar(Usuario usuario) {
		this.session.save(usuario);
		
	}

	@Override
	public void excluir(Usuario usuario) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void atualizar(Usuario usuario) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Usuario carregar(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Usuario> listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Usuario usuarioPorLogin(String login) {
		Query hql = this.session.createQuery("from Usuario as usuario where usuario.login = :login");
		hql.setParameter("login",login);
		return (Usuario) hql.uniqueResult();
	}

}
