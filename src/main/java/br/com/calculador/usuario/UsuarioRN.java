package br.com.calculador.usuario;

import Util.DAOFactory;

public class UsuarioRN {
	
	private UsuarioDAO usuarioDAO;
	
	public UsuarioRN(){
		this.usuarioDAO = DAOFactory.criarUsuario();
	}
	/**
	 * Busca usuário por login
	 * @param login
	 * @return
	 */
	public Usuario usuarioPorLogin(String login){
		return usuarioDAO.usuarioPorLogin(login);
	}
	/**
	 * Salva o usuário
	 * @param usuario
	 */
	public void salvar(Usuario usuario){
		this.usuarioDAO.salvar(usuario);
	}
}
