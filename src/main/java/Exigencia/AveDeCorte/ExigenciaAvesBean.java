package Exigencia.AveDeCorte;


import java.math.BigDecimal;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import Util.Mensagens;


@ManagedBean(name = "exigenciaCorteBean")
@SessionScoped
public class ExigenciaAvesBean { 
	
	//
	private int dias;
	private Curva curvaPadrao;
	private Curva curvaSimulada;

	private ExigenciaAves exigenciaAves;

	public ExigenciaAvesBean() {
		this.exigenciaAves = new ExigenciaAves();
		this.exigenciaAves.setFasesDinamico(exigenciaAves.getFasesPadrao());
	}
	
	
	public void destribuiFases(){
		//Dia tem que ser maior de 43 dias
		
		int[][] fases = exigenciaAves.getFasesPadrao();
		
		this.dias = exigenciaAves.getIdadeAbate(); 
				
		if((dias > 43)&&(dias<56)){
			
			System.out.println("Periodo abaixo de 56.");
			fases = exigenciaAves.getFasesPadrao();
			fases[4][1] = dias;
			exigenciaAves.setFasesDinamico(fases); 	
		}else if(dias == 56){
			System.out.println("O periodo padrao foi determinado.");
			fases = exigenciaAves.getFasesPadrao();
			
		}else{	
			//Enviar mensagem de tervalo inv�lido.
			System.out.println("Dia est� fora do periodo.");
		}
		
		for(int i=0; i<5;i++){
			for(int b=0; b<2;b++){
				
				System.out.println(fases[i][b]);
			}
			System.out.println(" ");
		}

	}
	
	public void ajustaFases(int fase) {

		System.out.println("Ativo fase " + fase);

		int[][] fases = exigenciaAves.getFasesDinamico();

		if (fase == 1) {
			if (fases[0][0] < fases[0][1] && fases[0][1] < fases[1][1]) {
				fases[1][0] = fases[0][1] + 1;
				exigenciaAves.setFasesDinamico(fases);
			} else {
				Mensagens.adicionarMensagemErro("N�o est� dentro do intervalo de " + fases[0][0] + " a " + fases[1][1]);
			}
		} else if (fase == 2) {
			if (fases[1][0] < fases[1][1] && fases[1][1] < fases[2][1]) {
				fases[2][0] = fases[1][1] + 1;
				exigenciaAves.setFasesDinamico(fases);
			} else {
				Mensagens.adicionarMensagemErro("N�o est� dentro do intervalo de " + fases[1][0] + " a " + fases[2][1]);
			}
		} else if (fase == 3) {
			if (fases[2][0] < fases[2][1] && fases[2][1] < fases[3][1]) {
				fases[3][0] = fases[2][1] + 1;
				exigenciaAves.setFasesDinamico(fases);
			} else {
				Mensagens.adicionarMensagemErro("N�o est� dentro do intervalo de " + fases[2][0] + " a " + fases[3][1]);
			}
		} else if (fase == 4) {
			if (fases[3][0] < fases[3][1] && fases[3][1] < fases[4][1]) {
				fases[4][0] = fases[3][1] + 1;
				exigenciaAves.setFasesDinamico(fases);
			} else {
				Mensagens.adicionarMensagemErro("N�o est� dentro do intervalo de " + fases[3][0] + " a " + fases[4][1]);
			}
		}
	}
	
	public Double[] calcularConversaoAlimentar(Double[] consumoRacao,Double[] ganhoPeso){
		
		Double[] conversaoAlimentar = new Double[56];
		
		for(int i = 0 ; i <56 ; i++){
			conversaoAlimentar[i] = consumoRacao[i]/ganhoPeso[i];
		}
		
		return conversaoAlimentar;
	}
	
	

	public Curva[] calcularCurvaPadrao(Double[] consumoRacao,Double[] ganhoPeso, Double[] gompertzPesoVivoAcumulado,Double[] gompertzGanhoPesoAcumulado) {
		
		Curva[] curvaPadrao = new Curva[56];
		
		System.out.println(" 1 disparou");
		 for(Double GGP:gompertzGanhoPesoAcumulado){
	        	System.out.println(GGP);
	        }
		
		
		
//		curvaPadrao[0].setConsumoRacao(consumoRacao[0]);
//		System.out.println("1");
//		curvaPadrao[0].setGanhoPeso(ganhoPeso[0]);
//		System.out.println("2");
		//curvaPadrao[0].setPesoAcumulado(gompertzPesoVivoAcumulado[0]);
        //System.out.println("3");
        //curvaPadrao[0].setConversaoAlimentar(gompertzGanhoPesoAcumulado[0]/gompertzPesoVivoAcumulado[0]);
        //System.out.println("4");
        //curvaPadrao[0].setConsumoRacaoAcumulado(gompertzGanhoPesoAcumulado[0]);
		
		System.out.println(" 2 disparou");
		for(int i = 0 ; i<56; i++){
			
		   //curvaPadrao[i].setConsumoRacao(consumoRacao[i]);
		   curvaPadrao[1].setGanhoPeso(7.0);
		  // curvaPadrao[i].setPesoAcumulado(gompertzPesoVivoAcumulado[i]);
		  // curvaPadrao[i].setConversaoAlimentar(gompertzGanhoPesoAcumulado[i]/gompertzPesoVivoAcumulado[i]);
		  // curvaPadrao[i].setConsumoRacaoAcumulado(gompertzGanhoPesoAcumulado[i-1]+consumoRacao[i]);
		   
		   System.out.println(" 1 disparou repeti��o"+ i);
			
		}

      return curvaPadrao;
	}
	
	public Curva[] calcularCurvaSimulada(Curva[] curvaPadrao) {
		
		Curva[] curvaSimulada = new Curva[curvaPadrao.length];
		
		Double pesoAbateTabelado = curvaPadrao[exigenciaAves.getIdadeAbate()].getPesoAcumulado();
		Double consumoTabelado = curvaPadrao[exigenciaAves.getIdadeAbate()].getConsumoRacaoAcumulado();
		Double ganhoPesoDiarioTabelado = pesoAbateTabelado / exigenciaAves.getIdadeAbate();
		Double pesoAbateDesejado =  exigenciaAves.getAbate()*exigenciaAves.getGanhoPesoDiario();
		Double consumoDesejado = pesoAbateDesejado * exigenciaAves.getConsumoAcumulado();
		Double ganhoPesoDiarioSimulado = pesoAbateDesejado / exigenciaAves.getIdadeAbate();
		Double pesoAbateDiferenca = (pesoAbateDesejado*100)/ pesoAbateTabelado;
		Double consumoDiferenca = (consumoDesejado*100)/consumoTabelado;
		Double ganhoPesoDiferenca = (ganhoPesoDiarioSimulado*100)/ganhoPesoDiarioTabelado;
		
		curvaSimulada[0].setConsumoRacao(curvaPadrao[0].getConsumoRacao()*consumoDiferenca);
		curvaSimulada[0].setGanhoPeso(curvaPadrao[0].getGanhoPeso()*ganhoPesoDiferenca);
		curvaSimulada[0].setPesoAcumulado(curvaPadrao[0].getPesoAcumulado()*pesoAbateDiferenca);
		curvaSimulada[0].setConsumoRacaoAcumulado(curvaSimulada[0].getConsumoRacao());
		curvaSimulada[0].setConversaoAlimentar(curvaSimulada[0].getConsumoRacaoAcumulado()/curvaSimulada[0].getPesoAcumulado());
		
		for(int i = 0 ; i<curvaPadrao.length;i++){
			
			curvaSimulada[i].setConsumoRacao(curvaPadrao[i].getConsumoRacao()*consumoDiferenca);
			curvaSimulada[i].setGanhoPeso(curvaPadrao[i].getGanhoPeso()*ganhoPesoDiferenca);
			curvaSimulada[i].setPesoAcumulado(curvaPadrao[i].getPesoAcumulado()*pesoAbateDiferenca);
			curvaSimulada[i].setConsumoRacaoAcumulado(curvaSimulada[i].getConsumoRacao());
			curvaSimulada[i].setConversaoAlimentar(curvaSimulada[i].getConsumoRacao()+curvaSimulada[i-1].getConsumoRacao());
			
		}

      return curvaSimulada;
	}
	
	public Double[] gompertzPesoVivo(Double pesoMaturidade,Double taxaMaturidade,Double idade, Double idadeCrescimentoMaximo,Double exp){
		
		Double[] gompertzPeso = new Double[57];
		
		for(int i = 0; i<=56; i++){
			//gompertzPeso[i] = pesoMaturidade*Math.pow(exp,-1* Math.pow(exp, ((-taxaMaturidade)*(idade-idadeCrescimentoMaximo))));
			gompertzPeso[i] = pesoMaturidade*Math.pow(exp,-1* Math.pow(exp, ((-taxaMaturidade)*(i-idadeCrescimentoMaximo))));
		}
		return gompertzPeso;
	}
	
    public Double[] gompertzGanhoPeso(Double pesoMaturidade,Double taxaMaturidade,Double idade, Double idadeCrescimentoMaximo,Double exp){
    	
    	Double[] gompertzGanho = new Double[57];
    	
    	for(int i = 0; i<=56; i++){
    		//pesoMaturidade*taxaMaturidade* Math.pow(exp, (((-taxaMaturidade)*(idade-idadeCrescimentoMaximo)) - Math.pow(exp, ((-taxaMaturidade)*(idade-idadeCrescimentoMaximo)))));
    		gompertzGanho[i] = pesoMaturidade*taxaMaturidade* Math.pow(exp, (((-taxaMaturidade)*(i-idadeCrescimentoMaximo)) - Math.pow(exp, ((-taxaMaturidade)*(i-idadeCrescimentoMaximo)))));
    	}
		return gompertzGanho;
	}
	
 

	public ExigenciaAves getExigenciaAves() {
		return exigenciaAves;
	}

	public void setExigenciaAves(ExigenciaAves exigenciaAves) {
		this.exigenciaAves = exigenciaAves;
	}

	public int getDias() {
		return dias;
	}

	public void setDias(int dias) {
		this.dias = dias;
	}

}
