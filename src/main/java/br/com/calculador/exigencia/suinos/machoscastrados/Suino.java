package br.com.calculador.exigencia.suinos.machoscastrados;

import java.io.Serializable;

public class Suino implements Serializable {
	
	private static final long serialVersionUID = 1900063570412597347L;
	private Integer id;
	private double pesoMedio;
	private double ganhoPeso;
	private double consumoRacao;
	private double energeriaMet;
	private double temperatura;
	private int desempenho;
	
	
	
	public Suino() {
		this.pesoMedio = 0.0;
		this.ganhoPeso = 0.0;
		this.consumoRacao = 0.0;
		this.energeriaMet = 0.0;
		this.desempenho = 0;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public double getPesoMedio() {
		return pesoMedio;
	}
	public void setPesoMedio(double pesoMedio) {
		this.pesoMedio = pesoMedio;
	}
	public double getGanhoPeso() {
		return ganhoPeso;
	}
	public void setGanhoPeso(double ganhoPeso) {
		this.ganhoPeso = ganhoPeso;
	}
	public double getConsumoRacao() {
		return consumoRacao;
	}
	public void setConsumoRacao(double consumoRacao) {
		this.consumoRacao = consumoRacao;
	}
	public double getEnergeriaMet() {
		return energeriaMet;
	}
	public void setEnergeriaMet(double energeriaMet) {
		this.energeriaMet = energeriaMet;
	}
	public int getDesempenho() {
		return desempenho;
	}
	public void setDesempenho(int desempenho) {
		this.desempenho = desempenho;
	}
	public double getTemperatura() {
		return temperatura;
	}
	public void setTemperatura(double temperatura) {
		this.temperatura = temperatura;
	}
	@Override
	public String toString() {
		return "Suino [id=" + id + ", pesoMedio=" + pesoMedio + ", ganhoPeso=" + ganhoPeso + ", consumoRacao="
				+ consumoRacao + ", energeriaMet=" + energeriaMet + ", temperatura=" + temperatura + ", desempenho="
				+ desempenho + "]";
	}

	
	
}
