package AlimentoNutrientes;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import Nutriente.Nutriente;
import br.com.calculador.alimento.Alimento;

/**
 *
 * @author Rodrigo
 * @author alkxyly
 * @since 19 de setembro de 2015
 * @version 1.1
 * 
 */
@Entity
@Table(name ="tb_alimento_nutriente")
public class AlimentoNutriente implements Serializable {

	private static final long serialVersionUID = 1277584792078058767L;

	@Id
	@GeneratedValue
	private Integer id;
	
	private double valorNutritivo;
	private int unidade;
	
	@ManyToOne  
	@JoinColumn(name="alimento_id")
	private Alimento alimento;
	
	@ManyToOne  
	@JoinColumn(name="nutriente_id")
	private Nutriente nutriente;

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Alimento getAlimento() {
		return alimento;
	}

	public void setAlimento(Alimento alimento) {
		this.alimento = alimento;
	}

	
	public Nutriente getNutriente() {
		return nutriente;
	}

	public void setNutriente(Nutriente nutriente) {
		this.nutriente = nutriente;
	}

	public double getValorNutritivo() {
		return valorNutritivo;
	}

	public void setValorNutritivo(double valorNutritivo) {
		this.valorNutritivo = valorNutritivo;
	}

	public int getUnidade() {
		return unidade;
	}

	public void setUnidade(int unidade) {
		this.unidade = unidade;
	}


	

}
