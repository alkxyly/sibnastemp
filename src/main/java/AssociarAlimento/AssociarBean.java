package AssociarAlimento;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import AlimentoNutrientes.AlimentoNutriente;
import AlimentoNutrientes.AlimentoNutrienteRN;
import FiltrosBusca.NutrienteFilter;
import Nutriente.Nutriente;
import Nutriente.NutrienteRN;
import Util.Mensagens;
import br.com.calculador.alimento.Alimento;
import relatorio.RelatorioUtil;

@ManagedBean(name = "associarBean")
@ViewScoped
public class AssociarBean implements Serializable{

	private static final long serialVersionUID = 7159722609927260753L;

	private final String PESQUISA_ALIMENTO_JSF = "/admin/alimentos/PesquisarAlimentos";

	private Alimento alimento;
	private List<Nutriente> listaNutriente;
	private int qtd_nutrientes;
	private String associado;
	private NutrienteFilter nutrienteFilter =  new NutrienteFilter();
	private boolean ativaAjax  = true;

	private List<AlimentoNutriente> listaAlteracao =  new ArrayList<>();
	private List<AlimentoNutriente> listaAssociar = new ArrayList<>();
	
	private Nutriente nutrienteSelecionado;

	private boolean associarLista;
	private boolean nAssociarLista;
	private boolean mostrarCheck;
	
	List<Nutriente> listaExcluir = new ArrayList<Nutriente>();

	@PostConstruct
	public void init() {
		this.alimento = (Alimento) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("alimento");
		NutrienteRN nutrienteRN = new NutrienteRN();
		List<Nutriente> naoAssociados = new ArrayList<Nutriente>();
		naoAssociados =	nutrienteRN.listarNaoAssociados(this.alimento);		
		nutrienteRN.listar();

		associado = this.alimento.getAlimentoNutriente().size() > 0 ? "Alterar" : "Associar"; // alterar o nome do botao dinamicamente
		ativaAjax =  associado.equalsIgnoreCase("alterar") ? false : true; // ativa o ajax somente quando for alterar


		if(this.alimento.getAlimentoNutriente().size() > 0){//verifica se o alimento esta assiciado
			this.mostrarCheck = true;
			this.listaNutriente = new ArrayList<Nutriente>();
			for (int i = 0; i < this.alimento.getAlimentoNutriente().size(); i++) {
				Nutriente n = this.alimento.getAlimentoNutriente().get(i).getNutriente();
				double valorNutritivo = this.alimento.getAlimentoNutriente().get(i).getValorNutritivo();
				int uni = this.alimento.getAlimentoNutriente().get(i).getUnidade();
				int codAssociacao = this.alimento.getAlimentoNutriente().get(i).getId();
				
				n.setUnidade(uni);
				n.setValor_nutritivo(valorNutritivo);
				n.setCodAssociacao(codAssociacao);
				this.listaNutriente.add(n);
			}
			for (int i = 0; i < naoAssociados.size(); i++) {
				Nutriente naoAssociado = naoAssociados.get(i);			
				naoAssociado.setUnidade(0);
				naoAssociado.setValor_nutritivo(0.0);
				this.listaNutriente.add(naoAssociado);
			}

		}else{
			this.listaNutriente = nutrienteRN.listar();	
			this.mostrarCheck = false;
		}	
		this.qtd_nutrientes = 0;
	}
	/**
	 * @author alkxyly
	 * 
	 * Realiza chamada a regra de negocio 
	 * referente a fazer a lógica de associação
	 * @return página de pesquisa de alimento
	 */
	public String associar(){
		//conterá todos os nutrientes que tiveram o valor nutritivo alterado.
		AlimentoNutrienteRN alimentoNutrienteRN =  new AlimentoNutrienteRN();
		List<AlimentoNutriente> alimentoNutrientes =  new ArrayList<AlimentoNutriente>();

		if(associado.equalsIgnoreCase("associar")){
			for (int i = 0; i < listaNutriente.size(); i++) { //criar uma RN 
				AlimentoNutriente alimentoNutriente = null;		
				if(listaNutriente.get(i).getValor_nutritivo() != 0 ){// campo alterado				
					alimentoNutriente = new AlimentoNutriente();
					Nutriente nutriente  = this.listaNutriente.get(i);
					double valorNutritivo = this.listaNutriente.get(i).getValor_nutritivo();
					int unidade = this.listaNutriente.get(i).getUnidade();

					alimentoNutriente.setAlimento(this.alimento);
					alimentoNutriente.setNutriente(nutriente);
					alimentoNutriente.setValorNutritivo(valorNutritivo);
					alimentoNutriente.setUnidade(unidade);

					alimentoNutrientes.add(alimentoNutriente);
				}
			}
			//criar uma RN para isso
			for (int i = 0; i <alimentoNutrientes.size(); i++) {
				alimentoNutrienteRN.salvar(alimentoNutrientes.get(i));
			}

		}else{
			AlimentoNutrienteRN al = new AlimentoNutrienteRN();
			System.out.println("ALTERACAO");
			System.out.println(listaAlteracao.size());
			for (int i = 0; i <listaAlteracao.size(); i++) {
				System.out.println(listaAlteracao.get(i).getId());
				System.out.println(listaAlteracao.get(i).getValorNutritivo());
				System.out.println(listaAlteracao.get(i).getUnidade());
				al.atualizar(listaAlteracao.get(i));
			}
			// Salva no banco as associacoes novas na alteracao de associacões
			for (int i = 0; i <listaAssociar.size(); i++) {
				al.salvar(listaAssociar.get(i));
			}

		}

		return PESQUISA_ALIMENTO_JSF;
	}
	/**
	 * 
	 * @return
	 */
	public String excluir(){
		AlimentoNutrienteRN alimentoNutrienteRN = new AlimentoNutrienteRN();
		if(listaExcluir.size() > 0 ){
			for (int i = 0; i < listaExcluir.size(); i++) {
				alimentoNutrienteRN.excluirID(listaExcluir.get(i).getCodAssociacao());
			}
			return PESQUISA_ALIMENTO_JSF;
		}else { 
			System.out.println("Nenhum Nutriente foi selecionado");
			Mensagens.adicionarMensagemErro("Não é possivel excluir, nenhum nutriente foi selecionado");
			return null;
		}
		
	}
	public void excluirNutrientes(Nutriente nutriente){
		if(nutriente.getCodAssociacao() != null){
			if(nutriente.isSelecionou()){
					listaExcluir.add(nutriente);
					System.out.println("Clicou "+listaExcluir.size());
			}else if(nutriente.isSelecionou() == false){
				for (int i = 0; i < listaExcluir.size(); i++) {
					if(nutriente.getCodAssociacao() == listaExcluir.get(i).getCodAssociacao()){
						listaExcluir.remove(i);
						break;
					}
				}
				
				System.out.println("Desclicou "+listaExcluir.size());
				
			}
			
		}
	}
	/**
	 * @author alkxyly
	 * Realiza alteração verificndo se o mesmo está associado ou não
	 * @param nutriente
	 */
	public void verificarAlteracao(Nutriente nutriente){

		AlimentoNutrienteRN al = new AlimentoNutrienteRN();

		AlimentoNutriente alNutriente = al.verificaAlteracaoAssociacao(this.alimento, nutriente);

		if(alNutriente != null){
			System.out.println("Associado");
			System.out.println(nutriente.getUnidade());
			double valorAtual = nutriente.getValor_nutritivo();
			double valorAnterior = alNutriente.getValorNutritivo();
			int unidadeAnterior = alNutriente.getUnidade();
			int unidadeAtual = nutriente.getUnidade();
			System.out.println("unidade Anterior "+unidadeAnterior);
			System.out.println("unidade Atual "+unidadeAtual);
			if(valorAtual != valorAnterior || unidadeAnterior != unidadeAtual){
				alNutriente.setValorNutritivo(valorAtual);
				alNutriente.setUnidade(nutriente.getUnidade());
				boolean inserir = true;
				int pos = -1;

				//Solucao paliativa para nao inserir duplicado
				for (int i = 0; i < listaAlteracao.size(); i++) {
					if(alNutriente.getId() == listaAlteracao.get(i).getId()){
						inserir = false;
						pos = i;
					}
				}

				//Solucao paliativa para nao inserir duplicado
				if(inserir){					
					listaAlteracao.add(alNutriente);			
				}else{
					listaAlteracao.remove(pos);
					listaAlteracao.add(pos,alNutriente);
				}
								System.out.println("ID da associacao "+alNutriente.getId());
								System.out.println("HOuve Alteracao");
								System.out.println("Valor Atual "+valorAtual);
								System.out.println("Valor Anterior "+valorAnterior);
			}else System.out.println("Não HOuve alteracao");

		}else{
			System.out.println("Não está associado");
			System.out.println("Alimento : "+alimento.getCodigo());
			System.out.println("Código do Nutriente : "+nutriente.getId());
			System.out.println("Nutriente : "+nutriente.getNome());
			System.out.println("Valor nutritivo : "+nutriente.getValor_nutritivo());
			System.out.println("Unidade : "+nutriente.getUnidade());

			AlimentoNutriente ali = new AlimentoNutriente();
			ali.setAlimento(alimento);
			ali.setNutriente(nutriente);
			ali.setUnidade(nutriente.getUnidade());
			ali.setValorNutritivo(nutriente.getValor_nutritivo());

			if(ali.getValorNutritivo() != 0){
				System.out.println("Hove alteracao nova associacao sera criada");
				boolean inserir = true;
				int pos = -1;

				//Solucao paliativa para nao inserir duplicado
				for (int i = 0; i < listaAssociar.size(); i++) {
					if(ali.getNutriente().getId() == listaAssociar.get(i).getNutriente().getId()){
						inserir = false;
						pos = i;
					}
				}

				//Solucao paliativa para nao inserir duplicado
				if(inserir){					
					listaAssociar.add(ali);		
					System.out.println("Adicionou");
				}else{
					listaAssociar.remove(pos);
					listaAssociar.add(pos,ali);
				}
			}

		}

	}
	/**
	 * @author alkxyly
	 * Metodo responsável por adicionar a tabela por requisição ajax os alimentos 
	 * associados.
	 */
	public void listarAssociado(){
		AlimentoNutrienteRN alimentoNutrienteRN = new AlimentoNutrienteRN();
		NutrienteRN nutrienteRN = new NutrienteRN();

		this.listaNutriente = new ArrayList<Nutriente>();
		List<Nutriente> naoAssociados = new ArrayList<Nutriente>();
		naoAssociados =	nutrienteRN.listarNaoAssociados(this.alimento);
		if(associarLista){
			for (int i = 0; i < this.alimento.getAlimentoNutriente().size(); i++) {
				Nutriente n = this.alimento.getAlimentoNutriente().get(i).getNutriente();
				double valorNutritivo = this.alimento.getAlimentoNutriente().get(i).getValorNutritivo();
				int uni = this.alimento.getAlimentoNutriente().get(i).getUnidade();
				n.setUnidade(uni);
				n.setValor_nutritivo(valorNutritivo);
				this.listaNutriente.add(n);
			}
		}else{
			for (int i = 0; i < this.alimento.getAlimentoNutriente().size(); i++) {
				Nutriente n = this.alimento.getAlimentoNutriente().get(i).getNutriente();
				double valorNutritivo = this.alimento.getAlimentoNutriente().get(i).getValorNutritivo();
				int uni = this.alimento.getAlimentoNutriente().get(i).getUnidade();
				n.setUnidade(uni);
				n.setValor_nutritivo(valorNutritivo);
				this.listaNutriente.add(n);
			}
			for (int i = 0; i < naoAssociados.size(); i++) {
				Nutriente naoAssociado = naoAssociados.get(i);			
				naoAssociado.setUnidade(0);
				naoAssociado.setValor_nutritivo(0.0);
				this.listaNutriente.add(naoAssociado);
			}
		}	

	}
	/**
	 * @author alkxyly
	 * 
	 * Realiza a atualização da tabela com de acordo com o 
	 * checkbox para listar os nutrientes nao associados
	 * caso desmarcado ele retorna todos os nutrientes 
	 * associados e nao associados
	 * 
	 */
	public void listarNaoAssociado(){
		this.listaNutriente = new ArrayList<Nutriente>();
		NutrienteRN nutrienteRN = new NutrienteRN();
		List<Nutriente> naoAssociados = new ArrayList<Nutriente>();
		naoAssociados =	nutrienteRN.listarNaoAssociados(this.alimento);
		if(nAssociarLista){
			for (int i = 0; i < naoAssociados.size(); i++) {
				Nutriente naoAssociado = naoAssociados.get(i);			
				naoAssociado.setUnidade(0);
				naoAssociado.setValor_nutritivo(0.0);
				this.listaNutriente.add(naoAssociado);
			}
		}else{
			for (int i = 0; i < this.alimento.getAlimentoNutriente().size(); i++) {
				Nutriente n = this.alimento.getAlimentoNutriente().get(i).getNutriente();
				double valorNutritivo = this.alimento.getAlimentoNutriente().get(i).getValorNutritivo();
				int uni = this.alimento.getAlimentoNutriente().get(i).getUnidade();
				n.setUnidade(uni);
				n.setValor_nutritivo(valorNutritivo);
				this.listaNutriente.add(n);
			}
			for (int i = 0; i < naoAssociados.size(); i++) {
				Nutriente naoAssociado = naoAssociados.get(i);			
				naoAssociado.setUnidade(0);
				naoAssociado.setValor_nutritivo(0.0);
				this.listaNutriente.add(naoAssociado);
			}

		}

	}

	
	public Alimento getAlimento() {
		return alimento;
	}

	public void setAlimento(Alimento alimento) {
		this.alimento = alimento;
	}
	public List<Nutriente> getListaNutriente() {
		return listaNutriente;
	}
	public void setListaNutriente(List<Nutriente> listaNutriente) {
		this.listaNutriente = listaNutriente;
	}

	public int getQtd_nutrientes() {
		this.qtd_nutrientes = this.listaNutriente.size();
		return qtd_nutrientes;
	}
	public void setQtd_nutrientes(int qtd_nutrientes) {
		this.qtd_nutrientes = qtd_nutrientes;
	}
	public NutrienteFilter getNutrienteFilter() {
		return nutrienteFilter;
	}
	public void setNutrienteFilter(NutrienteFilter nutrienteFilter) {
		this.nutrienteFilter = nutrienteFilter;
	}
	public String getAssociado() {
		return associado;
	}
	public void setAssociado(String associado) {
		this.associado = associado;
	}
	public boolean isAtivaAjax() {
		return ativaAjax;
	}
	public void setAtivaAjax(boolean ativaAjax) {
		this.ativaAjax = ativaAjax;
	}
	public boolean isAssociarLista() {
		return associarLista;
	}
	public void setAssociarLista(boolean associarLista) {
		this.associarLista = associarLista;
	}
	public boolean isnAssociarLista() {
		return nAssociarLista;
	}
	public void setnAssociarLista(boolean nAssociarLista) {
		this.nAssociarLista = nAssociarLista;
	}
	public boolean isMostrarCheck() {
		return mostrarCheck;
	}
	public void setMostrarCheck(boolean mostrarCheck) {
		this.mostrarCheck = mostrarCheck;
	}
	public Nutriente getNutrienteSelecionado() {
		return nutrienteSelecionado;
	}
	public void setNutrienteSelecionado(Nutriente nutrienteSelecionado) {
		this.nutrienteSelecionado = nutrienteSelecionado;
	}
	
	


}
