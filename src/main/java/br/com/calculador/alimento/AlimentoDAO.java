package br.com.calculador.alimento;

import java.util.List;

import FiltrosBusca.AlimentoFilter;

public interface AlimentoDAO {
	public void salvar(Alimento alimento);
	public void excluir(Alimento alimento);
	public void atualizar(Alimento alimento);
	public Alimento carregar(Integer id);
	public List<Alimento> listar();
	public List<Alimento> listarPorCodigo(String codigo);
	public List<Alimento> pesquisar(AlimentoFilter alimentoFilter);

}
