package br.com.calculador.exigencia.suinos.machoscastrados;

import Util.Constantes;
import br.com.calulador.exigencia.Exigencia;

public class SuinosRN {
	/**
	 * Calcula energia metabolizavel
	 * @param suino
	 * @return
	 */
	public double energiaMetabolizavel(Suino suino){
		double em = 0.0;
		em = (106 * Math.pow(suino.getPesoMedio(),0.75) + (2182.6 + 70.8863 * suino.getPesoMedio() - 0.2026 * Math.pow(suino.getPesoMedio(),2 )) * suino.getGanhoPeso());
		return em;
	}
	/**
	 * 
	 * @param suino
	 * @return
	 */
	public double energiaMetabolizavelMCMS(Suino suino){
		double em = 0.0;
		em = (106 * Math.pow(suino.getPesoMedio(),0.75) + (2182.6 + 70.8863 * suino.getPesoMedio() - 0.2026 * Math.pow(suino.getPesoMedio(),2 )) * suino.getGanhoPeso()) * 0.95;
		return em;
	}
	/**
	 * 
	 * @param suino
	 * @return
	 */
	public double energiaMetabolizavelFemeaRM(Suino suino){
		double em = 0.0;
		em = (106 * Math.pow(suino.getPesoMedio(),0.75) + (2211.6 + 76.66 * suino.getPesoMedio() - 0.3726 * Math.pow(suino.getPesoMedio(),2 )) * suino.getGanhoPeso());
		return em;
	}
	/**
	 * 
	 * @param suino
	 * @return
	 */
	public double energiaMetabolizavelFemeaMS(Suino suino){
		double em = 0.0;
		em = (106 * Math.pow(suino.getPesoMedio(),0.75) + (2211.6 + 76.66 * suino.getPesoMedio() - 0.3726 * Math.pow(suino.getPesoMedio(),2 )) * suino.getGanhoPeso()) * 0.95;
		return em;
	}
	/**
	 * 
	 * @param suino
	 * @return
	 */
	public double energiaMetabolizavelInteiro(Suino suino){
		double em = 0.0;
		em = (106 * Math.pow(suino.getPesoMedio(),0.75) + (2117.5 + 55.873 * suino.getPesoMedio() - 0.1664 * Math.pow(suino.getPesoMedio(),2 )) * suino.getGanhoPeso());
		return em;
	}
	
	
	
	/**
	 * Calcula a média baseado na formula de energia metabolizavel e as tabelas padrões de ganho e peso
	 * @param faseInicial
	 * @param faseFinal
	 * @return
	 */
	public double energiaMetabolizavelMedia(int faseInicial, int faseFinal, int categoria){
		double peso = 0.0;
		double ganho = 0.0;
		double em;
		int acumulo = 0;
		double media = 0.0;
		Suino s =  new Suino();
		switch (categoria) {

		case 2:
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					System.out.println("I "+(i+21));
					peso = Tabelas.peso_medio[i][categoria];
					ganho = Tabelas.ganho_de_peso[i][categoria];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavel(s)/Tabelas.consumo_racao[i][categoria];
					acumulo++;
					media = em + media;
				}
			}
			System.out.println("Acumulo "+acumulo);
			break;
		case 3:
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					System.out.println("I "+(i+21));
					peso = Tabelas.peso_medio[i][categoria];
					ganho = Tabelas.ganho_de_peso[i][categoria];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelMCMS(s)/Tabelas.consumo_racao[i][categoria];
					System.out.println("ENERGIA TESTE TESTE "+(i+21)+" "+em);
					acumulo++;
					media = em + media;
				}
			}
			break;
		case 4:
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					System.out.println("I "+(i+21));
					peso = Tabelas.peso_medio[i][categoria];
					ganho = Tabelas.ganho_de_peso[i][categoria];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelFemeaRM(s)/Tabelas.consumo_racao[i][categoria];
					acumulo++;
					media = em + media;
				}
			}
			break;
		case 5:
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					System.out.println("I "+(i+21));
					peso = Tabelas.peso_medio[i][categoria];
					ganho = Tabelas.ganho_de_peso[i][categoria];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelFemeaMS(s)/Tabelas.consumo_racao[i][categoria];
					acumulo++;
					media = em + media;
				}
			}
			break;
		case 6:
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					System.out.println("I "+(i+21));
					peso = Tabelas.peso_medio[i][categoria];
					ganho = Tabelas.ganho_de_peso[i][categoria];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelInteiro(s)/Tabelas.consumo_racao[i][categoria];
					acumulo++;
					media = em + media;
				}
			}
			break;
		default:
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					System.out.println("I "+(i+21));
					peso = Tabelas.peso_medio[i][categoria];
					ganho = Tabelas.ganho_de_peso[i][categoria];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavel(s)/Tabelas.consumo_racao[i][categoria];
					acumulo++;
					media = em + media;
				}
			}
			break;
		}
		return media/acumulo;
	}
	/**
	 * Calcula a média baseado na formula de energia metabolizavel e as tabelas padrões de ganho e peso
	 * @param faseInicial
	 * @param faseFinal
	 * @return
	 */
	public double energiaMetabolizavelMediaSimulada(double tabela[][],int faseInicial, int faseFinal, int categoria){
		double peso = 0.0;
		double ganho = 0.0;
		double em;
		int acumulo = 0;
		double media = 0.0;
		Suino s =  new Suino();
		switch (categoria) {
		
		case 2:
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					System.out.println("I "+(i+21));
					peso = tabela[i][0];
					ganho =tabela[i][1];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavel(s)/tabela[i][3];
					acumulo++;
					media = em + media;
				}
				
				
			}
			break;
		case 3 : 
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					System.out.println("I "+(i+21));
					peso = tabela[i][0];
					ganho =tabela[i][1];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelMCMS(s)/tabela[i][3];
					System.out.println("ENERGIA TESTE TESTE "+(i+21)+" "+em);
					acumulo++;
					media = em + media;
				}
				
				
			}
			break;
		case 4 :
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					System.out.println("I "+(i+21));
					peso = tabela[i][0];
					ganho =tabela[i][1];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelFemeaRM(s)/tabela[i][3];
					acumulo++;
					media = em + media;
				}
				
				
			}
			break;
		case 5 :
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					System.out.println("I "+(i+21));
					peso = tabela[i][0];
					ganho =tabela[i][1];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelFemeaMS(s)/tabela[i][3];
					acumulo++;
					media = em + media;
				}
				
				
			}
			break;
		case 6 :
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					System.out.println("I "+(i+21));
					peso = tabela[i][0];
					ganho =tabela[i][1];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelInteiro(s)/tabela[i][3];
					acumulo++;
					media = em + media;
				}
				
				
			}
			break;
		default:
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					System.out.println("I "+(i+21));
					peso = tabela[i][0];
					ganho =tabela[i][1];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavel(s)/tabela[i][3];
					acumulo++;
					media = em + media;
				}
				
				
			}
			break;
		}
		return media/acumulo;
	}
	 /**]
	  * Realiza a correção na energia metabolizável kg na primeira fase
	  * @param inicial
	  * @param faseFinal
	  * @return
	  */
	public double energiaMetabolizavelKgFaixa(int inicial, int faseFinal){
		double energia = 0.0;
		if(inicial >= 21 && faseFinal <=32 )
			energia = 3400;
		else if(inicial >= 33 && faseFinal <=42)
			energia = 3375;
		else energia = (3400 + 3375)/2;
		return energia;
	}
	/**
	 * Calcula a média baseado na formula de energia metabolizavel e as tabelas padrões de ganho e peso
	 * @param faseInicial
	 * @param faseFinal
	 * @return
	 */
	public double energiaMetabolizavelDiaMedia(int faseInicial, int faseFinal, int categoria){
		double peso = 0.0;
		double ganho = 0.0;
		double em;
		int acumulo = 0;
		double media = 0.0;
		Suino s =  new Suino();
		switch (categoria) {
		
		case 2:
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					peso = Tabelas.peso_medio[i][categoria];
					ganho = Tabelas.ganho_de_peso[i][categoria];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavel(s);
					System.out.println("I "+(i+21) +" = "+em);
					acumulo++;
					media = em + media;
				}				
			}
			break;
		case 3:
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					peso = Tabelas.peso_medio[i][categoria];
					ganho = Tabelas.ganho_de_peso[i][categoria];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelMCMS(s);
					System.out.println("I "+(i+21) +" = "+em);
					acumulo++;
					media = em + media;
				}				
			}
			break;
		case 4 :
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					peso = Tabelas.peso_medio[i][categoria];
					ganho = Tabelas.ganho_de_peso[i][categoria];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelFemeaRM(s);
					System.out.println("I "+(i+21) +" = "+em);
					acumulo++;
					media = em + media;
				}
				
				
			}
			break;
		case 5 :
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					peso = Tabelas.peso_medio[i][categoria];
					ganho = Tabelas.ganho_de_peso[i][categoria];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelFemeaMS(s);
					System.out.println("I "+(i+21) +" = "+em);
					acumulo++;
					media = em + media;
				}	
				
			}
			break;
		case 6:
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					peso = Tabelas.peso_medio[i][categoria];
					ganho = Tabelas.ganho_de_peso[i][categoria];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelInteiro(s);
					System.out.println("I "+(i+21) +" = "+em);
					acumulo++;
					media = em + media;
				}					
			}
			break;
		default:
		
			break;
		}
		return media/acumulo;
	}
	
	public double energiaMetabolizavelDiaMediaSimulada(double[][] tabela, int faseInicial, int faseFinal, int categoria){
		double peso = 0.0;
		double ganho = 0.0;
		double em;
		int acumulo = 0;
		double media = 0.0;
		Suino s =  new Suino();
		switch (categoria) {
		
		case 2:
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					peso = tabela[i][0];
					ganho = tabela[i][1];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavel(s);
					System.out.println("I "+(i+21) +" = "+em);
					acumulo++;
					media = em + media;
				}				
			}
			break;
		case 3:
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					peso = tabela[i][0];
					ganho = tabela[i][1];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelMCMS(s);
					System.out.println("I "+(i+21) +" = "+em);
					acumulo++;
					media = em + media;
				}				
			}
			break;
		case 4 :
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					peso = tabela[i][0];
					ganho = tabela[i][1];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelFemeaRM(s);
					System.out.println("I "+(i+21) +" = "+em);
					acumulo++;
					media = em + media;
				}
				
				
			}
			break;
		case 5 :
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					peso = tabela[i][0];
					ganho = tabela[i][1];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelFemeaMS(s);
					System.out.println("I "+(i+21) +" = "+em);
					acumulo++;
					media = em + media;
				}	
				
			}
			break;
		case 6:
			for (int i = faseInicial; i <= faseFinal; i++) {
				if((i % 7) == 0){
					peso = tabela[i][0];
					ganho = tabela[i][1];
					s.setPesoMedio(peso);
					s.setGanhoPeso(ganho);
					em = energiaMetabolizavelInteiro(s);
					System.out.println("I "+(i+21) +" = "+em);
					acumulo++;
					media = em + media;
				}					
			}
			break;
		default:
		
			break;
		}
		return media/acumulo;
	}
	public double somade7(int faseInicial, int faseFinal, int categoria){
		double soma = 0.0;
		for (int i = faseInicial + 7; i <= faseFinal; i++) {
			soma += Tabelas.peso_medio[i][categoria];
		}
		return soma;
	}
	/**
	 * 
	 * @param faseInicial
	 * @param faseFinal
	 * @param categoria
	 * @param consumo
	 * @return
	 */
	public double consumoPeriodoCalc(int faseInicial,int faseFinal,int categoria, double consumo){
		double peso = 0.0;
		double ganho = 0.0;
		double em;
		Suino s =  new Suino();
		double consumoPeriodo = 0.0;
		switch (categoria) {

		case 2:
			for (int i = faseInicial+1; i <= faseFinal; i++) {
				peso = Tabelas.peso_medio[i][categoria];
				ganho = Tabelas.ganho_de_peso[i][categoria];
				s.setPesoMedio(peso);
				s.setGanhoPeso(ganho);
				em = energiaMetabolizavel(s);
//				System.out.println("CR "+em/consumo);
				consumoPeriodo +=em/consumo;
			}
			
			break;

		case 3:
			for (int i = faseInicial+1; i <= faseFinal; i++) {
				peso = Tabelas.peso_medio[i][categoria];
				ganho = Tabelas.ganho_de_peso[i][categoria];
				s.setPesoMedio(peso);
				s.setGanhoPeso(ganho);
				em = energiaMetabolizavelMCMS(s);
//				System.out.println("CR "+em/consumo);
				consumoPeriodo +=em/consumo;
			}
			
			break;

		case 4:
			for (int i = faseInicial+1; i <= faseFinal; i++) {
				peso = Tabelas.peso_medio[i][categoria];
				ganho = Tabelas.ganho_de_peso[i][categoria];
				s.setPesoMedio(peso);
				s.setGanhoPeso(ganho);
				em = energiaMetabolizavelFemeaRM(s);
//				System.out.println("CR "+em/consumo);
				consumoPeriodo +=em/consumo;
			}
			
			break;

		case 5:
			for (int i = faseInicial+1; i <= faseFinal; i++) {
				peso = Tabelas.peso_medio[i][categoria];
				ganho = Tabelas.ganho_de_peso[i][categoria];
				s.setPesoMedio(peso);
				s.setGanhoPeso(ganho);
				em = energiaMetabolizavelFemeaMS(s);
//				System.out.println("CR "+em/consumo);
				consumoPeriodo +=em/consumo;
			}
			
			break;

		case 6:
			for (int i = faseInicial+1; i <= faseFinal; i++) {
				peso = Tabelas.peso_medio[i][categoria];
				ganho = Tabelas.ganho_de_peso[i][categoria];
				s.setPesoMedio(peso);
				s.setGanhoPeso(ganho);
				em = energiaMetabolizavelInteiro(s);
//				System.out.println("CR "+em/consumo);
				consumoPeriodo +=em/consumo;
			}
			
			break;
		
		default:
		
			break;
		}
		return consumoPeriodo;
	}
	/**
	 * 
	 * @param faseInicial
	 * @param faseFinal
	 * @param categoria
	 * @return
	 */
	public double consumoMedio(int faseInicial,int faseFinal,int categoria){
		
		double consumo;
		int acumulo = 0;
		double media = 0.0;
		Suino s =  new Suino();
		switch (categoria) {

		case 2:
			for (int i = faseInicial; i <= faseFinal; i++) {
				consumo = Tabelas.consumo_racao[i][categoria];
				media = media + consumo;
				acumulo++;				
			}
			break;
		default:
			for (int i = faseInicial; i <= faseFinal; i++) {
				consumo = Tabelas.consumo_racao[i][categoria];
				media = media + consumo;
				acumulo++;				
			}
			break;
		}
		return media/acumulo;
	}
	
	public int temperaturaMedia(double peso){
		int temp = 0;
		 if( peso >= 15 && peso <= 30.9){
			temp = 25;
		}else if( peso >= 31 && peso <= 50.9){
			temp = 24;
		}else if( peso >= 51 && peso <= 70.9){
			temp = 23;
		}else if( peso >= 71  && peso <= 100.9){
			temp = 22;
		}else if( peso >= 101 && peso <= 130.9){
			temp = 21;
		}
		return temp;
	}
	/**
	 * Realiza a correcao da energia metabolizavel de acordo com a temperatura.
	 * @param suino
	 * @param temperaturaMedia
	 * @return
	 */
	public double correcaoTemperatura(Suino suino, int temperaturaMedia){
		double correcao = 0.0;
		System.out.println("Temperatura Média "+temperaturaMedia);
		correcao =( 2.4 * Math.pow(suino.getPesoMedio(),0.75)) * (temperaturaMedia - suino.getTemperatura());
		return correcao ;
	}

	/**
	 * @author alkxyly
	 * Calcula o fosforo disponivel do Suino
	 * Fórmula :(0.046 * ( pesoMedio)^0,75 + 5,81 * GanhoPeso) 
	 * @param suino
	 * @return
	 */
	public double fosforoDisponivel(Suino suino){
		double peso = suino.getPesoMedio();
		double fosforoDisponivel = 0.0;

		if(peso >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_MIN && peso <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_MAX){
			fosforoDisponivel = (0.046 * Math.pow(suino.getPesoMedio(),0.75)) + (5.81 * suino.getGanhoPeso());

		}else if(peso >= 51 && peso <= 185)
			fosforoDisponivel =  0.046 * Math.pow(suino.getPesoMedio(),0.75) + (5.33 * suino.getGanhoPeso());
		return fosforoDisponivel;
	}
	
	public double femeaFosforoDisponivel(Suino suino){
		double peso = suino.getPesoMedio();
		double fosforoDisponivel = 0.0;

		fosforoDisponivel = (0.046 * Math.pow(suino.getPesoMedio(),0.75)) + (5.96 * suino.getGanhoPeso());

		return fosforoDisponivel;
	}
	/**
	 * 
	 * @param suino
	 * @return
	 */
	public double fosforoDigest(Suino suino){
		double peso = suino.getPesoMedio();
		double fosforoDigest = 0.0;

		if(peso >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_MIN && peso <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_MAX){
			fosforoDigest = (0.046 * Math.pow(suino.getPesoMedio(),0.75)) + (5.60 * suino.getGanhoPeso());

		}else if(peso >= 51 && peso <= 185)
			fosforoDigest =  0.046 * Math.pow(suino.getPesoMedio(),0.75) + (5.30 * suino.getGanhoPeso());
		return fosforoDigest;
	}
	
	public double femeaFosforoDigest(Suino suino){
		double peso = suino.getPesoMedio();
		double fosforoDigest = 0.0;

		fosforoDigest = (0.046 * Math.pow(suino.getPesoMedio(),0.75)) + (5.75 * suino.getGanhoPeso());

		return fosforoDigest;
	}
	/**
	 * 
	 * @param categoria
	 * @param suino
	 * @return
	 */
	public double fosforoDisponivelCategoria(int categoria,Suino suino){
		double fosforo = 0.0;

		switch (categoria) {
		case 2:
			fosforo = fosforoDisponivel(suino);
			System.out.println("FOsfoto "+fosforo);
			break;
		case 3:
			fosforo = fosforoDisponivel(suino);
			break;
		case 4:
			fosforo = femeaFosforoDisponivel(suino);
			break;
		case 5:
			fosforo = femeaFosforoDisponivel(suino);
			break;
		case 6:
			fosforo = femeaFosforoDisponivel(suino);
			break;
		default:
			break;
		}

		return fosforo;
	}
	/**
	 * 
	 * @param categoria
	 * @param suino
	 * @return
	 */
	public double fosforoDigestivelCategoria(int categoria,Suino suino){
		double fosforo = 0.0;

		switch (categoria) {
		case 2:
			fosforo = fosforoDigest(suino);
			System.out.println("FOsfoto "+fosforo);
			break;
		case 3:
			fosforo = fosforoDigest(suino);
			break;
		case 4:
			fosforo = femeaFosforoDigest(suino);
			break;
		case 5:
			fosforo = femeaFosforoDigest(suino);
			break;
		case 6:
			fosforo = femeaFosforoDigest(suino);
			break;

		default:
			break;
		}

		return fosforo;
	}
	/**
	 * @author alkxyly
	 * Fórmula : (fosforoDisponivel / (consumoRacao / 10))
	 * @param exigencia atributo necessário fosforoDisponivel ja calculado
	 * @param suino - necessário consumo de ração
	 * @return
	 */
	public double fosforoDisponivelExigencia(Exigencia exigencia,Suino suino){
		return (exigencia.getFosforoDisponivelExigencia()/(suino.getConsumoRacao()/10))/100;
	}
	/**
	 * @author alkxyly
	 * fosforo digestivel% de acordo com o desempenho
	 * @param exigencia
	 * @param suino
	 * @return
	 */
	public double fosforoDigestivel(Suino suino){
		double fde = 0.0;
		if(suino.getDesempenho() == Constantes.REGULAR){
			fde = Constantes.FOSFORO_DIGESTIVEL_REGULAR;
		}else if(suino.getDesempenho() == Constantes.MEDIO){
			fde = Constantes.FOSFORO_DIGESTIVEL_MEDIO;
		}else if(suino.getDesempenho() == Constantes.SUPERIOR){
			fde = Constantes.FOSFORO_DIGESTIVEL_SUPERIOR;
		}
		return fde;
	}
	/**
	 * @author alkxyly
	 * fosforo digestivel% de acordo com o desempenho
	 * @param exigencia
	 * @param suino
	 * @return
	 */
	public double fosforoDigestivelCrescimento(Suino suino){
		double fde = 0.0;
		if(suino.getDesempenho() == Constantes.REGULAR){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				fde = Constantes.FOSFORO_DIGESTIVEL_REGULAR_CRESCIMENTO_1;
			else fde = Constantes.FOSFORO_DIGESTIVEL_REGULAR_CRESCIMENTO_2;
		}else if(suino.getDesempenho() == Constantes.MEDIO){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				fde = Constantes.FOSFORO_DIGESTIVEL_MEDIO_CRESCIMENTO_1;
			else fde = Constantes.FOSFORO_DIGESTIVEL_MEDIO_CRESCIMENTO_2;
		}else if(suino.getDesempenho() == Constantes.SUPERIOR){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				fde = Constantes.FOSFORO_DIGESTIVEL_SUPERIOR_CRESCIMENTO_1;
			else fde = Constantes.FOSFORO_DIGESTIVEL_SUPERIOR_CRESCIMENTO_2;
		}
		return fde;
	}
	/**
	 * Calculo da exigência Digestivel%
	 * Fórmula : fosforoDigestivel / (consumoRacao / 10)
	 * @param exigencia
	 * @param suino
	 * @return
	 */
	public double fosforoDigestivelExigencia(Exigencia exigencia,Suino suino){
		return (exigencia.getFosforoDigestivelExigencia()/(suino.getConsumoRacao() /10))/100;
	}
	/**
	 * @author alkxyly
	 * @param exigencia
	 * @param suino
	 * @return
	 */
	public double calcio(Exigencia exigencia,Suino suino){
		return ((exigencia.getFosforoDisponivel() * 2.03)  +( exigencia.getFosforoDigestivel() * 2.08))/2;
	}
	/**
	 * Calcula a proteina de acordo com o desempenho
	 * @param suino
	 * @return
	 */
	public double proteina(Suino suino){
		double proteina = 0.0;
		if(suino.getDesempenho() == Constantes.REGULAR){
			proteina = Constantes.PROTEINA_REGULAR;
		}else if(suino.getDesempenho() == Constantes.MEDIO){
			proteina = Constantes.PROTEINA_MEDIO;
		}else if(suino.getDesempenho() == Constantes.SUPERIOR){
			proteina = Constantes.PROTEINA_SUPERIOR;
		}
		return proteina;
	}
	/**
	 * Calcula a proteina de acordo com o desempenho para crescimento
	 * @param suino
	 * @return
	 */
	public double proteinaCrescimento(Suino suino){
		double proteina = 0.0;
		if(suino.getDesempenho() == Constantes.REGULAR){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				proteina = Constantes.PROTEINA_REGULAR_CRESCIMENTO_1;
			else proteina = Constantes.PROTEINA_REGULAR_CRESCIMENTO_2;
		}else if(suino.getDesempenho() == Constantes.MEDIO){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				proteina = Constantes.PROTEINA_MEDIO_CRESCIMENTO_1;
			else proteina = Constantes.PROTEINA_MEDIO_CRESCIMENTO_2;
		}else if(suino.getDesempenho() == Constantes.SUPERIOR){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				proteina = Constantes.PROTEINA_SUPERIOR_CRESCIMENTO_1;
			else proteina = Constantes.PROTEINA_SUPERIOR_CRESCIMENTO_2;
		}
		return proteina;
	}
	/**
	 * @author alkxyly
	 * Calcula o sodio de acordo com o desempenho
	 * @param suino
	 * @return
	 */
	public double sodio(Suino suino){
		double sodio = 0.0;
		if(suino.getDesempenho() == Constantes.REGULAR){
			sodio = Constantes.SODIO_REGULAR;
		}else if(suino.getDesempenho() == Constantes.MEDIO){
			sodio = Constantes.SODIO_MEDIO;
		}else if(suino.getDesempenho() == Constantes.SUPERIOR){
			sodio = Constantes.SODIO_SUPERIOR;
		}
		return sodio;
	}
	/**
	 * Sódio novo baseado na nova Tabela Brasileira
	 * @param pesoMedio
	 * @param em
	 * @return
	 */
	public double sodioNovo(double pesoMedio, double em){
		 return(( (68.4 - 0.346 * pesoMedio + 0.0014 * Math.pow(pesoMedio, 2) )/ 1000) * em)/1000;
	}
	/**
	 * @author alkxyly
	 * Calcula o sodio de acordo com o desempenho para crescimento
	 * @param suino
	 * @return
	 */
	public double sodioCrescimento(Suino suino){
		double sodio = 0.0;
		if(suino.getDesempenho() == Constantes.REGULAR){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				sodio = Constantes.SODIO_REGULAR_CRESCIMENTO_1;
			else sodio = Constantes.SODIO_REGULAR_CRESCIMENTO_2;
		}else if(suino.getDesempenho() == Constantes.MEDIO){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				sodio = Constantes.SODIO_MEDIO_CRESCIMENTO_1;
			else sodio = Constantes.SODIO_MEDIO_CRESCIMENTO_2;
		}else if(suino.getDesempenho() == Constantes.SUPERIOR){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				sodio = Constantes.SODIO_SUPERIOR_CRESCIMENTO_1;
			else sodio = Constantes.SODIO_SUPERIOR_CRESCIMENTO_2;
		}
		return sodio;
	}
	/**
	 * @author alkxyly
	 * Calcula o cloro de acordo com o desempenho
	 * @param suino
	 * @return
	 */
	public double cloro(Suino suino){
		double cloro = 0.0;
		if(suino.getDesempenho() == Constantes.REGULAR){
			cloro = Constantes.CLORO_REGULAR;
		}else if(suino.getDesempenho() == Constantes.MEDIO){
			cloro = Constantes.CLORO_MEDIO;
		}else if(suino.getDesempenho() == Constantes.SUPERIOR){
			cloro = Constantes.CLORO_SUPERIOR;
		}
		return cloro;
	}
	/**
	 * Cloro novo baseado na nova Tabela Brasileira
	 * @param pesoMedio
	 * @param em
	 * @return
	 */
	public double cloroNovo(double pesoMedio, double em){
		return(( (65.5 - 0.346 * pesoMedio + 0.0014 * Math.pow(pesoMedio, 2) )/ 1000) * em)/1000;
	}
	/**
	 * @author alkxyly
	 * Calcula o cloro de acordo com o desempenho para crescimento
	 * @param suino
	 * @return
	 */
	public double cloroCrescimento(Suino suino){
		double cloro = 0.0;
		if(suino.getDesempenho() == Constantes.REGULAR){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				cloro = Constantes.CLORO_REGULAR_CRESCIMENTO_1;
			else cloro = Constantes.CLORO_REGULAR_CRESCIMENTO_2;
		}else if(suino.getDesempenho() == Constantes.MEDIO){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				cloro = Constantes.CLORO_MEDIO_CRESCIMENTO_1;
			else cloro = Constantes.CLORO_MEDIO_CRESCIMENTO_2;
		}else if(suino.getDesempenho() == Constantes.SUPERIOR){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				cloro = Constantes.CLORO_SUPERIOR_CRESCIMENTO_1;
			else cloro = Constantes.CLORO_SUPERIOR_CRESCIMENTO_2;
		}
		return cloro;
	}
	/**
	 * @author alkxyly
	 * Calcula o potassio de acordo com o desempenho
	 * @param suino
	 * @return
	 */
	public double potassio(Suino suino){
		double potassio = 0.0;
		if(suino.getDesempenho() == Constantes.REGULAR){
			potassio = Constantes.POTASSIO_REGULAR;
		}else if(suino.getDesempenho() == Constantes.MEDIO){
			potassio = Constantes.POTASSIO_MEDIO;
		}else if(suino.getDesempenho() == Constantes.SUPERIOR){
			potassio = Constantes.POTASSIO_SUPERIOR;
		}
		return potassio;
	}
	/**
	 * Baseado na nova Tabela Brasileira 
	 * @param pesoMedio
	 * @param em
	 * @return
	 */
	public double postassioNovo(double pesoMedio, double em){
		return(( (154.9 - 0.427 * pesoMedio + 0.0006 * Math.pow(pesoMedio, 2) )/ 1000) * em)/1000;
	}
	/**
	 * @author alkxyly
	 * Calcula o potassio de acordo com o desempenho para crescimento
	 * @param suino
	 * @return
	 */
	public double potassioCrescimento(Suino suino){
		double potassio = 0.0;
		if(suino.getDesempenho() == Constantes.REGULAR){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				potassio = Constantes.POTASSIO_REGULAR_CRESCIMENTO_1;
			else potassio = Constantes.POTASSIO_REGULAR_CRESCIMENTO_2;
		}else if(suino.getDesempenho() == Constantes.MEDIO){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				potassio = Constantes.POTASSIO_MEDIO_CRESCIMENTO_1;
			else potassio = Constantes.POTASSIO_MEDIO_CRESCIMENTO_2;
		}else if(suino.getDesempenho() == Constantes.SUPERIOR){
			if(suino.getPesoMedio() >= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN &&
					suino.getPesoMedio() <= Constantes.PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX)
				potassio = Constantes.POTASSIO_SUPERIOR_CRESCIMENTO_1;
			else potassio = Constantes.POTASSIO_SUPERIOR_CRESCIMENTO_2;
		}
		return potassio;
	}
	/**
	 * @author alkxyly
	 * Calcula a lisina Digestivel
	 * Fórmula Lis Dig (g/dia) = 0,036 x P^0,75 + (g. Lis Dig. / kg Ganho) x G
	 * P = Peso Corporal Médio em kg 2g. Lis Dig. / kg Ganho = 14,885 + 0,2282 x (Peso Médio) - 0,0017 x (Peso Médio)
	 * G = Ganho / dia em kg
	 * @return
	 */
	public double lisinaDigestivel(Suino suino){
		double lisinaDisgestivel = 0.0;
		double gLis = gLisinaDisgestivel(suino.getPesoMedio());
		lisinaDisgestivel = 0.036 * Math.pow(suino.getPesoMedio(), 0.75) + (gLis * suino.getGanhoPeso());
		return lisinaDisgestivel;
	}
	
	public double glisina(Suino suino,double lisina){
		return 0.036 * Math.pow(suino.getPesoMedio(), 0.75) + (lisina * suino.getGanhoPeso());
	}

	public double lisinaExigenciaCategoria(int categoria, Suino suino){
		double lisina = 0.0;
		switch (categoria) {
		case 2:
			lisina = lisinaDigestivel(suino);
			break;
		case 3:
			lisina = lisinaDigestivel(suino);
			break;
		case 4:
			lisina = femeaGLisinaDisgestivel(suino);
			break;
		case 5:
			lisina = femeaGLisinaDisgestivel(suino);			
			break;
		case 6:
			lisina = machoInteiroGLisinaDisgestivel(suino);
		default:
			
			break;
		}

		return lisina;

	}
	/**
	 * Lisina Digestivel para machos castrados	grama/dia
	 * @author alkxyly
	 * @param pesoMedio
	 * @return
	 */
	public double gLisinaDisgestivel(double pesoMedio){
		return 16.664 + (0.0736 * pesoMedio) - (0.0003 * Math.pow(pesoMedio,2));
	}
	/**
	 * Lisina Digestivel para macho	
	 * @author alkxyly
	 * @param pesoMedio
	 * @return
	 */
	public double machoInteiroGLisinaDisgestivel(Suino s){
		double lisina = 15.795 + (0.142 * s.getPesoMedio()) - (0.0008 * Math.pow(s.getPesoMedio(),2));
		
		return this.glisina(s, lisina);
	}
	/**
	 * Lisina Digestivel para femea
	 * @author alkxyly
	 * @param pesoMedio
	 * @return
	 */
	public double femeaGLisinaDisgestivel(Suino s){
		double lisina =  16.241 + (0.1065 * s.getPesoMedio()) - (0.0005 * Math.pow(s.getPesoMedio(),2));
		return this.glisina(s, lisina);
	}


	/**
	 * @author alkxyly
	 * @param lisinaDigestivel
	 * @param consumoRacao
	 * @return
	 */
	public double lisinaDigestivelExigencia(double lisinaDigestivel,double consumoRacao){
		return (lisinaDigestivel/consumoRacao)/10;
	}
	/**
	 * Retorna a lisina total baseado na lisina Digestivel com 88%
	 * @param lisinaDigestivel
	 * @param consumoRacao
	 * @return
	 */
	public double lisinaTotal(double lisinaDigestivel,double consumoRacao){
		double lisinaD = lisinaDigestivelExigencia(lisinaDigestivel, consumoRacao);
		return lisinaD /  0.88;
	}
	/**
	 * Calcula a lisina proteina ideal total baseada na digestivel
	 * @param lisinaProteinaIdeal
	 * @return
	 */
	public double lisinaProteinaIdealTotal(double lisinaProteinaIdeal){
		return lisinaProteinaIdeal/0.88;
	}
	/**
	 * @author alkxyly
	 * Retorna o valor da metionina digestível
	 * @param lisinaDigExigencia - Lisina Exigência
	 * @return Metionina Digestível
	 */
	public double metioninaDigestivel(double lisinaProteinaIdeal){
		return (lisinaProteinaIdeal * Constantes.METIONINA_INICIAL_DIGESTIVEL)/100;
	}
	/**
	 * Novos Coeficientes de Metionina baseada na nova Tabela
	 * @param lisinaProteinaIdeal
	 * @param faseInicial
	 * @param faseFinal
	 * @return
	 */
	public double metioninaDigestivelNovo(double lisinaProteinaIdeal, double peso){
		double metionina = 0.0;
		if(peso <= 14.9){
			 metionina = Constantes.METIONINA_INICIAL_DIGESTIVEL;			
		}else if( peso >= 15 && peso <= 30.9){
			metionina = Constantes.METIONINA_CRESCIMENTO_DIGESTIVEL;
		}else if( peso >= 31 && peso <= 70.9){
			metionina = 30;
		}else metionina = 30;
		
		return (lisinaProteinaIdeal * metionina)/100;
	}
	/**
	 * Novos Coeficientes de Metionina baseada na nova Tabela
	 * @param lisinaProteinaIdeal
	 * @param faseInicial
	 * @param faseFinal
	 * @return
	 */
	public double metioninaTotalNovo(double lisinaProteinaIdeal, double peso){
		double metionina = 0.0;
		if(peso <= 14.9){
			metionina = 27;			
		}else if( peso >= 15 && peso <= 30.9){
			metionina = 28;
		}else if( peso >= 31 && peso <= 70.9){
			metionina = 29;
		}else metionina = 30;
		
		return (lisinaProteinaIdeal * metionina)/100;
	}
	/**
	 * @author alkxyly
	 * Retorna o valor da metionina digestível para Crescimento
	 * @param lisinaDigExigencia - Lisina Exigência
	 * @return Metionina Digestível
	 */
	public double metioninaCrescimentoDigestivel(double lisinaDigExigencia){
		return (lisinaDigExigencia * Constantes.METIONINA_CRESCIMENTO_DIGESTIVEL)/100;
	}
	/**
	 * Meionina Total
	 * @param lisinaDigExigencia
	 * @return
	 */
	public double metioninaTotal(double lisinaTotal){
		return (lisinaTotal * Constantes.METIONINA_INICIAL_TOTAL)/100;
	}
	/**
	 * Meionina Total para Crescimento
	 * @param lisinaDigExigencia
	 * @return
	 */
	public double metioninaCrescimentoTotal(double lisinaTotal){
		return (lisinaTotal * Constantes.METIONINA_CRESCIMENTO_TOTAL)/100;
	}
	/**
	 * 
	 * @param lisinaDigExigencia
	 * @return
	 */
	public double metioninaCistina(double lisinaDigExigencia){
		return (lisinaDigExigencia * Constantes.METIONINA_CISTINA_INICIAL_DIGESTIVEL)/100;
	}
	/**
	 * 
	 * @param lisinaDigExigencia
	 * @return
	 */
	public double metioninaCistinaCrescimento(double lisinaDigExigencia){
		return (lisinaDigExigencia * Constantes.METIONINA_CISTINA_CRESCIMENTO_DIGESTIVEL)/100;
	}
	/**
	 * 
	 * @param lisinaTotal
	 * @return
	 */
	public double metioninaCistinaTotal(double lisinaTotal){
		
		return (lisinaTotal * Constantes.METIONINA_CISTINA_INICIAL_TOTaL)/100;
	}
	/**
	 * Nova metionina Cisteina 
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double metioninaCistinaDigestivel(double proteinaIdeal,double peso){
		double met = 0.0;
		if(peso <= 14.9){
			met = 56;			
		}else if( peso >= 15 && peso <= 30.9){
			met = 57;
		}else if( peso >= 31 && peso <= 70.9){
			met = 59;
		}else met = 60;
		return (met * proteinaIdeal)/100;
	}
	/**
	 * Nova metionina Cisteina 
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double metioninaCistinaTotal(double proteinaIdeal,double peso){
		double met = 0.0;
		if(peso <= 14.9){
			met = 55;			
		}else if( peso >= 15 && peso <= 30.9){
			met = 56;
		}else if( peso >= 31 && peso <= 70.9){
			met = 58;
		}else met = 59;
		return (met * proteinaIdeal)/100;
	}
	/**
	 * 
	 * @param lisinaTotal
	 * @return
	 */
	public double metioninaCistinaCrescimentoTotal(double lisinaTotal){
		return (lisinaTotal * Constantes.METIONINA_CISTINA_CRESCIMENTO_TOTaL)/100;
	}
	/**
	 * 
	 * @param lisinaDigExigencia
	 * @return
	 */
	public double treoninaDigestivel(double proteinaIdeal,double peso){
		double treonina = 0.0;
		if(peso <= 14.9){
			treonina = 67;			
		}else if( peso >= 15 && peso <= 30.9){
			treonina = 65;
		}else if( peso >= 31 && peso <= 70.9){
			treonina = 65;
		}else treonina = 65;
		return (treonina * proteinaIdeal)/100;
	}
	/**
	 * 
	 * @param lisinaTotExigencia
	 * @return
	 */
	public double treoninaTotal(double proteinaIdeal,double peso){
		double treonina = 0.0;
		if(peso <= 14.9){
			treonina = 70;			
		}else if( peso >= 15 && peso <= 30.9){
			treonina = 68;
		}else if( peso >= 31 && peso <= 70.9){
			treonina = 68;
		}else treonina = 68;
		return (treonina * proteinaIdeal)/100;
	}
	/**
	 * 
	 * @param lisinaDigExigencia
	 * @return
	 */
	public double treoninaCrescimentoDigestivel(double lisinaDigExigencia){
		return (lisinaDigExigencia * Constantes.TREONINA_CRESCIMENTO_DIGESTIVEL)/100;
	}
	/**
	 * 
	 * @param lisinaTotal
	 * @return
	 */
	public double treoninaTotal(double lisinaTotal){
		return (lisinaTotal * Constantes.TREONINA_INICIAL_TOTAL)/100;
	}
	/**
	 * 
	 * @param lisinaTotal
	 * @return
	 */
	public double treoninaCrescimentoTotal(double lisinaTotal){
		return (lisinaTotal * Constantes.TREONINA_CRESCIMENTO_TOTAL)/100;
	}
	/**
	 * 
	 * @param lisina
	 * @return
	 */
	public double triptofano(double lisina){
		return (lisina  * Constantes.TRIPTOFANO_INICIAL_DIGESTIVEL)/100;
	}
	/**
	 * 
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double triptofanoDigestivel(double proteinaIdeal, double peso){
		double triptofano = 0.0;
		if(peso <= 14.9){
			triptofano = 19;			
		}else if( peso >= 15 && peso <= 30.9){
			triptofano = 19;
		}else if( peso >= 31 && peso <= 70.9){
			triptofano = 20;
		}else triptofano = 20;
		
		return (triptofano * proteinaIdeal)/100;
	}
	/**
	 * 
	 * @param lisina
	 * @return
	 */
	public double triptofanoTotal(double lisina){
		return (lisina  * Constantes.TRIPTOFANO_INICIAL_TOTAL)/100;
	}
	/**
	 * 
	 * @param lisina
	 * @return
	 */
	public double arginina(double lisina){
		return (lisina  * Constantes.ARGININA_INICIAL_DIGESTIVEL)/100;
	}
	 	
	/**
	 * Arginina baseada na nova tebela brasileira
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double argininaDigestiva(double proteinaIdeal, double peso){
		double arginina = 0.0;
		if(peso <= 14.9){
			arginina = 100;			
		}else if( peso >= 15 && peso <= 30.9){
			arginina = 45;
		}else if( peso >= 31 && peso <= 70.9){
			arginina = 42;
		}else arginina = 40;
		
		return (arginina * proteinaIdeal)/100;
	}
	/**
	 * Arginina baseada na nova tebela brasileira
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double argininaTotal(double proteinaIdeal, double peso){
		double arginina = 0.0;
		if(peso <= 14.9){
			arginina = 99;			
		}else if( peso >= 15 && peso <= 30.9){
			arginina = 44;
		}else if( peso >= 31 && peso <= 70.9){
			arginina = 40;
		}else arginina = 38;
		
		return (arginina * proteinaIdeal)/100;
	}
	/**
	 * 
	 * @param lisina
	 * @return
	 */
	public double argininaCrescimento(double lisina){
		return (lisina  * Constantes.ARGININA_CRESCIMENTO_DIGESTIVEL)/100;
	}
	/**
	 * 
	 * @param lisina
	 * @return
	 */
	public double argininaTotal(double lisina){
		return (lisina  * Constantes.ARGININA_INICIAL_TOTAL)/100;
	}
	/**
	 * 
	 * @param lisina
	 * @return
	 */
	public double argininaCrescimentoTotal(double lisina){
		return (lisina  * Constantes.ARGININA_CRESCIMENTO_TOTAL)/100;
	}
	/**
	 * 
	 * @param lisina
	 * @return
	 */
	public double valina(double lisina){
		return (lisina  * Constantes.VALINA_INICIAL_DIGESTIVEL)/100;
	}
	/**
	 * Valina nova baseada pela tabela brasileira
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double valinaDigestivel(double proteinaIdeal, double peso){
		double valina = 0.0;
		if(peso <= 14.9){
			valina = 69;			
		}else if( peso >= 15 && peso <= 30.9){
			valina = 69;
		}else if( peso >= 31 && peso <= 70.9){
			valina = 69;
		}else valina = 69;
		
		return (valina * proteinaIdeal)/100;
	}
	/**
	 * Valina nova baseada pela tabela brasileira
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double valinaTotal(double proteinaIdeal, double peso){
		double valina = 0.0;
		if(peso <= 14.9){
			valina = 70;			
		}else if( peso >= 15 && peso <= 30.9){
			valina = 70;
		}else if( peso >= 31 && peso <= 70.9){
			valina = 70;
		}else valina = 70;
		
		return (valina * proteinaIdeal)/100;
	}
	/**
	 * 
	 * @param lisina
	 * @return
	 */
	public double valinaTotal(double lisina){
		return (lisina  * Constantes.VALINA_INICIAL_TOTAL)/100;
	}
	/**
	 * 
	 * @param lisina
	 * @return
	 */
	public double isoleucina(double lisina){
		return (lisina  * Constantes.ISOLEUCINA_INICIAL_DIGESTIVEL)/100;
	}
	/**
	 * 
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double isoleucinaDigestivel(double proteinaIdeal, double peso){
		double isoleucina = 0.0;
		if(peso <= 14.9){
			isoleucina = 55;			
		}else if( peso >= 15 && peso <= 30.9){
			isoleucina = 55;
		}else if( peso >= 31 && peso <= 70.9){
			isoleucina = 55;
		}else isoleucina = 55;
		
		return (isoleucina * proteinaIdeal)/100;
	}
	/**
	 * 
	 * @param lisina
	 * @return
	 */
	public double isoleucinaTotal(double lisina){
		return (lisina  * Constantes.ISOLEUCINA_INICIAL_TOTAL)/100;
	}
	/**
	 * 
	 * @param lisina
	 * @return
	 */
	public double leucina(double lisina){
		return (lisina  * Constantes.LEUCINA_INICIAL_DIGESTIVEL)/100;
	}
	/**
	 * 
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double leucinaDigestivel(double proteinaIdeal, double peso){
		double leucina = 0.0;
		if(peso <= 14.9){
			leucina = 100;			
		}else if( peso >= 15 && peso <= 30.9){
			leucina = 100;
		}else if( peso >= 31 && peso <= 70.9){
			leucina = 100;
		}else leucina = 100;
		
		return (leucina * proteinaIdeal)/100;
	}
	/**
	 * 
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double leucinaTotal(double proteinaIdeal, double peso){
		double leucina = 0.0;
		if(peso <= 14.9){
			leucina = 97;			
		}else if( peso >= 15 && peso <= 30.9){
			leucina = 97;
		}else if( peso >= 31 && peso <= 70.9){
			leucina = 97;
		}else leucina = 97;
		
		return (leucina * proteinaIdeal)/100;
	}
	/**
	 * 
	 * @param lisina
	 * @return
	 */
	public double leucinaTotal(double lisina){
		return (lisina  * Constantes.LEUCINA_INICIAL_TOTAL)/100;
	}
	/**
	 * 
	 * @param lisina
	 * @return
	 */
	public double histidina(double lisina){
		return (lisina  * Constantes.HISTIDINA_INICIAL_DIGESTIVEL)/100;
	}
	/**
	 * 
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double histidinaDigestivel(double proteinaIdeal, double peso){
		double histidina = 0.0;
		if(peso <= 14.9){
			histidina = 33;			
		}else if( peso >= 15 && peso <= 30.9){
			histidina = 33;
		}else if( peso >= 31 && peso <= 70.9){
			histidina = 33;
		}else histidina = 33;
		
		return (histidina * proteinaIdeal)/100;	
	}
	/**
	 * 
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double histidinaTotal(double proteinaIdeal, double peso){
		double histidina = 0.0;
		if(peso <= 14.9){
			histidina = 32;			
		}else if( peso >= 15 && peso <= 30.9){
			histidina = 32;
		}else if( peso >= 31 && peso <= 70.9){
			histidina = 32;
		}else histidina = 32;
		
		return (histidina * proteinaIdeal)/100;	
	}
	public double histidinaTotal(double lisina){
		return (lisina  * Constantes.HISTIDINA_INICIAL_TOTAL)/100;
	}
	public double fenilalanina(double lisina){
		return (lisina  * Constantes.FENILALANINA_INICIAL_DIGESTIVEL)/100;
	}
	/**
	 * 
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double fenilalaninaDigestivel(double proteinaIdeal, double peso){
		double fenilalanina = 0.0;
		if(peso <= 14.9){
			fenilalanina = 50;			
		}else if( peso >= 15 && peso <= 30.9){
			fenilalanina = 50;
		}else if( peso >= 31 && peso <= 70.9){
			fenilalanina = 50;
		}else fenilalanina = 50;
		
		return (fenilalanina * proteinaIdeal)/100;	
	}
	/**
	 * 
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double fenilalaninaTotal(double proteinaIdeal, double peso){
		double fenilalanina = 0.0;
		if(peso <= 14.9){
			fenilalanina = 49;			
		}else if( peso >= 15 && peso <= 30.9){
			fenilalanina = 49;
		}else if( peso >= 31 && peso <= 70.9){
			fenilalanina = 49;
		}else fenilalanina = 49;
		
		return (fenilalanina * proteinaIdeal)/100;	
	}
	/**
	 * 
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double fenilalaninaTirosinaDigestivel(double proteinaIdeal, double peso){
		double fenilalanina = 0.0;
		if(peso <= 14.9){
			fenilalanina = 100;			
		}else if( peso >= 15 && peso <= 30.9){
			fenilalanina = 100;
		}else if( peso >= 31 && peso <= 70.9){
			fenilalanina = 100;
		}else fenilalanina = 100;
		
		return (fenilalanina * proteinaIdeal)/100;	
	}
	/**
	/**
	 * 
	 * @param proteinaIdeal
	 * @param peso
	 * @return
	 */
	public double fenilalaninaTirosinaTotal(double proteinaIdeal, double peso){
		double fenilalanina = 0.0;
		if(peso <= 14.9){
			fenilalanina = 98;			
		}else if( peso >= 15 && peso <= 30.9){
			fenilalanina = 98;
		}else if( peso >= 31 && peso <= 70.9){
			fenilalanina = 98;
		}else fenilalanina = 98;
		
		return (fenilalanina * proteinaIdeal)/100;	
	}
	/**
	 * Calcula o Nitrogenio Essencial Baseado na Tabela 3.19 pg 27.
	 * @param exig
	 * @return
	 */
	public double nitrogenioEssencial(Exigencia exig, double peso){
		double nitrogenio = 0.0;
		nitrogenio  = (exig.getLisinaProteinaIdeal() * 19.16)/100;
		nitrogenio += (exig.getMetioninaDigestivel() * 9.39)/100;
		nitrogenio += (exig.getTreoninaDigestivel() * 11.76)/100;
		nitrogenio += (exig.getTriptofano() * 13.72)/100;
		if(peso <= 14.9){
			nitrogenio += (exig.getArginina() * 35)/100;
		}else	nitrogenio += (exig.getArginina() * 32.16)/100;
		nitrogenio += (exig.getValina() * 11.96)/100;
		nitrogenio += (exig.getIsoleucina() * 10.68)/100;
		nitrogenio += (exig.getLeucina() * 10.68)/100;
		nitrogenio += (exig.getHistidina() * 27.08)/100;
		nitrogenio += (exig.getFenilalanina() * 8.48)/100;		
		return nitrogenio;
	}
	/**
	 * Calcula o Nitrogenio Essencial Total Baseado na Tabela 3.19 pg 27.
	 * @param exig
	 * @return
	 */
	public double nitrogenioEssencialTotal(Exigencia exig, double peso){
		double nitrogenio = 0.0;
		nitrogenio  = (exig.getLisinaTotal() * 19.16)/100;
		nitrogenio += (exig.getMetioninaTotal() * 9.39)/100;
		nitrogenio += (exig.getTreoninaTotal() * 11.76)/100;
		nitrogenio += (exig.getTriptofanoTotal() * 13.72)/100;
		if(peso <= 14.9){
			nitrogenio += (exig.getArgininaTotal() * 35)/100;
		}else	nitrogenio += (exig.getArgininaTotal() * 32.16)/100;
		nitrogenio += (exig.getValinaTotal() * 11.96)/100;
		nitrogenio += (exig.getIsoleucinaTotal() * 10.68)/100;
		nitrogenio += (exig.getLeucinaTotal() * 10.68)/100;
		nitrogenio += (exig.getHistidinaTotal() * 27.08)/100;
		nitrogenio += (exig.getFenilalaninaTotal() * 8.48)/100;		
		return nitrogenio;
	}
	/**
	 * Altera o ganho da ultima fase baseada nos dias e no nível de ractopamina
	 * @param dia
	 * @param ppm
	 * @return
	 */
	public double ractopaminaGanho(int dia , int ppm){
		double ract = 0.0;
		if(dia == 21 && ppm == 5){
			ract = 107;
		}else 	if(dia == 28 && ppm == 5){
			ract = 100;
		}else if(dia == 21 && ppm == 10){
			ract = 123;
		}else 	if(dia == 28 && ppm == 10){
			ract = 115;
		}else if(dia == 21 && ppm == 15){
			ract = 134;
		}else 	if(dia == 28 && ppm == 15){
			ract = 125;
		}else if(dia == 21 && ppm == 20){
			ract = 139;
		}else 	if(dia == 28 && ppm == 20){
			ract = 130;
		}
		return ract;
	}
	/**
	 * Altera o consumo da ultima fase baseada nos dias e no nível de ractopamina
	 * @param dia
	 * @param ppm
	 * @return
	 */
	public double ractopaminaConsumo(int dia , int ppm){
		double ract = 0.0;
		if(dia == 21 && ppm == 5){
			ract = -43;
		}else 	if(dia == 28 && ppm == 5){
			ract = -40;
		}else if(dia == 21 && ppm == 10){
			ract = -64;
		}else 	if(dia == 28 && ppm == 10){
			ract = -60;
		}else if(dia == 21 && ppm == 15){
			ract = -96;
		}else 	if(dia == 28 && ppm == 15){
			ract = -90;
		}else if(dia == 21 && ppm == 20){
			ract = -139;
		}else 	if(dia == 28 && ppm == 20){
			ract = -130;
		}
		return ract;
	}
	/**
	 * Altera a lisina digestivel da ultima fase baseada nos dias e no nível de ractopamina
	 * @param dia
	 * @param ppm
	 * @return
	 */
	public double ractopaminaLisinaDigDia(int dia , int ppm){
		double ract = 0.0;
		if(dia == 21 && ppm == 5){
			ract = 3.9;
		}else 	if(dia == 28 && ppm == 5){
			ract = 3.7;
		}else if(dia == 21 && ppm == 10){
			ract = 4.6;
		}else 	if(dia == 28 && ppm == 10){
			ract = 4.3;
		}else if(dia == 21 && ppm == 15){
			ract = 5;
		}else 	if(dia == 28 && ppm == 15){
			ract = 4.7;
		}else if(dia == 21 && ppm == 20){
			ract = 5.4;
		}else 	if(dia == 28 && ppm == 20){
			ract = 5;
		}
		return ract;
	}
	/**
	 * Altera a lisina digestivel kg da ultima fase baseada nos dias e no nível de ractopamina
	 * @param dia
	 * @param ppm
	 * @return
	 */
	public double ractopaminaLisinaDigKg(int dia , int ppm){
		double ract = 0.0;
		if(dia == 21 && ppm == 5){
			ract = 0.139;
		}else 	if(dia == 28 && ppm == 5){
			ract = 0.123;
		}else if(dia == 21 && ppm == 10){
			ract = 0.157;
		}else 	if(dia == 28 && ppm == 10){
			ract =0.146;
		}else if(dia == 21 && ppm == 15){
			ract = 0.178;
		}else 	if(dia == 28 && ppm == 15){
			ract = 0.167;
		}else if(dia == 21 && ppm == 20){
			ract = 0.203;
		}else 	if(dia == 28 && ppm == 20){
			ract = 0.187;
		}
		return ract;
	}
	/**
	 * Calcula a proteína bruna digestivel baseada na tabela 3.19
	 * @param nitrogencioEssencial
	 * @return proteina bruta digestivel
	 */
	public double proteinaBrutaDigestivel(double nitrogencioEssencial){
		return (nitrogencioEssencial/0.37) * 6.25;
	}
	/**
	 * Calcula a proteína bruna digestivel baseada na tabela 3.19
	 * @param nitrogencioEssencial
	 * @return proteina bruta digestivel
	 */
	public double proteinaBrutaTotal(double nitrogencioEssencial){
		return (nitrogencioEssencial/0.37) * 6.25;
	}
	public double fenilalaninaTotal(double lisina){
		return (lisina  * Constantes.FENILALANINA_INICIAL_TOTAL)/100;
	}
	public double fenilalaninaTirosina(double lisina){
		return (lisina  * Constantes.FENILALANINA_TIROSINA_INICIAL_DIGESTIVEL)/100;
	}
	public double fenilalaninaTirosinaTotal(double lisina){
		return (lisina  * Constantes.FENILALANINA_TIROSINA_INICIAL_TOTAL)/100;
	}
	/**
	 * @since 9/2/2017
	 * @param idadeAbate  - idade final 
	 * @param idadeInicial - idade Inicial
	 * @param ganhoPesoDiario - Ganho de peso diario
	 * @param pesoAlojamento - peso Alojamento
	 * @return
	 */
	public double pesoAbate(double idadeAbate,double idadeInicial, double ganhoPesoDiario,double pesoAlojamento){
		double pesoAbate = 0.0;
		pesoAbate = ((idadeAbate - idadeInicial) * ganhoPesoDiario )+ pesoAlojamento;
		return pesoAbate;
	}

	/**
	 * @since 09/02/2017
	 * @param conversaoAlimentar
	 * @param ganhoAoAbate
	 * @return
	 */
	public double consumoRacao(double conversaoAlimentar,double ganhoAoAbate){
		double consumoRacao = 0.0;		
		consumoRacao = conversaoAlimentar  * ganhoAoAbate;		
		return consumoRacao;
	}
	/**
	 * 
	 * @param pesoAoAbate
	 * @param pesoAlojamento
	 * @return
	 */
	public double ganhoAoAbate(double pesoAoAbate,double pesoAlojamento){
		double ganhoAoAbate = 0.0;
		ganhoAoAbate = pesoAoAbate - pesoAlojamento;
		return ganhoAoAbate;
	}
	/**
	 * 
	 * Curva Simulada = |PesoAcumulado||Ganho de Peso|Consumo Acumulado Simulado| Consumo Racao Diario Simulado
	 * 
	 * @param idadeInicial
	 * @param pesoAbate
	 * @param pesoMedio
	 * @param categoria
	 * @param pesoAbateCalculado
	 * @param pesoAlojamento
	 * @param consumoRacaoCalculado
	 * @return
	 */
	public double[][] curvaSimulada(int idadeInicial,int idadeAbate,int categoria, double pesoAbateCalculado,double pesoAlojamento,double consumoRacaoCalculado){
		double curvaSimulada[][] = new double[182][4];
		
		System.out.println("Peso Abate Calculado "+pesoAbateCalculado);
		double pesoAbateMedioDias =  Tabelas.peso_medio[idadeAbate][categoria];
		double consumoMedioDias = Tabelas.consumo_racao_acumulado[idadeAbate][categoria];
		
		double percentual = (pesoAbateCalculado*100)/pesoAbateMedioDias;
		double percentualConsumo = (consumoRacaoCalculado * 100)/(Tabelas.consumo_racao_acumulado[idadeAbate][categoria] - Tabelas.consumo_racao_acumulado[idadeInicial][categoria]);
		
		System.out.println("Percentual Peso "+percentual);
		System.out.println("Percentual Consumo "+percentualConsumo);
		
		
		
		double pesoMedioDias = Tabelas.peso_medio[idadeInicial][categoria];
//		double consumoAbateMedioDias = Tabelas.consumoAcumulado[pesoAbate][categoria];

		double ganhoPeso = pesoAbateMedioDias - pesoMedioDias;
		double ganhoAbate = pesoAbateCalculado - pesoAlojamento;

//		double consumo = consumoAbateMedioDias - consumoMedioDias;
//		double percentualConsumo = (((consumoRacaoCalculado * 100)/consumo) - 100);
		//		double consumoAbate = consumoRacaoCalculado - 
		System.out.println("Peso Medio no Dia "+(idadeInicial+21) +" = "+pesoMedioDias);
		System.out.println("Peso Medio Abate no Dia "+(idadeAbate+21) +" = "+pesoAbateMedioDias);
		System.out.println("Ganho de Peso = "+ganhoPeso);
		System.out.println("Ganho ao Abate = "+ganhoAbate);
		System.out.println("Percentual  = "+percentual);
		System.out.println("Aos 21 Simulada = "+((pesoMedioDias * percentual) + pesoMedioDias));
//		System.out.println("Consumo no Dia "+(idadeInicial+21)+" = "+consumoMedioDias);
//		System.out.println("Consumo no Dia "+(pesoAbate+21)+" = "+consumoAbateMedioDias);
//		System.out.println("Consumo "+consumo);
//		System.out.println("Consumo Percentual "+percentualConsumo);

		for (int i = 0; i < Tabelas.peso_medio.length; i++) {
			curvaSimulada[i][0] = ( Tabelas.peso_medio[i][categoria] * percentual)/100;
			System.out.println("Dia Peso Acumulado "+(i+21)+ " = "+curvaSimulada[i][0]);
		}
		for (int i = 0; i < Tabelas.ganho_de_peso.length; i++) {
			curvaSimulada[i][1] = ( Tabelas.ganho_de_peso[i][categoria] * percentual)/100;
			System.out.println("Ganho de Peso Simulador - > Dia "+(i+21)+ " = "+curvaSimulada[i][1]);
		}

		for (int i = 0; i < Tabelas.consumo_racao_acumulado.length; i++) {
			curvaSimulada[i][2] = (Tabelas.consumo_racao_acumulado[i][categoria]  * percentualConsumo)/100;
			System.out.println("Consumo Simulador - > Dia "+(i+21)+ " = "+curvaSimulada[i][2]);
		}

		for (int i = 0; i < Tabelas.consumo_racao.length; i++) {
			curvaSimulada[i][3] =  (Tabelas.consumo_racao[i][categoria]  * percentualConsumo)/100 ;
			System.out.println("Consumo Ração Diário Simulador - > Dia "+(i+21)+ " = "+curvaSimulada[i][3]);
		}
		return curvaSimulada;
	}

	public double media(double tabela[][], int inicioFase, int fimFase, int categoria){
		double media = 0.0;

		for (int i = inicioFase; i <= fimFase; i++) {
			System.out.println("Dias "+i +" Valor "+tabela[i][categoria]);
			media += tabela[i][categoria];
		}
		System.out.println("MEDIA "+media);
		return (media/((fimFase-inicioFase)+1));
	}
	/**
	 * Aplica o percentual a tabela referente ao peso
	 * @param percentual
	 * @param idadeAbate
	 */
	public void percentualCorrecaoPeso(double[][] percentual,int idadeAbate, int categoria, double pesoAbate){
		double pesoPadrao = Tabelas.pesoMedio_machosCastrados[idadeAbate][categoria];
		System.out.println(pesoPadrao);

		double percent = ((pesoAbate * 100)/pesoPadrao) - 100;
		for (int i = 0; i < percentual.length; i++) {
			percentual[i][categoria] = percentual[i][categoria] * percent;
		}
		System.out.println("PERCENTUAL = "+percent);
	}

	/**
	 * Calcula o peso Medio de acordo com os valores tabelados
	 * @param categoria sexo do animal
	 * @param faseInicial inicio da fase 
	 * @param faseFinal final da fase
	 * @return peso medio tabelado
	 */
	public double pesoMediaTabelado(int categoria, int faseInicial, int faseFinal){
		double pesoMedio = 0.0;
		int qtd = 0;
		for (int i = faseInicial; i <= faseFinal; i++) {
			pesoMedio += Tabelas.peso_medio[i][categoria];
			qtd++;			
		}	
		return pesoMedio/qtd;
	}
	/**
	 * Calcula o peso Medio de acordo com os valores simulado
	 * @param categoria sexo do animal
	 * @param faseInicial inicio da fase 
	 * @param faseFinal final da fase
	 * @return peso medio tabelado
	 */
	public double pesoMediaSimulado(double[][] tabela,int categoria, int faseInicial, int faseFinal){
		double pesoMedio = 0.0;
		int qtd = 0;
		for (int i = faseInicial; i <= faseFinal; i++) {
			pesoMedio += tabela[i][0];
			qtd++;			
		}	
		return pesoMedio/qtd;
	}
	/**
	 * Ganho de peso ignorando o primeiro dia 
	 * @param categoria
	 * @param faseInicial
	 * @param faseFinal
	 * @return
	 */
	public double ganhoDePeso(int categoria, int faseInicial , int faseFinal){
		double ganhoPeso = 0.0;
		for (int i = faseInicial; i <= faseFinal; i++) {
			ganhoPeso += Tabelas.ganho_de_peso[i][categoria];
		}
		return ganhoPeso - Tabelas.ganho_de_peso[faseInicial][categoria];

	}
	/**
	 * Ganho de peso ignorando o primeiro dia 
	 * @param categoria
	 * @param faseInicial
	 * @param faseFinal
	 * @return
	 */
	public double ganhoDePesoSimulado(double[][] tabela,int categoria, int faseInicial , int faseFinal){
		double ganhoPeso = 0.0;
		for (int i = faseInicial; i <= faseFinal; i++) {
			ganhoPeso += tabela[i][1];
		}
		return ganhoPeso - tabela[faseInicial][1];
		
	}
	/**
	 * Calcula o ganho de Peso sem desconsiderar o primeiro dia
	 * @param categoria
	 * @param faseInicial
	 * @param faseFinal
	 * @return
	 */
	public double ganhoDePesoFases(int categoria, int faseInicial , int faseFinal){
		double ganhoPeso = 0.0;
		for (int i = faseInicial; i <= faseFinal; i++) {
			ganhoPeso += Tabelas.ganho_de_peso[i][categoria];
		}
		return ganhoPeso;

	}
	/**
	 * Calcula o ganho de Peso sem desconsiderar o primeiro dia
	 * @param categoria
	 * @param faseInicial
	 * @param faseFinal
	 * @return
	 */
	public double ganhoDePesoFasesSimulado(double[][] tabela,int categoria, int faseInicial , int faseFinal){
		double ganhoPeso = 0.0;
		for (int i = faseInicial; i <= faseFinal; i++) {
			ganhoPeso += tabela[i][1];
		}
		return ganhoPeso;
		
	}
	/**
	 * Consum medio desconsiderando o primeiro dia 
	 * @param categoria
	 * @param faseInicial
	 * @param faseFinal
	 * @return
	 */
	public double consumoMedia(int categoria, int faseInicial , int faseFinal){
		double consumo = 0.0;
		for (int i = faseInicial; i <= faseFinal; i++) {
			consumo += Tabelas.consumo_racao[i][categoria];
		}
		return consumo - Tabelas.consumo_racao[faseInicial][categoria];

	}
	/**
	 * Consum medio desconsiderando o primeiro dia 
	 * @param categoria
	 * @param faseInicial
	 * @param faseFinal
	 * @return
	 */
	public double consumoMediaSimulado(double[][] tabela,int categoria, int faseInicial , int faseFinal){
		double consumo = 0.0;
		for (int i = faseInicial; i <= faseFinal; i++) {
			consumo += tabela[i][3];
		}
		return consumo - tabela[faseInicial][3];
		
	}
	/**
	 * Consumo medio da segunda fase em diante
	 * @param categoria
	 * @param faseInicial
	 * @param faseFinal
	 * @return
	 */
	public double consumoMediaFases(int categoria, int faseInicial , int faseFinal){
		double consumo = 0.0;
		for (int i = faseInicial; i <= faseFinal; i++) {
			consumo += Tabelas.consumo_racao[i][categoria];
		}
		return consumo;

	}
	/**
	 * Consumo medio da segunda fase em diante
	 * @param categoria
	 * @param faseInicial
	 * @param faseFinal
	 * @return
	 */
	public double consumoMediaFasesSimulado(double[][] tabela,int categoria, int faseInicial , int faseFinal){
		double consumo = 0.0;
		for (int i = faseInicial; i <= faseFinal; i++) {
			consumo += tabela[i][3];
		}
		return consumo;
		
	}
	


}
