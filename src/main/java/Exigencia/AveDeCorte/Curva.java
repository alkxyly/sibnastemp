package Exigencia.AveDeCorte;

public class Curva {

	private Double consumoRacao;
	private Double ganhoPeso;
	private Double conversaoAlimentar;
	private Double pesoAcumulado;
	private Double consumoRacaoAcumulado;

	public Double getConsumoRacao() {
		return consumoRacao;
	}
	public void setConsumoRacao(Double consumoRacao) {
		this.consumoRacao = consumoRacao;
	}
	public Double getGanhoPeso() {
		return ganhoPeso;
	}
	public void setGanhoPeso(Double ganhoPeso) {
		this.ganhoPeso = ganhoPeso;
	}
	public Double getConversaoAlimentar() {
		return conversaoAlimentar;
	}
	public void setConversaoAlimentar(Double conversaoAlimentar) {
		this.conversaoAlimentar = conversaoAlimentar;
	}
	public Double getPesoAcumulado() {
		return pesoAcumulado;
	}
	public void setPesoAcumulado(Double pesoAcumulado) {
		this.pesoAcumulado = pesoAcumulado;
	}
	public Double getConsumoRacaoAcumulado() {
		return consumoRacaoAcumulado;
	}
	public void setConsumoRacaoAcumulado(Double consumoRacaoAcuimulado) {
		this.consumoRacaoAcumulado = consumoRacaoAcuimulado;
	}
}
