package Converter;

import java.io.Serializable;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import Origem.Origem;
import entidades.EntidadeAbst;


@FacesConverter(value = "entidadeConverter", forClass = EntidadeAbst.class)
public class EntidadeConverter implements Converter , Serializable{

	private static final long serialVersionUID = -1866102177947131322L;

	@Override
    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
		if (value != null && !value.isEmpty()) {
            return (Origem) component.getAttributes().get(value);
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext ctx, UIComponent component, Object value) {
    	//return value == null ? "" : value.toString();
    	if(value instanceof Origem){
    		Origem origem = (Origem) value;
            if (origem != null && origem instanceof Origem && origem.getId() != null) {
                component.getAttributes().put(origem.getId().toString(), origem);
                return origem.getId().toString();
         	}
    	}
    	return "";
    }

    protected void addAttribute(UIComponent component, EntidadeAbst o) {
        String key = o.getId().toString();
        this.getAttributesFrom(component).put(key, o);
    }

    protected Map<String, Object> getAttributesFrom(UIComponent component) {
        return component.getAttributes();
    }

}
