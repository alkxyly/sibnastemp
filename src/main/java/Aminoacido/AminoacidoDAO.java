package Aminoacido;

import java.util.List;

public interface AminoacidoDAO {
	public void salvar(Aminoacido alimentoNutriente);
	public void excluir(Aminoacido alimentoNutriente);
	public void atualizar(Aminoacido alimentoNutriente);
	public Aminoacido carregar(Integer id);
	public List<Aminoacido> listar();
}
