package Util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * 
 * @author alkxly
 * @since 18 de outrubro de 2015
 */
public class Mensagens {
	/**
	 * @author alkxly
	 * @since 18 de outrubro de 2015
	 * @param mensagem - mensagem que será exibida.
	 * Adiciona uma mensagem de confirmação
	 */
	public static void adicionarMensagemConfirmacao(String mensagem){
		FacesContext context = FacesContext.getCurrentInstance();         
		context.addMessage(null, new FacesMessage(mensagem) );
	}
	public static void adicionarMensagemErro(String mensagem){
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage msm = new FacesMessage(mensagem);
		msm.setSeverity(FacesMessage.SEVERITY_ERROR);
		context.addMessage(null,msm);
		
	}
}
