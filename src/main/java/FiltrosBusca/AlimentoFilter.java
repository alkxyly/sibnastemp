package FiltrosBusca;

public class AlimentoFilter {
	private String codigo;
	private String nome;
	
	public AlimentoFilter(){
		this.codigo = "";
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
}
