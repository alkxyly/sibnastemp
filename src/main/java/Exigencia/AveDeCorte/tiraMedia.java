package Exigencia.AveDeCorte;

public class tiraMedia {

	public static void main(String[] args) {
		
		ExigenciaAvesBean ea = new ExigenciaAvesBean();		
		System.out.println("Gompertz Peso vivo Macho");
		System.out.println(ea.gompertzPesoVivo(6.56574, 0.0427, 45.0, 36.48888, 2.718282));
        System.out.println("-----");
        System.out.println("Gompertz Ganho de Peso Macho");
        System.out.println(ea.gompertzGanhoPeso(6.56574*1000, 0.0427, 45.0, 36.48888, 2.718282));
        
        System.out.println("-----");		
		System.out.println("Gompertz Peso vivo Femea");
		System.out.println(ea.gompertzPesoVivo(5.02701, 0.04376, 1.0, 33.99013, 2.718282));
        System.out.println("-----");
        System.out.println("Gompertz Ganho de Peso Femea");
        System.out.println(ea.gompertzGanhoPeso(5.02701*1000,0.04376, 1.0, 33.9901, 2.718282));
        
        System.out.println(" ");
        System.out.println("Teste Convers�o alimentar Macho m�dio");
        System.out.println(" ");
          Double[] ca = ea.calcularConversaoAlimentar(ea.getExigenciaAves().getConsumoRacaoMistoMedio(), ea.getExigenciaAves().getGanhoPesoMistoMedio());
        for(int i = 0; i<56;i++){
        	
        	System.out.println("Idade de abate "+ i +" "+ca[i]);
        	
        }
        
        System.out.println("");
        
        int i = 1;
        for(Double CRMM:ea.getExigenciaAves().getConsumoRacaoMistoMedio()){
        	System.out.println(CRMM);
        }
        
        System.out.println();
        for(Double GPMM:ea.getExigenciaAves().getGanhoPesoMistoMedio()){
        	System.out.println(GPMM);
        }
        
        System.out.println();
        for(Double GPV:ea.gompertzPesoVivo(6.56574, 0.0427, 56.0, 36.48888, 2.718282)){
        	System.out.println(GPV);
        }
        
        for(Double GGP:ea.gompertzGanhoPeso(6.56574*1000, 0.0427, 56.0, 36.48888, 2.718282)){
        	System.out.println(GGP);
        }
        
        Curva[] curvaPadrao = ea.calcularCurvaPadrao(ea.getExigenciaAves().getConsumoRacaoMistoMedio(), ea.getExigenciaAves().getGanhoPesoMistoMedio(), ea.gompertzPesoVivo(6.56574, 0.0427, 56.0, 36.48888, 2.718282), ea.gompertzGanhoPeso(6.56574*1000, 0.0427, 56.0, 36.48888, 2.718282));
        
       
        
        for(Curva cp:curvaPadrao){
        	System.out.println("Dia "+ i);
        	//System.out.println(cp.getConsumoRacao());
        	System.out.println(cp.getGanhoPeso());
        	//System.out.println(cp.getConversaoAlimentar());
        	//System.out.println(cp.getConsumoRacaoAcumulado());
        	//System.out.println(cp.getPesoAcumulado());
        	System.out.println("-------------------");
        }
	}

}
