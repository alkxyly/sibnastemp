package AlimentoNutrientes;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import br.com.calculador.alimento.Alimento;

public class AlimentoNutrienteDAOHibernate implements AlimentoNutrienteDAO {
	private Session session ;


	public void setSession(Session session) {
		this.session = session;

	}

	@Override
	public void salvar(AlimentoNutriente alimentoNutriente) {
		this.session.save(alimentoNutriente);
	}

	@Override
	public void excluir(AlimentoNutriente alimentoNutriente) {
		this.session.delete(alimentoNutriente);

	}

	@Override
	public void atualizar(AlimentoNutriente alimentoNutriente) {
		this.session.update(alimentoNutriente);

	}

	@Override
	public AlimentoNutriente carregar(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AlimentoNutriente> listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isAlimentoAssociado(Alimento alimento) {
		boolean isAssociado = false;		
		Integer id = alimento.getId();
		
		Query query = this.session.createQuery("from AlimentoNutriente al where al.alimento.id = :id "); 
		query.setParameter("id", id );
		
		List<AlimentoNutriente> alimentoNutriente = query.list();
	
		if(alimentoNutriente.size() > 0){
			isAssociado = true;
		}else 
			isAssociado = false;
		
		return isAssociado;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<AlimentoNutriente> listaComAlimento(Alimento alimento) {
		Query query = this.session.createQuery("from AlimentoNutriente al where al.alimento.id = :id and al.nutriente.codigo IN(111,123,145,701,740,711,720,721,123,144,121,700,710,730,100,151,141,211)");
		query.setParameter("id", alimento.getId());
		
		List<AlimentoNutriente> alimentoNutriente = query.list();
		return alimentoNutriente;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AlimentoNutriente> listaComAlimentoSuino(Alimento alimento) {
		Query query = this.session.createQuery("from AlimentoNutriente al where al.alimento.id = :id and al.nutriente.codigo IN(111,145,140,125,701,740,711,720,721,144,121,701,730,740,100,151,141,228,225,220,221,226)");
		query.setParameter("id", alimento.getId());
		
		List<AlimentoNutriente> alimentoNutriente = query.list();
		return alimentoNutriente;
	}

	@Override
	public void excluirID(Integer id) {
		Query query = this.session.createQuery("delete AlimentoNutriente al where al.id = :id");
		query.setParameter("id", id);
		int result = query.executeUpdate();
		System.out.println("resultado "+result);
	}

}
