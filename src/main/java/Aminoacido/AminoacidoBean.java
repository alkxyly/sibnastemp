package Aminoacido;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "aminoacidoBean")
@ViewScoped
public class AminoacidoBean {
	private final String CADASTRAR_AMINOACIDO = "CadastrarAminoacido";

	private Aminoacido aminoacido;
	private List<Aminoacido> listar;

	public AminoacidoBean(){
		this.aminoacido = new Aminoacido();
	}

	/**
	 * @author alkxly
	 * @since 20 de setembro de 2015
	 * @version 1.0
	 * @return página após  o cadastro de aminoácido.
	 */
	public String salvar(){
		AminoacidoRN aminoacidoRN = new AminoacidoRN();
		aminoacidoRN.salvar(this.aminoacido);
		this.aminoacido = null;
		return CADASTRAR_AMINOACIDO;
	}
	
	public Aminoacido getAminoacido() {
		return aminoacido;
	}
	public void setAminoacido(Aminoacido aminoacido) {
		this.aminoacido = aminoacido;
	}
	public List<Aminoacido> getListar() {
		
		AminoacidoRN aminoacidoRN = new AminoacidoRN();
	
		if(this.listar == null){
			this.listar = aminoacidoRN.listar();
		}
		
		return listar;
	}
	public void setListar(List<Aminoacido> listar) {
		this.listar = listar;
	}

}
