package br.com.calculador.alimento;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import FiltrosBusca.AlimentoFilter;


public class AlimentoDAOHibernate implements AlimentoDAO {
	private Session session;

	public void setSession(Session session) {
		this.session = session;
	}

	@Override
	public void salvar(Alimento alimento) {
		this.session.save(alimento);

	}

	@Override
	public void excluir(Alimento alimento) {
		this.session.delete(alimento);		
	}

	@Override
	public void atualizar(Alimento alimento) {
		this.session.saveOrUpdate(alimento);

	}

	@Override
	public Alimento carregar(Integer codigo) {
		return (Alimento) this.session.get(Alimento.class,codigo);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Alimento> listar() {
		Query queySql = this.session.createSQLQuery("select * from tb_alimentos order by codigo asc").addEntity(Alimento.class);
		List<Alimento> lista = queySql.list();
		return lista;
		//		return  (List<Alimento>) this.session.createSQLQuery("select * from tb_alimentos").addEntity(Alimento.class);
		//	createCriteria(Alimento.class).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Alimento> listarPorCodigo(String codigo) {

		Query hql = this.session.createQuery("from Alimento as alimento where alimento.codigo like :codigo");
		hql.setParameter("codigo",codigo+"%");
		List<Alimento> lista=  hql.list();
		return lista;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Alimento> pesquisar(AlimentoFilter alimentoFilter) {
		Criteria criteria = this.session.createCriteria(Alimento.class);

		if(StringUtils.isNotBlank(alimentoFilter.getCodigo())){
			criteria.add(Restrictions.eq("codigo",alimentoFilter.getCodigo()));
		}
		if(StringUtils.isNotBlank(alimentoFilter.getNome())){
			criteria.add(Restrictions.ilike("nome",alimentoFilter.getNome(),MatchMode.ANYWHERE));
		}
		return criteria.addOrder(Order.asc("codigo")).list();
	}

}
