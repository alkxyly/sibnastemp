//package Exigencia.AvePoedeira;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.faces.bean.ManagedBean;
//import javax.faces.bean.SessionScoped;
//
//import org.primefaces.context.RequestContext;
//
//@ManagedBean(name = "exigenciaAvePoedeiraBean")
//@SessionScoped
//public class ExigenciaAvesBean {
//
//	private ExigenciaAves exigenciaAves1;
//	private ExigenciaAves exigenciaAves2;
//	private ExigenciaAves exigenciaAves3;
//	private ExigenciaAves exigenciaAves4;
//	private ExigenciaAves exigenciaAves5;
//	private ExigenciaAves exigenciaAves6;
//	private ExigenciaAves exigenciaAves7;
//	private ExigenciaAves exigenciaAves8;
//	private ExigenciaAves exigenciaAves9;
//
//
//	public ExigenciaAvesBean() {
//		this.exigenciaAves1 = new ExigenciaAves();
//		this.exigenciaAves2 = new ExigenciaAves();
//		this.exigenciaAves3 = new ExigenciaAves();
//		this.exigenciaAves4 = new ExigenciaAves();
//		this.exigenciaAves5 = new ExigenciaAves();
//		this.exigenciaAves6 = new ExigenciaAves();
//		this.exigenciaAves7 = new ExigenciaAves();
//		this.exigenciaAves8 = new ExigenciaAves();
//		this.exigenciaAves9 = new ExigenciaAves();
//	}
//
//	public void calcular(String exigencia) {
//
//		ExigenciaAvesRN exigenciaRN = new ExigenciaAvesRN();
//
//		// 1- 7
//		if (exigencia.equals("1")) {
//			this.exigenciaAves1 = exigenciaRN.CalcularExigencias(exigenciaAves1);
//		}
//
//		if (exigencia.equals("2")) {
//			this.exigenciaAves2 = exigenciaRN.CalcularExigencias(exigenciaAves2);
//		}
//
//		// 8-21
//		if (exigencia.equals("3")) {
//			this.exigenciaAves3 = exigenciaRN.CalcularExigencias(exigenciaAves3);
//		}
//
//		if (exigencia.equals("4")) {
//			this.exigenciaAves4 = exigenciaRN.CalcularExigencias(exigenciaAves4);
//		}
//
//		// 22-42
//		if (exigencia.equals("5")) {
//			this.exigenciaAves5 = exigenciaRN.CalcularExigencias(exigenciaAves5);
//		}
//
//		if (exigencia.equals("6")) {
//			this.exigenciaAves6 = exigenciaRN.CalcularExigencias(exigenciaAves6);
//		}
//
//		if (exigencia.equals("7")) {
//			this.exigenciaAves7 = exigenciaRN.CalcularExigencias(exigenciaAves7);
//		}
//
//		// 43-56
//		if (exigencia.equals("8")) {
//			this.exigenciaAves8 = exigenciaRN.CalcularExigencias(exigenciaAves8);
//		}
//	}
//
//	public void PerfilAminoacidosDigestiveis(Integer dialog) {
//		Map<String, Object> options = new HashMap<String, Object>();
//
//		options.put("modal", true);
//		options.put("width", 680);
//		options.put("height", 400);
//		options.put("contentHeight", 430);
//		options.put("contentWidth", 680);
//		options.put("headerElement", "customheader");
//		options.put("resizable", false);
//
//		if (dialog == 0) {
//			RequestContext.getCurrentInstance().openDialog("/cliente/exigencia/aves/AA/DigestiveisAvesDeCorte", options, null);
//		} else if (dialog == 1) {
//			RequestContext.getCurrentInstance().openDialog("/cliente/exigencia/aves/AA/TotaisAvesDeCorte", options, null);
//		}
//	}
//
//	public ExigenciaAves getExigenciaAves1() {
//		return exigenciaAves1;
//	}
//
//	public void setExigenciaAves1(ExigenciaAves exigenciaAves1) {
//		this.exigenciaAves1 = exigenciaAves1;
//	}
//
//	public ExigenciaAves getExigenciaAves2() {
//		return exigenciaAves2;
//	}
//
//	public void setExigenciaAves2(ExigenciaAves exigenciaAves2) {
//		this.exigenciaAves2 = exigenciaAves2;
//	}
//
//	public ExigenciaAves getExigenciaAves3() {
//		return exigenciaAves3;
//	}
//
//	public void setExigenciaAves3(ExigenciaAves exigenciaAves3) {
//		this.exigenciaAves3 = exigenciaAves3;
//	}
//
//	public ExigenciaAves getExigenciaAves4() {
//		return exigenciaAves4;
//	}
//
//	public void setExigenciaAves4(ExigenciaAves exigenciaAves4) {
//		this.exigenciaAves4 = exigenciaAves4;
//	}
//
//	public ExigenciaAves getExigenciaAves5() {
//		return exigenciaAves5;
//	}
//
//	public void setExigenciaAves5(ExigenciaAves exigenciaAves5) {
//		this.exigenciaAves5 = exigenciaAves5;
//	}
//
//	public ExigenciaAves getExigenciaAves6() {
//		return exigenciaAves6;
//	}
//
//	public void setExigenciaAves6(ExigenciaAves exigenciaAves6) {
//		this.exigenciaAves6 = exigenciaAves6;
//	}
//
//	public ExigenciaAves getExigenciaAves7() {
//		return exigenciaAves7;
//	}
//
//	public void setExigenciaAves7(ExigenciaAves exigenciaAves7) {
//		this.exigenciaAves7 = exigenciaAves7;
//	}
//
//	public ExigenciaAves getExigenciaAves8() {
//		return exigenciaAves8;
//	}
//
//	public void setExigenciaAves8(ExigenciaAves exigenciaAves8) {
//		this.exigenciaAves8 = exigenciaAves8;
//	}
//
//	public ExigenciaAves getExigenciaAves9() {
//		return exigenciaAves9;
//	}
//
//	public void setExigenciaAves9(ExigenciaAves exigenciaAves9) {
//		this.exigenciaAves9 = exigenciaAves9;
//	}
//
//	
//}
