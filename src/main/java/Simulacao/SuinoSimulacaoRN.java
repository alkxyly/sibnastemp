package Simulacao;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.labels.IntervalCategoryItemLabelGenerator;


import AlimentoNutrientes.AlimentoNutriente;
import Nutriente.Nutriente;
import Origem.Origem;

/**
 * 
 * @author Rodrigo
 *
 */

public class SuinoSimulacaoRN {

	/**
	 * Calcula a energia digestivel suino
	 * 
	 * 
	 * 
	 * @param listaNutrientes
	 *            e orig lista com os nutrientes que entram na simulação e a
	 *            origem.
	 * @return valor da energia digestivel
	 */

	public double calcularSuinosEnergiaDigestivel(List<AlimentoNutriente> listaNutrientes, Origem orig) {

		Double EdSuino = 0.0;

		if (orig.getNome().equals("Vegetal")) {
			EdSuino = suinoEnergiaDigestivelVegetalLacteos(listaNutrientes);
		} else {
			EdSuino = suinoEnergiaDigestivelAnimalGorduras(listaNutrientes);
		}

		return EdSuino;
	}

	/**
	 * Calcula a energia metabolizavel suino
	 * 
	 * 
	 * 
	 * @param listaNutrientes
	 *            e orig lista com os nutrientes que entram na simulação e a
	 *            origem.
	 * @return valor da energia digestivel
	 */

	public double calcularSuinosEnergiaMetabolizavel(List<AlimentoNutriente> listaNutrientes, Origem orig) {

		Double EmSuino = 0.0;

		if (orig.getNome().equals("Vegetal")) {
			EmSuino = suinoEnergiaMetabolizavelVegetalLacteos(listaNutrientes);
		}
		if (orig.getNome().equals("Animal")) {
			EmSuino = suinoEnergiaMetabolizavelAnimal(listaNutrientes);
		}
		if (orig.getNome().equals("Gordura")) {
			EmSuino = suinoEnergiaMetabolizavelCarboidratosGorduras(listaNutrientes);
		}
		return EmSuino;
	}

	/**
	 * Calcula a energia liquida suino
	 * 
	 * 
	 * 
	 * @param listaNutrientes
	 * 
	 * @return valor da energia digestivel
	 */
	public double calcularSuinosEnergiaLiquida(List<AlimentoNutriente> listaNutrientes) {

		return suinoEnergiaLiquida(listaNutrientes);
	}

	/**
	 * Calcula a energia digestivel porcas
	 * 
	 * 
	 * 
	 * @param listaNutrientes
	 *            e orig lista com os nutrientes que entram na simulação e a
	 *            origem.
	 * @return valor da energia digestivel
	 */

	public double calcularPorcasEnergiaDigestivel(List<AlimentoNutriente> listaNutrientes, Origem orig) {

		Double EdSuino = 0.0;

		if (orig.getNome().equals("Vegetal")) {
			EdSuino = porcaEnergiaDigestivelVegetal(listaNutrientes);
		}

		return EdSuino;
	}

	/**
	 * Calcula a energia liquida porcas
	 * 
	 * 
	 * 
	 * @param listaNutrientes
	 * 
	 * @return valor da energia digestivel
	 */
	public double calcularPorcasEnergiaLiquida(List<AlimentoNutriente> listaNutrientes, Origem orig) {

		Double ELporcas = 0.0;

		if (orig.getNome().equals("Vegetal")) {
			ELporcas = porcaEnergiaLiquida(listaNutrientes);
		}
		return ELporcas;
	}

	/**
	 * Calcula a energia metabolizavel porcas
	 * 
	 * 
	 * 
	 * @param listaNutrientes
	 *            e orig lista com os nutrientes que entram na simulação e a
	 *            origem.
	 * @return valor da energia digestivel
	 */

	public double calcularPorcasEnergiaMetabolizavel(List<AlimentoNutriente> listaNutrientes, Origem orig) {

		Double EmSuino = 0.0;

		if (orig.getNome().equals("Vegetal")) {
			EmSuino = porcaEnergiaMetabolizavelVegetal(listaNutrientes);
		}

		return EmSuino;
	}

	/**
	 * Calcula a energia disgetivel dos alimentos de origem vegetal e lacteos.
	 * 
	 *
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia disgetivel
	 */
	public Double suinoEnergiaDigestivelVegetalLacteos(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double CofDPB = 0.0;/// Coef. Disg. Proteina bruta Suino
		Double CoefDGB = 0.0;/// Coef. Disg. Gordura bruta
		Double coefMO = 0.0;// Coef. dig. MAteria organica.
		Double g = 0.0; // gordura bruta
		Double MO = 0.0;// Materia Organica
		Double EDsuino = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {

			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("701")) {
				CofDPB = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				g = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("711")) {
				CoefDGB = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("740")) {
				coefMO = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("145")) {
				MO = alimentoNutriente.getValorNutritivo();
			}

		}

		EDsuino = (5.65 * (PB * (CofDPB / 100))) + (9.45 * (g * (CoefDGB / 100)))
				+ (4.14 * (((MO * (coefMO / 100))) - (PB * (CofDPB / 100)) - (g * (CoefDGB / 100))));
		return EDsuino * 10;
	}

	/**
	 * Calcula a energia disgetivel dos alimentos de origem animal e gordura.
	 * 
	 * 
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia disgetivel
	 */
	public Double suinoEnergiaDigestivelAnimalGorduras(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double CofDPB = 0.0;/// Coef. Disg. Proteina bruta Suino
		Double CoefDGB = 0.0;/// Coef. Disg. Gordura bruta
		Double g = 0.0; // gordura bruta
		Double EDsuino = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {

			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				g = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("711")) {
				CoefDGB = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("701")) {
				CofDPB = alimentoNutriente.getValorNutritivo();
			}
		}

		EDsuino = 5.65 * (PB * (CofDPB / 100)) + (9.45 * (g * (CoefDGB / 100)));
		;
		return EDsuino * 10;
	}

	/**
	 * Calcula a energia metabolizavel dos alimentos de origem vegetal e
	 * lacteos.
	 * 
	 * 
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia disgetivel
	 */
	public Double suinoEnergiaMetabolizavelVegetalLacteos(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double CofDPB = 0.0;/// Coef. Disg. Proteina bruta Suino
		Double CoefDGB = 0.0;/// Coef. Disg. Gordura bruta
		Double coefMO = 0.0;// Coef. dig. MAteria organica.
		Double g = 0.0; // gordura bruta
		Double MO = 0.0;// Materia Organica
		Double EDsuino = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {

			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("701")) {
				CofDPB = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				g = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("711")) {
				CoefDGB = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("740")) {
				coefMO = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("145")) {
				MO = alimentoNutriente.getValorNutritivo();
			}

		}

		EDsuino = (4.952 * (PB * (CofDPB / 100)) + (9.45 * (g * (CoefDGB / 100)))
				+ (4.14 * (((MO * (coefMO / 100))) - (PB * (CofDPB / 100)) - (g * (CoefDGB / 100)))));

		return EDsuino * 10;
	}

	/**
	 * Calcula a energia metabolizavel dos alimentos de origem animal.
	 * 
	 * 
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia disgetivel
	 */

	public Double suinoEnergiaMetabolizavelAnimal(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double G = 0.0; // gordura bruta
		Double CofDPB = 0.0;/// Coef. Disg. Proteina bruta Suino
		Double CoefDGB = 0.0;/// Coef. Disg. Gordura bruta
		Double EMsuino = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {

			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				G = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("711")) {
				CoefDGB = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("701")) {
				CofDPB = alimentoNutriente.getValorNutritivo();
			}
		}

		EMsuino = (4.952 * (PB * (CofDPB / 100))) + (9.45 * (G * (CoefDGB / 100)));

		return EMsuino * 10;
	}

	/**
	 * Calcula a energia metabolizavel dos alimentos de origem carboidrato e
	 * gordura.
	 * 
	 * 
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia disgetivel
	 */

	public Double suinoEnergiaMetabolizavelCarboidratosGorduras(List<AlimentoNutriente> listaNutrientes) {

		Double EMsuino = 0.0; // Fibra bruta

		EMsuino = 0.965 * suinoEnergiaDigestivelAnimalGorduras(listaNutrientes);

		return EMsuino;
	}

	/**
	 * Calcula a energia liquida suino.
	 * 
	 *
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia liquida
	 */

	public Double suinoEnergiaLiquida(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta - 111
		Double FB = 0.0; // Fibra bruta - 141
		Double G = 0.0; // gordura bruta - 121
		Double A = 0.0; // Amido - 140
		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta -
							// 701
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura - 125
		Double EMsuino = 0.0; // Energia metabolizavel suino
		Double ELsuino = 0.0; // Energia liquida

		EMsuino = suinoEnergiaMetabolizavelVegetalLacteos(listaNutrientes);

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {

			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				G = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("141")) {
				FB = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("701")) {
				PBd = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("125")) {
				GD = alimentoNutriente.getValorNutritivo();
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("140")) {
				A = alimentoNutriente.getValorNutritivo();
			}
		}

		ELsuino = (0.73 * EMsuino) + (13.1 * G) + (3.7 * A) - (6.7 * PB) - (9.7 * FB);
		return ELsuino;
	}

	/**
	 * Calcula a energia disgetivel dos alimentos de origem vegetal para porcas.
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia disgetivel
	 */
	public Double porcaEnergiaDigestivelVegetal(List<AlimentoNutriente> listaNutrientes) {

		Double coefMO = 0.0; // Coeficiente de materia organica
		Double MO = 0.0; // Mat. Org�nica
		Double MOND = 0.0; // Mat. Org�nica N�o Digerida Su�nos, g/kg
		Double EDporca = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {

			if (alimentoNutriente.getNutriente().getCodigo().equals("740")) {
				coefMO = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("145")) {
				MO = alimentoNutriente.getValorNutritivo();
			}
		}

		MOND = ((100 - coefMO) * MO) / 100;

		EDporca = suinoEnergiaDigestivelVegetalLacteos(listaNutrientes) + MOND;

		return EDporca;
	}

	/**
	 * Calcula a energia disgetivel dos alimentos de origem vegetal para porcas.
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia disgetivel
	 */
	public Double porcaEnergiaMetabolizavelVegetal(List<AlimentoNutriente> listaNutrientes) {

		Double coefMO = 0.0; // Coeficiente de materia organica
		Double MO = 0.0; // Mat. Org�nica
		Double MOND = 0.0; // Mat. Org�nica N�o Digerida Su�nos, g/kg
		Double EMporca = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {

			if (alimentoNutriente.getNutriente().getCodigo().equals("740")) {
				coefMO = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("145")) {
				MO = alimentoNutriente.getValorNutritivo();
			}

		}

		MOND = ((100 - coefMO) * MO) / 100;

		EMporca = suinoEnergiaMetabolizavelVegetalLacteos(listaNutrientes) + (0.75 * MOND);

		return EMporca;
	}

	/**
	 * Calcula a energia liquida suino.
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia liquida
	 */

	public Double porcaEnergiaLiquida(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double FB = 0.0; // Fibra bruta
		Double G = 0.0; // gordura bruta
		Double A = 0.0; // Amido
		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		Double EMporcas = 0.0; // Energia metabolizavel suino de origem vegetal
		Double ELporca = 0.0; // Energia liquida

		EMporcas = porcaEnergiaMetabolizavelVegetal(listaNutrientes);

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {

			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				G = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("141")) {
				FB = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("701")) {
				PBd = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("711")) {
				GD = alimentoNutriente.getValorNutritivo();
			}

		}

		ELporca = (0.73 * EMporcas) + (13.1 * G) + (3.7 * A) - ((6.7 * PB) - (9.7 * FB));

		return ELporca;
	}

	public List<Comparativo> carregarComparativos(List<AlimentoNutriente> original, String tipo) {
		List<Comparativo> comps = new ArrayList<>();

		if (tipo.equals("Suinos")) {
			for (AlimentoNutriente alinut : original) {
				if (alinut.getNutriente().getCodigo().equals("228")) {
					Comparativo comp = new Comparativo();
					Nutriente nutTab = new Nutriente();
					nutTab = alinut.getNutriente();
					nutTab.setValor_nutritivo(alinut.getValorNutritivo());
					comp.setEnerTab(nutTab);

					Nutriente nutCalLi = new Nutriente();
					nutCalLi.setValor_nutritivo(ajustarEnergia(calcularSuinosEnergiaLiquida(original)));
					comp.setEnerCal(nutCalLi);
					comps.add(comp);

				}
				if (alinut.getNutriente().getCodigo().equals("225")) {
					Comparativo comp = new Comparativo();
					Nutriente nut = alinut.getNutriente();
					nut.setValor_nutritivo(alinut.getValorNutritivo());
					comp.setEnerTab(nut);

					Nutriente nutCalMet = new Nutriente();
					nutCalMet.setValor_nutritivo(ajustarEnergia(
							calcularSuinosEnergiaMetabolizavel(original, alinut.getAlimento().getOrigem())));
					comp.setEnerCal(nutCalMet);
					comps.add(comp);
				}
				if (alinut.getNutriente().getCodigo().equals("220")) {
					Comparativo comp = new Comparativo();
					Nutriente nut = alinut.getNutriente();
					nut.setValor_nutritivo(alinut.getValorNutritivo());
					comp.setEnerTab(nut);

					Nutriente nutCalDi = new Nutriente();
					nutCalDi.setValor_nutritivo(ajustarEnergia(
							calcularSuinosEnergiaDigestivel(original, alinut.getAlimento().getOrigem())));
					comp.setEnerCal(nutCalDi);
					comps.add(comp);
				}
			}
			comps = OrdenarComparativo(comps,"Suinos");
		}

		if (tipo.equals("Porcas")) {
			for (AlimentoNutriente alinut : original) {
				if (alinut.getNutriente().getCodigo().equals("226")) {
					Comparativo comp = new Comparativo();
					Nutriente nut = alinut.getNutriente();
					nut.setValor_nutritivo(alinut.getValorNutritivo());
					comp.setEnerTab(nut);

					Nutriente nutCalMet = new Nutriente();
					nutCalMet.setValor_nutritivo(ajustarEnergia(
							calcularPorcasEnergiaMetabolizavel(original, alinut.getAlimento().getOrigem())));
					comp.setEnerCal(nutCalMet);
					comps.add(comp);
				}

				if (alinut.getNutriente().getCodigo().equals("221")) {
					Comparativo comp = new Comparativo();
					Nutriente nut = alinut.getNutriente();
					nut.setValor_nutritivo(alinut.getValorNutritivo());
					comp.setEnerTab(nut);

					Nutriente nutCalDi = new Nutriente();
					nutCalDi.setValor_nutritivo(ajustarEnergia(
							calcularPorcasEnergiaDigestivel(original, alinut.getAlimento().getOrigem())));
					comp.setEnerCal(nutCalDi);
					comps.add(comp);
				}
			}
			comps = OrdenarComparativo(comps,"Porcas");
		}
		return comps;
	}

	public List<Comparativo> simularEnergia(List<Comparativo> comparativos,
			List<AlimentoNutriente> nutrientessAssociadoSimular, String tipo) {
		    Origem origem = nutrientessAssociadoSimular.get(1).getAlimento().getOrigem();

		List<Comparativo> comps = new ArrayList<>();

		if (tipo.equals("Suinos")) {
			for (Comparativo comparativo : comparativos) {

					if (comparativo.getEnerTab().getCodigo().equals("228")) {
						Nutriente nutCalSi = new Nutriente();
						nutCalSi.setValor_nutritivo(
								ajustarEnergia(calcularSuinosEnergiaLiquida(nutrientessAssociadoSimular)));
						comparativo.setEnerSim(nutCalSi);
						comps.add(comparativo);
						

					}

					if (comparativo.getEnerTab().getCodigo().equals("225")) {
						Nutriente nutCalSi = new Nutriente();
						nutCalSi.setValor_nutritivo(ajustarEnergia(calcularSuinosEnergiaMetabolizavel(
								nutrientessAssociadoSimular,origem)));
						comparativo.setEnerSim(nutCalSi);
						comps.add(comparativo);
						

					}
					if (comparativo.getEnerTab().getCodigo().equals("220")) {
						Nutriente nutCalSi = new Nutriente();
						nutCalSi.setValor_nutritivo(ajustarEnergia(calcularSuinosEnergiaDigestivel(
								nutrientessAssociadoSimular, origem)));
						comparativo.setEnerSim(nutCalSi);
						comps.add(comparativo);
						

					}
			}
		}
		if (tipo.equals("Porcas")) {
				for (Comparativo comparativo : comparativos) {

					if (comparativo.getEnerTab().getCodigo().equals("221")) {
						Nutriente nutCalSi = new Nutriente();
						nutCalSi.setValor_nutritivo(ajustarEnergia(calcularPorcasEnergiaDigestivel(
								nutrientessAssociadoSimular, origem)));
						comparativo.setEnerSim(nutCalSi);
						comps.add(comparativo);
						

					}

					if (comparativo.getEnerTab().getCodigo().equals("226")) {
						Nutriente nutCalSi = new Nutriente();
						nutCalSi.setValor_nutritivo(ajustarEnergia(calcularPorcasEnergiaMetabolizavel(
								nutrientessAssociadoSimular, origem)));
						comparativo.setEnerSim(nutCalSi);
						comps.add(comparativo);
						
					}
				
			}
		}
		return comps;
	}

	public List<AlimentoNutriente> OrdenarListaDeSimulacao(List<AlimentoNutriente> list) {
		List<AlimentoNutriente> ListAUX = new ArrayList<>();
		String[] ordenacao = { "100", "111", "701", "121", "125", "711", "151", "141", "720", "140", "145", "740",
				"144", "721" };
		
		Double PB = 0.0;
		Double CofDPB = 0.0;
		Double coefMO = 0.0;
		Double MO = 0.0;
		for (AlimentoNutriente alinut : list) {
			if (alinut.getNutriente().getCodigo().equals("111")) {
				PB = alinut.getValorNutritivo();
			}

			if (alinut.getNutriente().getCodigo().equals("701")) {
				CofDPB = alinut.getValorNutritivo();
			}
			if (alinut.getNutriente().getCodigo().equals("740")) {
				coefMO = alinut.getValorNutritivo();
			}

			if (alinut.getNutriente().getCodigo().equals("145")) {
				MO = alinut.getValorNutritivo();
			}
		}

		for (String ord : ordenacao) {
			for (AlimentoNutriente alinut : list) {
				if (alinut.getNutriente().getCodigo().equals(ord)) {
					ListAUX.add(alinut);
					if (ord.equals("111")) {

						AlimentoNutriente aliNut = new AlimentoNutriente();
						Nutriente nut = new Nutriente();

						nut.setCodigo("000");
						nut.setNome("Prote�na digestivel");
						aliNut.setAlimento(alinut.getAlimento());
						aliNut.setNutriente(nut);
						double valorNutritivo = (PB * CofDPB) / 100;

						try {
							DecimalFormat formato = new DecimalFormat("#.##");
							valorNutritivo = Double.valueOf(formato.format(valorNutritivo));
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
						aliNut.setValorNutritivo(valorNutritivo);
						ListAUX.add(aliNut);
						break;
					}
					if (ord.equals("740")) {

						AlimentoNutriente aliNut = new AlimentoNutriente();
						Nutriente nut = new Nutriente();

						nut.setCodigo("000");
						nut.setNome("Mat.Org�nica digestivel");
						aliNut.setAlimento(alinut.getAlimento());
						aliNut.setNutriente(nut);
						double valorNutritivo = (coefMO * MO) / 100;
						try {
							DecimalFormat formato = new DecimalFormat("#.##");
							valorNutritivo = Double.valueOf(formato.format(valorNutritivo));
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
						aliNut.setValorNutritivo(valorNutritivo);
						ListAUX.add(aliNut);
						break;
					}
					
					break;
				}
			}
		}

		return ListAUX;

	}

	public List<Comparativo> OrdenarComparativo(List<Comparativo> comparativos, String tipo){
		List<Comparativo> comparativosOrdenados = new ArrayList<>();
		String[] ordenacaoSuinos = {"220","225","228"};
		String[] ordenacaoPorcas = {"221","226"};
		
		if(tipo.equals("Suinos")){
		for (String ord : ordenacaoSuinos) {
			for (Comparativo comparativo : comparativos) {
				if (comparativo.getEnerTab().getCodigo().equals(ord)) {
					comparativosOrdenados.add(comparativo);
					break;
				}
			}
		  }
		}
		
		if(tipo.equals("Porcas")){
		for (String ord : ordenacaoPorcas) {
			for (Comparativo comparativo : comparativos) {
				if (comparativo.getEnerTab().getCodigo().equals(ord)) {
						comparativosOrdenados.add(comparativo);
					break;
				}
			 }
		  } 
		}
		
		return comparativosOrdenados;
	}

	public boolean OcultarEnergia(Integer codigo) {
		boolean ocultar = true;

		if (codigo == 226) {
			ocultar = false;
		}

		return ocultar;
	}
	
	public boolean naoEditavelRN(Integer codigo) {
		boolean naoEditavel = true;

		if (codigo.equals(111)) {
			naoEditavel = false;
		}
		if (codigo.equals(121)) {
			naoEditavel = false;
		}
		if (codigo.equals(140)) {
			naoEditavel = false;
		}
		if (codigo.equals(141)) {
			naoEditavel = false;
		}
		if (codigo.equals(145)) {
			naoEditavel = false;
		}
		if (codigo.equals(740)) {
			naoEditavel = false;
		}
		if (codigo.equals(701)) {
			naoEditavel = false;
		}
		if (codigo.equals(711)) {
			naoEditavel = false;
		}

		return naoEditavel;
	}

	public Double ajustarEnergia(Double energiaDouble) {
		return Double.valueOf(Math.round(energiaDouble));
	}
}