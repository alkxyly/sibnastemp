//package Exigencia.AvePoedeira;
//
//import Util.Mensagens;
//
//public class ExigenciaAvesRN {
//
//	public ExigenciaAves CalcularExigencias(ExigenciaAves ea) {
//
//		if (verificarDias(ea) == true) {
//			//if (VerificaFaixaDePeso(ea)) {
//				ea.setLisinaDigestivel(ExigenciaLisinaDigestivel(ea));
//				ea.setFosforoDigestivel(ExigenciaFosforoDigestivel(ea));
//				ea.setFosforoDisponivel(ExigenciaFosforoDisponivel(ea));
//				ea = ExigenciaAminoacidosAvesCorteDigestiveisTotais(ea);
//				ea = ExigenciaPorcetagem(ea);
//			//}
//		}
//		return ea;
//	}
//
//	public boolean verificarDias(ExigenciaAves EA) {
//		boolean autorizacao = false;
//
//		if (EA.getIdadeInicial() > EA.getIdadeFinal()) {
//			Mensagens.adicionarMensagemErro("Idade inicial tem que ser menor que idade final.");
//		} else {
//			autorizacao = true;
//		}
//
//		return autorizacao;
//	}
//
//	public ExigenciaAves ExigenciaAminoacidosAvesCorteDigestiveisTotais(ExigenciaAves ea) {
//		// 1-21 dias
//		if ((ea.getIdadeInicial() >= 1) & (ea.getIdadeFinal() <= 21)) {
//			// Aminoacidos Digestiveis
//			ea.setLisinaAAD(ExigenciaLisinaDia(ea));// lisina
//			ea.setMetioninaAAD(ExigenciaAminoacidoDia(ea, 39.0));// Metionina
//			ea.setMetioninaCistinaAAD(ExigenciaAminoacidoDia(ea, 72.0));// Metionina+Cistina
//			ea.setTreoninaAAD(ExigenciaAminoacidoDia(ea, 65.0));// Treonina
//			ea.setTriptofanoAAD(ExigenciaAminoacidoDia(ea, 17.0));// Tripfano
//			ea.setArgininaAAD(ExigenciaAminoacidoDia(ea, 108.0));// Arginina
//			ea.setGlicinaSerinaAAD(ExigenciaAminoacidoDia(ea, 147.0));// Glicina+Serina
//			ea.setValinaAAD(ExigenciaAminoacidoDia(ea, 77.0));// Valina
//			ea.setIsoleucinaAAD(ExigenciaAminoacidoDia(ea, 67.0));// Isoleucina
//			ea.setLeucinaAAD(ExigenciaAminoacidoDia(ea, 107.0));// Leucina
//			ea.setHistidinaAAD(ExigenciaAminoacidoDia(ea, 37.0));// Histi
//			ea.setFenilalaninaAAD(ExigenciaAminoacidoDia(ea, 63.0));// Feni
//			ea.setFenil_TirAAD(ExigenciaAminoacidoDia(ea, 115.0));// Feni+Tiro
//
//			// Aminoacidos Totais
//			ea.setLisinaAAT(ExigenciaLisinaTotal(ea));// lisina
//			ea.setMetioninaAAT(ExigenciaAminoacidoTotal(ea, 38.0));// Metionina
//			ea.setMetioninaCistinaAAT(ExigenciaAminoacidoTotal(ea, 72.0));// Metionina+Cistina
//			ea.setTreoninaAAT(ExigenciaAminoacidoTotal(ea, 68.0));// Treonina
//			ea.setTriptofanoAAT(ExigenciaAminoacidoTotal(ea, 17.0));// Tripfano
//			ea.setArgininaAAT(ExigenciaAminoacidoTotal(ea, 105.0));// Arginina
//			ea.setGlicinaSerinaAAT(ExigenciaAminoacidoTotal(ea, 150.0));// Glicina+Serina
//			ea.setValinaAAT(ExigenciaAminoacidoTotal(ea, 79.0));// Valina
//			ea.setIsoleucinaAAT(ExigenciaAminoacidoTotal(ea, 67.0));// Isoleucina
//			ea.setLeucinaAAT(ExigenciaAminoacidoTotal(ea, 107.0));// Leucina
//			ea.setHistidinaAAT(ExigenciaAminoacidoTotal(ea, 37.0));// Histi
//			ea.setFenilalaninaAAT(ExigenciaAminoacidoTotal(ea, 63.0));// Feni
//			ea.setFenil_TirAAT(ExigenciaAminoacidoTotal(ea, 115.0));// Feni+Tiro
//		} else if ((ea.getIdadeInicial() >= 22) & (ea.getIdadeFinal() <= 56)) { // 22-56
//																				// dias
//			// Aminoacidos Digestiveis
//			ea.setLisinaAAD(ExigenciaLisinaDia(ea));// lisina
//			ea.setMetioninaAAD(ExigenciaAminoacidoDia(ea, 40.0));// Metionina
//			ea.setMetioninaCistinaAAD(ExigenciaAminoacidoDia(ea, 73.0));// Metionina+Cistina
//			ea.setTreoninaAAD(ExigenciaAminoacidoDia(ea, 65.0));// Treonina
//			ea.setTriptofanoAAD(ExigenciaAminoacidoDia(ea, 18.0));// Tripfano
//			ea.setArgininaAAD(ExigenciaAminoacidoDia(ea, 108.0));// Arginina
//			ea.setGlicinaSerinaAAD(ExigenciaAminoacidoDia(ea, 134.0));// Glicina+Serina
//			ea.setValinaAAD(ExigenciaAminoacidoDia(ea, 78.0));// Valina
//			ea.setIsoleucinaAAD(ExigenciaAminoacidoDia(ea, 68.0));// Isoleucina
//			ea.setLeucinaAAD(ExigenciaAminoacidoDia(ea, 108.0));// Leucina
//			ea.setHistidinaAAD(ExigenciaAminoacidoDia(ea, 37.0));// Histi
//			ea.setFenilalaninaAAD(ExigenciaAminoacidoDia(ea, 63.0));// Feni
//			ea.setFenil_TirAAD(ExigenciaAminoacidoDia(ea, 115.0));// Feni+Tiro
//
//			// Aminoacidos Totais
//			ea.setLisinaAAT(ExigenciaLisinaTotal(ea));// lisina
//			ea.setMetioninaAAT(ExigenciaAminoacidoTotal(ea, 49.0));// Metionina
//			ea.setMetioninaCistinaAAT(ExigenciaAminoacidoTotal(ea, 73.0));// Metionina+Cistina
//			ea.setTreoninaAAT(ExigenciaAminoacidoTotal(ea, 68.0));// Treonina
//			ea.setTriptofanoAAT(ExigenciaAminoacidoTotal(ea, 18.0));// Tripfano
//			ea.setArgininaAAT(ExigenciaAminoacidoTotal(ea, 105.0));// Arginina
//			ea.setGlicinaSerinaAAT(ExigenciaAminoacidoTotal(ea, 137.0));// Glicina+Serina
//			ea.setValinaAAT(ExigenciaAminoacidoTotal(ea, 80.0));// Valina
//			ea.setIsoleucinaAAT(ExigenciaAminoacidoTotal(ea, 68.0));// Isoleucina
//			ea.setLeucinaAAT(ExigenciaAminoacidoTotal(ea, 108.0));// Leucina
//			ea.setHistidinaAAT(ExigenciaAminoacidoTotal(ea, 37.0));// Histi
//			ea.setFenilalaninaAAT(ExigenciaAminoacidoTotal(ea, 63.0));// Feni
//			ea.setFenil_TirAAT(ExigenciaAminoacidoTotal(ea, 115.0));// Feni+Tiro
//
//		}
//
//		return ea;
//	}
//
//	public Double ExigenciaLisinaDia(ExigenciaAves ea) {
//		return (ea.getLisinaDigestivel() / ea.getConsumo()) * 100;
//	}
//
//	public Double ExigenciaLisinaTotal(ExigenciaAves ea) {
//		return (ea.getLisinaAAD() / 90.7) * 100;
//	}
//
//	public Double ExigenciaAminoacidoDia(ExigenciaAves ea, Double porcentagem) {
//		return (ea.getLisinaAAD() * porcentagem) / 100;
//	}
//
//	public Double ExigenciaAminoacidoTotal(ExigenciaAves ea, Double porcentagem) {
//		return (ea.getLisinaAAT() * porcentagem) / 100;
//	}
//
//	public Double ExigenciaLisinaDigestivel(ExigenciaAves ea) {
//		Double pesoMedio = ea.getPesoMedio();
//		Double ganhoPeso = ea.getGanhoPeso() / 1000;
//		Double lisinaDigMantenca = (0.07 * Math.pow(pesoMedio, 0.75));
//		Double lisinaDigestivel = 0.0;
//
//		// F�mea = 1, Macho = 2
//		if (ea.getSexo().equals("1")) {
//			Double lisinaDigGanho = ((14.42 + (2.859 * pesoMedio)) - (0.292 * Math.pow(pesoMedio, 2)));
//
//			lisinaDigestivel = lisinaDigMantenca + lisinaDigGanho * ganhoPeso;
//		}
//
//		if (ea.getSexo().equals("2")) {
//			Double lisinaDigGanho = ((14.43 + (2.543 * pesoMedio)) - (0.271 * Math.pow(pesoMedio, 2)));
//
//			lisinaDigestivel = lisinaDigMantenca + lisinaDigGanho * ganhoPeso;
//		}
//		return lisinaDigestivel;
//	}
//
//	public Double ExigenciaFosforoDisponivel(ExigenciaAves ea) {
//		Double pesoMedio = ea.getPesoMedio();
//		Double ganhoPeso = ea.getGanhoPeso() / 1000;
//		Double fosforoDisponivel = 0.0;
//
//		if ((ea.getIdadeInicial() >= 8) & (ea.getIdadeFinal() <= 21)) {
//
//			fosforoDisponivel = 0.026 * pesoMedio * 0.75 + 5.2 * ganhoPeso;
//		}
//
//		if ((ea.getIdadeInicial() >= 22) & (ea.getIdadeFinal() <= 56)) {
//
//			fosforoDisponivel = 0.026 * pesoMedio * 0.75 + 5.5 * ganhoPeso;
//		}
//
//		return fosforoDisponivel;
//	}
//
//	public Double ExigenciaFosforoDigestivel(ExigenciaAves ea) {
//		Double pesoMedio = ea.getPesoCorporal();
//		Double ganhoPeso = ea.getGanhoPeso() / 1000;
//		Double fosforoDigestivel = 0.0;
//
//		if ((ea.getIdadeInicial() >= 8) & (ea.getIdadeFinal() <= 21)) {
//
//			fosforoDigestivel = 0.026 * pesoMedio * 0.75 + 4.53 * ganhoPeso;
//		}
//
//		if ((ea.getIdadeInicial() >= 22) & (ea.getIdadeFinal() <= 56)) {
//
//			fosforoDigestivel = 0.026 * pesoMedio * 0.75 + 5.0 * ganhoPeso;
//		}
//
//		return fosforoDigestivel;
//	}
//
//	public ExigenciaAves DesempenhoMachoRegular(ExigenciaAves ea) {
//
//		if ((ea.getIdadeInicial() >= 1) & (ea.getIdadeFinal() <= 7)) {
//			System.out.println("Dentro do metodo");
//			ea.setProteina(22.00);
//			ea.setCalcio(0.920);
//			ea.setFosforoDigestivelEP(0.395);
//			ea.setFosforoDisponivelEP(0.470);
//			ea.setPotassio(0.590);
//			ea.setSodio(0.220);
//			ea.setCloro(0.200);
//			ea.setAcidoLinoleico(1.090);
//		} else if ((ea.getIdadeInicial() >= 8) & (ea.getIdadeFinal() <= 21)) {
//			ea.setProteina(20.00);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.585);
//			ea.setSodio(0.210);
//			ea.setCloro(0.190);
//			ea.setAcidoLinoleico(1.060);
//		} else if ((ea.getIdadeInicial() >= 22) & (ea.getIdadeFinal() <= 33)) {
//			ea.setProteina(19.00);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.580);
//			ea.setSodio(0.200);
//			ea.setCloro(0.180);
//			ea.setAcidoLinoleico(1.040);
//		} else if ((ea.getIdadeInicial() >= 34) & (ea.getIdadeFinal() <= 42)) {
//			ea.setProteina(17.80);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.580);
//			ea.setSodio(0.195);
//			ea.setCloro(0.170);
//			ea.setAcidoLinoleico(1.020);
//		} else if ((ea.getIdadeInicial() >= 43) & (ea.getIdadeFinal() <= 56)) {
//			ea.setProteina(17.00);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.580);
//			ea.setSodio(0.190);
//			ea.setCloro(0.165);
//			ea.setAcidoLinoleico(1.000);
//		}
//		return ea;
//	}
//
//	public ExigenciaAves DesempenhoMachoMedio(ExigenciaAves ea) {
//
//		if ((ea.getIdadeInicial() >= 1) & (ea.getIdadeFinal() <= 7)) {
//			ea.setProteina(22.20);
//			ea.setCalcio(0.920);
//			ea.setFosforoDigestivelEP(0.395);
//			ea.setFosforoDisponivelEP(0.470);
//			ea.setPotassio(0.590);
//			ea.setSodio(0.220);
//			ea.setCloro(0.200);
//			ea.setAcidoLinoleico(1.090);
//		} else if ((ea.getIdadeInicial() >= 8) & (ea.getIdadeFinal() <= 21)) {
//			ea.setProteina(20.80);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.585);
//			ea.setSodio(0.210);
//			ea.setCloro(0.190);
//			ea.setAcidoLinoleico(1.060);
//		} else if ((ea.getIdadeInicial() >= 22) & (ea.getIdadeFinal() <= 33)) {
//			ea.setProteina(19.50);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.580);
//			ea.setSodio(0.200);
//			ea.setCloro(0.180);
//			ea.setAcidoLinoleico(1.040);
//		} else if ((ea.getIdadeInicial() >= 34) & (ea.getIdadeFinal() <= 42)) {
//			ea.setProteina(18.00);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.580);
//			ea.setSodio(0.195);
//			ea.setCloro(0.170);
//			ea.setAcidoLinoleico(1.020);
//		} else if ((ea.getIdadeInicial() >= 43) & (ea.getIdadeFinal() <= 56)) {
//			ea.setProteina(17.50);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.580);
//			ea.setSodio(0.190);
//			ea.setCloro(0.165);
//			ea.setAcidoLinoleico(1.000);
//		}
//		return ea;
//	}
//
//	public ExigenciaAves DesempenhoMachoSuperior(ExigenciaAves ea) {
//
//		if ((ea.getIdadeInicial() >= 1) & (ea.getIdadeFinal() <= 7)) {
//			ea.setProteina(22.40);
//			ea.setCalcio(0.920);
//			ea.setFosforoDigestivelEP(0.395);
//			ea.setFosforoDisponivelEP(0.470);
//			ea.setPotassio(0.590);
//			ea.setSodio(0.220);
//			ea.setCloro(0.200);
//			ea.setAcidoLinoleico(1.090);
//		} else if ((ea.getIdadeInicial() >= 8) & (ea.getIdadeFinal() <= 21)) {
//			ea.setProteina(21.20);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.585);
//			ea.setSodio(0.210);
//			ea.setCloro(0.190);
//			ea.setAcidoLinoleico(1.060);
//		} else if ((ea.getIdadeInicial() >= 22) & (ea.getIdadeFinal() <= 33)) {
//			ea.setProteina(19.80);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.580);
//			ea.setSodio(0.200);
//			ea.setCloro(0.180);
//			ea.setAcidoLinoleico(1.040);
//		} else if ((ea.getIdadeInicial() >= 34) & (ea.getIdadeFinal() <= 42)) {
//			ea.setProteina(18.40);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.580);
//			ea.setSodio(0.195);
//			ea.setCloro(0.170);
//			ea.setAcidoLinoleico(1.020);
//		} else if ((ea.getIdadeInicial() >= 43) & (ea.getIdadeFinal() <= 56)) {
//			ea.setProteina(17.60);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.580);
//			ea.setSodio(0.190);
//			ea.setCloro(0.165);
//			ea.setAcidoLinoleico(1.000);
//		}
//		return ea;
//	}
//
//	public ExigenciaAves DesempenhoFemeaRegular(ExigenciaAves ea) {
//
//		if ((ea.getIdadeInicial() >= 1) & (ea.getIdadeFinal() <= 7)) {
//			ea.setProteina(21.60);
//			ea.setCalcio(0.920);
//			ea.setFosforoDigestivelEP(0.395);
//			ea.setFosforoDisponivelEP(0.470);
//			ea.setPotassio(0.590);
//			ea.setSodio(0.220);
//			ea.setCloro(0.200);
//			ea.setAcidoLinoleico(1.090);
//		} else if ((ea.getIdadeInicial() >= 8) & (ea.getIdadeFinal() <= 21)) {
//			ea.setProteina(20.00);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.560);
//			ea.setSodio(0.200);
//			ea.setCloro(0.185);
//			ea.setAcidoLinoleico(1.060);
//		} else if ((ea.getIdadeInicial() >= 22) & (ea.getIdadeFinal() <= 33)) {
//			ea.setProteina(18.80);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.555);
//			ea.setSodio(0.195);
//			ea.setCloro(0.172);
//			ea.setAcidoLinoleico(1.040);
//		} else if ((ea.getIdadeInicial() >= 34) & (ea.getIdadeFinal() <= 42)) {
//			ea.setProteina(17.30);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.550);
//			ea.setSodio(0.185);
//			ea.setCloro(0.162);
//			ea.setAcidoLinoleico(1.020);
//		} else if ((ea.getIdadeInicial() >= 43) & (ea.getIdadeFinal() <= 56)) {
//			ea.setProteina(16.80);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.540);
//			ea.setSodio(0.180);
//			ea.setCloro(0.155);
//			ea.setAcidoLinoleico(1.000);
//		}
//		return ea;
//	}
//
//	public ExigenciaAves DesempenhoFemeaMedio(ExigenciaAves ea) {
//
//		if ((ea.getIdadeInicial() >= 1) & (ea.getIdadeFinal() <= 7)) {
//			ea.setProteina(21.80);
//			ea.setCalcio(0.920);
//			ea.setFosforoDigestivelEP(0.395);
//			ea.setFosforoDisponivelEP(0.470);
//			ea.setPotassio(0.590);
//			ea.setSodio(0.220);
//			ea.setCloro(0.200);
//			ea.setAcidoLinoleico(1.090);
//		} else if ((ea.getIdadeInicial() >= 8) & (ea.getIdadeFinal() <= 21)) {
//			ea.setProteina(20.40);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.560);
//			ea.setSodio(0.200);
//			ea.setCloro(0.185);
//			ea.setAcidoLinoleico(1.060);
//		} else if ((ea.getIdadeInicial() >= 22) & (ea.getIdadeFinal() <= 33)) {
//			ea.setProteina(19.00);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.555);
//			ea.setSodio(0.195);
//			ea.setCloro(0.172);
//			ea.setAcidoLinoleico(1.040);
//		} else if ((ea.getIdadeInicial() >= 34) & (ea.getIdadeFinal() <= 42)) {
//			ea.setProteina(17.50);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.550);
//			ea.setSodio(0.185);
//			ea.setCloro(0.162);
//			ea.setAcidoLinoleico(1.020);
//		} else if ((ea.getIdadeInicial() >= 43) & (ea.getIdadeFinal() <= 56)) {
//			ea.setProteina(17.00);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.540);
//			ea.setSodio(0.180);
//			ea.setCloro(0.155);
//			ea.setAcidoLinoleico(1.000);
//		}
//		return ea;
//	}
//
//	public ExigenciaAves DesempenhoFemeaSuperior(ExigenciaAves ea) {
//
//		if ((ea.getIdadeInicial() >= 1) & (ea.getIdadeFinal() <= 7)) {
//			ea.setProteina(22.00);
//			ea.setCalcio(0.920);
//			ea.setFosforoDigestivelEP(0.395);
//			ea.setFosforoDisponivelEP(0.470);
//			ea.setPotassio(0.590);
//			ea.setSodio(0.220);
//			ea.setCloro(0.200);
//			ea.setAcidoLinoleico(1.090);
//		} else if ((ea.getIdadeInicial() >= 8) & (ea.getIdadeFinal() <= 21)) {
//			ea.setProteina(20.80);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.560);
//			ea.setSodio(0.200);
//			ea.setCloro(0.185);
//			ea.setAcidoLinoleico(1.060);
//		} else if ((ea.getIdadeInicial() >= 22) & (ea.getIdadeFinal() <= 33)) {
//			ea.setProteina(19.20);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.555);
//			ea.setSodio(0.195);
//			ea.setCloro(0.172);
//			ea.setAcidoLinoleico(1.040);
//		} else if ((ea.getIdadeInicial() >= 34) & (ea.getIdadeFinal() <= 42)) {
//			ea.setProteina(17.80);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.550);
//			ea.setSodio(0.185);
//			ea.setCloro(0.162);
//			ea.setAcidoLinoleico(1.020);
//		} else if ((ea.getIdadeInicial() >= 43) & (ea.getIdadeFinal() <= 56)) {
//			ea.setProteina(17.10);
//			ea.setFosforoDigestivelEP(FostoroDigestivel(ea));
//			ea.setFosforoDisponivelEP(FostoroDisponivel(ea));
//			ea.setCalcio(Calcio(ea));
//			ea.setPotassio(0.540);
//			ea.setSodio(0.180);
//			ea.setCloro(0.155);
//			ea.setAcidoLinoleico(1.000);
//		}
//		return ea;
//	}
//
//	// Exigencia Fosforo Digestivel em porcentagem
//	public Double FostoroDigestivel(ExigenciaAves ea) {
//		return null;//(ea.getFosforoDigestivel() / ea.getConsumo()) * 100;
//	}
//
//	// Exigencia Fosforo Disponivel em porcentagem
//	public Double FostoroDisponivel(ExigenciaAves ea) {
//		return null; //(ea.getFosforoDisponivel() / ea.getConsumo()) * 100;
//	}
//
//	public Double Calcio(ExigenciaAves ea) {
//		return ((ea.getFosforoDisponivelEP() * 2.13) + (ea.getFosforoDigestivelEP() * 2.35) / 2);
//	}
//
//	public ExigenciaAves ExigenciaPorcetagem(ExigenciaAves ea) {
//
//		return ea;
//	}
//
//	public boolean VerificaFaixaDePeso(ExigenciaAves ea) {
//		boolean condicao = false;
//
//		return condicao;
//	}
//
//}
