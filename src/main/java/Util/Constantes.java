package Util;

public class Constantes {
	public static double PESO_MEDIO_MACHOS_CASTRADOS_MIN = 5;
	public static double PESO_MEDIO_MACHOS_CASTRADOS_MAX = 50.9;
	
	public static double PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MIN = 30;
	public static double PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_2_MIN = 50;
	public static double PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_1_MAX = 49.9;
	public static double PESO_MEDIO_MACHOS_CASTRADOS_CRESCIMENTO_2_MAX = 69.9;
	//Desempenho
	public static int REGULAR = 1;
	public static int MEDIO = 2;
	public static int SUPERIOR = 3;
	
	//Machos Castrados
	public static double FOSFORO_DIGESTIVEL_REGULAR = 3.78;
	public static double FOSFORO_DIGESTIVEL_MEDIO = 4.36;
	public static double FOSFORO_DIGESTIVEL_SUPERIOR = 4.65;
	//Corresponde ao coeficiente relacionado ao peso de 30 a 50 
	public static double FOSFORO_DIGESTIVEL_REGULAR_CRESCIMENTO_1 = 5.55;
	public static double FOSFORO_DIGESTIVEL_MEDIO_CRESCIMENTO_1 = 5.59;
	public static double FOSFORO_DIGESTIVEL_SUPERIOR_CRESCIMENTO_1 = 5.97;
	//Corresponde ao coeficiente relacionado ao peso de 51 a 70
	public static double FOSFORO_DIGESTIVEL_REGULAR_CRESCIMENTO_2 = 6.08;
	public static double FOSFORO_DIGESTIVEL_MEDIO_CRESCIMENTO_2 = 5.59;
	public static double FOSFORO_DIGESTIVEL_SUPERIOR_CRESCIMENTO_2 = 5.97;
	
	public static double PROTEINA_REGULAR = 17.35;
	public static double PROTEINA_MEDIO = 18.13;
	public static double PROTEINA_SUPERIOR = 19.24;
	
	public static double PROTEINA_REGULAR_CRESCIMENTO_1 = 15.80;
	public static double PROTEINA_MEDIO_CRESCIMENTO_1 = 16.82;
	public static double PROTEINA_SUPERIOR_CRESCIMENTO_1 = 18.25;
	
	public static double PROTEINA_REGULAR_CRESCIMENTO_2 = 14.30;
	public static double PROTEINA_MEDIO_CRESCIMENTO_2 = 15.43;
	public static double PROTEINA_SUPERIOR_CRESCIMENTO_2 = 17.07;
	
	
	
	public static double CLORO_REGULAR = 0.190;
	public static double CLORO_MEDIO = 0.190;
	public static double CLORO_SUPERIOR =0.190;
	
	public static double CLORO_REGULAR_CRESCIMENTO_1 =0.170;
	public static double CLORO_MEDIO_CRESCIMENTO_1 = 0.170;
	public static double CLORO_SUPERIOR_CRESCIMENTO_1  =0.170;
	
	public static double CLORO_REGULAR_CRESCIMENTO_2 = 0.160;
	public static double CLORO_MEDIO_CRESCIMENTO_2 = 0.160;
	public static double CLORO_SUPERIOR_CRESCIMENTO_2  =0.160;

	public static double SODIO_REGULAR = 0.200;
	public static double SODIO_MEDIO = 0.200;
	public static double SODIO_SUPERIOR = 0.200;
	
	public static double SODIO_REGULAR_CRESCIMENTO_1 = 0.180;
	public static double SODIO_MEDIO_CRESCIMENTO_1  = 0.180;
	public static double SODIO_SUPERIOR_CRESCIMENTO_1  = 0.180;
	
	public static double SODIO_REGULAR_CRESCIMENTO_2 = 0.170;
	public static double SODIO_MEDIO_CRESCIMENTO_2 = 0.170;
	public static double SODIO_SUPERIOR_CRESCIMENTO_2 = 0.170;
	
	public static double POTASSIO_REGULAR = 0.470;
	public static double POTASSIO_MEDIO = 0.470;
	public static double POTASSIO_SUPERIOR = 0.470;	

	public static double POTASSIO_REGULAR_CRESCIMENTO_1 = 0.448;
	public static double POTASSIO_MEDIO_CRESCIMENTO_1  = 0.448;
	public static double POTASSIO_SUPERIOR_CRESCIMENTO_1  = 0.448;
	
	public static double POTASSIO_REGULAR_CRESCIMENTO_2 = 0.425;
	public static double POTASSIO_MEDIO_CRESCIMENTO_2 = 0.425;
	public static double POTASSIO_SUPERIOR_CRESCIMENTO_2 = 0.425;
	
	// Machos Castrados - Coeficientes Aminoácids Tabela 3.15 - Relação Aminoácido/ 
	//Lisina Utilizada para Estimar Exigências de Aminoácidos de Suínos em Crescimento
	public static int METIONINA_INICIAL_DIGESTIVEL = 28;
	public static int METIONINA_INICIAL_TOTAL = 27;	
	public static int METIONINA_CRESCIMENTO_DIGESTIVEL = 29;
	public static int METIONINA_CRESCIMENTO_TOTAL = 29;	
	public static int METIONINA_TERMINACAO_DIGESTIVEL = 31;
	public static int METIONINA_TERMINACAO_TOTAL = 30;
	
	public static int METIONINA_CISTINA_INICIAL_DIGESTIVEL = 56;
	public static int METIONINA_CISTINA_INICIAL_TOTaL = 55;	
	public static int METIONINA_CISTINA_CRESCIMENTO_DIGESTIVEL = 59;
	public static int METIONINA_CISTINA_CRESCIMENTO_TOTaL = 58;	
	public static int METIONINA_CISTINA_TERMINACAO_DIGESTIVEL = 60;
	public static int METIONINA_CISTINA_TERMINACAO_TOTaL = 59;
	
	public static int TREONINA_INICIAL_DIGESTIVEL = 63;
	public static int TREONINA_INICIAL_TOTAL = 67;	
	public static int TREONINA_CRESCIMENTO_DIGESTIVEL = 65;
	public static int TREONINA_CRESCIMENTO_TOTAL = 69;	
	public static int TREONINA_TERMINACAO_DIGESTIVEL = 67;
	public static int TREONINA_TERMINACAO_TOTAL = 71;
	
	public static int TRIPTOFANO_INICIAL_DIGESTIVEL = 18;
	public static int TRIPTOFANO_INICIAL_TOTAL = 18;
	public static int TRIPTOFANO_CRESCIMENTO_DIGESTIVEL = 18;
	public static int TRIPTOFANO_CRESCIMENTO_TOTAL = 18;
	public static int TRIPTOFANO_TERMINACAO_DIGESTIVEL = 18;
	public static int TRIPTOFANO_TERMINACAO_TOTAL = 18;
	
	public static int ARGININA_INICIAL_DIGESTIVEL = 42;
	public static int ARGININA_INICIAL_TOTAL = 40;
	public static int ARGININA_CRESCIMENTO_DIGESTIVEL = 41;
	public static int ARGININA_CRESCIMENTO_TOTAL = 39;
	public static int ARGININA_TERMINACAO_DIGESTIVEL = 32;
	public static int ARGININA_TERMINACAO_TOTAL = 30;
	
	public static int VALINA_INICIAL_DIGESTIVEL = 69;
	public static int VALINA_INICIAL_TOTAL = 70;
	public static int VALINA_CRESCIMENTO_DIGESTIVEL = 69;
	public static int VALINA_CRESCIMENTO_TOTAL = 70;
	public static int VALINA_TERMINACAO_DIGESTIVEL = 69;
	public static int VALINA_TERMINACAO_TOTAL = 70;
	
	public static int ISOLEUCINA_INICIAL_TOTAL = 55;
	public static int ISOLEUCINA_INICIAL_DIGESTIVEL = 55;
	public static int ISOLEUCINA_CRESCIMENTO_TOTAL = 55;
	public static int ISOLEUCINA_CRESCIMENTO_DIGESTIVEL = 55;
	public static int ISOLEUCINA_TERMINACAO_TOTAL = 55;
	public static int ISOLEUCINA_TERMINACAO_DIGESTIVEL = 55;
	
	public static int LEUCINA_INICIAL_DIGESTIVEL =100;
	public static int LEUCINA_INICIAL_TOTAL = 97;
	public static int LEUCINA_CRESCIMENTO_DIGESTIVEL =100;
	public static int LEUCINA_CRESCIMENTO_TOTAL = 97;
	public static int LEUCINA_TERMINACAO_DIGESTIVEL =100;
	public static int LEUCINA_TERMINACAO_TOTAL = 97;
	
	public static int HISTIDINA_INICIAL_DIGESTIVEL =33;
	public static int HISTIDINA_INICIAL_TOTAL =32;
	public static int HISTIDINA_CRESCIMENTO_DIGESTIVEL =33;
	public static int HISTIDINA_CRESCIMENTO_TOTAL =32;
	public static int HISTIDINA_TERMINACAO_DIGESTIVEL =33;
	public static int HISTIDINA_TERMINACAO_TOTAL =32;
	
	public static int FENILALANINA_INICIAL_DIGESTIVEL = 50;
	public static int FENILALANINA_INICIAL_TOTAL =49;
	public static int FENILALANINA_CRESCIMENTO_DIGESTIVEL = 50;
	public static int FENILALANINA_CRESCIMENTO_TOTAL =49;
	public static int FENILALANINA_TERMINACAO_DIGESTIVEL = 50;
	public static int FENILALANINA_TERMINACAO_TOTAL =49;
	
	public static int FENILALANINA_TIROSINA_INICIAL_DIGESTIVEL = 100;
	public static int FENILALANINA_TIROSINA_INICIAL_TOTAL = 98;
	public static int FENILALANINA_TIROSINA_CRESCIMENTO_DIGESTIVEL = 100;
	public static int FENILALANINA_TIROSINA_CRESCIMENTO_TOTAL = 98;
	public static int FENILALANINA_TIROSINA_TERMINACAO_DIGESTIVEL = 100;
	public static int FENILALANINA_TIROSINA_TERMINACAO_TOTAL = 98;
}
