package Simulacao;

import Nutriente.Nutriente;

public class Comparativo{
	
	private Nutriente enerTab; // Energia tabela brasileira
	private Nutriente enerCal; // Energia Calculado
	private Nutriente enerSim; // Energia Simulado
	
	public Comparativo(){
		enerTab = new Nutriente();
		enerCal = new Nutriente();
		enerSim = new Nutriente();
	}

	public Nutriente getEnerTab() {
		return enerTab;
	}

	public void setEnerTab(Nutriente enerTab) {
		this.enerTab = enerTab;
	}

	public Nutriente getEnerCal() {
		return enerCal;
	}

	public void setEnerCal(Nutriente enerCal) {
		this.enerCal = enerCal;
	}

	public Nutriente getEnerSim() {
		return enerSim;
	}

	public void setEnerSim(Nutriente enerSim) {
		this.enerSim = enerSim;
	}

	
}