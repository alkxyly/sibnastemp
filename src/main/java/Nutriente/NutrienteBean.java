package Nutriente;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import FiltrosBusca.NutrienteFilter;
import Util.Mensagens;


@ManagedBean(name = "nutrienteBean")
@SessionScoped
public class NutrienteBean {
	private final String CADASTRO_NUTRIENTE_JSF = "/admin/nutriente/CadastrarNutriente";
	private final String PESQUISAR_NUTRIENTE_JSF = "/admin/nutriente/PesquisarNutriente";
	private Nutriente nutriente;
	private List<Nutriente> listar;
	private Nutriente nutrienteSelecionado;

	private int qtdNutrientes ;

	private NutrienteFilter nutrienteFilter;

	public NutrienteBean(){
		this.nutriente =  new Nutriente();
		this.nutrienteSelecionado = null;
		this.qtdNutrientes = 0;
		nutrienteFilter = new NutrienteFilter();
	}
	
	@PostConstruct
	public void construct() {
		NutrienteRN nutrienteRN = new NutrienteRN();
		this.listar = nutrienteRN.pesquisar(nutrienteFilter);	
	}
	
	/**
	 * @author alkxly
	 * @since 14 de setembro de 2015
	 * @version 1.0
	 * 
	 * Metódo responsável por chamar a regra de negócio que irá
	 * persistir o objeto alimento na base de dados.
	 */
	public String salvar(){
		NutrienteRN nutrienteRN = new NutrienteRN();
		if(nutriente.getId() != null){
			nutrienteRN.atualizar(nutriente);
			this.nutriente = new Nutriente();
			return PESQUISAR_NUTRIENTE_JSF;
		}else{
			nutrienteRN.salvar(nutriente);
			Mensagens.adicionarMensagemConfirmacao("Nutriente cadastrado com sucesso.");
			this.nutriente = new Nutriente();
			return CADASTRO_NUTRIENTE_JSF;
		}
	

	}
	
	public boolean habilataCodigo(){
		boolean habilitaCodigo = false;
		try{
		if(!nutriente.getCodigo().equals(null)){
			habilitaCodigo = true;
		}
		}catch(NullPointerException e){
			
		}
		return habilitaCodigo;
	}

	public String editar(){
		return CADASTRO_NUTRIENTE_JSF;
	}

	public void atualizar(){
		NutrienteRN nutrienteRN = new NutrienteRN();
		nutrienteRN.atualizar(nutriente);
		this.nutriente = null;
	}

	public void excluir(){
		NutrienteRN nutrienteRN = new NutrienteRN();
		if(this.nutrienteSelecionado != null){
			nutrienteRN.excluir(nutrienteSelecionado);	
			Mensagens.adicionarMensagemConfirmacao("Nutriente excluido com sucesso");
		}
	}
	/**
	 * @author alkxly
	 * pesquisa de acordo com os filtros pré definidos na 
	 * página de pesquisa de nutrientes
	 * @return página destino após a consulta.
	 */
	public void pesquisar(){
		NutrienteRN nutrienteRN = new NutrienteRN();
		this.listar = nutrienteRN.pesquisar(nutrienteFilter);
	}
	public Nutriente getNutriente() {		
		return nutriente;
	}

	public void setNutriente(Nutriente nutriente) {
		this.nutriente = nutriente;
	}
	public List<Nutriente> getListar() {
		return listar;
	}
	public void setListar(List<Nutriente> listar) {
		this.listar = listar;
	}
	public Nutriente getNutrienteSelecionado() {
		return nutrienteSelecionado;
	}
	public void setNutrienteSelecionado(Nutriente nutrienteSelecionado) {
		this.nutrienteSelecionado = nutrienteSelecionado;
	}
	public int getQtdNutrientes() {
		NutrienteRN nutrienteRN = new NutrienteRN();
		this.qtdNutrientes = nutrienteRN.listar().size();
		return qtdNutrientes;
	}
	public void setQtdNutrientes(int qtdNutrientes) {
		this.qtdNutrientes = qtdNutrientes;
	}
	public NutrienteFilter getNutrienteFilter() {
		return nutrienteFilter;
	}
	public void setNutrienteFilter(NutrienteFilter nutrienteFilter) {
		this.nutrienteFilter = nutrienteFilter;
	}

}
