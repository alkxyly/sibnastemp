package Simulacao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.event.CellEditEvent;

import AlimentoNutrientes.AlimentoNutriente;
import AlimentoNutrientes.AlimentoNutrienteRN;
import FiltrosBusca.AlimentoFilter;
import Util.Mensagens;
import br.com.calculador.alimento.Alimento;
import br.com.calculador.alimento.AlimentoRN;
import relatorio.Relatorio;
import relatorio.RelatorioSimulacaoAve;

@ManagedBean(name = "aveSimulacaoBean")
@SessionScoped
public class AveSimulacaoBean extends Relatorio{

	private List<Alimento> listAlimento;

	private Alimento alimentoSelecionado;

	private List<AlimentoNutriente> listAlimentoNutriente;
	private List<AlimentoNutriente> listAlimentoNutrienteSimular;

	private AlimentoFilter alimentoFilter;

	private Double avesJovensEnergiaMetabolizada;
	private Double galinhasEnergiaMetabolizada;
	private Double padraoAvesJovensEnergiaMetabolizada;
	private Double padraoGalinhasEnergiaMetabolizada;

	public AveSimulacaoBean() {
		alimentoSelecionado = new Alimento();
		this.avesJovensEnergiaMetabolizada = 0.0;
		this.galinhasEnergiaMetabolizada = 0.0;
		this.padraoAvesJovensEnergiaMetabolizada = 0.0;
		this.padraoGalinhasEnergiaMetabolizada = 0.0;
		this.alimentoFilter = new AlimentoFilter();
	}

	public void onCellEdit(CellEditEvent event) {
		Object oldValue = event.getOldValue();
		Object newValue = event.getNewValue();
		System.out.println("Chamou");

	}

	public List<Alimento> getListAlimento() {
		AlimentoRN alimentoRN = new AlimentoRN();
		this.listAlimento = alimentoRN.pesquisar(alimentoFilter);
		return listAlimento;
	}

	/**
	 * Ao Selecionar o alimento é adicionado os nutrientes as listas que irão
	 * preencher as tabelas.
	 */
	public void selecionar() {
		AlimentoNutrienteRN al = new AlimentoNutrienteRN();
		AveSimulacaoRN AS = new AveSimulacaoRN();

		this.listAlimentoNutriente = al.listaComAlimento(alimentoSelecionado);
		this.listAlimentoNutrienteSimular = al.listaComAlimento(alimentoSelecionado);
		this.avesJovensEnergiaMetabolizada = AS.calcularAvesJovensEnergiaMetabolizada(listAlimentoNutrienteSimular,
				alimentoSelecionado.getOrigem());
		this.galinhasEnergiaMetabolizada = AS.calcularGalinhasEnergiaMetabolizada(listAlimentoNutrienteSimular,
				alimentoSelecionado.getOrigem());
		this.padraoAvesJovensEnergiaMetabolizada = AS
				.calcularAvesJovensEnergiaMetabolizada(listAlimentoNutrienteSimular, alimentoSelecionado.getOrigem());
		this.padraoGalinhasEnergiaMetabolizada = AS.calcularGalinhasEnergiaMetabolizada(listAlimentoNutrienteSimular,
				alimentoSelecionado.getOrigem());
		this.listAlimentoNutrienteSimular = AS.OrdenarListaDeSimulacao(listAlimentoNutrienteSimular);
		this.listAlimentoNutriente = listAlimentoNutrienteSimular;
	}

	/**
	 * Busca o alimento baseado nos filtros de consulta
	 */
	public void pesquisar() {
		AlimentoRN alimentoRN = new AlimentoRN();
		this.listAlimento = alimentoRN.pesquisar(alimentoFilter);
	}

	/**
	 * Simula a tabela de dados baseado nos novos valores inseridos.
	 */
	public void simular() {
		AveSimulacaoRN AS = new AveSimulacaoRN();
		this.avesJovensEnergiaMetabolizada = AS.calcularAvesJovensEnergiaMetabolizada(listAlimentoNutrienteSimular,
				alimentoSelecionado.getOrigem());
		this.galinhasEnergiaMetabolizada = AS.calcularGalinhasEnergiaMetabolizada(listAlimentoNutrienteSimular,
				alimentoSelecionado.getOrigem());
	}

	public boolean naoEditavel(Integer codigo) {
		AveSimulacaoRN avesRN = new AveSimulacaoRN();
		return avesRN.naoEditavelRN(codigo);
	}

	public void gerarRelatorioList() throws Exception {
		if(listAlimentoNutrienteSimular != null){
		String nomeAlimento = alimentoSelecionado.getNome();
		List<RelatorioSimulacaoAve> lista = new ArrayList<>();
		for (int i = 0; i < listAlimentoNutrienteSimular.size(); i++) {
			RelatorioSimulacaoAve relAve = new RelatorioSimulacaoAve();
			relAve.setNome(listAlimentoNutrienteSimular.get(i).getNutriente().getNome());
			relAve.setUnidade(String.valueOf(listAlimentoNutrienteSimular.get(i).getUnidade()));
			relAve.setValor(String.valueOf(listAlimentoNutrienteSimular.get(i).getValorNutritivo()));
			lista.add(relAve);
		}
		System.out.println("PREPARAR PARA BAIXAR "+lista.size());
		HashMap param = new HashMap<>();
		param.put("nomeAlimento",nomeAlimento);
		gerarRelatorio("simularAve", param, lista);
		}else{
			Mensagens.adicionarMensagemErro("É nescessário selecionar o alimento");
		}
//		try {
//			List<UF> listagemResultado = new ArrayList<UF>();
//			UF uf = new UF();
//			uf.setId(new Integer(1));
//			uf.setNome("UF 1");
//			uf.setSigla("SGD");
//			listagemResultado.add(uf);
//			UF uf2 = new UF();
//			uf.setId(new Integer(1));
//			uf.setNome("UF 2");
//			uf.setSigla("SGDD");
//			listagemResultado.add(uf2);
//			
//			HashMap paramRel = new HashMap<>();
//			String nomeRelatorio = "relUF";
//			gerarRelatorio(nomeRelatorio, paramRel, listagemResultado);
//		} catch (Exception e) {
//		}
	}

	public void setListAlimento(List<Alimento> listAlimento) {
		this.listAlimento = listAlimento;
	}

	public Alimento getAlimentoSelecionado() {
		return alimentoSelecionado;
	}

	public void setAlimentoSelecionado(Alimento alimentoSelecionado) {
		this.alimentoSelecionado = alimentoSelecionado;
	}

	public List<AlimentoNutriente> getListAlimentoNutriente() {
		return listAlimentoNutriente;
	}

	public void setListAlimentoNutriente(List<AlimentoNutriente> listAlimentoNutriente) {
		this.listAlimentoNutriente = listAlimentoNutriente;
	}

	public List<AlimentoNutriente> getListAlimentoNutrienteSimular() {
		return listAlimentoNutrienteSimular;
	}

	public void setListAlimentoNutrienteSimular(List<AlimentoNutriente> listAlimentoNutrienteSimular) {
		this.listAlimentoNutrienteSimular = listAlimentoNutrienteSimular;
	}

	public double getAvesJovensEnergiaMetabolizada() {
		return avesJovensEnergiaMetabolizada;
	}

	public void setAvesJovensEnergiaMetabolizada(double avesJovensEnergiaMetabolizada) {
		this.avesJovensEnergiaMetabolizada = avesJovensEnergiaMetabolizada;
	}

	public double getGalinhasEnergiaMetabolizada() {
		return galinhasEnergiaMetabolizada;
	}

	public void setGalinhasEnergiaMetabolizada(double galinhasEnergiaMetabolizada) {
		this.galinhasEnergiaMetabolizada = galinhasEnergiaMetabolizada;
	}

	public AlimentoFilter getAlimentoFilter() {
		return alimentoFilter;
	}

	public void setAlimentoFilter(AlimentoFilter alimentoFilter) {
		this.alimentoFilter = alimentoFilter;
	}

	public Double getPadraoAvesJovensEnergiaMetabolizada() {
		return padraoAvesJovensEnergiaMetabolizada;
	}

	public void setPadraoAvesJovensEnergiaMetabolizada(Double padraoAvesJovensEnergiaMetabolizada) {
		this.padraoAvesJovensEnergiaMetabolizada = padraoAvesJovensEnergiaMetabolizada;
	}

	public Double getPadraoGalinhasEnergiaMetabolizada() {
		return padraoGalinhasEnergiaMetabolizada;
	}

	public void setPadraoGalinhasEnergiaMetabolizada(Double padraoGalinhasEnergiaMetabolizada) {
		this.padraoGalinhasEnergiaMetabolizada = padraoGalinhasEnergiaMetabolizada;
	}

	public void setAvesJovensEnergiaMetabolizada(Double avesJovensEnergiaMetabolizada) {
		this.avesJovensEnergiaMetabolizada = avesJovensEnergiaMetabolizada;
	}

	public void setGalinhasEnergiaMetabolizada(Double galinhasEnergiaMetabolizada) {
		this.galinhasEnergiaMetabolizada = galinhasEnergiaMetabolizada;
	}

}
