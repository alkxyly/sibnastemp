package Util;

import AlimentoNutrientes.AlimentoNutrienteDAO;
import AlimentoNutrientes.AlimentoNutrienteDAOHibernate;
import Aminoacido.AminoacidoDAO;
import Aminoacido.AminoacidoDAOHibernate;
import Nutriente.NutrienteDAO;
import Nutriente.NutrienteDAOHibernate;
import Origem.OrigemDAO;
import Origem.OrigemDAOHibernate;
import br.com.calculador.alimento.AlimentoDAO;
import br.com.calculador.alimento.AlimentoDAOHibernate;
import br.com.calculador.usuario.UsuarioDAO;
import br.com.calculador.usuario.UsuarioDAOHibernate;
import conexaoHibernate.HibernateUtil;




public class DAOFactory {
	
	public static AlimentoDAO criarAlimentoDAO(){
		AlimentoDAOHibernate alimentoDAO =  new AlimentoDAOHibernate();
		alimentoDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return alimentoDAO;
	}
	
	public static NutrienteDAO criarNutrienteDAO(){
		NutrienteDAOHibernate nutrienteDAO =  new NutrienteDAOHibernate();
		nutrienteDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return nutrienteDAO;
	}
	
	public static OrigemDAO criarOrigemDAO(){
		OrigemDAOHibernate origemDAO =  new OrigemDAOHibernate();
		origemDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return origemDAO;
	}
	public static AminoacidoDAO criarAminoacidoDAO(){
		AminoacidoDAOHibernate aminoacidoDAO =  new AminoacidoDAOHibernate();
		aminoacidoDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return aminoacidoDAO;
	}
	public static AlimentoNutrienteDAO criarAlimentoNutriente(){
		AlimentoNutrienteDAOHibernate alimentoNutrienteDAO =  new AlimentoNutrienteDAOHibernate();
		alimentoNutrienteDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return alimentoNutrienteDAO;
	}	
	public static UsuarioDAO criarUsuario(){
		UsuarioDAOHibernate usuarioDAO = new UsuarioDAOHibernate();
		usuarioDAO.setSession(HibernateUtil.getSessionfactory().getCurrentSession());
		return usuarioDAO;
	}
}
