package Origem;

import java.util.List;

public interface OrigemDAO {
	public void salvar(Origem origem);
	public void excluir(Origem origem);
	public void atualizar(Origem origem);
	public Origem carregar(Integer id);
	public List<Origem> listar();
}