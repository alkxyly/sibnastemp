package br.com.calulador.exigencia;

public class Exigencia {
	private double fosforoDisponivel;
	private double fosforoDisponivelExigencia;
	private double fosforoDigestivel;
	private double fosforoDigestivelExigencia;
	private double calcio;
	private double proteina;
	private double potassio;
	private double sodio;
	private double cloro;
	private double lisinaDigestivel;
	private double lisinaDigestivelExigencia;
	private double lisinaProteinaIdeal;
	private double metioninaDigestivel;
	private double metioninaCistina;
	private double treoninaDigestivel;
	private double treoninaTotal;
	private double triptofano;
	private double arginina;
	private double valina ;
	private double isoleucina;
	private double leucina;
	private double histidina;
	private double fenilalanina;
	private double fenilalaninaTirosina;
	
	private double lisinaTotal;
	private double metioninaTotal;
	private double metioninaCistinaTotal;
	private double treoninaDigestivelTotal;
	private double treoninaTotalTotal;
	private double triptofanoTotal;
	private double argininaTotal;
	private double valinaTotal ;
	private double isoleucinaTotal;
	private double leucinaTotal;
	private double histidinaTotal;
	private double fenilalaninaTotal;
	private double fenilalaninaTirosinaTotal;
	
	private double energiaMetabolizavelKg;
	private double energiaMetabolizavelDia;
	private double energiaMetDet;
	
	private double nitrogenioEssencial;
	private double nitrogenioEssencialTotal;
	private double proteinaBrutaDisgestivel;
	private double proteinaBrutaTotal;
	public Exigencia(){
		this.fosforoDisponivel = 0.0;
		this.fosforoDisponivelExigencia = 0.0;
		this.fosforoDigestivel = 0.0;
		this.fosforoDigestivelExigencia = 0.0;
		this.calcio = 0.0;
		this.proteina = 0.0;
		this.potassio = 0.0;
		this.sodio = 0.0;
		this.cloro = 0.0;
		this.lisinaDigestivel = 0.0;
		this.lisinaDigestivelExigencia = 0.0;
		this.metioninaDigestivel = 0.0;
		this.metioninaCistina = 0.0;
		this.treoninaDigestivel = 0.0;
		this.treoninaTotal = 0.0;
		this.triptofano = 0.0;
		this.arginina = 0.0;
		this.isoleucina = 0.0;
		this.leucina = 0.0;
		this.histidina = 0.0;
		this.fenilalanina = 0.0;
		this.fenilalaninaTirosina = 0.0;
		this.valina = 0.0;
		
	}
	
	public double getFosforoDisponivel() {
		return fosforoDisponivel;
	}

	public void setFosforoDisponivel(double fosforoDisponivel) {
		this.fosforoDisponivel = fosforoDisponivel;
	}
	public double getFosforoDisponivelExigencia() {
		return fosforoDisponivelExigencia;
	}
	public void setFosforoDisponivelExigencia(double fosforoDisponivelExigencia) {
		this.fosforoDisponivelExigencia = fosforoDisponivelExigencia;
	}
	public double getCalcio() {
		return calcio;
	}
	public void setCalcio(double calcio) {
		this.calcio = calcio;
	}
	public double getFosforoDigestivel() {
		return fosforoDigestivel;
	}
	public void setFosforoDigestivel(double fosforoDigestivel) {
		this.fosforoDigestivel = fosforoDigestivel;
	}
	public double getFosforoDigestivelExigencia() {
		return fosforoDigestivelExigencia;
	}
	public void setFosforoDigestivelExigencia(double fosforoDigestivelExigencia) {
		this.fosforoDigestivelExigencia = fosforoDigestivelExigencia;
	}
	public double getProteina() {
		return proteina;
	}
	public void setProteina(double proteina) {
		this.proteina = proteina;
	}
	public double getPotassio() {
		return potassio;
	}
	public void setPotassio(double potassio) {
		this.potassio = potassio;
	}
	public double getSodio() {
		return sodio;
	}
	public void setSodio(double sodio) {
		this.sodio = sodio;
	}
	public double getCloro() {
		return cloro;
	}
	public void setCloro(double cloro) {
		this.cloro = cloro;
	}
	public double getLisinaDigestivel() {
		return lisinaDigestivel;
	}
	public void setLisinaDigestivel(double lisinaDigestivel) {
		this.lisinaDigestivel = lisinaDigestivel;
	}
	public double getLisinaDigestivelExigencia() {
		return lisinaDigestivelExigencia;
	}
	public void setLisinaDigestivelExigencia(double lisinaDigestivelExigencia) {
		this.lisinaDigestivelExigencia = lisinaDigestivelExigencia;
	}

	public double getMetioninaDigestivel() {
		return metioninaDigestivel;
	}

	public void setMetioninaDigestivel(double metioninaDigestivel) {
		this.metioninaDigestivel = metioninaDigestivel;
	}

	public double getMetioninaCistina() {
		return metioninaCistina;
	}

	public void setMetioninaCistina(double metioninaCistina) {
		this.metioninaCistina = metioninaCistina;
	}

	public double getTreoninaDigestivel() {
		return treoninaDigestivel;
	}

	public void setTreoninaDigestivel(double treoninaDigestivel) {
		this.treoninaDigestivel = treoninaDigestivel;
	}

	public double getTreoninaTotal() {
		return treoninaTotal;
	}

	public void setTreoninaTotal(double treoninaTotal) {
		this.treoninaTotal = treoninaTotal;
	}

	public double getTriptofano() {
		return triptofano;
	}

	public void setTriptofano(double triptofano) {
		this.triptofano = triptofano;
	}

	public double getArginina() {
		return arginina;
	}

	public void setArginina(double arginina) {
		this.arginina = arginina;
	}

	public double getIsoleucina() {
		return isoleucina;
	}

	public void setIsoleucina(double isoleucina) {
		this.isoleucina = isoleucina;
	}

	public double getLeucina() {
		return leucina;
	}

	public void setLeucina(double leucina) {
		this.leucina = leucina;
	}

	public double getHistidina() {
		return histidina;
	}

	public void setHistidina(double histidina) {
		this.histidina = histidina;
	}

	public double getFenilalanina() {
		return fenilalanina;
	}

	public void setFenilalanina(double fenilalanina) {
		this.fenilalanina = fenilalanina;
	}

	public double getFenilalaninaTirosina() {
		return fenilalaninaTirosina;
	}

	public void setFenilalaninaTirosina(double fenilalaninaTirosina) {
		this.fenilalaninaTirosina = fenilalaninaTirosina;
	}

	public double getValina() {
		return valina;
	}

	public void setValina(double valina) {
		this.valina = valina;
	}

	public double getLisinaTotal() {
		return lisinaTotal;
	}

	public void setLisinaTotal(double lisinaTotal) {
		this.lisinaTotal = lisinaTotal;
	}

	public double getMetioninaTotal() {
		return metioninaTotal;
	}

	public void setMetioninaTotal(double metioninaTotal) {
		this.metioninaTotal = metioninaTotal;
	}

	public double getMetioninaCistinaTotal() {
		return metioninaCistinaTotal;
	}

	public void setMetioninaCistinaTotal(double metioninaCistinaTotal) {
		this.metioninaCistinaTotal = metioninaCistinaTotal;
	}

	public double getTreoninaDigestivelTotal() {
		return treoninaDigestivelTotal;
	}

	public void setTreoninaDigestivelTotal(double treoninaDigestivelTotal) {
		this.treoninaDigestivelTotal = treoninaDigestivelTotal;
	}

	public double getTreoninaTotalTotal() {
		return treoninaTotalTotal;
	}

	public void setTreoninaTotalTotal(double treoninaTotalTotal) {
		this.treoninaTotalTotal = treoninaTotalTotal;
	}

	public double getTriptofanoTotal() {
		return triptofanoTotal;
	}

	public void setTriptofanoTotal(double triptofanoTotal) {
		this.triptofanoTotal = triptofanoTotal;
	}

	public double getArgininaTotal() {
		return argininaTotal;
	}

	public void setArgininaTotal(double argininaTotal) {
		this.argininaTotal = argininaTotal;
	}

	public double getValinaTotal() {
		return valinaTotal;
	}

	public void setValinaTotal(double valinaTotal) {
		this.valinaTotal = valinaTotal;
	}

	public double getIsoleucinaTotal() {
		return isoleucinaTotal;
	}

	public void setIsoleucinaTotal(double isoleucinaTotal) {
		this.isoleucinaTotal = isoleucinaTotal;
	}

	public double getLeucinaTotal() {
		return leucinaTotal;
	}

	public void setLeucinaTotal(double leucinaTotal) {
		this.leucinaTotal = leucinaTotal;
	}

	public double getHistidinaTotal() {
		return histidinaTotal;
	}

	public void setHistidinaTotal(double histidinaTotal) {
		this.histidinaTotal = histidinaTotal;
	}

	public double getFenilalaninaTotal() {
		return fenilalaninaTotal;
	}

	public void setFenilalaninaTotal(double fenilalaninaTotal) {
		this.fenilalaninaTotal = fenilalaninaTotal;
	}

	public double getFenilalaninaTirosinaTotal() {
		return fenilalaninaTirosinaTotal;
	}

	public void setFenilalaninaTirosinaTotal(double fenilalaninaTirosinaTotal) {
		this.fenilalaninaTirosinaTotal = fenilalaninaTirosinaTotal;
	}

	public double getEnergiaMetabolizavelKg() {
		return energiaMetabolizavelKg;
	}

	public void setEnergiaMetabolizavelKg(double energiaMetabolizavelKg) {
		this.energiaMetabolizavelKg = energiaMetabolizavelKg;
	}

	public double getEnergiaMetabolizavelDia() {
		return energiaMetabolizavelDia;
	}

	public void setEnergiaMetabolizavelDia(double energiaMetabolizavelDia) {
		this.energiaMetabolizavelDia = energiaMetabolizavelDia;
	}

	public double getEnergiaMetDet() {
		return energiaMetDet;
	}

	public void setEnergiaMetDet(double energiaMetDet) {
		this.energiaMetDet = energiaMetDet;
	}

	public double getLisinaProteinaIdeal() {
		return lisinaProteinaIdeal;
	}

	public void setLisinaProteinaIdeal(double lisinaProteinaIdeal) {
		this.lisinaProteinaIdeal = lisinaProteinaIdeal;
	}

	public double getNitrogenioEssencial() {
		return nitrogenioEssencial;
	}

	public void setNitrogenioEssencial(double nitrogenioEssencial) {
		this.nitrogenioEssencial = nitrogenioEssencial;
	}

	public double getProteinaBrutaDisgestivel() {
		return proteinaBrutaDisgestivel;
	}

	public void setProteinaBrutaDisgestivel(double proteinaBrutaDisgestivel) {
		this.proteinaBrutaDisgestivel = proteinaBrutaDisgestivel;
	}

	public double getProteinaBrutaTotal() {
		return proteinaBrutaTotal;
	}

	public void setProteinaBrutaTotal(double proteinaBrutaTotal) {
		this.proteinaBrutaTotal = proteinaBrutaTotal;
	}

	public double getNitrogenioEssencialTotal() {
		return nitrogenioEssencialTotal;
	}

	public void setNitrogenioEssencialTotal(double nitrogenioEssencialTotal) {
		this.nitrogenioEssencialTotal = nitrogenioEssencialTotal;
	}
	
	
	

}
