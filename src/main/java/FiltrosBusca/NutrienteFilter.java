package FiltrosBusca;

/**
 * @author alkxyly
 *
 */
public class NutrienteFilter {
	private String codigo;
	private String nome;
	
	public NutrienteFilter(){
		this.codigo = "";
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}
