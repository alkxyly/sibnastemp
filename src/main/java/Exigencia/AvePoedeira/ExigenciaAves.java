package Exigencia.AvePoedeira;

import java.io.Serializable;

public class ExigenciaAves implements Serializable {

	private static final long serialVersionUID = 7869251576099674502L;
	
	// Dados para simula��o
	private Double pesoCorporal;// Em Quilograma
	private Double ganhoPeso;// g/dia
	private Double consumoDeterminado;// g/dia
	private Double massaOvo;// Massa de ovo g/dia
	private Double energiaMetabolizavelRacao;// Kcal/kg
	private String temperaturaMedia; // Varia��o de 15�C a 27�C

	// Exig�ncia - Kcal/kg
	private Double consumoEnergiaMetabolizavelDeterminado = 0.0;
	private Double energiaMetabolizavelCalculada = 0.0;

	// Exig�ncia - g/dia
	private Double consumoCalculado = 0.0;
	private Double lisinaDigestivel = 0.0;

	// Exig�ncia - %
	private Double proteina = 0.0;
	private Double calcio = 0.0;
	private Double fosforoDisponivelEP = 0.0;
	private Double fosforoDigestivelEP = 0.0;
	private Double potassio = 0.0;
	private Double sodio = 0.0;
	private Double cloro = 0.0;
	private Double lisinaDigestivelEP = 0.0;
	private Double acidoLinoleico = 0.0;

	// AA Digest�vel%
	private Double lisinaAAD = 0.0;
	private Double metioninaAAD = 0.0;
	private Double metioninaCistinaAAD = 0.0;
	private Double treoninaAAD = 0.0;
	private Double triptofanoAAD = 0.0;
	private Double argininaAAD = 0.0;
	private Double glicinaSerinaAAD = 0.0;
	private Double valinaAAD = 0.0;
	private Double isoleucinaAAD = 0.0;
	private Double leucinaAAD = 0.0;
	private Double histidinaAAD = 0.0;
	private Double fenilalaninaAAD = 0.0;
	private Double fenil_TirAAD = 0.0;

	// AA Total%
	private Double lisinaAAT = 0.0;
	private Double metioninaAAT = 0.0;
	private Double metioninaCistinaAAT = 0.0;
	private Double treoninaAAT = 0.0;
	private Double triptofanoAAT = 0.0;
	private Double argininaAAT = 0.0;
	private Double glicinaSerinaAAT = 0.0;
	private Double valinaAAT = 0.0;
	private Double isoleucinaAAT = 0.0;
	private Double leucinaAAT = 0.0;
	private Double histidinaAAT = 0.0;
	private Double fenilalaninaAAT = 0.0;
	private Double fenil_TirAAT = 0.0;
	public Double getPesoCorporal() {
		return pesoCorporal;
	}
	public void setPesoCorporal(Double pesoCorporal) {
		this.pesoCorporal = pesoCorporal;
	}
	public Double getGanhoPeso() {
		return ganhoPeso;
	}
	public void setGanhoPeso(Double ganhoPeso) {
		this.ganhoPeso = ganhoPeso;
	}
	public Double getConsumoDeterminado() {
		return consumoDeterminado;
	}
	public void setConsumoDeterminado(Double consumoDeterminado) {
		this.consumoDeterminado = consumoDeterminado;
	}
	public Double getMassaOvo() {
		return massaOvo;
	}
	public void setMassaOvo(Double massaOvo) {
		this.massaOvo = massaOvo;
	}
	public Double getEnergiaMetabolizavelRacao() {
		return energiaMetabolizavelRacao;
	}
	public void setEnergiaMetabolizavelRacao(Double energiaMetabolizavelRacao) {
		this.energiaMetabolizavelRacao = energiaMetabolizavelRacao;
	}
	public String getTemperaturaMedia() {
		return temperaturaMedia;
	}
	public void setTemperaturaMedia(String temperaturaMedia) {
		this.temperaturaMedia = temperaturaMedia;
	}
	public Double getConsumoEnergiaMetabolizavelDeterminado() {
		return consumoEnergiaMetabolizavelDeterminado;
	}
	public void setConsumoEnergiaMetabolizavelDeterminado(Double consumoEnergiaMetabolizavelDeterminado) {
		this.consumoEnergiaMetabolizavelDeterminado = consumoEnergiaMetabolizavelDeterminado;
	}
	public Double getEnergiaMetabolizavelCalculada() {
		return energiaMetabolizavelCalculada;
	}
	public void setEnergiaMetabolizavelCalculada(Double energiaMetabolizavelCalculada) {
		this.energiaMetabolizavelCalculada = energiaMetabolizavelCalculada;
	}
	public Double getConsumoCalculado() {
		return consumoCalculado;
	}
	public void setConsumoCalculado(Double consumoCalculado) {
		this.consumoCalculado = consumoCalculado;
	}
	public Double getLisinaDigestivel() {
		return lisinaDigestivel;
	}
	public void setLisinaDigestivel(Double lisinaDigestivel) {
		this.lisinaDigestivel = lisinaDigestivel;
	}
	public Double getProteina() {
		return proteina;
	}
	public void setProteina(Double proteina) {
		this.proteina = proteina;
	}
	public Double getCalcio() {
		return calcio;
	}
	public void setCalcio(Double calcio) {
		this.calcio = calcio;
	}
	public Double getFosforoDisponivelEP() {
		return fosforoDisponivelEP;
	}
	public void setFosforoDisponivelEP(Double fosforoDisponivelEP) {
		this.fosforoDisponivelEP = fosforoDisponivelEP;
	}
	public Double getFosforoDigestivelEP() {
		return fosforoDigestivelEP;
	}
	public void setFosforoDigestivelEP(Double fosforoDigestivelEP) {
		this.fosforoDigestivelEP = fosforoDigestivelEP;
	}
	public Double getPotassio() {
		return potassio;
	}
	public void setPotassio(Double potassio) {
		this.potassio = potassio;
	}
	public Double getSodio() {
		return sodio;
	}
	public void setSodio(Double sodio) {
		this.sodio = sodio;
	}
	public Double getCloro() {
		return cloro;
	}
	public void setCloro(Double cloro) {
		this.cloro = cloro;
	}
	public Double getLisinaDigestivelEP() {
		return lisinaDigestivelEP;
	}
	public void setLisinaDigestivelEP(Double lisinaDigestivelEP) {
		this.lisinaDigestivelEP = lisinaDigestivelEP;
	}
	public Double getAcidoLinoleico() {
		return acidoLinoleico;
	}
	public void setAcidoLinoleico(Double acidoLinoleico) {
		this.acidoLinoleico = acidoLinoleico;
	}
	public Double getLisinaAAD() {
		return lisinaAAD;
	}
	public void setLisinaAAD(Double lisinaAAD) {
		this.lisinaAAD = lisinaAAD;
	}
	public Double getMetioninaAAD() {
		return metioninaAAD;
	}
	public void setMetioninaAAD(Double metioninaAAD) {
		this.metioninaAAD = metioninaAAD;
	}
	public Double getMetioninaCistinaAAD() {
		return metioninaCistinaAAD;
	}
	public void setMetioninaCistinaAAD(Double metioninaCistinaAAD) {
		this.metioninaCistinaAAD = metioninaCistinaAAD;
	}
	public Double getTreoninaAAD() {
		return treoninaAAD;
	}
	public void setTreoninaAAD(Double treoninaAAD) {
		this.treoninaAAD = treoninaAAD;
	}
	public Double getTriptofanoAAD() {
		return triptofanoAAD;
	}
	public void setTriptofanoAAD(Double triptofanoAAD) {
		this.triptofanoAAD = triptofanoAAD;
	}
	public Double getArgininaAAD() {
		return argininaAAD;
	}
	public void setArgininaAAD(Double argininaAAD) {
		this.argininaAAD = argininaAAD;
	}
	public Double getGlicinaSerinaAAD() {
		return glicinaSerinaAAD;
	}
	public void setGlicinaSerinaAAD(Double glicinaSerinaAAD) {
		this.glicinaSerinaAAD = glicinaSerinaAAD;
	}
	public Double getValinaAAD() {
		return valinaAAD;
	}
	public void setValinaAAD(Double valinaAAD) {
		this.valinaAAD = valinaAAD;
	}
	public Double getIsoleucinaAAD() {
		return isoleucinaAAD;
	}
	public void setIsoleucinaAAD(Double isoleucinaAAD) {
		this.isoleucinaAAD = isoleucinaAAD;
	}
	public Double getLeucinaAAD() {
		return leucinaAAD;
	}
	public void setLeucinaAAD(Double leucinaAAD) {
		this.leucinaAAD = leucinaAAD;
	}
	public Double getHistidinaAAD() {
		return histidinaAAD;
	}
	public void setHistidinaAAD(Double histidinaAAD) {
		this.histidinaAAD = histidinaAAD;
	}
	public Double getFenilalaninaAAD() {
		return fenilalaninaAAD;
	}
	public void setFenilalaninaAAD(Double fenilalaninaAAD) {
		this.fenilalaninaAAD = fenilalaninaAAD;
	}
	public Double getFenil_TirAAD() {
		return fenil_TirAAD;
	}
	public void setFenil_TirAAD(Double fenil_TirAAD) {
		this.fenil_TirAAD = fenil_TirAAD;
	}
	public Double getLisinaAAT() {
		return lisinaAAT;
	}
	public void setLisinaAAT(Double lisinaAAT) {
		this.lisinaAAT = lisinaAAT;
	}
	public Double getMetioninaAAT() {
		return metioninaAAT;
	}
	public void setMetioninaAAT(Double metioninaAAT) {
		this.metioninaAAT = metioninaAAT;
	}
	public Double getMetioninaCistinaAAT() {
		return metioninaCistinaAAT;
	}
	public void setMetioninaCistinaAAT(Double metioninaCistinaAAT) {
		this.metioninaCistinaAAT = metioninaCistinaAAT;
	}
	public Double getTreoninaAAT() {
		return treoninaAAT;
	}
	public void setTreoninaAAT(Double treoninaAAT) {
		this.treoninaAAT = treoninaAAT;
	}
	public Double getTriptofanoAAT() {
		return triptofanoAAT;
	}
	public void setTriptofanoAAT(Double triptofanoAAT) {
		this.triptofanoAAT = triptofanoAAT;
	}
	public Double getArgininaAAT() {
		return argininaAAT;
	}
	public void setArgininaAAT(Double argininaAAT) {
		this.argininaAAT = argininaAAT;
	}
	public Double getGlicinaSerinaAAT() {
		return glicinaSerinaAAT;
	}
	public void setGlicinaSerinaAAT(Double glicinaSerinaAAT) {
		this.glicinaSerinaAAT = glicinaSerinaAAT;
	}
	public Double getValinaAAT() {
		return valinaAAT;
	}
	public void setValinaAAT(Double valinaAAT) {
		this.valinaAAT = valinaAAT;
	}
	public Double getIsoleucinaAAT() {
		return isoleucinaAAT;
	}
	public void setIsoleucinaAAT(Double isoleucinaAAT) {
		this.isoleucinaAAT = isoleucinaAAT;
	}
	public Double getLeucinaAAT() {
		return leucinaAAT;
	}
	public void setLeucinaAAT(Double leucinaAAT) {
		this.leucinaAAT = leucinaAAT;
	}
	public Double getHistidinaAAT() {
		return histidinaAAT;
	}
	public void setHistidinaAAT(Double histidinaAAT) {
		this.histidinaAAT = histidinaAAT;
	}
	public Double getFenilalaninaAAT() {
		return fenilalaninaAAT;
	}
	public void setFenilalaninaAAT(Double fenilalaninaAAT) {
		this.fenilalaninaAAT = fenilalaninaAAT;
	}
	public Double getFenil_TirAAT() {
		return fenil_TirAAT;
	}
	public void setFenil_TirAAT(Double fenil_TirAAT) {
		this.fenil_TirAAT = fenil_TirAAT;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acidoLinoleico == null) ? 0 : acidoLinoleico.hashCode());
		result = prime * result + ((argininaAAD == null) ? 0 : argininaAAD.hashCode());
		result = prime * result + ((argininaAAT == null) ? 0 : argininaAAT.hashCode());
		result = prime * result + ((calcio == null) ? 0 : calcio.hashCode());
		result = prime * result + ((cloro == null) ? 0 : cloro.hashCode());
		result = prime * result + ((consumoCalculado == null) ? 0 : consumoCalculado.hashCode());
		result = prime * result + ((consumoDeterminado == null) ? 0 : consumoDeterminado.hashCode());
		result = prime * result + ((consumoEnergiaMetabolizavelDeterminado == null) ? 0
				: consumoEnergiaMetabolizavelDeterminado.hashCode());
		result = prime * result
				+ ((energiaMetabolizavelCalculada == null) ? 0 : energiaMetabolizavelCalculada.hashCode());
		result = prime * result + ((energiaMetabolizavelRacao == null) ? 0 : energiaMetabolizavelRacao.hashCode());
		result = prime * result + ((fenil_TirAAD == null) ? 0 : fenil_TirAAD.hashCode());
		result = prime * result + ((fenil_TirAAT == null) ? 0 : fenil_TirAAT.hashCode());
		result = prime * result + ((fenilalaninaAAD == null) ? 0 : fenilalaninaAAD.hashCode());
		result = prime * result + ((fenilalaninaAAT == null) ? 0 : fenilalaninaAAT.hashCode());
		result = prime * result + ((fosforoDigestivelEP == null) ? 0 : fosforoDigestivelEP.hashCode());
		result = prime * result + ((fosforoDisponivelEP == null) ? 0 : fosforoDisponivelEP.hashCode());
		result = prime * result + ((ganhoPeso == null) ? 0 : ganhoPeso.hashCode());
		result = prime * result + ((glicinaSerinaAAD == null) ? 0 : glicinaSerinaAAD.hashCode());
		result = prime * result + ((glicinaSerinaAAT == null) ? 0 : glicinaSerinaAAT.hashCode());
		result = prime * result + ((histidinaAAD == null) ? 0 : histidinaAAD.hashCode());
		result = prime * result + ((histidinaAAT == null) ? 0 : histidinaAAT.hashCode());
		result = prime * result + ((isoleucinaAAD == null) ? 0 : isoleucinaAAD.hashCode());
		result = prime * result + ((isoleucinaAAT == null) ? 0 : isoleucinaAAT.hashCode());
		result = prime * result + ((leucinaAAD == null) ? 0 : leucinaAAD.hashCode());
		result = prime * result + ((leucinaAAT == null) ? 0 : leucinaAAT.hashCode());
		result = prime * result + ((lisinaAAD == null) ? 0 : lisinaAAD.hashCode());
		result = prime * result + ((lisinaAAT == null) ? 0 : lisinaAAT.hashCode());
		result = prime * result + ((lisinaDigestivel == null) ? 0 : lisinaDigestivel.hashCode());
		result = prime * result + ((lisinaDigestivelEP == null) ? 0 : lisinaDigestivelEP.hashCode());
		result = prime * result + ((massaOvo == null) ? 0 : massaOvo.hashCode());
		result = prime * result + ((metioninaAAD == null) ? 0 : metioninaAAD.hashCode());
		result = prime * result + ((metioninaAAT == null) ? 0 : metioninaAAT.hashCode());
		result = prime * result + ((metioninaCistinaAAD == null) ? 0 : metioninaCistinaAAD.hashCode());
		result = prime * result + ((metioninaCistinaAAT == null) ? 0 : metioninaCistinaAAT.hashCode());
		result = prime * result + ((pesoCorporal == null) ? 0 : pesoCorporal.hashCode());
		result = prime * result + ((potassio == null) ? 0 : potassio.hashCode());
		result = prime * result + ((proteina == null) ? 0 : proteina.hashCode());
		result = prime * result + ((sodio == null) ? 0 : sodio.hashCode());
		result = prime * result + ((temperaturaMedia == null) ? 0 : temperaturaMedia.hashCode());
		result = prime * result + ((treoninaAAD == null) ? 0 : treoninaAAD.hashCode());
		result = prime * result + ((treoninaAAT == null) ? 0 : treoninaAAT.hashCode());
		result = prime * result + ((triptofanoAAD == null) ? 0 : triptofanoAAD.hashCode());
		result = prime * result + ((triptofanoAAT == null) ? 0 : triptofanoAAT.hashCode());
		result = prime * result + ((valinaAAD == null) ? 0 : valinaAAD.hashCode());
		result = prime * result + ((valinaAAT == null) ? 0 : valinaAAT.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExigenciaAves other = (ExigenciaAves) obj;
		if (acidoLinoleico == null) {
			if (other.acidoLinoleico != null)
				return false;
		} else if (!acidoLinoleico.equals(other.acidoLinoleico))
			return false;
		if (argininaAAD == null) {
			if (other.argininaAAD != null)
				return false;
		} else if (!argininaAAD.equals(other.argininaAAD))
			return false;
		if (argininaAAT == null) {
			if (other.argininaAAT != null)
				return false;
		} else if (!argininaAAT.equals(other.argininaAAT))
			return false;
		if (calcio == null) {
			if (other.calcio != null)
				return false;
		} else if (!calcio.equals(other.calcio))
			return false;
		if (cloro == null) {
			if (other.cloro != null)
				return false;
		} else if (!cloro.equals(other.cloro))
			return false;
		if (consumoCalculado == null) {
			if (other.consumoCalculado != null)
				return false;
		} else if (!consumoCalculado.equals(other.consumoCalculado))
			return false;
		if (consumoDeterminado == null) {
			if (other.consumoDeterminado != null)
				return false;
		} else if (!consumoDeterminado.equals(other.consumoDeterminado))
			return false;
		if (consumoEnergiaMetabolizavelDeterminado == null) {
			if (other.consumoEnergiaMetabolizavelDeterminado != null)
				return false;
		} else if (!consumoEnergiaMetabolizavelDeterminado.equals(other.consumoEnergiaMetabolizavelDeterminado))
			return false;
		if (energiaMetabolizavelCalculada == null) {
			if (other.energiaMetabolizavelCalculada != null)
				return false;
		} else if (!energiaMetabolizavelCalculada.equals(other.energiaMetabolizavelCalculada))
			return false;
		if (energiaMetabolizavelRacao == null) {
			if (other.energiaMetabolizavelRacao != null)
				return false;
		} else if (!energiaMetabolizavelRacao.equals(other.energiaMetabolizavelRacao))
			return false;
		if (fenil_TirAAD == null) {
			if (other.fenil_TirAAD != null)
				return false;
		} else if (!fenil_TirAAD.equals(other.fenil_TirAAD))
			return false;
		if (fenil_TirAAT == null) {
			if (other.fenil_TirAAT != null)
				return false;
		} else if (!fenil_TirAAT.equals(other.fenil_TirAAT))
			return false;
		if (fenilalaninaAAD == null) {
			if (other.fenilalaninaAAD != null)
				return false;
		} else if (!fenilalaninaAAD.equals(other.fenilalaninaAAD))
			return false;
		if (fenilalaninaAAT == null) {
			if (other.fenilalaninaAAT != null)
				return false;
		} else if (!fenilalaninaAAT.equals(other.fenilalaninaAAT))
			return false;
		if (fosforoDigestivelEP == null) {
			if (other.fosforoDigestivelEP != null)
				return false;
		} else if (!fosforoDigestivelEP.equals(other.fosforoDigestivelEP))
			return false;
		if (fosforoDisponivelEP == null) {
			if (other.fosforoDisponivelEP != null)
				return false;
		} else if (!fosforoDisponivelEP.equals(other.fosforoDisponivelEP))
			return false;
		if (ganhoPeso == null) {
			if (other.ganhoPeso != null)
				return false;
		} else if (!ganhoPeso.equals(other.ganhoPeso))
			return false;
		if (glicinaSerinaAAD == null) {
			if (other.glicinaSerinaAAD != null)
				return false;
		} else if (!glicinaSerinaAAD.equals(other.glicinaSerinaAAD))
			return false;
		if (glicinaSerinaAAT == null) {
			if (other.glicinaSerinaAAT != null)
				return false;
		} else if (!glicinaSerinaAAT.equals(other.glicinaSerinaAAT))
			return false;
		if (histidinaAAD == null) {
			if (other.histidinaAAD != null)
				return false;
		} else if (!histidinaAAD.equals(other.histidinaAAD))
			return false;
		if (histidinaAAT == null) {
			if (other.histidinaAAT != null)
				return false;
		} else if (!histidinaAAT.equals(other.histidinaAAT))
			return false;
		if (isoleucinaAAD == null) {
			if (other.isoleucinaAAD != null)
				return false;
		} else if (!isoleucinaAAD.equals(other.isoleucinaAAD))
			return false;
		if (isoleucinaAAT == null) {
			if (other.isoleucinaAAT != null)
				return false;
		} else if (!isoleucinaAAT.equals(other.isoleucinaAAT))
			return false;
		if (leucinaAAD == null) {
			if (other.leucinaAAD != null)
				return false;
		} else if (!leucinaAAD.equals(other.leucinaAAD))
			return false;
		if (leucinaAAT == null) {
			if (other.leucinaAAT != null)
				return false;
		} else if (!leucinaAAT.equals(other.leucinaAAT))
			return false;
		if (lisinaAAD == null) {
			if (other.lisinaAAD != null)
				return false;
		} else if (!lisinaAAD.equals(other.lisinaAAD))
			return false;
		if (lisinaAAT == null) {
			if (other.lisinaAAT != null)
				return false;
		} else if (!lisinaAAT.equals(other.lisinaAAT))
			return false;
		if (lisinaDigestivel == null) {
			if (other.lisinaDigestivel != null)
				return false;
		} else if (!lisinaDigestivel.equals(other.lisinaDigestivel))
			return false;
		if (lisinaDigestivelEP == null) {
			if (other.lisinaDigestivelEP != null)
				return false;
		} else if (!lisinaDigestivelEP.equals(other.lisinaDigestivelEP))
			return false;
		if (massaOvo == null) {
			if (other.massaOvo != null)
				return false;
		} else if (!massaOvo.equals(other.massaOvo))
			return false;
		if (metioninaAAD == null) {
			if (other.metioninaAAD != null)
				return false;
		} else if (!metioninaAAD.equals(other.metioninaAAD))
			return false;
		if (metioninaAAT == null) {
			if (other.metioninaAAT != null)
				return false;
		} else if (!metioninaAAT.equals(other.metioninaAAT))
			return false;
		if (metioninaCistinaAAD == null) {
			if (other.metioninaCistinaAAD != null)
				return false;
		} else if (!metioninaCistinaAAD.equals(other.metioninaCistinaAAD))
			return false;
		if (metioninaCistinaAAT == null) {
			if (other.metioninaCistinaAAT != null)
				return false;
		} else if (!metioninaCistinaAAT.equals(other.metioninaCistinaAAT))
			return false;
		if (pesoCorporal == null) {
			if (other.pesoCorporal != null)
				return false;
		} else if (!pesoCorporal.equals(other.pesoCorporal))
			return false;
		if (potassio == null) {
			if (other.potassio != null)
				return false;
		} else if (!potassio.equals(other.potassio))
			return false;
		if (proteina == null) {
			if (other.proteina != null)
				return false;
		} else if (!proteina.equals(other.proteina))
			return false;
		if (sodio == null) {
			if (other.sodio != null)
				return false;
		} else if (!sodio.equals(other.sodio))
			return false;
		if (temperaturaMedia == null) {
			if (other.temperaturaMedia != null)
				return false;
		} else if (!temperaturaMedia.equals(other.temperaturaMedia))
			return false;
		if (treoninaAAD == null) {
			if (other.treoninaAAD != null)
				return false;
		} else if (!treoninaAAD.equals(other.treoninaAAD))
			return false;
		if (treoninaAAT == null) {
			if (other.treoninaAAT != null)
				return false;
		} else if (!treoninaAAT.equals(other.treoninaAAT))
			return false;
		if (triptofanoAAD == null) {
			if (other.triptofanoAAD != null)
				return false;
		} else if (!triptofanoAAD.equals(other.triptofanoAAD))
			return false;
		if (triptofanoAAT == null) {
			if (other.triptofanoAAT != null)
				return false;
		} else if (!triptofanoAAT.equals(other.triptofanoAAT))
			return false;
		if (valinaAAD == null) {
			if (other.valinaAAD != null)
				return false;
		} else if (!valinaAAD.equals(other.valinaAAD))
			return false;
		if (valinaAAT == null) {
			if (other.valinaAAT != null)
				return false;
		} else if (!valinaAAT.equals(other.valinaAAT))
			return false;
		return true;
	}

}
