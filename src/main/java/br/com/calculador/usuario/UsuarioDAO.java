package br.com.calculador.usuario;

import java.util.List;



public interface UsuarioDAO {
	public void salvar(Usuario usuario);
	public void excluir(Usuario usuario);
	public void atualizar(Usuario usuario);
	public Usuario carregar(Integer id);
	public List<Usuario> listar();
	public Usuario usuarioPorLogin(String login);
}
