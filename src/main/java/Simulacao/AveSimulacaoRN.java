package Simulacao;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import AlimentoNutrientes.AlimentoNutriente;
import Nutriente.Nutriente;
import Origem.Origem;

/**
 * 
 * @author alkxyly
 *
 *         Nomenclatura e fórmulas
 * 
 *         ENN (Extrato não nitrogenado) = ((Matéria seca)-(Proteína
 *         bruta)-(Gordura)-(Matéria mineral)-(Fibra bruta)) ENNd (Extrato não
 *         nitrogenado digestível) = ((Coef. ENN)(ENN))/100 ENDF (Extrato não
 *         nitrogenado digestível + Fibra bruta) =((ENN)-(Coef. ENN)+(Fibra
 *         bruta))
 */
public class AveSimulacaoRN {
	
	public List<AlimentoNutriente> OrdenarListaDeSimulacao(List<AlimentoNutriente> list) {

		List<AlimentoNutriente> ListAUX = new ArrayList<>();
		String[] ordenacao = { "100", "111", "700", "121", "123", "710","151", "141", "144", "730", "211" };

		Double PB = 0.0;
		Double CDPB = 0.0;
		Double ENN = 0.0;
		Double CDENN = 0.0;
		for (AlimentoNutriente alinut : list) {
			if (alinut.getNutriente().getCodigo().equals("111")) {
				PB = alinut.getValorNutritivo();
			}
			if (alinut.getNutriente().getCodigo().equals("700")) {
				CDPB = alinut.getValorNutritivo();
			}
			if (alinut.getNutriente().getCodigo().equals("144")) {
				ENN = alinut.getValorNutritivo();
			}
			if (alinut.getNutriente().getCodigo().equals("730")) {
				CDENN = alinut.getValorNutritivo();
			}
		}

		for (String ord : ordenacao) {
			for (AlimentoNutriente alinut : list) {
				if (alinut.getNutriente().getCodigo().equals(ord)) {
					ListAUX.add(alinut);

					if (ord.equals("700")) {

						AlimentoNutriente aliNut = new AlimentoNutriente();
						Nutriente nut = new Nutriente();

						nut.setCodigo("000");
						nut.setNome("Proteina digestivel");
						aliNut.setAlimento(alinut.getAlimento());
						aliNut.setNutriente(nut);
						double valorNutritivo = (PB * CDPB) / 100;

						try {
							DecimalFormat formato = new DecimalFormat("#.##");
							valorNutritivo = Double.valueOf(formato.format(valorNutritivo));
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
						aliNut.setValorNutritivo(valorNutritivo);
						ListAUX.add(aliNut);
						break;
					}
					if (ord.equals("730")) {

						AlimentoNutriente aliNut = new AlimentoNutriente();
						Nutriente nut = new Nutriente();

						nut.setCodigo("000");
						nut.setNome("Extrativo digestivel");
						aliNut.setAlimento(alinut.getAlimento());
						aliNut.setNutriente(nut);
						double valorNutritivo = (ENN * CDENN) / 100;
						try {
							DecimalFormat formato = new DecimalFormat("#.##");
							valorNutritivo = Double.valueOf(formato.format(valorNutritivo));
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
						aliNut.setValorNutritivo(valorNutritivo);
						ListAUX.add(aliNut);
						break;
					}
				}

			}
		}
		return ListAUX;
	}
	
	public List<AlimentoNutriente> OrdenarListaPadrao(List<AlimentoNutriente> list) {
        
		List<AlimentoNutriente> ListAUX = OrdenarListaDeSimulacao(list);
		
		for(AlimentoNutriente alinut:list){
			if(alinut.getNutriente().getCodigo().equals("211")){
				ListAUX.add(alinut);
			}
		}
		return ListAUX;
	}
	
	public boolean naoEditavelRN(Integer codigo) {
		boolean naoEditavel = true;

		if (codigo.equals(111)) {
			naoEditavel = false;
		}
		if (codigo.equals(730)) {
			naoEditavel = false;
		}
		if (codigo.equals(141)) {
			naoEditavel = false;
		}
		if (codigo.equals(144)) {
			naoEditavel = false;
		}
		if (codigo.equals(710)) {
			naoEditavel = false;
		}
		if (codigo.equals(700)) {
			naoEditavel = false;
		}
		if (codigo.equals(121)) {
			naoEditavel = false;
		}

		return naoEditavel;
	}

	/**
	 * Calcula a energia metabolizada para aves jovens
	 * 
	 * fórmula EMaves = 4,31(PBd)+9,29(GD)+4,14(ENNd)
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia metabolizada
	 */
	public double calcularAvesJovensEnergiaMetabolizada(List<AlimentoNutriente> listaNutrientes, Origem orig) {
		Origem origem = orig;
		double eMaves = 0.0;

		if (origem.getNome().equals("Vegetal")) {
			eMaves = avesJovensEnergiaMetabolizadaVegetal(listaNutrientes);
		} else {
			eMaves = Galinhas_AvesJovensEnergiaMetabolizadaAnimalVegetal(listaNutrientes);
		}

		return ajustarEnergia(eMaves);
	}

	/**
	 * Calcula a energia metabolizada para aves jovens
	 * 
	 * fórmula EMaves = 4,31(PBd)+9,29(GD)+4,14(ENNd)
	 * 
	 * @param listaNutrientes
	 *            lista com os nutrientes que entram na simulação
	 * @return valor da energia metabolizada
	 */
	public double calcularGalinhasEnergiaMetabolizada(List<AlimentoNutriente> listaNutrientes, Origem orig) {
		Origem origem = orig;
		double eMaves = 0.0;

		if (origem.getNome().equals("Vegetal")) {
			eMaves = GalinhasEnergiaMetabolizadaVegetal(listaNutrientes);
		} else {
			//eMaves = Galinhas_AvesJovensEnergiaMetabolizadaAnimalVegetal(listaNutrientes);
		}

		return ajustarEnergia(eMaves);
	}

	/**
	 * 
	 * @author Rodrigo
	 * 
	 * @param avesJovensEnergiaMetabolizadaVegetal
	 * @return Retorna a energia metaboliz�vel para aves jovens com alimentos de
	 *         origem vegetal Metodo para calcular energia para aves jovens com
	 *         alimento de origem vegetal
	 * 
	 */
	
	public Double avesJovensEnergiaMetabolizadaVegetal(List<AlimentoNutriente> listaNutrientes) {

		Double PB = 0.0; // proteina bruta
		Double g = 0.0; // gordura bruta
		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		Double ENN = 0.0; // Extrato n�o nitrog�nado
		Double ENNd = 0.0; // Coeficiente de digestibilidade de Extrato n�o nitrog�nado
		// Double ENDF = 0.0; // Extrato n�o nitrogenado digest�vel + Fibra
		// bruta
		Double EMaves = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {
			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				g = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("700")) {
				PBd = alimentoNutriente.getValorNutritivo();
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("710")) {
				GD = alimentoNutriente.getValorNutritivo();
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("144")) {
				ENN = alimentoNutriente.getValorNutritivo();
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("730")) {
				ENNd = alimentoNutriente.getValorNutritivo();
			}

		}

		EMaves = (4.31 * (PB *(PBd/100))) + (9.29 * (g*(GD/100))) + (4.14 *(ENN* (ENNd/100)));
		return ajustarEnergia(EMaves*10);
	}

	/**
	 * 
	 * @author Rodrigo
	 * @param GalinhasEnergiaMetabolizadaVegetal
	 * @return Retorna a energia metaboliz�vel para galinhas com alimentos de
	 *         origem vegetal
	 * 
	 *         Metodo para calcular energia para galinhas com alimento de origem
	 *         vegetal
	 */
	public Double GalinhasEnergiaMetabolizadaVegetal(List<AlimentoNutriente> listaNutrientes) {
		Double PB = 0.0; // proteina bruta
		Double Fibra = 0.0; // Fibra bruta
		Double g = 0.0; // gordura bruta
		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		Double ENN = 0.0; // Extrato n�o nitrog�nado
		Double ENNd = 0.0; // Coeficiente de digestibilidade de Extrato n�o nitrog�nado
		// Double ENDF = 0.0; // Extrato n�o nitrogenado digest�vel + Fibra
		// bruta
		Double EMaves = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {
			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				g = alimentoNutriente.getValorNutritivo();
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("141")) {
				Fibra = alimentoNutriente.getValorNutritivo();
			}

			if (alimentoNutriente.getNutriente().getCodigo().equals("700")) {
				PBd = alimentoNutriente.getValorNutritivo();
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("710")) {
				GD = alimentoNutriente.getValorNutritivo();
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("144")) {
				ENN = alimentoNutriente.getValorNutritivo();
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("730")) {
				ENNd = alimentoNutriente.getValorNutritivo();
			}

		}

		EMaves = (4.31 * (PB *(PBd/100))) + (9.29 * (g*(GD/100))) + (4.14 *(ENN* (ENNd/100))) + (0.3*((ENN*((100-ENNd)/100))+Fibra));
		
		//
		// ENDF(ENN n�o digerido) - (0.3*((ENN*((100-ENNd)/100))+Fibra))
		//

		return ajustarEnergia(EMaves*10);
	}

	/**
	 * 
	 * @author Rodrigo
	 * 
	 * @param GalinhasEnergiaMetabolizadaVegetal
	 * @return Retorna a energia metaboliz�vel para aves jovens e galinhas com
	 *         alimentos de origens animal , vegetal entre outros.
	 * 
	 *         Metodo para calcular energia para galinhas e aves jovens com
	 *         alimentos de origem animal, vegetais entre outros.
	 */

	public Double Galinhas_AvesJovensEnergiaMetabolizadaAnimalVegetal(List<AlimentoNutriente> listaNutrientes) {

		Double PBd = 0.0; // Coeficiente de digestibilidade da proteina bruta
		Double GD = 0.0; // Coeficiente de digestibilidade da gordura
		 Double PB = 0.0; // Proteina Bruta
		Double GDB = 0.0; // Extrato n�o nitrog�nado disgetivel
		// Double ENDF = 0.0; // Extrato n�o nitrogenado digest�vel + Fibra
		// bruta
		Double EMaves = 0.0; // Energia metabolizavel aves jovens

		for (AlimentoNutriente alimentoNutriente : listaNutrientes) {

			if (alimentoNutriente.getNutriente().getCodigo().equals("700")) {
				PBd = alimentoNutriente.getValorNutritivo();
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("710")) {
				GD = alimentoNutriente.getValorNutritivo();
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("111")) {
				PB = alimentoNutriente.getValorNutritivo();
			}
			if (alimentoNutriente.getNutriente().getCodigo().equals("121")) {
				GDB = alimentoNutriente.getValorNutritivo();
			}

		}

		EMaves = (4.31 * PBd*(PB/100)) + (9.29 * GD*(GDB/100));

		return ajustarEnergia(EMaves*10);
	}
	
	public Double ajustarEnergia(Double energiaDouble) {
		return Double.valueOf(Math.round(energiaDouble));
	}
}
