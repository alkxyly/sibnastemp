package Simulacao;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.ListableBeanFactory;

import AlimentoNutrientes.AlimentoNutriente;
import AlimentoNutrientes.AlimentoNutrienteRN;
import FiltrosBusca.AlimentoFilter;
import br.com.calculador.alimento.Alimento;
import br.com.calculador.alimento.AlimentoRN;

@ManagedBean(name = "suinoSimulacaoBean")
@SessionScoped
public class SuinoSimulacaoBean {
	
private List<Alimento> listAlimento;
	
	private Alimento alimentoSelecionado;
	
	private List<AlimentoNutriente> listAlimentoNutrienteAUX;
	private List<AlimentoNutriente> listAlimentoNutriente;
	private List<AlimentoNutriente> listAlimentoNutrienteSimular;
	private List<AlimentoNutriente> listAlimentoNutrienteSuinos;
	private List<AlimentoNutriente> listAlimentoNutrientePorcas;
	private List<Comparativo> comparativosSuinos;
	private List<Comparativo> comparativosPorcas;
	private AlimentoFilter alimentoFilter;

	public SuinoSimulacaoBean(){
		alimentoSelecionado = new Alimento();
		comparativosSuinos = new ArrayList<>();
		comparativosPorcas = new ArrayList<>();
		alimentoFilter = new AlimentoFilter();
	}
	
	public void selecionar(){
		AlimentoNutrienteRN al = new AlimentoNutrienteRN();
		SuinoSimulacaoRN ss = new SuinoSimulacaoRN();
         this.listAlimentoNutrienteAUX = al.listaComAlimentoSuino(alimentoSelecionado);
		this.listAlimentoNutriente =ss.OrdenarListaDeSimulacao(al.listaComAlimentoSuino(alimentoSelecionado));
		this.listAlimentoNutrienteSimular =ss.OrdenarListaDeSimulacao(al.listaComAlimentoSuino(alimentoSelecionado));
		carregarComparativos();
	}
	
	public void pesquisar(){
		AlimentoRN alimentoRN = new AlimentoRN();
		this.listAlimento = alimentoRN.pesquisar(alimentoFilter);
	}

	public List<Alimento> getListAlimento() {
		AlimentoRN alimentoRN = new AlimentoRN();
		this.listAlimento = alimentoRN.pesquisar(alimentoFilter);
		return listAlimento;
	}
	
	public void carregarComparativos(){
		SuinoSimulacaoRN suinoRN = new SuinoSimulacaoRN();
		
		comparativosSuinos =  suinoRN.carregarComparativos(listAlimentoNutrienteAUX,"Suinos");
		comparativosPorcas =  suinoRN.carregarComparativos(listAlimentoNutrienteAUX,"Porcas");
	}
	
	public void simular(){
		SuinoSimulacaoRN suinoRN = new SuinoSimulacaoRN();
		comparativosSuinos =  suinoRN.simularEnergia(comparativosSuinos, listAlimentoNutriente,"Suinos");
		comparativosPorcas =  suinoRN.simularEnergia(comparativosPorcas, listAlimentoNutriente,"Porcas");
	}
	
	
	
	public boolean OcultarEnergia(Integer codigo){
		SuinoSimulacaoBean suinoRN = new SuinoSimulacaoBean();
		return suinoRN.OcultarEnergia(codigo);
	}
	
	public boolean naoEditavel(Integer codigo) {
		SuinoSimulacaoRN sRN = new SuinoSimulacaoRN();
		return sRN.naoEditavelRN(codigo);
	}

	public void setListAlimento(List<Alimento> listAlimento) {
		this.listAlimento = listAlimento;
	}

	public Alimento getAlimentoSelecionado() {
		return alimentoSelecionado;
	}

	public void setAlimentoSelecionado(Alimento alimentoSelecionado) {
		this.alimentoSelecionado = alimentoSelecionado;
	}

	public List<AlimentoNutriente> getListAlimentoNutriente() {
		return listAlimentoNutriente;
	}

	public void setListAlimentoNutriente(List<AlimentoNutriente> listAlimentoNutriente) {
		this.listAlimentoNutriente = listAlimentoNutriente;
	}

	public List<AlimentoNutriente> getListAlimentoNutrienteSimular() {
		return listAlimentoNutrienteSimular;
	}

	public void setListAlimentoNutrienteSimular(List<AlimentoNutriente> listAlimentoNutrienteSimular) {
		this.listAlimentoNutrienteSimular = listAlimentoNutrienteSimular;
	}

	public List<AlimentoNutriente> getListAlimentoNutrienteSuinos() {
		return listAlimentoNutrienteSuinos;
	}

	public void setListAlimentoNutrienteSuinos(List<AlimentoNutriente> listAlimentoNutrienteSuinos) {
		this.listAlimentoNutrienteSuinos = listAlimentoNutrienteSuinos;
	}

	public List<AlimentoNutriente> getListAlimentoNutrientePorcas() {
		return listAlimentoNutrientePorcas;
	}

	public void setListAlimentoNutrientePorcas(List<AlimentoNutriente> listAlimentoNutrientePorcas) {
		this.listAlimentoNutrientePorcas = listAlimentoNutrientePorcas;
	}

	public List<Comparativo> getComparativosSuinos() {
		return comparativosSuinos;
	}

	public void setComparativosSuinos(List<Comparativo> comparativosSuinos) {
		this.comparativosSuinos = comparativosSuinos;
	}

	public List<Comparativo> getComparativosPorcas() {
		return comparativosPorcas;
	}

	public void setComparativosPorcas(List<Comparativo> comparativosPorcas) {
		this.comparativosPorcas = comparativosPorcas;
	}

	public AlimentoFilter getAlimentoFilter() {
		return alimentoFilter;
	}

	public void setAlimentoFilter(AlimentoFilter alimentoFilter) {
		this.alimentoFilter = alimentoFilter;
	}
	
	
}
