package Proteina;

import java.util.List;


public interface ProteinaDAO {
	public void salvar(Proteina alimentoNutriente);
	public void excluir(Proteina alimentoNutriente);
	public void atualizar(Proteina alimentoNutriente);
	public Proteina carregar(Integer id);
	public List<Proteina> listar();
}
