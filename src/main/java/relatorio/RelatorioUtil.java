package relatorio;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.enterprise.context.spi.Context;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.mysql.jdbc.Connection;

import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;


public class RelatorioUtil {
	public static final int RELATORIO_PDF = 1;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public StreamedContent gerarRelatorio(HashMap parametrosRelatorio,String nomeRelarioJasper,String nomeRelatorioSaida, int tipoRelatorio){
		StreamedContent arquivoRetorno = null;
		try {
			Connection conexao = getConexao();
			FacesContext contextoFaces = FacesContext.getCurrentInstance();
			ExternalContext contextoExterno = contextoFaces.getExternalContext();
			ServletContext contextoServlet = (ServletContext) contextoExterno.getContext();
			
			String caminhoRelatorios = contextoServlet.getRealPath("/relatorios");
			String caminhoArquivoJasper = caminhoRelatorios + File.separator + nomeRelarioJasper+".jasper";
			String caminhoArquivoRelatorio = caminhoRelatorios + File.separator + nomeRelatorioSaida;
			
			JasperReport relatorioJasper = (JasperReport) JRLoader.loadObjectFromFile(caminhoArquivoJasper);
			JasperPrint impressoraJasper =JasperFillManager.fillReport(relatorioJasper, parametrosRelatorio,conexao);
			
			String extensao = null;
			File arquivoGerado = null;
			
			switch (tipoRelatorio) {
			case RelatorioUtil.RELATORIO_PDF:
					JRPdfExporter pdfExportado = new JRPdfExporter();
					extensao = "pdf";
					arquivoGerado =  new java.io.File(caminhoArquivoRelatorio + "." + extensao);
//					pdfExportado.setExporterInput(new SimpleExporterInput(impressoraJasper));
//					pdfExportado.setExporterOutput(new SimpleOutputStreamExporterOutput(arquivoGerado));
//					pdfExportado.exportReport();
					arquivoGerado.deleteOnExit();
				break;

			default:
				break;
			}
			InputStream conteudoRelatorio = new FileInputStream(arquivoGerado);
			arquivoRetorno = new DefaultStreamedContent(conteudoRelatorio,"application/"+extensao, nomeRelatorioSaida + "." + extensao);
		} catch (Exception e) {

		}
		return arquivoRetorno;
	}

	private Connection getConexao() throws SQLException {
		javax.sql.DataSource ds = null;
		try {
			Context initContext = (Context) new InitialContext();
			Context envContext = (Context) ((InitialContext) initContext).lookup("java:/comp/env/");
			ds = (DataSource) ((InitialContext) envContext).lookup("jdbc/CalculadorDB");

		} catch (Exception e) {

		}
		return (Connection) ds.getConnection();
	}
	
	public static void gerarRelatorio(String nomeRelatorio,HashMap paramRel,List listaRel){
		   FacesContext context = FacesContext.getCurrentInstance();
		   HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse(); 
		   ServletContext sc = (ServletContext) context.getExternalContext().getContext();   
		   String relPath = sc.getRealPath("/");  
//		   String imagemLogo = relPath + "resources/imagens/logo_mmo.jpg";
//	       paramRel.put("imagemLogo", imagemLogo);
		   paramRel.put("nmSistema", "Calculador");
		   paramRel.put("REPORT_LOCALE", new Locale("pt", "BR"));
		   try  {
			   JasperPrint print = null;
			   JRBeanCollectionDataSource rel = new JRBeanCollectionDataSource(listaRel);
			   print = JasperFillManager.fillReport(relPath + "relatorios/"+nomeRelatorio+".jasper", paramRel,rel);				   
			   response.setContentType("application/pdf");
			   response.addHeader("Content-disposition", "attachment; filename=\"" + nomeRelatorio + ".pdf\"");
			   JasperExportManager.exportReportToPdfStream(print,response.getOutputStream());
			   ServletOutputStream responseStream = response.getOutputStream();
			   responseStream.flush();
			   responseStream.close();
			   FacesContext.getCurrentInstance().renderResponse();
			   FacesContext.getCurrentInstance().responseComplete();   
			} catch (Exception e) { 
				e.printStackTrace();				
			} 
		}
	
}
